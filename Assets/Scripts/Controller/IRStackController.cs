using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the distribution of incoming eventTopics to the matching transitions of the IRs.
/// </summary>
public class IRStackController : MonoBehaviour
{
    /// <summary>Contains the Interaction Rules</summary>
    public CommunicationActContainer communicationActContainer;
    /// <summary>Controls the execution of the individual states of an IR</summary>
    public InteractionRuleController interactionRuleController;

    private void OnEnable()
    {
        BehaviorManager.raiseInternalEvent += EventTopicReceived;
    }

    private void OnDisable()
    {
        BehaviorManager.raiseInternalEvent -= EventTopicReceived;
    }

    /// <summary>
    /// When a new eventTopic is raised, is is searched for the first IR transition, that matches the eventTopic.
    /// Takes the transition to the new states and triggers the execution of that state.
    /// </summary>
    /// <param name="eventTopic"></param>
    private void EventTopicReceived(string eventTopic)
    {
        foreach(InteractionRule interactionRule in communicationActContainer.GetActiveInteractionRules())
        {
            foreach(IRTransition transition in interactionRule.currentState.transitionList)
            {
                if (transition.eventTopic.Equals(eventTopic))
                {
                    StartCoroutine(interactionRuleController.TickInteractionRule(interactionRule.name, transition));
                    return;
                }
            }
        }
    }
}