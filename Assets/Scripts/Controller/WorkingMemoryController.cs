using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Data;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;

/// <summary>
/// Controller of the working memory to manage the reading and writing process.
/// </summary>
public class WorkingMemoryController : MonoBehaviour
{
    /// <summary>Instance and getter/setter of itself</summary>
    public static WorkingMemoryController Instance { get; set; }

    /// <summary>Instance and getter/setter of the data struct workingMemory</summary>
    private WorkingMemory workingMemory {get; set; }

    public static event Action workingMemoryCleared;

    public void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            workingMemory = new WorkingMemory();
        }
    }


    /// <summary>
    /// Entry to write into the working memory.
    /// </summary>
    /// <param name="jsonValue">JSON key value pair to add to the memory.</param>
    public void AddMemoryEntryFromDomainTask(string jsonValue)
    {
        string jsonResolved = ResolveJSONVariables(jsonValue);
        if(jsonResolved != null)
        {
            this.workingMemory.WriteIntoMemory(jsonResolved);
        }
    }

    /// <summary>
    /// Getter by key. Returns whole JSON.
    /// </summary>
    /// <param name="key">key to search for.</param>
    /// <returns>Whole JSON entry with the given key.</returns>
    public string GetRequestResponseByKey(string key)
    {
        return workingMemory.GetEntryAsJSON(key);
    }

    /// <summary>
    /// Getter for only the value by key as dynamic.
    /// </summary>
    /// <param name="key">Key to search for the value.</param>
    /// <returns>Inside the dynamic the value as dynamic and the initial type of the value is saved and returned.</returns>
    public dynamic GetMemoryData(string key)
    {
        return this.workingMemory.GetMemoryValue(key);
    }

    /// <summary>
    /// Checks, if the key exists inside the working memory.
    /// </summary>
    /// <param name="key">key to check for.</param>
    /// <returns>True, if the key exists inside the workingMemory</returns>
    public bool KeyExistsInMemory(string key)
    {
        if(this.workingMemory.dictionaryData.ContainsKey(key))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Entry to import a predefined data model into the working memory. Used by the XMLDatamodelParser.
    /// </summary>
    /// <param name="importedDataEntries">List of JSON strings to import into the working memory.</param>
    public void ImportDataModel(List<string> importedDataEntries)
    {
        foreach (string json in importedDataEntries)
        {
            workingMemory.WriteIntoMemory(json);
        }
    }

    /// <summary>
    /// Debug function.
    /// </summary>
    public void DebugMemory()
    {
        Debug.Log(workingMemory.DebugWorkingMemory());
    }

    /// <summary>
    /// Getter for all keys inside the Memory as List of strings.
    /// </summary>
    /// <returns>Returns all Keys of the working memory as list of strings.</returns>
    public List<string> GetWorkingMemoryKeys()
    {
        List<string> keys = new List<string>(workingMemory.dictionaryData.Keys);
        return keys;
    }

    /// <summary>
    /// Delete an entry by key.
    /// </summary>
    /// <param name="key">Key of the entry to delete.</param>
    public void DeleteWorkingMemoryEntryByKey(string key)
    {
        workingMemory.DeleteWorkingMemoryEntry(key);
    }

    /// <summary>
    /// Resolve WorkingMemory variables in JSON string by writing into the memory.
    /// </summary>
    /// <param name="jsonValue">JSON contains variables.</param>
    /// <returns>Resolved string without variables, but with memory entries!</returns>
    private string ResolveJSONVariables(string jsonValue)
    {
        string equation = jsonValue.Replace("}", "").Split(':')[1];

        // Filtering for possible variables inside of an equation. 
        // possible variables are everything starting with a char, underscore or ' like
        // var3, 'Andre' or temp3_Var or human_attention
        string pattern = @"'\b[a-zA-Z_][a-zA-Z0-9_]*\b'|\b[a-zA-Z_][a-zA-Z0-9_]*\b";

        Regex regex = new Regex(pattern);
        MatchCollection matches = regex.Matches(equation);

        foreach (Match match in matches)
        {
            if((match.Value.ToLower() == "true" || match.Value.ToLower() == "false"))
            {
                if(matches.Count > 1)
                {
                    return null;
                }
                else
                {
                    return jsonValue.Split(':')[0] + ": " + match.Value.ToLower() + "}";
                }
            }
            else if((match.Value.StartsWith("'") && match.Value.EndsWith("'")))
            {
                if (matches.Count > 1)
                {
                    return null;
                }
                else
                {
                    return jsonValue.Split(':')[0] + ": " + match.Value + "}";
                }
            }
            else if (this.KeyExistsInMemory(match.Value))
            {
                if (IsNumber(this.GetMemoryData(match.Value).GetType()))
                {
                    equation = equation.Replace(match.Value, Convert.ToString(this.GetMemoryData(match.Value)));
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return jsonValue;
            }            
        }

        DataTable dt = new DataTable();
        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
        var v = dt.Compute(equation, "");

        return jsonValue.Split(':')[0] + ": " + v + "}";
    }

    /// <summary>
    /// Check if object type is numeric.
    /// </summary>
    /// <param name="value">Type to proof.</param>
    /// <returns>True if type is numeric.</returns>
    public bool IsNumber(Type value)
    {
        return value == typeof(sbyte)
                || value == typeof(byte)
                || value == typeof(short)
                || value == typeof(ushort)
                || value == typeof(int)
                || value == typeof(uint)
                || value == typeof(long)
                || value == typeof(ulong)
                || value == typeof(float)
                || value == typeof(double)
                || value == typeof(decimal);
    }

    /// <summary>
    /// Clear whole WorkingMemory and destroy all WM UI elements.
    /// </summary>
    public void ClearWorkingMemory()
    {
        this.workingMemory.dictionaryData.Clear();

        workingMemoryCleared?.Invoke();
    }
}