using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using UnityEngine;
using Newtonsoft.Json.Linq;
/// <summary>
/// XML Parser for the starting knowledge of the working memory.
/// Validated by an XSD.
/// </summary>
public class JSONWorkingMemoryParser : MonoBehaviour
{
    /// <summary>
    /// Parser of the JSON file into a internal structure with a validation by an XSD file. 
    /// </summary>
    /// <param name="jsonFile">Name of the JSON file with the predefined knowledge for the working memory</param>
    /// <param name="wmPath">Path to the JSON file</param>
    /// <returns>List of all Key Value pairs as JSON</returns>
    public List<string> ParseDataModels(string jsonFile, string xsdPath)
    {
        List<string> datamodels = new List<string>();

        using StreamReader reader = new(jsonFile);
        var json = reader.ReadToEnd();

        JObject jsonObject = JObject.Parse(json);

        JArray workingMemoryArray = (JArray)jsonObject["WorkingMemory"];

        foreach (JToken item in workingMemoryArray)
        {
            datamodels.Add(item.ToString());
        }        

        return datamodels;
    }

    /// <summary>
    /// Validation of the XML File by an XSD
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <exception cref="Exception"></exception>
    static void ValidationEventHandler(object sender, ValidationEventArgs e) {
        XmlSeverityType type = XmlSeverityType.Warning;
        if (Enum.TryParse < XmlSeverityType > ("Error", out type)) {
            if (type == XmlSeverityType.Error) throw new Exception(e.Message);
        }
    }
}