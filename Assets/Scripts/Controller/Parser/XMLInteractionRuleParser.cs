using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using UnityEngine;
using static IRFunctionTypeEnum;
using static TaskManagementEnum;

/// <summary>
/// XML Parser for the XML interactionRules to parse into the internal representation.
/// All XMLs are validated by an XSD description.
/// </summary>
public class XMLInteractionRuleParser : MonoBehaviour
{
    /// <summary>XSD Namespace.</summary>
    private XNamespace irXSD;

    /// <summary>
    /// Parser of the XML files to the internal interaction rule structure.
    /// With validation by an XSD.
    /// Initial entry function for parsing.
    /// As unique ID, the IR name is used.
    /// </summary>
    /// <param name="xmlFile">Name of the XML file</param>
    /// <param name="irPath">Path to the XML file.</param>
    /// <returns></returns>
    public List<InteractionRule> ParseInteractionRulesFromFile(string xmlFile, string xsdPath)
    {
        List<InteractionRule> interactionRuleList = new List<InteractionRule>();

        XmlSchemaSet schema = new XmlSchemaSet();
        schema.Add("irXSD", xsdPath + "/IR.xsd");

        XDocument doc = XDocument.Load(xmlFile);

        irXSD = doc.Root.GetDefaultNamespace();

        try {
            doc.Validate(schema, ValidationEventHandler);

            string[] xmlFileArray = xmlFile.Split("/");
            Debug.Log("Validation successful: " + xmlFileArray[xmlFileArray.Length - 1]);
        } catch (Exception e) {
             Debug.LogError("Validation failed: " + e.Message);
             return interactionRuleList;
        }
        var interactionRules = doc.Root.Descendants(irXSD + "InteractionRule");

        foreach (XElement interactionRule in interactionRules)
        {
            List<IRState> iRStates;

            string irName;
            string irInitialState;
            bool irAutostart = false;
            int irPriority = 0;

            irName = interactionRule.Attribute("name").Value; //ToDo: unique name for IR, catch if missing
            irInitialState = interactionRule.Attribute("initial").Value;

            if (interactionRule.Attribute("autostart") != null)
            {
                irAutostart = bool.Parse(interactionRule.Attribute("autostart").Value);
            }
            if (interactionRule.Attribute("priority") != null)
            {
                irPriority = int.Parse(interactionRule.Attribute("priority").Value);
            }

            iRStates = new List<IRState>();

            foreach (XElement states in interactionRule.Descendants(irXSD + "State"))
            {
                string id = states.Attribute("id").Value;

                iRStates.Add(new IRState(id, ParseOnEntryWise(states.Descendants(irXSD + "onEntry").First()), ParseTransitions(states.Descendants(irXSD + "transition"))));
            }

            interactionRuleList.Add(new InteractionRule(irName, irInitialState, iRStates, irAutostart, irPriority));
        }

        return interactionRuleList;
    }
    /// <summary>
    /// Validation of the XMl Structure by the given XSD
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <exception cref="Exception"></exception>
    static void ValidationEventHandler(object sender, ValidationEventArgs e) {
        XmlSeverityType type = XmlSeverityType.Warning;
        if (Enum.TryParse < XmlSeverityType > ("Error", out type)) {
            if (type == XmlSeverityType.Error) throw new Exception(e.Message);
        }
    }
    /// <summary>
    /// Parser of one full Interaction Rule Entry structure.
    /// </summary>
    /// <param name="onEntryElement">XML Interaction Rule on Entry element</param>
    /// <returns>fully parsed onEntry IR element</returns>
    private IROnEntry ParseOnEntryWise(XElement onEntryElement)
    {
        List<IRAbstractOnEntryElement> onEntryElements = new List<IRAbstractOnEntryElement>();

        int executionLevel = 0;

        foreach (XElement element in onEntryElement.Elements())
        {
            if (element.Name == irXSD + "function")
            {
                onEntryElements.Add(ParseSingleFunction(element, executionLevel));
            }
            if (element.Name == irXSD + "if")
            {
                onEntryElements.AddRange(ParseIFStatement(element, executionLevel));

                if (element.Descendants(irXSD + "elseif").Count() > 0)
                {
                    foreach (XElement elseElement in element.Descendants(irXSD + "elseif"))
                    {
                        onEntryElements.AddRange(ParseIFStatement(elseElement, executionLevel));
                    }
                }

                if (element.Descendants(irXSD + "else").Count() > 0)
                {
                    onEntryElements.AddRange(ParseIFStatement(element.Descendants(irXSD + "else").First(), executionLevel));
                }
            }

            executionLevel++;
        }

        return new IROnEntry(onEntryElements);
    }

    /// <summary>
    /// Parser of the if condition inside the onEntry element.
    /// </summary>
    /// <param name="ifStatement">XML representation of the if Statements.</param>
    /// <param name="executionLevel">ExecutionLevel to track the depth of the if elseif else statement.</param>
    /// <returns>List of all ifStatements with functions and execution level. </returns>
    private List<IRIFCond> ParseIFStatement(XElement ifStatement, int executionLevel)
    {
        List<IRIFCond> irIFCondList = new List<IRIFCond>();

        string ifConditionStatement = "";
        if (ifStatement.Attribute("cond") != null)
        {
            ifConditionStatement = ifStatement.Attribute("cond").Value;
        }
        irIFCondList.Add(new IRIFCond(ifConditionStatement, ParseFunctions(ifStatement.Elements(irXSD + "function"), executionLevel + 1), executionLevel));

        return irIFCondList;
    }

    /// <summary>
    /// Parser of Functions inside the onEntry element. Can be nested inside the if statements.
    /// </summary>
    /// <param name="functionList"></param>
    /// <param name="executionLevel">executionLevel to track the depth of execution, if the function is inside some if statements.</param>
    /// <returns>List of all IR functions at this exectionLevel</returns>
    private List<IRFunction> ParseFunctions(IEnumerable<XElement> functionList, int executionLevel)
    {
        List<IRFunction> iRFunctionList = new List<IRFunction>();

        foreach (var function in functionList)
        {
            IRFunctionType functionType = (IRFunctionType)Enum.Parse(typeof(IRFunctionType), function.Attributes().First().Name.ToString());
            string referenceBehavior = function.Attributes().First().Value;

            ExecutionType executionType = ExecutionType.Blocking;
            if (function.Attribute("executionType") != null)
            {
                executionType = (ExecutionType)Enum.Parse(typeof(ExecutionType), function.Attribute("executionType").Value);
            }

            iRFunctionList.Add(new IRFunction(functionType, referenceBehavior, executionType, executionLevel));
        }

        return iRFunctionList;
    }

    /// <summary>
    /// If the IR function is not nested inside IF statements, its direly parsed in this fiction instead of ParseFunctions.
    /// </summary>
    /// <param name="function">XML representation of the function</param>
    /// <param name="executionLevel">current execution Level.</param>
    /// <returns></returns>
    private IRFunction ParseSingleFunction(XElement function, int executionLevel)
    {
        IRFunctionType functionType = (IRFunctionType)Enum.Parse(typeof(IRFunctionType), function.Attributes().First().Name.ToString());
        string referenceBehavior = function.Attributes().First().Value;

        ExecutionType executionType = ExecutionType.Blocking;
        if (function.Attribute("executionType") != null)
        {
            executionType = (ExecutionType)Enum.Parse(typeof(ExecutionType), function.Attribute("executionType").Value);
        }

        return new IRFunction(functionType, referenceBehavior, executionType, executionLevel);
    }

    /// <summary>
    /// Parses the Transitions of one state into a list of IR transitions structure with the given eventTopic for the transition and the target. 
    /// </summary>
    /// <param name="transitionList">XML representation of all Transitions in one state.</param>
    /// <returns>List of all transitions with eventTopic and target.</returns>
    private List<IRTransition> ParseTransitions(IEnumerable<XElement> transitionList)
    {
        List<IRTransition> iRTransitionList = new List<IRTransition>();

        foreach (var transition in transitionList)
        {
            string eventTopic = transition.Attribute("eventTopic").Value;
            string target = transition.Attribute("target").Value;

            iRTransitionList.Add(new IRTransition(eventTopic, target));
        }

        return iRTransitionList;
    }
}