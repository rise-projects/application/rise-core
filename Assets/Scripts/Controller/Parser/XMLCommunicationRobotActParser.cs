using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Linq;
using UnityEngine;

/// <summary>
/// XML Parser for parsing CommunicationRobotActs.
/// Validated by an XSD.
/// </summary>
public class XMLCommunicationRobotActParser : MonoBehaviour
{
    /// <summary>Namespace for the XSD</summary>
    private XNamespace craXSD;

    /// <summary>
    /// Parser of the XML files to the internal communication robot act structure.
    /// With validation by an XSD.
    /// Initial entry function for parsing.
    /// As unique ID, the CRA name is used.
    /// </summary>
    /// <param name="xmlFile">Name of the CRA file</param>
    /// <param name="craPath">Path to the CRA file </param>
    /// <returns></returns>
    public List<CommunicationRobotAct> ParseTrainingsActsFromFile(string xmlFile, string xsdPath)
    {
        List<CommunicationRobotAct> trainingsActDict = new List<CommunicationRobotAct>();

        XmlSchemaSet schema = new XmlSchemaSet();
        schema.Add("craXSD", xsdPath + "/CRA.xsd");

        XDocument doc = XDocument.Load(xmlFile);

        craXSD = doc.Root.GetDefaultNamespace();

        try {
            doc.Validate(schema, ValidationEventHandler);

            string[] xmlFileArray = xmlFile.Split("/");
            Debug.Log("Validation successful: " + xmlFileArray[xmlFileArray.Length - 1]);
        } catch (Exception e) {
            Debug.LogError("Validation failed: " + e.Message);
            return trainingsActDict;
        }

        var bmlDict = doc.Root.Descendants(craXSD + "Dialog");

        List<BehaviorAction> dict;
        string trainingsActName;

        foreach (XElement trainingsAct in bmlDict)
        {
            dict = new List<BehaviorAction>();
            trainingsActName = trainingsAct.Attribute("name").Value;

            foreach (ActionEnum.Action actionNum in Enum.GetValues(typeof(ActionEnum.Action)))
            {
                var actionDict = trainingsAct.Descendants(craXSD + actionNum.ToString());
                dict.AddRange(ParseActions(actionDict, actionNum, trainingsActName));
            }

            trainingsActDict.Add(new CommunicationRobotAct(trainingsActName, dict));
        }

        return trainingsActDict;
    }

    /// <summary>
    /// Validation with the given XSD.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <exception cref="Exception"></exception>
    static void ValidationEventHandler(object sender, ValidationEventArgs e) {
        XmlSeverityType type = XmlSeverityType.Warning;
        if (Enum.TryParse < XmlSeverityType > ("Error", out type)) {
            if (type == XmlSeverityType.Error) throw new Exception(e.Message);
        }
    }

    /// <summary>
    /// Get all actions who are waiting for ending of the action with action id
    /// </summary>
    /// <param name="actionDict">All XML elements to parse actions</param>
    /// <param name="actionState">Specific action type</param>
    /// <returns>List with all parsed actions for specific action type</returns>
    private List<BehaviorAction> ParseActions(IEnumerable<XElement> actionDict, ActionEnum.Action actionState, string trainingsActName)
    {
        List<BehaviorAction> dict = new List<BehaviorAction>();

        foreach (var act in actionDict)
        {
            var objectAction = ActionEnum.GetBehaviorObjectByEnum(actionState);
            List<string> attributeList = new List<string>();

            foreach (string tag in objectAction.GetAttributeTags())
            {

                var emptyValue = act.Element(tag);

                foreach (var val in act.Descendants(craXSD + tag)) { emptyValue = val; break; }

                if (emptyValue != null)
                {
                    attributeList.Add(TruncatOpenCloseTag(new XElement(emptyValue), tag));
                }
                else
                {
                    attributeList.Add("0");
                }
            }
            objectAction.Initialize(attributeList);

            if (!dict.Exists(x => (x.waitForActions.FindAll(y => objectAction.waitForActions.Contains(y)).Any() || !x.waitForActions.Any() && !objectAction.waitForActions.Any()) && x.GetType() == objectAction.GetType()))
            {
                dict.Add(objectAction);
            } else {
                Debug.Log("Conflict: The " + objectAction.actionState + " action with actionID " + objectAction.actionID + " from dialog " + trainingsActName + " was removed for being scheduled simultaneously with another " + objectAction.actionState + " action");
            }
        }

        return dict;
    }
    /// <summary>
    /// String replace function for the opening Tags.
    /// </summary>
    /// <param name="element">XML element with the tag inside</param>
    /// <param name="tag">Tag of the element that needs to be truncate</param>
    /// <returns></returns>
    private string TruncatOpenCloseTag(XElement element, string tag)
    {
        string content = element.ToString();
        while (tag.Equals("message") && content.Contains("<!--")) {
            int start = content.IndexOf("<!--");
            int length = content.IndexOf("-->") + 3 - start;
            content = content.Replace(content.Substring(start, length), "");
        }
        return content.Replace("<" + tag + " xmlns=\"" + craXSD + "\"" + ">", "").Replace("</" + tag + ">", "");
    }
}