﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Behavior Action controller to publish all actions to each robot. Sending ros messages via actionclients or publisher.
/// </summary>
public class BehaviorActionController : MonoBehaviour
{
    /// <summary>LookAt Action Client to trigger this action by publishing it.</summary>
    [Header("ROS ActionClients, Publishing Messages on Topics")]
    public LookAtTargetActionClient lookAtTargetActionClient;
    /// <summary>Animation Action Client to trigger this action by publishing it.</summary>
    public AnimationActionClient animationActionClient;
    /// <summary>Emotion Action Client to trigger this action by publishing it.</summary>
    public EmotionStateActionClient emotionStateActionClient;
    /// <summary>Speech Action Client to trigger this action by publishing it.</summary>
    public SpeechActionClient speechActionClient;
    /// <summary>Wait Action Client to trigger waiting actions by publishing it.</summary>
    public WaitActionClient waitActionClient;
    /// <summary>Social Expression Action Client to trigger social expression actions by publishing it.</summary>
    public SocialExpressionActionClient socialExpressionActionClient;

    /// <summary>UI Handler for get and setting ui values.</summary>
    [Header("General Settings")]
    public UIGetComponentValues uiComponentController;
    /// <summary>Working Memory Instance, to replace in line variables </summary>
    private WorkingMemoryController memoryController;

    private void Start()
    {
        this.memoryController = WorkingMemoryController.Instance;
    }

    /// <summary>
    /// Reflection to fill in line variables with variable types 
    /// </summary>
    /// <param name="action">action, thats been used for the reflection.</param>
    public void ReflectBehaviorActionAttributes(BehaviorAction action)
    {
        if(action.actionState.ToString()=="Speech")
        {
            ReflectSpeechActionAttributes(action);
            return;
        }
        action.replaceableAttributes.Remove("message");

        foreach (KeyValuePair<string, string> att in action.replaceableAttributes)
        {   
            string keyWithoutBrackets = att.Value.Substring(1, att.Value.Length - 2);
            if (memoryController.KeyExistsInMemory(keyWithoutBrackets))
            {
                dynamic memoryValue = memoryController.GetMemoryData(keyWithoutBrackets);

                if (typeof(BehaviorActionSpeech) == action.GetType()) 
                {
                    ((BehaviorActionSpeech)action).producedMessage = ((BehaviorActionSpeech)action).message.Replace(att.Value, memoryValue.ToString());
                    action.NotifyModelChanges();
                }
                else
                {
                    Type type = action.GetType();
                    PropertyInfo propertyInfo = type.GetProperty(att.Key);
                    if (propertyInfo != null)
                    {
                        propertyInfo.SetValue(action, Convert.ChangeType(memoryValue, propertyInfo.PropertyType));
                        action.NotifyModelChanges();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Reflection to fill in line variables with variable types 
    /// </summary>
    /// <param name="speechaction">action, thats been used for the Speech reflection.</param>
    public void ReflectSpeechActionAttributes(BehaviorAction speechAction)
    {
        if(!speechAction.replaceableAttributes.ContainsKey("message"))
            return;
            
        List<string> messageKeyValues = new List<string>();
        string messageKey="";
        foreach(char elem in speechAction.replaceableAttributes["message"])
        {
            if(elem!='|')
            {
                messageKey+=elem.ToString();
            }
            else
            {
                messageKeyValues.Add(messageKey);
                messageKey="";
            }
        }
        messageKeyValues.Add(messageKey);

        string message = ((BehaviorActionSpeech)speechAction).message;
        foreach (string keyValue in messageKeyValues)
        {
            string keyWithoutBrackets = keyValue.Substring(1, keyValue.Length - 2);
            if(memoryController.KeyExistsInMemory(keyWithoutBrackets))
            {
                dynamic memoryValue = memoryController.GetMemoryData(keyWithoutBrackets);
                if (typeof(BehaviorActionSpeech) == speechAction.GetType())
                {
                    message = message.Replace(keyValue, memoryValue.ToString());
                }
            }
        }
        ((BehaviorActionSpeech)speechAction).producedMessage = message;
        speechAction.NotifyModelChanges();
    }

    /// <summary>
    /// Final publication of the action depended on the behaviorActions actiontype and the selected robot.
    /// </summary>
    /// <param name="behaviorAction">Behavior to execute on the robot.</param>
    public void PublishTranslatedAction(BehaviorAction behaviorAction)
    {
        RobotEnum.Robot robot = uiComponentController.GetSelectedRobot();

        ActionEnum.Action actionType = RobotEnum.GetActionTranslationForRobot(robot, behaviorAction.actionState);

        switch (actionType)
        {
            case ActionEnum.Action.Animation:
                PublishAnimation((BehaviorActionAnimation)behaviorAction);
                break;
            case ActionEnum.Action.Emotion:
                PublishEmotion((BehaviorActionEmotion)behaviorAction);
                break;
            case ActionEnum.Action.LookAt:
                PublishLookAt((BehaviorActionLookAt)behaviorAction);
                break;
            //case ActionEnum.Action.PointAt:
            //    PublishPointAt((BehaviorActionTargeting)behaviorAction, robot);
            //    break;
            case ActionEnum.Action.Speech:
                PublishSpeech((BehaviorActionSpeech)behaviorAction);
                break;
            case ActionEnum.Action.Wait:
                PublishWait((BehaviorActionWait)behaviorAction);
                break;
            case ActionEnum.Action.SocialExpression:
                PublishSocialExpression((BehaviorActionSocialExpression)behaviorAction);
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// Publish a social expression action from the BehaviorAction.
    /// </summary>
    /// <param name="actionSocialExpression">BehaviorAction social expression</param>
    private void PublishSocialExpression(BehaviorActionSocialExpression actionSocialExpression)
    {
        ReflectBehaviorActionAttributes(actionSocialExpression);

        if (uiComponentController.IsDebugMode())
        {
            Debug.Log(actionSocialExpression.DebugBehaviorAction());
        }
        else
        {
            socialExpressionActionClient.PublishGoal(actionSocialExpression.value, actionSocialExpression.duration, actionSocialExpression.intensity);
        }
    }

    /// <summary>
    /// Publish a wait-action from the BehaviorAction.
    /// </summary>
    /// <param name="actionWait">BehaviorAction wait</param>
    private void PublishWait(BehaviorActionWait actionWait)
    {
        ReflectBehaviorActionAttributes(actionWait);

        if (uiComponentController.IsDebugMode())
        {
            Debug.Log(actionWait.DebugBehaviorAction());
        }
        else
        {
            waitActionClient.PublishGoal(actionWait.duration);
        }
    }

    /// <summary>
    /// Publish the message given message from the BehaviorAction for given robot
    /// </summary>
    /// <param name="actionSpeech">Publishing message</param>
    /// <param name="robot">Target robot</param>
    public void PublishSpeech(BehaviorActionSpeech actionSpeech)
    {
        ReflectBehaviorActionAttributes(actionSpeech);

        if (uiComponentController.IsDebugMode())
        {
            Debug.Log(actionSpeech.DebugBehaviorAction());
        }
        else
        {
            speechActionClient.ResetSpeechResult();
            speechActionClient.PublishGoal(actionSpeech.producedMessage);
        }
    }

    /// <summary>
    /// Publish the message given message from the BehaviorAction for given robot
    /// </summary>
    /// <param name="actionEmotion">Publishing message</param>
    /// <param name="robot">Target robot</param>
    public void PublishEmotion(BehaviorActionEmotion actionEmotion)
    {
        ReflectBehaviorActionAttributes(actionEmotion);

        if (uiComponentController.IsDebugMode())
        {
            Debug.Log(actionEmotion.DebugBehaviorAction());
        }
        else
        {
            emotionStateActionClient.PublishGoal(actionEmotion.value, actionEmotion.duration);
        }
    }

    /// <summary>
    /// Publish the message given message from the BehaviorAction for given robot
    /// </summary>
    /// <param name="actionAnimation">Publishing message</param>
    /// <param name="robot">Target robot</param>
    public void PublishAnimation(BehaviorActionAnimation actionAnimation)
    {
        ReflectBehaviorActionAttributes(actionAnimation);

        if (uiComponentController.IsDebugMode())
        {
            Debug.Log(actionAnimation.DebugBehaviorAction());
        }
        else
        {
            animationActionClient.PublishGoal(actionAnimation.target, actionAnimation.repetitions, actionAnimation.duration_each, actionAnimation.scale);
        }
    }

    /// <summary>
    /// Publish the message given message from the BehaviorAction for given robot
    /// </summary>
    /// <param name="actionTargeting">Publishing message</param>
    /// <param name="robot">Target robot</param>
    public void PublishLookAt(BehaviorActionLookAt actionLookAt)
    {
        ReflectBehaviorActionAttributes(actionLookAt);

        if (uiComponentController.IsDebugMode())
        {
            Debug.Log(actionLookAt.DebugBehaviorAction());
        }
        else
        {
            lookAtTargetActionClient.PublishGoal(actionLookAt.pointx, actionLookAt.pointy, actionLookAt.pointz, actionLookAt.roll);
        }
    }

    /// <summary>
    /// Publish the message to go to default position for the robots
    /// </summary>
    public void PublishDefaultPosture()
    {
        if (uiComponentController.IsDebugMode())
        {
            Debug.Log("GoTo Default Posture");
        }
        else
        {
            lookAtTargetActionClient.PublishGoal(0, 0, 1, 0.5f);
        }
    }

    public void PublishCancelAction(BehaviorAction action)
    {
        switch (action.actionState)
        {
            case ActionEnum.Action.Animation:
                animationActionClient.CancelGoal();
                break;
            case ActionEnum.Action.Emotion:
                emotionStateActionClient.CancelGoal();
                break;
            case ActionEnum.Action.LookAt:
                lookAtTargetActionClient.CancelGoal();
                break;
            case ActionEnum.Action.Speech:
                speechActionClient.CancelGoal();
                break;
            case ActionEnum.Action.Wait:
                waitActionClient.CancelGoal();
                break;
            case ActionEnum.Action.SocialExpression:
                socialExpressionActionClient.CancelGoal();
                break;

            default:
                break;
        }
    }
}