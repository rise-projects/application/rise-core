using B83.LogicExpressionParser;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static IRFunctionTypeEnum;
/// <summary>
/// Controller to manage the start and interruption of interaction rules, the execution of the states and the tick to see if a transition can be taken.
/// </summary>
public class InteractionRuleController : MonoBehaviour
{
    /// <summary>Contains the interactionRuleList</summary>
    public CommunicationActContainer communicationActContainer;
    /// <summary>UI Handler for get and setting ui values.</summary>
    public UIGetComponentValues uiComponentController;
    /// <summary>Instance of the scheduler</summary>
    public TaskScheduler taskScheduler;
    /// <summary>Instance of the working memory</summary>
    private WorkingMemoryController memoryController;
    /// <summary>Event, to notify about the switch into a new IR state</summary>
    public static event Action<string> newInteractionRuleState;
    /// <summary>last used transition for the UI, to highlight this transition</summary>
    private IRTransition lastUsedTransition;

    // Start is called before the first frame update
    void Start()
    {
        taskScheduler = DomainTaskActionServer.Instance.scheduler;
        memoryController = WorkingMemoryController.Instance;

        Invoke("Autostart", 2);
    }
    /// <summary>
    /// Delayed autostart, to wait for the interaction rules to be parsed.
    /// </summary>
    private void Autostart()
    {
        foreach (InteractionRule interactionRule in communicationActContainer.GetInteractionRuleList())
        {
            if (interactionRule.autostart)
            {
                Debug.Log("Start InteractionRule automatically");
                ExecuteInteractionRule(interactionRule.name);
            }
        }
    }


    /// <summary>
    /// Cancel the given interaction rule.
    /// </summary>
    /// <param name="interactionRule">IR to cancel the execution</param>
    public void StopInteractionRule(InteractionRule interactionRule)
    {
        communicationActContainer.DeactivateInteractionRule(interactionRule);
    }

    /// <summary>
    /// Cancel the given interaction rule.
    /// </summary>
    /// <param name="interactionRuleName">IR name to cancel the execution</param>
    public void StopInteractionRule(string interactionRuleName)
    {
        communicationActContainer.DeactivateInteractionRule(interactionRuleName);
    }

    /// <summary>
    /// Execution of the IR  by creation a coroutine.
    /// </summary>
    /// <param name="irName">Name as ID of the IR</param>
    /// <returns>Returns the coroutine. If the IR is already running, it returns null.</returns>
    public Coroutine ExecuteInteractionRule(string irName)
    {
        InteractionRule interactionRule = communicationActContainer.GetInteractionRuleByName(irName);

        if (!interactionRule.interactionRuleIsRunning)
        {
            return StartCoroutine(StartInteractionRule(interactionRule));
        }

        return null;
    }

    /// <summary>
    /// Star the IT by activating the IR in the communicationActContainer, setting the current state to the initial one and executing the IR.
    /// </summary>
    /// <param name="interactionRule">Name as ID of the IR<</param>
    /// <returns></returns>
    private IEnumerator StartInteractionRule(InteractionRule interactionRule)
    {
        communicationActContainer.ActivateInteractionRule(interactionRule);

        interactionRule.ChangeCurrentState(interactionRule.GetIRStateByName(interactionRule.initialState));
        ExecuteInteractionRuleState(interactionRule);

        yield return null;
    }

    /// <summary>
    /// Execution of the current State of the IR by running through the onEnty entry.
    /// Test with the logic expression parser, if given conditions are met.
    /// If this state has no further transition, the IR will be deactivated afterwards.
    /// </summary>
    /// <param name="interactionRule"></param>
    private void ExecuteInteractionRuleState(InteractionRule interactionRule)
    {
        string interactionrule_name = interactionRule.name;
        string previous_state = null;
        if(interactionRule.previousState != null)
        {
            previous_state = interactionRule.previousState.id;
        }
        string transition_event = null;
        if(lastUsedTransition != null)
        {
            transition_event = lastUsedTransition.eventTopic;
        }
        string current_state = interactionRule.currentState.id;
        
        var irStateUpdaterObject = new { interactionrule_name, previous_state, transition_event, current_state };
        string jsonData = JsonConvert.SerializeObject(irStateUpdaterObject);
        Debug.Log(jsonData);
        newInteractionRuleState?.Invoke(jsonData);

        interactionRule.currentState.ChangeIRStatus(true);

        bool ifCondExecuted = false;
        bool isMissingVariable = false;

        foreach (IRAbstractOnEntryElement onEntryElement in interactionRule.currentState.onEntry.onEntryElements)
        {
            if (onEntryElement is IRFunction)
            {
                ExecuteInteractionRuleFunction((IRFunction)onEntryElement);
            }
            else if ((onEntryElement is IRIFCond) && !ifCondExecuted)
            {
                // Hotfix: Init second LEP for parse variables from condition, deletion afterwards.
                // LEP needs known variables before evaluating or getting the equation.
                Parser lepForGettingVariables = new Parser();
                LogicExpression expressionForVariables = lepForGettingVariables.Parse(((IRIFCond)onEntryElement).conditionStatement);
                List<string> detectedVariables = lepForGettingVariables.ExpressionContext.GetVariables();

                lepForGettingVariables = null;
                expressionForVariables = null;

                Parser logicExpressionParser = new Parser();

                // Set variables
                foreach (string variableInExpression in detectedVariables)
                {
                    if (memoryController.KeyExistsInMemory(variableInExpression))
                    {
                        logicExpressionParser.ExpressionContext[variableInExpression].Set(memoryController.GetMemoryData(variableInExpression));
                    }
                    else
                    {
                        isMissingVariable = true;
                    }
                }

                LogicExpression real_expression = logicExpressionParser.Parse(((IRIFCond)onEntryElement).conditionStatement);

                if (!isMissingVariable && real_expression.GetResult())
                {
                    foreach (IRFunction functionInCond in ((IRIFCond)onEntryElement).functionList)
                    {
                        ExecuteInteractionRuleFunction(functionInCond);
                    }

                    ifCondExecuted = true;
                }

                isMissingVariable = false;
            }
        }

        if (interactionRule.currentState.transitionList.Count == 0)
        {
            communicationActContainer.DeactivateInteractionRule(interactionRule);
        }
    }

    /// <summary>
    /// Execution of a single function inside the interaction rule on entry element.
    /// </summary>
    /// <param name="function">Function to be executed.</param>
    private void ExecuteInteractionRuleFunction(IRFunction function)
    {
        string refBehavior = function.referenceBehavior;

        if (function.functionType == IRFunctionType.assignValue)
        {
            refBehavior = "{" + refBehavior.Replace("=", ":") + "}";
        }

        taskScheduler.AppendInteractionRuleFunction(refBehavior, function.functionType, function.executionType);
    }

    /// <summary>
    /// Selects the targetState as new currentState and executing the new state after waiting 250ms to present switching too fast between two states.
    /// </summary>
    /// <param name="irName">Name of the IR as ID</param>
    /// <param name="targetState">New current state.</param>
    /// <returns></returns>
    public IEnumerator TickInteractionRule(string irName, IRTransition targetState)
    {
        yield return new WaitForSeconds(0.25f);

        InteractionRule currentIR = communicationActContainer.GetInteractionRuleByName(irName);

        if (currentIR.interactionRuleIsRunning)
        {
            communicationActContainer.SetInteractionRuleOnTop(currentIR);

            // Changing Transition status
            if (lastUsedTransition != null)
            {
                lastUsedTransition.SetActive(false);
            }
            lastUsedTransition = targetState;
            targetState.SetActive(true);

            currentIR.currentState.ChangeIRStatus(false);
            currentIR.ChangeCurrentState(currentIR.GetIRStateByName(targetState.target));

            ExecuteInteractionRuleState(currentIR);
        }
        else
        {
            if (lastUsedTransition != null)
            {
                lastUsedTransition.SetActive(false);
            }
        }
    }
}