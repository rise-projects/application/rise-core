using System;
using System.Collections;
using UnityEngine;
using static IRFunctionTypeEnum;
using static TaskManagementEnum;

/// <summary>
/// Manages the execution and interruption of the communication robot acts and interaction rules.
/// Provides a public function for internal events topics.
/// </summary>
public class BehaviorManager : MonoBehaviour
{
    /// <summary>Reference to the dialogActController to run CRAs</summary>
    public CommunicationRobotActController dialogActController;
    /// <summary>Reference to the interactionRuleController to run IRs</summary>
    public InteractionRuleController interactionRuleController;
    /// <summary>Instance of the scheduler to change the current running state.</summary>
    public TaskScheduler scheduler;
    /// <summary>Internal event action to raise incoming event for the transition between two state inside a IR.</summary>
    public static event Action<string> raiseInternalEvent;

    void Start()
    {
        scheduler = DomainTaskActionServer.Instance.scheduler;
        scheduler.behaviorManager = this;
    }

    /// <summary>
    /// Execution of a DomainTask, managed by the scheduler. 
    /// Only used for CRAs and IR.
    /// </summary>
    /// <param name="domainTask">DomainTask with functionType and behaviorName as ID</param>
    /// <exception cref="InvalidOperationException"></exception>
    public void ExecuteDomainTask(DomainTask domainTask)
    {
        if (domainTask == null)
        {
            Debug.Log("NULL");
        }

        switch (domainTask.domainInputUnit.functionType)
        {
            case IRFunctionType.startCommunicationRobotAct:
                domainTask.domainInputUnit.behaviorName = GetRandomBehaviorFromList(domainTask.domainInputUnit.behaviorName);

                StartCoroutine(RunCommunicationRobotAct(domainTask));
                break;

            case IRFunctionType.startInteractionRule:
                scheduler.ManageAndUpdateDomainTask(domainTask, TaskStatus.Finished);
                interactionRuleController.ExecuteInteractionRule(domainTask.domainInputUnit.behaviorName);
                break;

            default:
                throw new InvalidOperationException("DomainTask not found");
        }
    }

    /// <summary>
    /// Execution of a CRA by creation a coroutine.
    /// When the Coroutine returns, the execution is completed and the status changed to Finished. 
    /// </summary>
    /// <param name="domainTask">domainTask with functionType startCommunicationRobotAct to execute.</param>
    /// <returns>Returns the running coroutine.</returns>
    private IEnumerator RunCommunicationRobotAct(DomainTask domainTask)
    {
        domainTask.runningCoroutine = dialogActController.ExecuteDialogAct(domainTask.domainInputUnit.behaviorName);
        scheduler.ManageAndUpdateDomainTask(domainTask, TaskStatus.Running);

        yield return domainTask.runningCoroutine;
        Debug.Log("Returned from "+ domainTask.domainInputUnit.behaviorName);
        scheduler.ManageAndUpdateDomainTask(domainTask, TaskStatus.Finished);
    }

    /// <summary>
    /// Interruption of a running domainTask by stopping the corresponding coroutine and update the task status.
    /// </summary>
    /// <param name="domainTask"></param>
    public void InterruptDomainTask(DomainTask domainTask)
    {
        dialogActController.StopCRA();
        if(domainTask.runningCoroutine != null && domainTask.taskStatus == TaskStatus.Running)
        {
            StopCoroutine(domainTask.runningCoroutine);
            scheduler.ManageAndUpdateDomainTask(domainTask, TaskStatus.Interrupted);
        }
    }

    /// <summary>
    /// Internal event action to raise incoming event for the transition between two state inside a IR.
    /// </summary>
    /// <param name="msg">event Topic to raises.</param>
    public void RaiseInternalEvent(string msg)
    {
        raiseInternalEvent?.Invoke(msg);
    }

    /// <summary>
    /// Selection of a random behavior, if multiple behaviors are given.
    /// If only one behavior is given, its also returned.
    /// </summary>
    /// <param name="behaviorList">String representation of a list of behaviors.</param>
    /// <returns>Returns one behavior of the list.</returns>
    private string GetRandomBehaviorFromList(string behaviorList)
    {
        if (behaviorList.StartsWith("[") && behaviorList.EndsWith("]"))
        {
            string behaviors = behaviorList.Trim('[', ']');
            string[] splittedBehaviors = behaviors.Split(',');

            System.Random rnd = new System.Random();
            return splittedBehaviors[rnd.Next(0, splittedBehaviors.Length)].Trim();
        }

        return behaviorList;
    }
}