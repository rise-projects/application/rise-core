using System;
using System.Collections.Generic;
using UnityEngine;
using static IRFunctionTypeEnum;
using static TaskManagementEnum;

/// <summary>
/// Schedules the domain tasks and their status.
/// </summary>
public class TaskScheduler : MonoBehaviour
{
    /// <summary>Rate of processing.</summary>
    public float tickRate = 0.25f;
    /// <summary>Instance of the Action Server</summary>
    private DomainTaskActionServer actionServer;
    /// <summary>List of all current domain tasks.</summary>
    private List<DomainTask> taskList;
    /// <summary>
    /// Getter/Setter of the behaviorManager.
    /// </summary>
    public BehaviorManager behaviorManager { get; set; }

    public static event Action<DomainTask> domainTaskListChanged;

    // Start is called before the first frame update
    void Start()
    {
        taskList = new List<DomainTask>();
        actionServer = DomainTaskActionServer.Instance;

        InvokeRepeating("TickScheduler", 1, tickRate);
    }
    /// <summary>
    /// Getter of the taskList.
    /// </summary>
    /// <returns>taskList as List of DomainTasks</returns>
    public List<DomainTask> GetDomainTaskList()
    {
        return taskList;
    }

    /// <summary>
    /// Appends a new DomainTask to the list with the status waiting.
    /// Invokes, that the list has changed.
    /// </summary>
    /// <param name="domainInputUnit">domainInputUnit to append as DomainTask</param>
    public void AppendDomainTask(DomainInputUnit domainInputUnit)
    {
        DomainTask newTask = new DomainTask(domainInputUnit, null, TaskStatus.Waiting);
        taskList.Add(newTask);

        domainTaskListChanged?.Invoke(newTask);
    }

    /// <summary>
    /// Appends a new internal goal at the actionServer.
    /// Used to append the functions from the IR as internal goal.
    /// </summary>
    /// <param name="behavior">ID of the behavior.</param>
    /// <param name="functionType">Type of the behavior like startInteractionRule</param>
    /// <param name="executionType">blocking/none</param>
    public void AppendInteractionRuleFunction(string behavior, IRFunctionType functionType, ExecutionType executionType)
    {
        //AppendDomainTask(new DomainInputUnit(int.Parse(actionServer.GetIncrementedActionGoalID()), behavior, ExecutionType.None));

        //going from inside to actionserver (outside) to append task on scheduler inside
        actionServer.InternalGoalReceived(behavior, functionType.ToString(), executionType.ToString());
    }

    /// <summary>
    /// Is triggered every tickrate ms.
    /// Checks, if currently no blocking task is running and starts the next task in the list.
    /// </summary>
    private void TickScheduler()
    {
        /**
         * ToDo: verarbeite tasklist, suche n�chstes, mappe auf laufenden Stack
         * Event sucht CA/CCA
         */

        //DebugTaskList();

        if (HasTaskListOpenTask())
        {
            if (!HasTaskListRunningBlockingTask())
            {
                DomainTask runningTask = GetRunningTask();
                if (runningTask != null)
                {
                    behaviorManager.InterruptDomainTask(runningTask);
                }
                else
                {
                    DomainTask nextTask = GetScheduledOpenTaskState();
                    behaviorManager.ExecuteDomainTask(nextTask);
                }
            }
        }
    }

    /// <summary>
    /// Updates the status of a domainTask and removes is, if it has finished.
    /// </summary>
    /// <param name="domainTask">DomainTask to update</param>
    /// <param name="domainTaskStatus">new Status of the DomainTask</param>
    public void ManageAndUpdateDomainTask(DomainTask domainTask, TaskStatus domainTaskStatus)
    {
        domainTask.taskStatus = domainTaskStatus;
        domainTaskListChanged?.Invoke(domainTask);

        switch (domainTaskStatus)
        {
            case TaskStatus.Finished:
                taskList.Remove(domainTask);
                actionServer.SetSucceeded(actionServer.GetGoalIDMsgFromServerByID(domainTask.domainInputUnit.domainInputID));

                //Trigger ending of an Task/Action/RobotDialog/...
                if (domainTask.domainInputUnit.functionType == IRFunctionType.startCommunicationRobotAct)
                {
                    string behaviorEnd = "/" + domainTask.domainInputUnit.behaviorName + "_end";
                    actionServer.InternalGoalReceived(behaviorEnd, IRFunctionType.raiseEventTopic.ToString(), ExecutionType.None.ToString());
                }
                break;

            case TaskStatus.Interrupted:
                taskList.Remove(domainTask);
                actionServer.SetAborted(actionServer.GetGoalIDMsgFromServerByID(domainTask.domainInputUnit.domainInputID));

                break;

            case TaskStatus.None:
                break;
        }

        domainTaskListChanged?.Invoke(domainTask);
    }

    /// <summary>
    /// Cancels the given Task by GoalID.
    /// </summary>
    /// <param name="goalID">goalID of the Task, that needs to be canceled.</param>
    public void ProcessSignalReceived(string goalID)
    {
        DomainTask process = GetDomainTaskByID(goalID);

        if (process.taskStatus.Equals(TaskStatus.Running))
        {
            behaviorManager.InterruptDomainTask(process);
        }

        //actionServer.SetSucceeded(actionServer.GetGoalIDMsgFromServerByID(goalID));
        actionServer.SetCanceled(actionServer.GetGoalIDMsgFromServerByID(goalID));
    }

    /// <summary>
    /// Interrupt the DomainTask by name.
    /// </summary>
    /// <param name="name">name of the DomainTask</param>
    public void InterruptDomainTask(string name)
    {
        if(this.GetDomainTaskByName(name) != null)
        {
            this.behaviorManager.InterruptDomainTask(this.GetDomainTaskByName(name));
        }
    }

    //-----


    /// <summary>
    /// Checks, if the taskList has task, that need to be processed.
    /// </summary>
    /// <returns>True, if a open task exists.</returns>
    private bool HasTaskListOpenTask()
    {
        foreach (DomainTask task in taskList)
        {
            if (TaskManagementEnum.IsOpenStatus(task.taskStatus))
            {
                return true;
            }
        }

        return false;
    }
    /// <summary>
    /// Returns the currently running Task as DomanTask
    /// </summary>
    /// <returns>Returns the running task, if exist. If there is currently no running task, null will be returned.</returns>
    private DomainTask GetRunningTask()
    {
        foreach (DomainTask task in taskList)
        {
            if (task.taskStatus == TaskStatus.Running)
            {
                return task;
            }
        }

        return null;
    }

    /// <summary>
    /// Getter for the DomainTask by name
    /// </summary>
    /// <param name="name">name of the DomainTask</param>
    /// <returns>Returns the DomainTask, if exists else null.</returns>
    private DomainTask GetDomainTaskByName(string name)
    {
        foreach (DomainTask task in taskList)
        {
            if (task.domainInputUnit.behaviorName.Equals(name))
            {
                return task;
            }
        }

        return null;
    }

    /// <summary>
    /// Checks the currently running Tasks for blocking Tasks
    /// </summary>
    /// <returns>True, if a blocking task is currently running</returns>
    private bool HasTaskListRunningBlockingTask()
    {
        foreach (DomainTask process in taskList)
        {
            if (process.taskStatus.Equals(TaskStatus.Running) && process.domainInputUnit.executionType.Equals(ExecutionType.Blocking))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Getter for the DomainTask by ID.
    /// </summary>
    /// <param name="id">ID of the DomainTask</param>
    /// <returns>Returns the Task if exists, if not throws an exception</returns>
    /// <exception cref="InvalidOperationException">If ID not exist.</exception>
    public DomainTask GetDomainTaskByID(string id)
    {
        foreach (DomainTask task in taskList)
        {
            if (task.domainInputUnit.domainInputID == id)
            {
                return task;
            }
        }

        throw new InvalidOperationException("Process not found");
    }

    /// <summary>
    /// Getter for the DomainTask by DomainInputUnit.
    /// </summary>
    /// <param name="task">DomainInputUnit to compare the IDs</param>
    /// <returns>DomainTask if exists.</returns>
    /// <exception cref="InvalidOperationException">If Task not exist.</exception>
    private DomainTask GetExecutedProcessForTask(DomainInputUnit task)
    {
        foreach (DomainTask process in taskList)
        {
            if (process.domainInputUnit.domainInputID == task.domainInputID && process.taskStatus.Equals(TaskStatus.Running))
            {
                return process;
            }
        }

        throw new InvalidOperationException("Task not executed");
    }

    /// <summary>
    /// Getter for the next DomainTask, that is currently not running.
    /// </summary>
    /// <returns>Returns a DomainTask, that is currently not running.</returns>
    /// <exception cref="InvalidOperationException">If the taskList is empty of there is no open Task.</exception>
    public DomainTask GetScheduledOpenTaskState()
    {
        if (taskList.Count > 0)
        {
            foreach (DomainTask process in taskList)
            {
                if (TaskManagementEnum.IsOpenStatus(process.taskStatus))
                {
                    return process;
                }
            }
        }

        throw new InvalidOperationException("Tasklist is empty");
    }

    /// <summary>
    /// Checks, if the Task is part of the taskList.
    /// </summary>
    /// <param name="searchingTask">Task to search for</param>
    /// <returns>True, if the Task is part of the taskList.</returns>
    public bool IsDomainTaskInTaskList(DomainTask searchingTask)
    {
        foreach (DomainTask process in taskList)
        {
            if (process.domainInputUnit.domainInputID == searchingTask.domainInputUnit.domainInputID)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Debug Function.
    /// </summary>
    public void DebugTaskList()
    {
        string opentasks = "Scheduled Tasks: ";
        foreach (DomainTask task in taskList)
        {
            opentasks += "(" + task.domainInputUnit.behaviorName + ", " + task.taskStatus + ")" + "->";
        }

        Debug.Log("------------------" + opentasks + '\n');
    }
}