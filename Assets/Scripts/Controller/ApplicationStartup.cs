using Newtonsoft.Json;
using System;
using System.IO;
using UnityEngine;

/// <summary>
/// Parser of the startup arguments for the initial paths to the structures, starting robot and headless or GUI mode.
/// 
/// </summary>
public class ApplicationStartup : MonoBehaviour
{
    /// <summary>FileManager to handle open file browser script.</summary>
    public FileManager fileManager;
    /// <summary>UI Handler for get and setting ui values.</summary>
    public UIGetComponentValues uiComponents;
    /// <summary>Handle value changes in the settings of ip variables of the UI.</summary>
    public IPChangeScript ipChanger;

    /// <summary>
    /// On startup, the startup arguments are processed for initial settings.
    /// Initial settings are: mode, robot, ip and the paths to the CRA, IR and WM.
    /// </summary>
    [Obsolete]
    private void Start()
    {
#if UNITY_EDITOR
        string[] args = new string[0];

        string saveFile = Application.dataPath + "/args.data";
        if (File.Exists(saveFile))
        {
            string fileContents = File.ReadAllText(saveFile);
            args = JsonConvert.DeserializeObject<string[]>(fileContents);
        }
        else
        {
            Debug.Log("Data Settings not into JSON file under filepath: " + saveFile);
        }
#else
        string[] args = System.Environment.GetCommandLineArgs();
#endif

        if (args.Length >= 1)
        {
            string configurationRootPath = "";
            string configurationCategoryPath = "";
            string configurationProjectPath = "";
            string configurationXSDPath = "";


            for (int i = 0; i < args.Length; i++)
            {
                switch (args[i].ToLower())
                {
                    case "-mode":
                        uiComponents.SetupExecutionMode(Enum.Parse(typeof(ExecutionModeEnum.ExecutionMode), args[i + 1].ToLower()).ToString());
                        break;

                    case "-robot":
                        uiComponents.SetupTopicPrefix(args[i + 1].ToLower());
                        break;

                    case "-ip":
                        ipChanger.ApplyIPFromCommandLine(args[i + 1]);
                        break;

                    case "-path_config_root":
                        configurationRootPath = args[i + 1];
                        break;

                    case "-path_config_category":
                        configurationCategoryPath = args[i + 1];
                        break;

                    case "-path_config_project":
                        configurationProjectPath = args[i + 1];
                        break;

                    case "-path_config_xsds":
                        configurationXSDPath = args[i + 1];
                        break;

                    default:
                        break;
                }
            }

            fileManager.SetupConfigurationFilePathsFromCommandLine(configurationRootPath, configurationCategoryPath, configurationProjectPath, configurationXSDPath);
        }
    }
}