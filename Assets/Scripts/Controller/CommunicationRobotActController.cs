using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

/// <summary>
/// Controller to execute and interrupt the CRAs started by the behaviorActionController.
/// </summary>
public class CommunicationRobotActController : MonoBehaviour
{
    /// <summary>Contains the communicationRobotActList</summary>
    [Header("Given Controlling for Communication")]
    public CommunicationActContainer dialogActContainer;
    /// <summary>controller of all available CRA actions</summary>
    public BehaviorActionController actionController;
    /// <summary>UI Handler for get and setting ui values.</summary>
    public UIGetComponentValues uiComponentController;

    /// <summary>List of all executed Jobs with coroutine, to be able to cancel them, is necessary.</summary>
    [Header("Calculation Structures, Job Queues and States")]
    public List<JobAction> executedJobs;
    /// <summary>List of behaviors, waiting for another behavior to finish. In the CRA XML represented as waitForActions</summary>
    public List<BehaviorAction> waitingJobs;
    /// <summary>Only one CRA can run at a time. This is the currently running CRA.</summary>
    public CommunicationRobotAct runningCRA;
    /// <summary>calculation of percentage, the CRA has finished as event to notify the UI element.</summary>
    public static event Action<float> progressHasChanged;
    /// <summary>Event, to notify about beginning, interruption and ending of a CRA</summary>
    public static event Action<string> communicationRobotActUpdate;

    void Start()
    {
        executedJobs = new List<JobAction>();
        waitingJobs = new List<BehaviorAction>();
        runningCRA = null;
    }
    /// <summary>
    /// Starts the given CRA as coroutine and returns these.
    /// </summary>
    /// <param name="craReference"></param>
    /// <returns>returns the started coroutine</returns>
    public Coroutine ExecuteDialogAct(string craReference)
    {
        //Debug.Log(craReference);
        return StartDialogAct(dialogActContainer.GetCommunicationRobotActByName(craReference));
    }

    /// <summary>
    /// Start the communication robot act as coroutine.
    /// </summary>
    /// <param name="communicationRobotAct">Starting trainings act</param>
    public Coroutine StartDialogAct(CommunicationRobotAct communicationRobotAct)
    {

        SendROSUpdate(communicationRobotAct.communicationRobotActName, DialogActUpdateState.Started);
        Debug.Log("StartDialogAct");
        this.runningCRA = communicationRobotAct;
        progressHasChanged?.Invoke(0.0f);
        return StartCoroutine(RunCRA(communicationRobotAct));
    }

    /// <summary>
    /// Interrupt the actual running CRA and updates the status.
    /// </summary>
    public void StopCRA()
    {
        if (this.runningCRA != null)
        {
            foreach (JobAction job in executedJobs)
            {
                StopCoroutine(job.runningCoroutine);
            }
            StopAllCoroutines();

            this.executedJobs.Clear();
            this.waitingJobs.Clear();

            foreach (BehaviorAction action in this.runningCRA.actionList)
            {
                action.SetActionStatus(false);
                action.CancelBehaviorAction();
            }

            this.runningCRA.SetCRAStatus(false);
            SendROSUpdate(this.runningCRA.communicationRobotActName, DialogActUpdateState.Interruped);
            this.runningCRA = null;

        }
    }

    /// <summary>
    /// Start and manage the CRA
    /// Main functionality for running the whole process
    /// </summary>
    /// <param name="cra">Starting training</param>
    private IEnumerator RunCRA(CommunicationRobotAct cra)
    {
        runningCRA.SetCRAStatus(true);

        runningCRA = cra;
        runningCRA.SortBehaviorDictByWaitRef();

        executedJobs = new List<JobAction>();
        waitingJobs = new List<BehaviorAction>();

        foreach (BehaviorAction action in runningCRA.actionList)
        {
            if (action.waitForActions[0] == 0)
            {
                executedJobs.Add(new JobAction(action.actionID, action.actionState, StartCoroutine(action.StartBehaviorAction())));
                progressHasChanged?.Invoke(CalculateTrainingsProgress());
            }
            else
            {
                waitingJobs.Add(action);
            }
        }

        BehaviorAction wJob;
        JobAction lastExecutedJob = executedJobs[0];

        while (waitingJobs.Count > 0)
        {
            SendROSUpdate(runningCRA.communicationRobotActName, DialogActUpdateState.InProgress);
            waitingJobs = waitingJobs.OrderBy(a => a.actionID).ToList();

            wJob = waitingJobs[0];
            lastExecutedJob = GetExecutedJobForWaitingJob(executedJobs, wJob.waitForActions);
            yield return lastExecutedJob.runningCoroutine;

            foreach (BehaviorAction eJob in GetWaitingJobsForFinishedActionID(waitingJobs, lastExecutedJob.actionID))
            {
                waitingJobs.Remove(eJob);
                executedJobs.Add(new JobAction(eJob.actionID, eJob.actionState, StartCoroutine(eJob.StartBehaviorAction())));
                progressHasChanged?.Invoke(CalculateTrainingsProgress());
            }
        }

        yield return new WaitUntil(() => !runningCRA.IsCRAIsRunning());
        progressHasChanged?.Invoke(1);
        SendROSUpdate(runningCRA.communicationRobotActName, DialogActUpdateState.Completed);
        runningCRA.SetCRAStatus(false);
    }

    /// <summary>
    /// Return the running JobAction which one of the waitOptions is waiting for.
    /// </summary>
    /// <param name="exeList">List with all executed jobs</param>
    /// <param name="waitOptions">Waiting ids for jobs in exeList</param>
    /// <returns>Return running job for one of the waitOptions</returns>
    private JobAction GetExecutedJobForWaitingJob(List<JobAction> exeList, List<int> waitOptions)
    {
        foreach (JobAction job in exeList)
        {
            foreach (int waitOption in waitOptions)
            {
                if (job.actionID == waitOption)
                {
                    return job;
                }
            }
        }

        return null;
    }

    /// <summary>
    /// Get all actions who are waiting for ending of the action with action id.
    /// </summary>
    /// <param name="waitList">All waiting jobs</param>
    /// <param name="actionID">Finished job action id</param>
    /// <returns>List with all next running action</returns>
    private List<BehaviorAction> GetWaitingJobsForFinishedActionID(List<BehaviorAction> waitList, int actionID)
    {
        List<BehaviorAction> sepWaitList = new List<BehaviorAction>();

        foreach (BehaviorAction waitJob in waitList)
        {
            foreach (int waitID in waitJob.waitForActions)
            {
                if (waitID == actionID)
                {
                    if (!sepWaitList.Contains(waitJob))
                    {
                        sepWaitList.Add(waitJob);
                    }
                }
            }
        }

        return sepWaitList;
    }
    /// <summary>
    /// Debug function.
    /// </summary>
    /// <returns>String list of executed jobs.</returns>
    private string PrintExecutedList()
    {
        string res = "Executed: ";

        foreach (JobAction job in executedJobs)
        {
            res += job.actionID + "->";
        }

        return res;
    }

    /// <summary>
    /// Debug function.
    /// </summary>
    /// <returns>String list of waiting jobs.</returns>
    private string PrintWaitingList()
    {
        string res = "Waiting: ";

        foreach (BehaviorAction job in waitingJobs)
        {
            res += job.actionID + "->";
        }

        return res;
    }
    /// <summary>
    /// Calculation of percentage process of executing the whole CRA.
    /// </summary>
    /// <returns>percentage of execution</returns>
    public float CalculateTrainingsProgress()
    {
        float prev = 0.0f;

        if (this.runningCRA == null)
        {
            return 0.0f;
        }

        float n = this.runningCRA.actionList.Count;

        if (executedJobs.Count - 1 >= 0)
        {
            prev = executedJobs[executedJobs.Count - 1].actionID - 1;
        }

        float res = ((prev * 2) + 1) / (n * 2);

        return res;
    }
    /// <summary>
    /// Getter function for behavior actions by ID.
    /// </summary>
    /// <param name="actionID">actionID of the requested behavior</param>
    /// <returns>the corresponding action, if exist. Otherwise it returns null if the ID it not taken.</returns>
    private BehaviorAction GetBehaviorActionByActionID(int actionID)
    {
        foreach (BehaviorAction action in runningCRA.actionList)
        {
            if (actionID == action.actionID)
            {
                return action;
            }
        }

        return null;
    }

    private void SendROSUpdate(string CRA_name, DialogActUpdateState state)
    {
        var CRAStateUpdaterObject = new { CRA_name, state };
        //new StringEnumConverter to convert the enum as string instead of underling integer.
        string jsonData = JsonConvert.SerializeObject(CRAStateUpdaterObject, new StringEnumConverter());


        Debug.Log(jsonData);
        communicationRobotActUpdate?.Invoke(jsonData);
    }

    public enum DialogActUpdateState
    {
        Started,
        InProgress,
        Completed,
        Interruped
    }
}