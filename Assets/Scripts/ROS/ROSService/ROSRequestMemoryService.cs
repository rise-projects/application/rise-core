using System.Collections;
using System.Collections.Generic;
using RosMessageTypes.MemoryWritingRequest;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;
/// <summary>
/// ROS Service to request a memory value of the given key.
/// </summary>
public class ROSRequestMemoryService : MonoBehaviour
{
    private ROSConnection ros;
    private WorkingMemoryController memoryController;
    public string serviceName = "/rise/set/memoryService";

    // Start is called before the first frame update
    void Start()
    {
        memoryController = WorkingMemoryController.Instance;

        ros = ROSConnection.GetOrCreateInstance();
        ros.ImplementService<WorkingMemoryServiceRequest, WorkingMemoryServiceResponse>(serviceName, RequestCallback);
    }

    /// <summary>
    /// Response to the request. Returns the whole Json key value pair string 
    /// </summary>
    /// <param name="request">requested key</param>
    /// <returns>Json key value pair of the requested key</returns>
    public WorkingMemoryServiceResponse RequestCallback(WorkingMemoryServiceRequest request)
    {
        WorkingMemoryServiceResponse response = new WorkingMemoryServiceResponse();

        response.json_response = memoryController.GetRequestResponseByKey(request.request_by_key);

        return response;
    }
}