using Unity.Robotics.ROSTCPConnector;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

/// <summary>
/// Abstract class for ROS Subscriber
/// Subscribers receive messages via ros on specific topics from the robot
/// </summary>
public abstract class ROSSubscriber<T> : MonoBehaviour where T : Message, new()
{
    [Header("Specify ROS Topic Name")]
    public string topic;

    private ROSConnection rosConnector;

    protected virtual void Start()
    {
        rosConnector = ROSConnection.GetOrCreateInstance();
        rosConnector.Subscribe<T>(topic, ReceiveMessage);
    }

    /// <summary>
    /// Abstract class for ROS Subscriber
    /// Subscribers receive messages via ros on specific topics from the robot
    /// </summary>
    /// <param name="message">Received message on subscribed topic</param>
    protected abstract void ReceiveMessage(T message);
}