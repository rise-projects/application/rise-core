using RosMessageTypes.Server;

/// <summary>
/// ROS ActionClient for Emotions.
/// </summary>
public class EmotionStateActionClient : ROSActionClient<EmotionstateAction, EmotionstateActionGoal, EmotionstateActionResult, EmotionstateActionFeedback, EmotionstateGoal, EmotionstateResult, EmotionstateFeedback>
{
    /// <summary>Value of the Emotion</summary>
    public int value;
    /// <summary>duration of the emotion</summary>
    public int duration;

    private void Start()
    {
        this.Initialize(UIGetComponentValues.topicRobotPrefix);
        this.action = new EmotionstateAction();
        this.goalStatus = new RosMessageTypes.Actionlib.GoalStatusMsg();
    }

    /// <summary>
    /// Publish emotion state message as goal for robot.
    /// </summary>
    /// <param name="value">Identify value for predefined emotion value</param>
    /// <param name="duration">Duration for this emotionsstate</param>
    /// <param name="robot">Robot target</param>
    public void PublishGoal(int value, int duration)
    {
        this.value = value;
        this.duration = duration;
        this.SendGoal();
    }

    /// <summary>
    /// Getter Function for this action goal.
    /// </summary>
    /// <returns>action_goal</returns>
    protected override EmotionstateActionGoal GetActionGoal()
    {
        this.action.action_goal.goal.value = System.Convert.ToByte(this.value);
        this.action.action_goal.goal.duration = System.Convert.ToUInt32(this.duration);

        return this.action.action_goal;
    }

    protected override void OnFeedbackReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnResultReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnStatusUpdated()
    {
        //throw new System.NotImplementedException();
    }
}