using RosMessageTypes.Server;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// ROS ActionClient for speak messages as Speech.
/// </summary>
public class SpeechActionClient : ROSActionClient<SpeechAction, SpeechActionGoal, SpeechActionResult, SpeechActionFeedback, SpeechGoal, SpeechResult, SpeechFeedback>
{
    /// <summary> The message that will be spoken by the robot. </summary>
    public string message;
    public string[] splittedMessages;

    private void Start()
    {
        this.Initialize(UIGetComponentValues.topicRobotPrefix);
        this.action = new SpeechAction();
        this.goalStatus = new RosMessageTypes.Actionlib.GoalStatusMsg();
    }

    /// <summary>
    /// Publish speech message as goal for robot.
    /// </summary>
    /// <param name="message">Text to speech message</param>
    public void PublishGoal(string message)
    {
        this.message = "";

        if(message != "")
        {
            this.splittedMessages = Regex.Split(message, @"(?<=[\.!:;\n\?])\s+");
        }
        
        if(this.splittedMessages.Length > 0)
        {
            this.message = splittedMessages[0];
            this.splittedMessages = splittedMessages.Skip(1).ToArray();
        }

        this.SendGoal();
    }

    /// <summary>
    /// Check actionclient result to detect speech is finish.
    /// </summary>
    /// <returns>True if actionclient result got an positive feedback. Text is done.</returns>
    public bool IsSpeechActionFinished()
    {
        if (this.action.action_result != null)
        {
            if (this.action.action_result.result.result == 1)
            {
                this.ResetSpeechResult();
                if (this.splittedMessages.Length > 0 && this.splittedMessages[0] != "")
                {
                    this.PublishGoal("");
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Sets the result of this speechaction to 0.
    /// </summary>
    public void ResetSpeechResult()
    {
        if (this.action.action_result != null)
        {
            this.action.action_result.result.result = 0;
        }
    }

    /// <summary>
    /// Updates the message before returning the current action_goal.
    /// </summary>
    /// <returns>Returns the action_goal</returns>
    protected override SpeechActionGoal GetActionGoal()
    {
        this.action.action_goal.goal.text = this.message;

        return this.action.action_goal;
    }

    protected override void OnFeedbackReceived()
    {
        //throw new NotImplementedException();
    }

    protected override void OnResultReceived()
    {
        //throw new NotImplementedException();
    }

    protected override void OnStatusUpdated()
    {
        //throw new NotImplementedException();
    }
}