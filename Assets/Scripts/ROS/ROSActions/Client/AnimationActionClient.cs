using RosMessageTypes.Server;

/// <summary>
/// ROS ActionClient for Animations.
/// </summary>
public class AnimationActionClient : ROSActionClient<AnimationAction, AnimationActionGoal, AnimationActionResult, AnimationActionFeedback, AnimationGoal, AnimationResult, AnimationFeedback>
{
    /// <summary>Identify value for predefined animation value.</summary>
    public int target;
    /// <summary>Number of repetitions of the animation</summary>
    public int repetitions;
    /// <summary>Duration for each animation</summary>
    public int duration_each;
    /// <summary>Velocity for robots movement</summary>
    public float scale;

    private void Start()
    {
        this.Initialize(UIGetComponentValues.topicRobotPrefix);
        this.action = new AnimationAction();
        this.goalStatus = new RosMessageTypes.Actionlib.GoalStatusMsg();
    }

    /// <summary>
    /// Publish animation state message as goal for robot.
    /// </summary>
    /// <param name="target">Identify value for predefined animation value</param>
    /// <param name="repetitions">Number of repetitions of the animation</param>
    /// <param name="duration">Duration for each animation</param>
    /// <param name="scale">Velocity for robots movement</param>
    /// <param name="robot">Robot target</param>
    public void PublishGoal(int target, int repetitions, int duration, float scale)
    {
        this.target = target;
        this.repetitions = repetitions;
        this.duration_each = duration;
        this.scale = scale;

        this.SendGoal();
    }
    /// <summary>
    /// Getter Function for this action goal.
    /// </summary>
    /// <returns>action_goal</returns>
    protected override AnimationActionGoal GetActionGoal()
    {
        this.action.action_goal.goal.target = System.Convert.ToByte(this.target);
        this.action.action_goal.goal.repetitions = System.Convert.ToUInt32(this.repetitions);
        this.action.action_goal.goal.duration_each = System.Convert.ToUInt32(this.duration_each);
        this.action.action_goal.goal.scale = this.scale;

        return this.action.action_goal;
    }

    protected override void OnFeedbackReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnResultReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnStatusUpdated()
    {
        //throw new System.NotImplementedException();
    }
}