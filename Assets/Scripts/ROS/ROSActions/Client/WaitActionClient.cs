using RosMessageTypes.Geometry;
using RosMessageTypes.RosTcpEndpoint;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class WaitActionClient : ROSActionClient<WaitAction, WaitActionGoal, WaitActionResult, WaitActionFeedback, WaitGoal, WaitResult, WaitFeedback>
{
    public int duration;

    // Start is called before the first frame update
    void Start()
    {
        this.Initialize(UIGetComponentValues.topicRobotPrefix);
        this.action = new WaitAction();
        this.goalStatus = new RosMessageTypes.Actionlib.GoalStatusMsg();
    }

    public void PublishGoal(int duration)
    {
        this.duration = duration;
        this.SendGoal();
    }

    protected override WaitActionGoal GetActionGoal()
    {
        this.action.action_goal.goal.duration = System.Convert.ToUInt32(this.duration);

        return this.action.action_goal;
    }

    protected override void OnFeedbackReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnResultReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnStatusUpdated()
    {
        //throw new System.NotImplementedException();
    }
}