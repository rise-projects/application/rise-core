using RosMessageTypes.Actionlib;
using RosMessageTypes.BuiltinInterfaces;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using UnityEngine;

/// <summary>
/// Abstract action client for the ROS actionlib action server.
/// </summary>
/// <typeparam name="TAction">Action object</typeparam>
/// <typeparam name="TActionGoal">ActionGoal object</typeparam>
/// <typeparam name="TActionResult">ActionResult object</typeparam>
/// <typeparam name="TActionFeedback">ActionFeedback object.</typeparam>
/// <typeparam name="TGoal">Goal Topic as Message object.</typeparam>
/// <typeparam name="TResult">Result Topic as Message object.</typeparam>
/// <typeparam name="TFeedback">Feedback Topic as Message object.</typeparam>
[DefaultExecutionOrder(1000)]
public abstract class ROSActionClient<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> : MonoBehaviour
        where TAction : Action<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>
        where TActionGoal : ActionGoal<TGoal>
        where TActionResult : ActionResult<TResult>
        where TActionFeedback : ActionFeedback<TFeedback>
        where TGoal : Message
        where TResult : Message
        where TFeedback : Message
{
    /// <summary>Instance of the ROS connection.</summary>
    private ROSConnection rosConnection;
    /// <summary>action name for the Topic.</summary>
    public string actionName;
    /// <summary> Full Topic consisting of the robot prefix and the action name.</summary>
    public string fullTopic;
    /// <summary>current action object</summary>
    public TAction action;
    /// <summary> status of the action</summary>
    public GoalStatusMsg goalStatus;
    /// <summary>Publisher to cancel the action.</summary>
    private RosTopicState cancelPublicationIDPublisher;
    /// <summary>Publisher for the actionGoal</summary>
    private RosTopicState goalPublicationIDPublisher;
    /// <summary>Last used goal id, for referencing cancel actions.</summary>
    private int lastGoalID;

    /// <summary>
    /// Initialization of the ROS instance and registration of the publisher and subscriber
    /// </summary>
    /// <param name="robotPrefix">Robot specific prefix for the ROS Topics.</param>
    public void Initialize(string robotPrefix)
    {
        fullTopic = '/' + robotPrefix + actionName;
        lastGoalID = 0;

        rosConnection = ROSConnection.GetOrCreateInstance();

        cancelPublicationIDPublisher = rosConnection.RegisterPublisher<GoalIDMsg>(fullTopic + "/cancel");
        goalPublicationIDPublisher = rosConnection.RegisterPublisher<TActionGoal>(fullTopic + "/goal");

        //rosConnection.GetTopicList(PrintTopicList);

        rosConnection.Subscribe<GoalStatusArrayMsg>(fullTopic + "/status", StatusCallback);
        rosConnection.Subscribe<TActionFeedback>(fullTopic + "/feedback", FeedbackCallback);
        rosConnection.Subscribe<TActionResult>(fullTopic + "/result", ResultCallback);
    }


    private void OnEnable()
    {
        UIGetComponentValues.robotTopicChanged += ResubscribeTopics;
    }

    private void OnDisable()
    {
        UIGetComponentValues.robotTopicChanged -= ResubscribeTopics;
    }
    /// <summary>
    /// Re-subscription to 
    /// </summary>
    /// <param name="robotPrefix">Robot specific prefix for the ROS Topics.</param>
    public void ResubscribeTopics(string robotPrefix)
    {
        //cancelPublicationIDPublisher.UnsubscribeAll();
        //goalPublicationIDPublisher.UnsubscribeAll();

        if (rosConnection != null)
        {
            rosConnection.Unsubscribe(fullTopic + "/status");
            rosConnection.Unsubscribe(fullTopic + "/feedback");
            rosConnection.Unsubscribe(fullTopic + "/result");

            Initialize(robotPrefix);
        }
    }

    /// <summary>
    /// Publishes the action goal throw the goalPublicationIDPublisher topic.
    /// </summary>
    public void SendGoal()
    {
        long milliSeconds = System.DateTimeOffset.Now.ToUnixTimeMilliseconds() - System.DateTimeOffset.Now.ToUnixTimeSeconds() * 1000;

        System.DateTimeOffset now = System.DateTimeOffset.UtcNow;
        long unixTimeStamp = now.ToUnixTimeSeconds();
        System.Random rnd = new System.Random();
        string uniqueID = "rise_gui_" + unixTimeStamp + "_" + lastGoalID;
        lastGoalID++;

        action.action_goal.goal_id = new GoalIDMsg(new TimeMsg((uint) System.DateTimeOffset.Now.ToUnixTimeSeconds(), (uint) (milliSeconds * 1000000)), uniqueID);

        action.action_goal = GetActionGoal();
        rosConnection.Publish(goalPublicationIDPublisher.Topic, action.action_goal);
    }

    /// <summary>
    /// Cancel this goal by publishing through the cancel topic.
    /// </summary>
    public void CancelGoal()
    {
        rosConnection.Publish(cancelPublicationIDPublisher.Topic, action.action_goal.goal_id);
    }

    // Implement by user to attach GoalID
    protected abstract TActionGoal GetActionGoal();

    // Implement by user to handle status
    protected abstract void OnStatusUpdated();
    private void StatusCallback(GoalStatusArrayMsg actionGoalStatusArray)
    {
        if (actionGoalStatusArray.status_list.Length > 0)
        {
            goalStatus = actionGoalStatusArray.status_list[actionGoalStatusArray.status_list.Length - 1];
        }
        OnStatusUpdated();
    }

    // Implement by user to handle feedback.
    protected abstract void OnFeedbackReceived();
    private void FeedbackCallback(TActionFeedback actionFeedback)
    {
        action.action_feedback = actionFeedback;
        OnFeedbackReceived();
    }

    // Implement by user to handle result.
    protected abstract void OnResultReceived();
    private void ResultCallback(TActionResult actionResult)
    {
        action.action_result = actionResult;
        OnResultReceived();
    }
}