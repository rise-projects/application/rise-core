using RosMessageTypes.RosTcpEndpoint;

public class SocialExpressionActionClient : ROSActionClient<SocialExpressionAction, SocialExpressionActionGoal, SocialExpressionActionResult, SocialExpressionActionFeedback, SocialExpressionGoal, SocialExpressionResult, SocialExpressionFeedback>
{
    /// <summary>Value of the expression</summary>
    public int value;
    /// <summary>duration of the expression</summary>
    public int duration;
    /// <summary>intensity of the expression</summary>
    public int intensity;

    private void Start()
    {
        this.Initialize(UIGetComponentValues.topicRobotPrefix);
        this.action = new SocialExpressionAction();
        this.goalStatus = new RosMessageTypes.Actionlib.GoalStatusMsg();
    }

    /// <summary>
    /// Publish social expression state message as goal for robot.
    /// </summary>
    /// <param name="value">Identify value for predefined social expression value</param>
    /// <param name="duration">Duration for this expression</param>
    /// <param name="intensity">Intensity for expression</param>
    public void PublishGoal(int value, int duration, int intensity)
    {
        this.value = value;
        this.duration = duration;
        this.intensity = intensity;
        this.SendGoal();
    }

    /// <summary>
    /// Getter Function for this action goal.
    /// </summary>
    /// <returns>action_goal</returns>
    protected override SocialExpressionActionGoal GetActionGoal()
    {
        this.action.action_goal.goal.expression_id = System.Convert.ToByte(this.value);
        this.action.action_goal.goal.expression_duration = System.Convert.ToUInt32(this.duration);
        this.action.action_goal.goal.expression_intensity = System.Convert.ToUInt32(this.intensity);

        return this.action.action_goal;
    }



    protected override void OnFeedbackReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnResultReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnStatusUpdated()
    {
        //throw new System.NotImplementedException();
    }
}