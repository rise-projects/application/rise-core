using RosMessageTypes.Geometry;
using RosMessageTypes.Server;
using UnityEngine;

/// <summary>
/// ROS ActionClient for Look at Targets.
/// </summary>
public class LookAtTargetActionClient : ROSActionClient<LookattargetAction, LookattargetActionGoal, LookattargetActionResult, LookattargetActionFeedback, LookattargetGoal, LookattargetResult, LookattargetFeedback>
{
    public PointStampedMsg point;
    public float roll;

    private void Start()
    {
        this.Initialize(UIGetComponentValues.topicRobotPrefix);
        this.action = new LookattargetAction();
        this.goalStatus = new RosMessageTypes.Actionlib.GoalStatusMsg();
    }

    /// <summary>
    /// Publish look at target message as goal for robot.
    /// </summary>
    /// <param name="x">x coordinate to focus target</param>
    /// <param name="y">y coordinate to focus target</param>
    /// <param name="z">z coordinate to focus target</param>
    /// <param name="roll">Velocity for robot movement</param>
    /// <param name="robot">Robot target</param>
    public void PublishGoal(double x, double y, double z, float roll)
    {
        this.point = new PointStampedMsg();
        this.point.point.x = x;
        this.point.point.y = y;
        this.point.point.z = z;
        this.roll = roll;
        this.SendGoal();
    }

    protected override LookattargetActionGoal GetActionGoal()
    {
        this.action.action_goal.goal.point = point;
        this.action.action_goal.goal.roll = roll;

        return this.action.action_goal;
    }

    protected override void OnFeedbackReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnResultReceived()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnStatusUpdated()
    {
        //throw new System.NotImplementedException();
    }
}