using RosMessageTypes.Actionlib;
using RosMessageTypes.RosTcpEndpoint;
using System;
using System.Threading;
using UnityEngine;
using static IRFunctionTypeEnum;
using static ROSCommunicationEnum;
using static TaskManagementEnum;
/// <summary>
/// Implementation of the ROSActionServer to handle incoming Domain Tasks.
/// </summary>
public class DomainTaskActionServer : ROSActionServer<DomainTaskAction, DomainTaskActionGoal, DomainTaskActionResult, DomainTaskActionFeedback, DomainTaskGoal, DomainTaskResult, DomainTaskFeedback>
{
    /// <summary>Instance of itself.</summary>
    public static DomainTaskActionServer Instance { get; set; }
    /// <summary>Reference to the Task Scheduler to append new tasks</summary>
    public TaskScheduler scheduler;
    /// <summary>Notifies a waiting thread about new occurred events.</summary>
    private ManualResetEvent isProcessingGoal = new ManualResetEvent(false);
    /// <summary>Thread to wait for new events when Preempting</summary>
    private Thread goalHandler;
    /// <summary>Reference to the WorkingMemoryController to process incoming assignValue DomainTasks.</summary>
    private WorkingMemoryController memoryController;
    /// <summary>Reference to the behaviorManager to process incoming raiseEventTopic DomainTasks. </summary>
    public BehaviorManager behaviorManager;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            scheduler = gameObject.AddComponent<TaskScheduler>();
        }
    }

    private void Start()
    {
        Initialize();

        memoryController = WorkingMemoryController.Instance;
    }

    /// <summary>
    /// On Goal Received this goal is translated to a DomainTaskActionGoal before its passed on to the GoalCallback.
    /// </summary>
    /// <param name="behavior">Name of behavior. Dependent on the Function Type its the name of an CRA, IRA, an EventTopic or an Json Key-Value pair for the assingValue function. </param>
    /// <param name="functionType">can be: startCommunicationRobotAct, startInteractionRule, raiseEventTopic, assignValue</param>
    /// <param name="callingType">blocking or nonblocking</param>
    public void InternalGoalReceived(string behavior, string functionType, string callingType)
    {
        //Debug.Log("Internal Goal received");
        DomainTaskActionGoal goal = new DomainTaskActionGoal(new RosMessageTypes.Std.HeaderMsg(), new GoalIDMsg(), new DomainTaskGoal(behavior, functionType, callingType));
        GoalCallback(goal);
    }

    protected bool IsGoalValid(DomainTaskAction action)
    {
        // ToDo: Reject goal if BML or InteractionRule is missing, DM doesnt find any rule/dialog for trigger

        return true;
    }

    /// <summary>
    /// If the goal is valid, the status of the goal will be accepted.
    /// </summary>
    /// <param name="action">received DomainTaskAction</param>
    protected override void OnGoalReceived(DomainTaskAction action)
    {
        if (IsGoalValid(action))
        {
            SetAccepted(action.action_goal.goal_id);
        }
        else
        {
            SetRejected(action.action_goal.goal_id);
        }
    }


    protected override void OnGoalRecalling(GoalIDMsg goalID)
    {
        // Left blank for this example
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="action">received DomainTaskAction</param>
    protected override void OnGoalPreempting(DomainTaskAction action)
    {
        isProcessingGoal.Reset();
        goalHandler.Join();

        scheduler.ProcessSignalReceived(action.action_goal.goal_id.id);
    }
    /// <summary>
    /// Debug message, when a goal is rejected. 
    /// </summary>
    /// <param name="action"></param>
    protected override void OnGoalRejected(DomainTaskAction action)
    {
        Debug.Log("Cannot processing taskstate with id: " + action.action_goal.goal_id.id + ". Goal Rejected");
    }

    /// <summary>
    /// When the goal is set to active.
    /// The functionType is checked, if its part of the IRFunctionType.
    /// Dependent on the FunctionType, the DomainTask will be executed immediately (for assignValue and raiseEventTopic) or scheduled (for startInteractionRule and startCommunicationRobotAct)
    /// </summary>
    /// <param name="action">received DomainTaskAction</param>
    /// <exception cref="Exception"The function type is not valid.</exception>
    protected override void OnGoalActive(DomainTaskAction action)
    {
        //Debug.Log("Execution Goal" + action.action_goal.goal.domain_task_behavior);

        IRFunctionType functionType = IRFunctionType.none;
        if (action.action_goal.goal.function_type != "")
        {
            try
            {
                functionType = (IRFunctionType)Enum.Parse(typeof(IRFunctionType), action.action_goal.goal.function_type);
            }
            catch (Exception e)
            {
                print("Function Type not found, overwritten with None with message: " + e.Message);
            }
        }
        else
        {
            throw new Exception("Function Type empty, please use correct type for execution");
        }

        ExecutionType executionType = ExecutionType.Blocking;
        if (action.action_goal.goal.calling_type != "")
        {
            try
            {
                executionType = (ExecutionType)Enum.Parse(typeof(ExecutionType), action.action_goal.goal.calling_type);
            }
            catch (Exception e)
            {
                print("Execution Type not found, overwritten with None with message: " + e.Message);
            }
        }

        if (functionType != IRFunctionType.none)
        {
            if (functionType == IRFunctionType.assignValue)
            {
                memoryController.AddMemoryEntryFromDomainTask(action.action_goal.goal.domain_task_behavior);
                this.SetSucceeded(this.GetGoalIDMsgFromServerByID(action.action_goal.goal_id.id));
            }
            else if (functionType == IRFunctionType.raiseEventTopic)
            {
                behaviorManager.RaiseInternalEvent(action.action_goal.goal.domain_task_behavior);
                this.SetSucceeded(this.GetGoalIDMsgFromServerByID(action.action_goal.goal_id.id));
            }
            else if (functionType == IRFunctionType.stopCommunicationRobotAct)
            {
                // Stop CRA! Kill all coroutines
                behaviorManager.scheduler.InterruptDomainTask(action.action_goal.goal.domain_task_behavior);
                this.SetSucceeded(this.GetGoalIDMsgFromServerByID(action.action_goal.goal_id.id));
            }
            else if (functionType == IRFunctionType.stopInteractionRule)
            {
                // Stop IR!
                behaviorManager.interactionRuleController.StopInteractionRule(action.action_goal.goal.domain_task_behavior);
                this.SetSucceeded(this.GetGoalIDMsgFromServerByID(action.action_goal.goal_id.id));
            }
            else
            {
                DomainInputUnit domainInputUnit = new DomainInputUnit(action.action_goal.goal_id.id, action.action_goal.goal.domain_task_behavior, functionType, executionType);
                scheduler.AppendDomainTask(domainInputUnit);
            }
        }
    }
    /// <summary>
    /// When the Goal is completed.
    /// </summary>
    /// <param name="goalHandler">DomainTaskFeedback of the goal</param>
    protected override void OnGoalSucceeded(AbstractActionGoalHandler<DomainTaskAction, DomainTaskActionGoal, DomainTaskActionResult, DomainTaskActionFeedback, DomainTaskGoal, DomainTaskResult, DomainTaskFeedback> goalHandler)
    {
        isProcessingGoal.Reset();
        //UpdateAndPublishStatus(goalHandler, ActionStatus.SUCCEEDED);
    }

    protected override void OnGoalAborted(DomainTaskAction action)
    {
        // Left blank for this example
    }
    /// <summary>
    /// Publication of the canceled actiongoal.
    /// </summary>
    /// <param name="action"></param>
    protected override void OnGoalCanceled(DomainTaskAction action)
    {
        PublishResult(action.action_goal.goal_id);
    }

    protected override AbstractActionGoalHandler<DomainTaskAction, DomainTaskActionGoal, DomainTaskActionResult, DomainTaskActionFeedback, DomainTaskGoal, DomainTaskResult, DomainTaskFeedback> CreateActionHandler()
    {
        DomainTaskActionGoalHandler goalHandler = new DomainTaskActionGoalHandler(new DomainTaskAction());

        return goalHandler;
    }

    /// <summary>
    /// Test, if the action goal exists.
    /// </summary>
    /// <param name="action">DomainTaskAction to test</param>
    /// <returns></returns>
    public bool HasDomainTaskInList(DomainTaskAction action)
    {
        foreach (DomainTaskActionGoalHandler handler in goalHandlers)
        {
            if (handler.action.action_goal.goal_id.id == action.action_goal.goal_id.id)
            {
                return true;
            }
        }

        return false;
    }
}