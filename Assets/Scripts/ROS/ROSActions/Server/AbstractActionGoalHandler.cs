using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using static ROSCommunicationEnum;

/// <summary>
/// Abstract ActionGoalHanlder for the ROSActionServer
/// </summary>
/// <typeparam name="TAction"></typeparam>
/// <typeparam name="TActionGoal"></typeparam>
/// <typeparam name="TActionResult"></typeparam>
/// <typeparam name="TActionFeedback"></typeparam>
/// <typeparam name="TGoal"></typeparam>
/// <typeparam name="TResult"></typeparam>
/// <typeparam name="TFeedback"></typeparam>
public abstract class AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>
        where TAction : Action<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>
        where TActionGoal : ActionGoal<TGoal>
        where TActionResult : ActionResult<TResult>
        where TActionFeedback : ActionFeedback<TFeedback>
        where TGoal : Message
        where TResult : Message
        where TFeedback : Message
{
    /// <summary>action of the GoalHanlder</summary>
    public TAction action { get; set; }
    /// <summary>current status</summary>
    public ActionStatus actionStatus { get; set; }
    /// <summary>action status text for publishing updates.</summary>
    public string actionStatusText { get; set; }

    public AbstractActionGoalHandler(TAction action, ActionStatus actionStatus = ActionStatus.NO_GOAL, string actionStatusText = "")
    {
        this.action = action;
        this.actionStatus = actionStatus;
        this.actionStatusText = actionStatusText;
    }
}