using RosMessageTypes.Actionlib;
using RosMessageTypes.BuiltinInterfaces;
using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using UnityEngine;
using static ROSCommunicationEnum;

/// <summary>
/// Abstract action server for the ROS actionlib
/// </summary>
/// <typeparam name="TAction">Action object</typeparam>
/// <typeparam name="TActionGoal">ActionGoal object</typeparam>
/// <typeparam name="TActionResult">ActionResult object</typeparam>
/// <typeparam name="TActionFeedback">ActionFeedback object.</typeparam>
/// <typeparam name="TGoal">Goal Topic as Message object.</typeparam>
/// <typeparam name="TResult">Result Topic as Message object.</typeparam>
/// <typeparam name="TFeedback">Feedback Topic as Message object.</typeparam>
/// 
public abstract class ROSActionServer<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> : MonoBehaviour
        where TAction : Action<TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>
        where TActionGoal : ActionGoal<TGoal>
        where TActionResult : ActionResult<TResult>
        where TActionFeedback : ActionFeedback<TFeedback>
        where TGoal : Message
        where TResult : Message
        where TFeedback : Message
{
    /// <summary>action name for the Topic.</summary>
    public string actionName;
    public float timeStep;
    /// <summary>Instance of the ROS connection.</summary>
    private ROSConnection rosConnection;

    /// <summary>Publisher for the status update.</summary>
    private RosTopicState statusPublicationID;
    /// <summary>Publisher for the feedback.</summary>
    private RosTopicState feedbackPublicationID;
    /// <summary>Publisher for the status result.</summary>
    private RosTopicState resultPublicationID;

    /// <summary>abstract ActionGoalHandler List to manage all actionGoals.</summary>
    public List<AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>> goalHandlers;

    public static event System.Action<AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>> actionGoalChanged;


    public void Initialize()
    {
        goalHandlers = new List<AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback>>();
        rosConnection = ROSConnection.GetOrCreateInstance();

        statusPublicationID = rosConnection.RegisterPublisher<GoalStatusArrayMsg>(actionName + "/status");
        feedbackPublicationID = rosConnection.RegisterPublisher<TActionFeedback>(actionName + "/feedback");
        resultPublicationID = rosConnection.RegisterPublisher<TActionResult>(actionName + "/result");

        rosConnection.Subscribe<GoalIDMsg>(actionName + "/cancel", CancelCallback);
        rosConnection.Subscribe<TActionGoal>(actionName + "/goal", GoalCallback);
        Debug.Log("Finish starting ROS-ActionServer");

    }
    /// <summary>
    /// Getter funktion for the action status by ID.
    /// </summary>
    /// <param name="goalID"></param>
    /// <returns>action Status of the ActionGoal</returns>
    public ActionStatus GetStatus(GoalIDMsg goalID)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        return goalHandler.actionStatus;
    }

    /// <summary>
    /// Client Triggered Actions when receive a new goal
    /// </summary>
    /// <param name="action">received action</param>
    protected abstract void OnGoalReceived(TAction action);

    /// <summary>
    /// creates a goalHanlder from the actionGoal with the status pending.
    /// Test if the ID, if the id is specified already exists or numeric is.
    /// Passes the goalHanlder to the OnGoalRecieved function.
    /// </summary>
    /// <param name="actionGoal"></param>
    public void GoalCallback(TActionGoal actionGoal)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = CreateActionHandler();
        goalHandler.action.action_goal = actionGoal;
        goalHandler.actionStatus = ActionStatus.PENDING;
        goalHandler.actionStatusText = "";

        string errorMsg;
        if (goalHandler.action.action_goal.goal_id.id != "")
        {
            if (!CheckIfGoalIDExists(goalHandler.action.action_goal.goal_id))
            {
                goalHandlers.Add(goalHandler);
                OnGoalReceived(goalHandler.action);
                return;
            }
            else
            {
                errorMsg = "GoalID already exists.";
            }
        }
        else
        {
            goalHandler.action.action_goal.goal_id.id = GetUniqueGoalID();
            goalHandlers.Add(goalHandler);

            UpdateAndPublishStatus(goalHandler, ActionStatus.PENDING, "Pending");

            OnGoalReceived(goalHandler.action);
            return;
        }

        UpdateAndPublishStatus(goalHandler, ActionStatus.REJECTED, "Invalid GoalID: " + errorMsg);
    }

    /// <summary>
    /// Checks for the highest number assigned and returns the next number.
    /// </summary>
    /// <returns>not jet assigned goal ID</returns>
    public string GetUniqueGoalID()
    {
        GoalIDMsg goalID = new GoalIDMsg();
        
        do {
            System.DateTimeOffset now = System.DateTimeOffset.UtcNow;
            long unixTimeStamp = now.ToUnixTimeSeconds();
            System.Random rnd = new System.Random();
            string uniqueID = "rise_server_" + unixTimeStamp + "_" + rnd.Next(0, 1000);

            goalID.id = uniqueID;
        } while(CheckIfGoalIDExists(goalID));

        return goalID.id;
    }

    /// <summary>
    /// Checks if a goal ID is already assigned.
    /// </summary>
    /// <param name="goalIDMsg">GoalID to check</param>
    /// <returns>true, if the ID is already assigned</returns>
    private bool CheckIfGoalIDExists(GoalIDMsg goalIDMsg)
    {
        foreach (AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler in goalHandlers)
        {
            if (goalHandler.action.action_goal.goal_id.id == goalIDMsg.id)
            {
                return true;
            }
        }

        return false;
    }

    protected abstract AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> CreateActionHandler();

    // When the goal is cancelled by the client
    protected abstract void OnGoalRecalling(GoalIDMsg goalID);
    protected abstract void OnGoalPreempting(TAction action);

    /// <summary>
    /// Cancelation of the given goal. Dependent on the previous status, the final status is selected. Pending->Recalling, Active->Preempting.
    /// </summary>
    /// <param name="goalID">Goal to cancel.</param>
    private void CancelCallback(GoalIDMsg goalID)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        switch (goalHandler.actionStatus)
        {
            case ActionStatus.PENDING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.RECALLING, "Recalling");
                OnGoalRecalling(goalID);
                break;
            case ActionStatus.ACTIVE:
                UpdateAndPublishStatus(goalHandler, ActionStatus.PREEMPTING, "Preempting");
                OnGoalPreempting(goalHandler.action);
                break;
            default:
                Debug.Log("Goal cannot be canceled under current state: " + goalHandler.actionStatus + ". Ignored");
                break;
        }
    }

    // Server Triggered Actions
    protected abstract void OnGoalActive(TAction action);

    /// <summary>
    /// Set the goal active and sent the given Text as update.
    /// </summary>
    /// <param name="goalID">Goal to set active</param>
    /// <param name="text">Update Text</param>
    public void SetAccepted(GoalIDMsg goalID)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        switch (goalHandler.actionStatus)
        {
            case ActionStatus.PENDING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.ACTIVE, "Active");
                OnGoalActive(goalHandler.action);
                break;
            case ActionStatus.RECALLING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.PREEMPTING, "Preempting");
                OnGoalPreempting(goalHandler.action);
                break;
            default:
                Debug.Log("Goal cannot succeed under current state: " + goalHandler.actionStatus + ". Ignored");
                break;
        }
    }
    /// <summary>
    /// Change of the status to Rejected after the goal was rejected.
    /// </summary>
    /// <param name="action">rejected action</param>
    protected virtual void OnGoalRejected(TAction action) { }
    public void SetRejected(GoalIDMsg goalID)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        switch (goalHandler.actionStatus)
        {
            case ActionStatus.PENDING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.REJECTED, "Rejected");
                OnGoalRejected(goalHandler.action);
                break;
            case ActionStatus.RECALLING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.REJECTED, "Rejected");
                OnGoalRejected(goalHandler.action);
                break;
            default:
                Debug.Log("Goal cannot be rejected under current state: " + goalHandler.actionStatus + ". Ignored");
                break;
        }
    }

    protected virtual void OnGoalSucceeded(AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler) { }

    /// <summary>
    /// Change of the status to Succeeded after the goal is succeeded.
    /// </summary>
    /// <param name="goalID">succeeded goal</param>
    /// <param name="result">Result Object</param>
    /// <param name="text">Text for the update publication</param>
    public void SetSucceeded(GoalIDMsg goalID, TResult result = null)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        switch (goalHandler.actionStatus)
        {
            case ActionStatus.ACTIVE:
                UpdateAndPublishStatus(goalHandler, ActionStatus.SUCCEEDED, "Succeeded");
                if (result != null)
                {
                    goalHandler.action.action_result.result = result;
                }
                PublishResult(goalID);
                OnGoalSucceeded(goalHandler);
                break;
            case ActionStatus.PREEMPTING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.SUCCEEDED, "Succeeded");
                if (result != null)
                {
                    goalHandler.action.action_result.result = result;
                }
                PublishResult(goalID);
                OnGoalSucceeded(goalHandler);
                break;
            default:
                Debug.Log("Goal cannot succeed under current state: " + goalHandler.actionStatus + ". Ignored");
                break;
        }
    }

    protected virtual void OnGoalAborted(TAction action) { }
    /// <summary>
    /// Change of the status to Aborted after the goal was aborted.
    /// </summary>
    /// <param name="goalID">aborted goal</param>
    /// <param name="text">Text for the update publication</param>
    public void SetAborted(GoalIDMsg goalID)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        switch (goalHandler.actionStatus)
        {
            case ActionStatus.ACTIVE:
                UpdateAndPublishStatus(goalHandler, ActionStatus.ABORTED, "Aborted");
                OnGoalAborted(goalHandler.action);
                break;
            case ActionStatus.PREEMPTING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.ABORTED, "Aborted");
                OnGoalAborted(goalHandler.action);
                break;
            default:
                Debug.Log("Goal cannot be rejected under current state: " + goalHandler.actionStatus + ". Ignored");
                break;
        }
    }

    protected virtual void OnGoalCanceled(TAction action) { }
    /// <summary>
    /// Change of the status to Canceled after the goal was canceled.
    /// </summary>
    /// <param name="goalID">canceled goal</param>
    /// <param name="result">final result</param>
    /// <param name="text">Text for the update publication</param>
    public void SetCanceled(GoalIDMsg goalID, TResult result = null)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        switch (goalHandler.actionStatus)
        {
            case ActionStatus.RECALLING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.RECALLED, "Recalled");
                break;
            case ActionStatus.PREEMPTING:
                UpdateAndPublishStatus(goalHandler, ActionStatus.PREEMPTED, "Preempted");
                break;
            default:
                Debug.Log("Goal cannot be canceled under current state: " + goalHandler.actionStatus + ". Ignored");
                return;
        }
        if (result != null)
        {
            goalHandler.action.action_result.result = result;
        }
        OnGoalCanceled(goalHandler.action);
    }
    /// <summary>
    /// Searches for the goalHanlder by ID.
    /// </summary>
    /// <param name="goalID">ID to search</param>
    /// <returns><goalHandler, if exists/returns>
    /// <exception cref="System.InvalidOperationException">throws a exception, if the ID doesn't exists.</exception>
    private AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> GetHandlerFromServerByID(GoalIDMsg goalID)
    {
        foreach (AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler in goalHandlers)
        {
            if (goalHandler.action.action_goal.goal_id.id == goalID.id)
            {
                return goalHandler;
            }
        }

        throw new System.InvalidOperationException("Handler with ID " + goalID.id + " not found");
    }
    /// <summary>
    /// Searches for a GoalHandler by ID to return the action_goal goal_ID
    /// </summary>
    /// <param name="goalID"></param>
    /// <returns>action_goal goal_id of the goalHanlder with the goalID</returns>
    /// <exception cref="System.InvalidOperationException">throws a exception, if the ID doesn't exists.</exception>
    public GoalIDMsg GetGoalIDMsgFromServerByID(string goalID)
    {
        foreach (AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler in goalHandlers)
        {
            if (goalHandler.action.action_goal.goal_id.id == goalID)
            {
                return goalHandler.action.action_goal.goal_id;
            }
        }

        throw new System.InvalidOperationException("Handler with ID " + goalID + " not found");
    }

    /// <summary>
    /// Updates the Status and Status Text before Publishing the new status.
    /// </summary>
    /// <param name="goalHandler">goalHandler to update</param>
    /// <param name="actionStatus">new action status</param>
    /// <param name="text">text for the actionStatusText</param>
    protected void UpdateAndPublishStatus(AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler, ActionStatus actionStatus, string text = "")
    {
        goalHandler.actionStatus = actionStatus;
        goalHandler.actionStatusText = text;

        actionGoalChanged?.Invoke(goalHandler);

        PublishStatus(goalHandler);
    }

    /// <summary>
    /// Publishing of the current status of a goalHanlder. Used after the status was changed.
    /// </summary>
    /// <param name="goalHandler">goalHanlder to publish the status</param>
    public void PublishStatus(AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler)
    {
        if (goalHandler.actionStatus == ActionStatus.NO_GOAL)
        {
            rosConnection.Publish(statusPublicationID.Topic, new GoalStatusArrayMsg());
        }
        else
        {
            long milliSeconds = System.DateTimeOffset.Now.ToUnixTimeMilliseconds() - System.DateTimeOffset.Now.ToUnixTimeSeconds() * 1000;
            rosConnection.Publish(statusPublicationID.Topic,
                new GoalStatusArrayMsg
                {
                    status_list = new GoalStatusMsg[]
                    {
                        new GoalStatusMsg {
                            goal_id = new GoalIDMsg(new TimeMsg((uint) System.DateTimeOffset.Now.ToUnixTimeSeconds(), (uint) (milliSeconds * 1000000)), goalHandler.action.action_goal.goal_id.id),
                            status = (byte)goalHandler.actionStatus,
                            text = goalHandler.actionStatusText
                        }
                    }
                }
            );
        }
    }
    /// <summary>
    /// Publishing feedback of a given goalID
    /// </summary>
    /// <param name="goalID">goalID of the goalHanlder</param>
    protected void PublishFeedback(GoalIDMsg goalID)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        goalHandler.action.action_feedback.status.status = (byte)goalHandler.actionStatus;
        goalHandler.action.action_feedback.status.goal_id = goalHandler.action.action_goal.goal_id;
        rosConnection.Publish(feedbackPublicationID.Topic, goalHandler.action.action_feedback);
    }

    /// <summary>
    /// Publishing the result of the given goalID
    /// </summary>
    /// <param name="goalID">goalID of the goalHanlder</param>
    protected void PublishResult(GoalIDMsg goalID)
    {
        AbstractActionGoalHandler<TAction, TActionGoal, TActionResult, TActionFeedback, TGoal, TResult, TFeedback> goalHandler = GetHandlerFromServerByID(goalID);

        long milliSeconds = System.DateTimeOffset.Now.ToUnixTimeMilliseconds() - System.DateTimeOffset.Now.ToUnixTimeSeconds() * 1000;

        GoalIDMsg result_goalID = new GoalIDMsg(new TimeMsg((uint) System.DateTimeOffset.Now.ToUnixTimeSeconds(), (uint) (milliSeconds * 1000000)), goalHandler.action.action_goal.goal_id.id);
                
        goalHandler.action.action_result.status.goal_id = result_goalID;
        goalHandler.action.action_result.status.status = (byte)goalHandler.actionStatus;
        goalHandler.action.action_result.status.text = goalHandler.actionStatus.ToString();

        rosConnection.Publish(resultPublicationID.Topic, goalHandler.action.action_result);
    }
}