using RosMessageTypes.RosTcpEndpoint;
using static ROSCommunicationEnum;
/// <summary>
/// Implementation of the ActionGoalHanlder as DomanTaskActionGoalHanlder.
/// </summary>
public class DomainTaskActionGoalHandler : AbstractActionGoalHandler<DomainTaskAction, DomainTaskActionGoal, DomainTaskActionResult, DomainTaskActionFeedback, DomainTaskGoal, DomainTaskResult, DomainTaskFeedback>
{
    public DomainTaskActionGoalHandler(DomainTaskAction action, ActionStatus actionStatus = ActionStatus.NO_GOAL, string actionStatusText = "") : base(action, actionStatus, actionStatusText)
    {

    }
}