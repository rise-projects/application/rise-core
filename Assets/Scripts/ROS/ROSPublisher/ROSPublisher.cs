using Unity.Robotics.ROSTCPConnector;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using UnityEngine;

/// <summary>
/// Abstract class for ROS Publisher
/// Publishers send messages via ros on specific topics to robots
/// </summary>
public abstract class ROSPublisher<T> : MonoBehaviour where T: Message
{
    [Header("Specify ROS Topic Name")]
    public string topic;

    private ROSConnection rosConnector;

    protected virtual void Start()
    {
        this.rosConnector = ROSConnection.GetOrCreateInstance();
        this.rosConnector.RegisterPublisher<T>(topic);
    }

    /// <summary>
    /// Sending message via ros by topic
    /// </summary>
    /// <param name="message">Message to publish</param>
    protected void Publish(T message)
    {
        this.rosConnector.Publish(topic, message);
    }
}