using RosMessageTypes.Std;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ROS Publisher to publish changes of the State in an InteractionRule.
/// </summary>
public class IRStatePublisher : ROSPublisher<StringMsg>
{
    private void OnEnable()
    {
        InteractionRuleController.newInteractionRuleState += PublishEventTopic;
    }

    private void OnDisable()
    {
        InteractionRuleController.newInteractionRuleState -= PublishEventTopic;
    }

    /// <summary>
    /// Publishes the eventTopic
    /// </summary>
    /// <param name="eventTopic"></param>
    private void PublishEventTopic(string eventTopic)
    {
        Publish(new StringMsg(eventTopic));
    }
}