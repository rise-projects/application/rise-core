using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static RobotEnum;

/// <summary>
/// UI Handler for get and setting ui values.
/// Handle this references as view to ui elements in this global context as "database like structure".
/// </summary>
public class UIGetComponentValues : MonoBehaviour
{
    [Header("Get Values")]
    /// <summary>UI field for switching the execution modes.</summary>
    public Dropdown debugMode;

    /// <summary>UI field for changing the robot target.</summary>
    public Dropdown robotMode;

    /// <summary>Default robot prefix for sending ROS commands on topic.</summary>
    public static string topicRobotPrefix = "naoqi";

    /// <summary>Triggers changed robot target settings.</summary>
    public static event Action<string> robotTopicChanged;

    void Start()
    {
        List<string> m_DropOptions = new List<string>();

        foreach (string robot in Enum.GetNames(typeof(RobotEnum.Robot)))
        {
            m_DropOptions.Add(robot);
        }
        robotMode.AddOptions(m_DropOptions);

        robotMode.onValueChanged.AddListener(delegate
        {
            topicRobotPrefix = RobotEnum.GetTopicForRobot(RobotEnum.GetRobotEnumByString(robotMode.options[robotMode.value].text.ToLower()));
            robotTopicChanged?.Invoke(topicRobotPrefix);
        });
    }

    /// <summary>
    /// Check if debug mode is turned on.
    /// </summary>
    /// <returns>Activation state of debug mode.</returns>
    public bool IsDebugMode()
    {
        if (Enum.Parse(typeof(ExecutionModeEnum.ExecutionMode), debugMode.options[debugMode.value].text.ToLower()).Equals(ExecutionModeEnum.ExecutionMode.debug))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Check if headless mode is turned on.
    /// </summary>
    /// <returns>Activation state of headless mode.</returns>
    public bool IsHeadlessMode()
    {
        if (Enum.Parse(typeof(ExecutionModeEnum.ExecutionMode), debugMode.options[debugMode.value].text.ToLower()).Equals(ExecutionModeEnum.ExecutionMode.headless))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Returns the selected robot from the UI robot selection field.
    /// </summary>
    /// <returns>Selected robot in enum.</returns>
    public RobotEnum.Robot GetSelectedRobot()
    {
        return RobotEnum.GetRobotEnumByString(robotMode.options[robotMode.value].text.ToLower());
    }

    /// <summary>
    /// Set the selected execution mode in the selection field.
    /// </summary>
    /// <param name="mode">Execution mode.</param>
    public void SetupExecutionMode(string mode)
    {
        debugMode.value = debugMode.options.FindIndex(option => option.text.ToLower() == mode.ToLower());

        Debug.Log("Execution mode: " + mode);
    }

    /// <summary>
    /// Set robots topics prefix for ROS communication.
    /// </summary>
    /// <param name="topicPrefix">Prefix related to the selected robot.</param>
    public void SetupTopicPrefix(string topicPrefix)
    {
        topicRobotPrefix = RobotEnum.GetTopicForRobot(RobotEnum.GetRobotEnumByString(topicPrefix.ToLower()));
        robotMode.value = robotMode.options.FindIndex(option => option.text.ToLower() == topicPrefix.ToLower());

        Debug.Log("Using topic prefix: " + topicPrefix);
    }
}