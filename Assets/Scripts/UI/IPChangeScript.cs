using System;
using System.Linq;
using Unity.Robotics.ROSTCPConnector;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handle value changes in the settings of ip variables of the UI.
/// </summary>
public class IPChangeScript : MonoBehaviour
{
    /// <summary>Connection prefab to ROS.</summary>
    private ROSConnection rosConnection;

    /// <summary>UI field for ROS-IP.</summary>
    public InputField rosIPInputField;

    /// <summary>UI button for changing the ip address via UI.</summary>
    public Button applyIPButton;

    /// <summary>Trigger for ip setting status field.</summary>
    public static event Action<bool, string> ipWarningSetting;

    void Start()
    {
        rosConnection = ROSConnection.GetOrCreateInstance();
        
        // Repeat function with a short delay and every n seconds!
        InvokeRepeating("CheckROSConnection", 0.0f, 1f);
    }

    /// <summary>
    /// Check if connection to ROS master is established.
    /// </summary>
    private void CheckROSConnection()
    {
        if (!rosConnection.HasConnectionError && rosConnection.HasConnectionThread)
        {
            applyIPButton.GetComponent<Image>().color = Color.green;
            ipWarningSetting?.Invoke(true, "");
        }
        else
        {
            applyIPButton.GetComponent<Image>().color = Color.red;
            ipWarningSetting?.Invoke(false, "Connection refused");
        }
    }

    /// <summary>
    /// Set and change ip address via UI.
    /// </summary>
    public void ApplyIPButton()
    {
        string ipAddress = this.rosIPInputField.text;

        if (IsValidIP(ipAddress))
        {
            ROSConnection.GetOrCreateInstance().Disconnect();
            ROSConnection.GetOrCreateInstance().RosIPAddress = ipAddress;
            ROSConnection.GetOrCreateInstance().Connect();
            ipWarningSetting?.Invoke(true, "");
        }
        else
        {
            this.rosIPInputField.text = "Invalid IP";

            Debug.Log("Invalid IP found: " + ipAddress);
            ipWarningSetting?.Invoke(false, "Invalid IP");

        }

        Debug.Log("ROS IP address changed to: " + ROSConnection.GetOrCreateInstance().RosIPAddress);
    }

    /// <summary>
    /// Set ip address via command line commands and apply settings.
    /// </summary>
    /// <param name="ip">IP address to ROS master.</param>
    public void ApplyIPFromCommandLine(string ip)
    {
        this.rosIPInputField.text = ip;
        this.ApplyIPButton();
    }

    /// <summary>
    /// Check if ip address is valid.
    /// </summary>
    /// <param name="ipString">IP address to ROS master.</param>
    /// <returns>Review of the correctness of the ip address.</returns>
    private bool IsValidIP(string ipString)
    {
        if (string.IsNullOrWhiteSpace(ipString))
        {
            return false;
        }

        string[] splitValues = ipString.Split('.');
        if (splitValues.Length != 4)
        {
            return false;
        }

        return splitValues.All(r => byte.TryParse(r, out byte tempForParsing));
    }
}