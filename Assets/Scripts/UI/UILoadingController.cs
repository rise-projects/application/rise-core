using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controller for initializing UI elements.
/// </summary>
public class UILoadingController : MonoBehaviour
{
    [Header("Initialize Loading/Storing Elements")]
    /// <summary>XML parser for CRAs.</summary>
    public XMLCommunicationRobotActParser xmlCommunicationRobotActParser;

    /// <summary>XML parser for IRs.</summary>
    public XMLInteractionRuleParser xmlInteractionRuleParser;

    /// <summary>XML parser for WM.</summary>
    public JSONWorkingMemoryParser xmlWorkingMemoryParser;

    /// <summary>Collection of CRAs and IRs for reference.</summary>
    public CommunicationActContainer communicationActContainer;

    /// <summary>File manager for loading configurations.</summary>
    public FileManager manager;

    /// <summary>Handling of CRAs.</summary>
    public CommunicationRobotActController communicationRobotActController;

    /// <summary>Handling of IRs.</summary>
    public InteractionRuleController interactionRuleController;

    /// <summary>Handling of the WM.</summary>
    public WorkingMemoryController workingMemoryController;

    [Header("UI Elements")]
    /// <summary>UI field for loading CRA configurations.</summary>
    public Button xmlReloadConfigurationRootButton;

    /// <summary>Trigger clean-up and reloading of the CRA state-machine.</summary>
    public static event Action resetStateMachineCRA;

    /// <summary>Trigger status changes in the settings.</summary>
    public static event Action<bool, string> updateSettingStatus;


    void Start()
    {
        this.xmlReloadConfigurationRootButton.onClick.AddListener(this.manager.ChangeRootPath);

        this.manager.configProjectDropdown.onValueChanged.AddListener(delegate
        {
            LoadCommunicationRobotActXMLFile();
            LoadInteractionRuleXMLFile();
            LoadWorkingMemoryXMLFile();
        });

        this.workingMemoryController = WorkingMemoryController.Instance;
    }

    public void OnEnable()
    {
        FileManager.loadOnHeadlessMode += LoadCommunicationRobotActXMLFile;
        FileManager.loadOnHeadlessMode += LoadInteractionRuleXMLFile;
        FileManager.loadOnHeadlessMode += LoadWorkingMemoryXMLFile;
        PresenterCommunicationRobotActListView.newStartedCommunicationRobotAct += StartCommunicationRobotAct;
        PresenterInteractionRuleListView.newStartedInteractionRule += StartInteractionRule;
    }

    public void OnDisable()
    {
        FileManager.loadOnHeadlessMode -= LoadCommunicationRobotActXMLFile;
        FileManager.loadOnHeadlessMode -= LoadInteractionRuleXMLFile;
        FileManager.loadOnHeadlessMode -= LoadWorkingMemoryXMLFile;
        PresenterCommunicationRobotActListView.newStartedCommunicationRobotAct -= StartCommunicationRobotAct;
        PresenterInteractionRuleListView.newStartedInteractionRule -= StartInteractionRule;
    }

    /// <summary>
    /// Starts a CRA.
    /// </summary>
    /// <param name="robotAct">Name of the CRA to start.</param>
    public void StartCommunicationRobotAct(CommunicationRobotAct robotAct)
    {
        if (robotAct != null)
        {
            if (!robotAct.communicationRobotActIsRunning)
            {
                this.interactionRuleController.taskScheduler.AppendInteractionRuleFunction(robotAct.communicationRobotActName, IRFunctionTypeEnum.IRFunctionType.startCommunicationRobotAct, TaskManagementEnum.ExecutionType.Blocking);
            }
            else
            {
                this.interactionRuleController.taskScheduler.InterruptDomainTask(robotAct.communicationRobotActName);

                resetStateMachineCRA?.Invoke();
            }
        }
    }

    /// <summary>
    /// Starts an IR.
    /// </summary>
    /// <param name="interactionRule">Name of the IR to start.</param>
    public void StartInteractionRule(InteractionRule interactionRule)
    {
        if (interactionRule != null)
        {
            if (!interactionRule.interactionRuleIsRunning)
            {
                this.interactionRuleController.ExecuteInteractionRule(interactionRule.name);
            }
            else
            {
                interactionRuleController.StopInteractionRule(interactionRule);
            }
        }
    }

    /// <summary>
    /// Loading WorkingMemory configuration file by UI field of the WM-path.
    /// </summary>
    public void LoadWorkingMemoryXMLFile()
    {
        DirectoryInfo d = new DirectoryInfo(this.manager.configurationRootDirPath);

        if (d.Exists)
        {
            this.workingMemoryController.ClearWorkingMemory();

            foreach (var file in this.manager.GetJSONWMFilesFromSelection())
            {
                this.workingMemoryController.ImportDataModel(xmlWorkingMemoryParser.ParseDataModels(file, this.manager.xsdValidationDirPath));
            }

            updateSettingStatus?.Invoke(true, "");
        }
        else
        {
            Debug.Log("Filepath does not exists: " + d.FullName);
            updateSettingStatus?.Invoke(false, "DataModel");
        }
    }

    /// <summary>
    /// Loading CommunicationRobotAct configuration file by UI field of the CRA-path.
    /// </summary>
    public void LoadCommunicationRobotActXMLFile()
    {
        DirectoryInfo d = new DirectoryInfo(this.manager.configurationRootDirPath);

        if (d.Exists)
        {
            this.communicationActContainer.ClearCommunicationRobotActList();

            foreach (string file in this.manager.GetXMLFilesFromSelectionByRootTag(this.manager.craRootTag))
            {
                this.communicationActContainer.AddRangeCommunicationRobotActsToList(xmlCommunicationRobotActParser.ParseTrainingsActsFromFile(file, this.manager.xsdValidationDirPath));
            }
            this.communicationActContainer.UniqueCommunicationRobotActList();

            updateSettingStatus?.Invoke(true, "");
        }
        else
        {
            Debug.Log("Filepath does not exists: " + d.FullName);
            updateSettingStatus?.Invoke(false, "CommunicationRobotActs");
        }
        Debug.Log("Loading CommunicationRobotAct files in directory: " + this.manager.configurationRootDirPath);
        Debug.Log("Parsed number of CommunicationRobotActs: " + communicationActContainer.communicationRobotActList.Count);
    }

    /// <summary>
    /// Loading InteractionRule configuration file by UI field of the IR-path.
    /// </summary>
    public void LoadInteractionRuleXMLFile()
    {
        DirectoryInfo d = new DirectoryInfo(this.manager.configurationRootDirPath);

        if (d.Exists)
        {
            this.communicationActContainer.ClearInteractionRuleList();

            foreach (var file in this.manager.GetXMLFilesFromSelectionByRootTag(this.manager.irRootTag))
            {
                this.communicationActContainer.AddRangeInteractionRuleToList(xmlInteractionRuleParser.ParseInteractionRulesFromFile(file, this.manager.xsdValidationDirPath));
            }
            this.communicationActContainer.UniqueInteractionRuleList();

            updateSettingStatus?.Invoke(true, "");
        }
        else
        {
            Debug.Log("Filepath does not exists: " + d.FullName);
            updateSettingStatus?.Invoke(false, "InteractionRules");
        }

        Debug.Log("Loading InteractionRule files in directory: " + this.manager.configurationRootDirPath);
        Debug.Log("Parsed number of InteractionRules: " + communicationActContainer.GetInteractionRuleList().Count);
    }
}