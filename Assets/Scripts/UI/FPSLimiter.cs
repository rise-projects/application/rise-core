using UnityEngine;

/// <summary>
/// Tool for limiting the frames per second of the unity application.
/// </summary>
public class FPSLimiter : MonoBehaviour
{
    /// <summary>Max frame rate per second.</summary>
    public int maxFrameRate = 60;

    void Start()
    {
        // Make the game run as fast as possible
        Application.targetFrameRate = -1;
        // Limit the framerate to 60
        Application.targetFrameRate = maxFrameRate;
    }
}