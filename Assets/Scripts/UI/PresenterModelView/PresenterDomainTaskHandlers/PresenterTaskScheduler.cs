using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (Task Scheduler) ans corresponding UI view.
/// </summary>
public class PresenterTaskScheduler : MonoBehaviour
{
    /// <summary>Handling incoming domain tasks and schedules them.</summary>
    private TaskScheduler taskScheduler;

    /// <summary>UI view container for all domain tasks on the scheduler.</summary>
    public GameObject taskSchedulerContainerView;

    /// <summary>UI view for one entry of a domain tasks on the scheduler.</summary>
    public GameObject domainTaskEntryViewPrefab;

    void Start()
    {
        taskScheduler = DomainTaskActionServer.Instance.scheduler;
    }

    void OnEnable()
    {
        UITabButton.taskSchedulerTabSelected += InitializeView;
        TaskScheduler.domainTaskListChanged += UpdateDomainTaskView;
    }

    void OnDisable()
    {
        UITabButton.taskSchedulerTabSelected -= InitializeView;
        TaskScheduler.domainTaskListChanged -= UpdateDomainTaskView;
    }

    /// <summary>
    /// Initialize the UI view elements by the scheduler informations.
    /// </summary>
    private void InitializeView()
    {
        foreach (DomainTask domainTask in taskScheduler.GetDomainTaskList())
        {
            UpdateDomainTaskView(domainTask);
        }
    }

    /// <summary>
    /// Updates the domain task scheduler UI view by changes in the domain tasks on the scheduler.
    /// </summary>
    /// <param name="updatedDomainTask">Updated domain task.</param>
    private void UpdateDomainTaskView(DomainTask updatedDomainTask)
    {
        Transform view = GetDomainTaskViewFromContainer(updatedDomainTask.domainInputUnit.domainInputID);

        if (view != null)
        {
            if (taskScheduler.IsDomainTaskInTaskList(updatedDomainTask))
            {
                view.Find("Status").Find("StatusText").GetComponent<Text>().text = Convert.ToString(updatedDomainTask.taskStatus);
            }
            else
            {
                Destroy(view.gameObject);
            }
        }
        else
        {
            GameObject wmEntryGo = Instantiate(domainTaskEntryViewPrefab, new Vector3(0, 0, 0), Quaternion.identity, taskSchedulerContainerView.transform) as GameObject;

            wmEntryGo.transform.Find("ID").Find("IDText").GetComponent<Text>().text = updatedDomainTask.domainInputUnit.domainInputID.ToString();
            wmEntryGo.transform.Find("FunctionType").Find("FunctionTypeText").GetComponent<Text>().text = updatedDomainTask.domainInputUnit.functionType.ToString();
            wmEntryGo.transform.Find("ExecutionType").Find("ExecutionTypeText").GetComponent<Text>().text = updatedDomainTask.domainInputUnit.executionType.ToString();
            wmEntryGo.transform.Find("Behavior").Find("BehaviorText").GetComponent<Text>().text = updatedDomainTask.domainInputUnit.behaviorName;
            wmEntryGo.transform.Find("Status").Find("StatusText").GetComponent<Text>().text = Convert.ToString(updatedDomainTask.taskStatus);
        }
    }

    /// <summary>
    /// Get the current UI view element of a domain tasks ID.
    /// </summary>
    /// <param name="domainInputID">Domain task ID to search.</param>
    /// <returns>UI view of the domain task element.</returns>
    private Transform GetDomainTaskViewFromContainer(string domainInputID)
    {
        foreach (Transform child in this.taskSchedulerContainerView.transform)
        {
            if (child.Find("ID").Find("IDText").GetComponent<Text>().text == domainInputID)
            {
                return child;
            }
        }

        return null;
    }
}