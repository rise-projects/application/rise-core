using RosMessageTypes.RosTcpEndpoint;
using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (ActionServer) and corresponding UI view.
/// </summary>
public class PresenterActionServer : MonoBehaviour
{
    /// <summary>Handling domain tasks as controller.</summary>
    private DomainTaskActionServer domainTaskActionServer;

    /// <summary>UI view for all domain tasks in the system.</summary>
    public GameObject domainTaskActionServerContainerView;

    /// <summary>UI view for a single entry of a domain task.</summary>
    public GameObject domainTaskEntryViewPrefab;

    /// <summary>Max. limit of visualized domain tasks in the listview.</summary>
    private int limitViewElements = 250;

    // Start is called before the first frame update
    void Start()
    {
        domainTaskActionServer = DomainTaskActionServer.Instance;

    }

    void Update()
    {
        ListViewLimitHandler();
    }

    void OnEnable()
    {
        DomainTaskActionServer.actionGoalChanged += UpdateActionView;
    }

    void OnDisable()
    {
        DomainTaskActionServer.actionGoalChanged -= UpdateActionView;
    }

    /// <summary>
    /// Update the view object of the list of domain tasks by changes in the model.
    /// </summary>
    /// <param name="updatedDomainTaskActionGoalHandler">Entry of a Domain Tasks in the model.</param>
    private void UpdateActionView(AbstractActionGoalHandler<DomainTaskAction, DomainTaskActionGoal, DomainTaskActionResult, DomainTaskActionFeedback, DomainTaskGoal, DomainTaskResult, DomainTaskFeedback> updatedDomainTaskActionGoalHandler)
    {   
        Transform view = GetActionViewFromContainer(updatedDomainTaskActionGoalHandler.action.action_goal.goal_id.id);

        if (view != null)
        {
            if (domainTaskActionServer.HasDomainTaskInList(updatedDomainTaskActionGoalHandler.action))
            {
                view.Find("Status").Find("StatusText").GetComponent<Text>().text = Convert.ToString(updatedDomainTaskActionGoalHandler.actionStatus);
            }
            else
            {
                Destroy(view.gameObject);
            }
        }
        else
        {
            GameObject wmEntryGo = Instantiate(domainTaskEntryViewPrefab, new Vector3(0, 0, 0), Quaternion.identity, domainTaskActionServerContainerView.transform) as GameObject;

            wmEntryGo.transform.Find("ID").Find("IDText").GetComponent<Text>().text = updatedDomainTaskActionGoalHandler.action.action_goal.goal_id.id;
            wmEntryGo.transform.Find("FunctionType").Find("FunctionTypeText").GetComponent<Text>().text = updatedDomainTaskActionGoalHandler.action.action_goal.goal.function_type;
            wmEntryGo.transform.Find("ExecutionType").Find("ExecutionTypeText").GetComponent<Text>().text = updatedDomainTaskActionGoalHandler.action.action_goal.goal.calling_type;
            wmEntryGo.transform.Find("Behavior").Find("BehaviorText").GetComponent<Text>().text = updatedDomainTaskActionGoalHandler.action.action_goal.goal.domain_task_behavior;
            wmEntryGo.transform.Find("Status").Find("StatusText").GetComponent<Text>().text = Convert.ToString(updatedDomainTaskActionGoalHandler.actionStatus);
        }
    }

    /// <summary>
    /// Limits domain tasks and destroys out of bounds elements.
    /// </summary>
    private void ListViewLimitHandler()
    {
        int diff = this.domainTaskActionServerContainerView.transform.childCount - limitViewElements;
        for(int i = 0; i < diff; i++)
        {
            Destroy(domainTaskActionServerContainerView.transform.GetChild(0).gameObject);
        }
    }

    /// <summary>
    /// Get searched view element as transform from the listview by goal id.
    /// </summary>
    /// <param name="goalID">Key attribute for searching in listview.</param>
    /// <returns>UI view element of the list view entry.</returns>
    private Transform GetActionViewFromContainer(string goalID)
    {
        foreach (Transform child in this.domainTaskActionServerContainerView.transform)
        {
            if (child.Find("ID").Find("IDText").GetComponent<Text>().text == goalID)
            {
                return child;
            }
        }

        return null;
    }
}