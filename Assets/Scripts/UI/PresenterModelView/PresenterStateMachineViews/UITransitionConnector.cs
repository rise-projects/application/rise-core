using UnityEngine;

/// <summary>
/// Script for attaching to state machine state.
/// Enable function to draw a transition between to objects of the state.
/// </summary>
public class UITransitionConnector : MonoBehaviour
{
    /// <summary>UI view for making transitions from source.</summary>
    public GameObject source;

    /// <summary>UI view for making transitions to target.</summary>
    public GameObject target;

    /// <summary>UI line width of the transition.</summary>
    public float lineWidth = 2;

    /// <summary>UI visualization of a transition as image.</summary>
    private RectTransform imageRectTransform;

    /// <summary>UI root canvas for referencing images on the screen.</summary>
    private Canvas canvas;

    /// <summary>
    /// Draw a line (an image object) between objects.
    /// </summary>
    public void DrawLine()
    {
        imageRectTransform = gameObject.GetComponent<RectTransform>();
        canvas = transform.root.GetComponent<Canvas>();

        Vector3 differenceVector = target.transform.position - source.transform.position;

        imageRectTransform.sizeDelta = new Vector2(differenceVector.magnitude / canvas.scaleFactor, lineWidth);
        imageRectTransform.pivot = new Vector2(0f, 0.5f);
        imageRectTransform.position = new Vector3(source.transform.position.x, source.transform.position.y, source.transform.position.z);

        float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
        imageRectTransform.rotation = Quaternion.Euler(0, 0, angle);
    }
}