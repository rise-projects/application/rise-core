using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using Image = UnityEngine.UI.Image;

/// <summary>
/// Presenter between model (Transitions of IR) and corresponding UI view.
/// </summary>
public class PresenterIRTransition : MonoBehaviour
{
    /// <summary>UI renderer for transitions as lines on the monitor.</summary>
    public UILineRenderer lineRenderer;

    /// <summary>List of all transitions in the state-machine.</summary>
    private List<IRTransition> transitionModelList = new();

    /// <summary>UI color of the line-renderer.</summary>
    private Color mainColor;

    /// <summary>UI color for lerping the progress of transitions.</summary>
    private float colorProgressLerp;

    /// <summary>Model of the starting state for a transition.</summary>
    public GameObject start;

    /// <summary>Model for the target state of a transition.</summary>
    public GameObject target;

    /// <summary>UI view for showing text in the UI.</summary>
    public Text descritionTextObject;

    /// <summary>Model for the description of the text.</summary>
    private string descritionText;

    /// <summary>UI image for a transitions.</summary>
    public Image arrow;

    /// <summary>UI color for transitions from left-right.</summary>
    private Color leftToRightColor = new Color(0f, 0f, 200f / 255f);

    /// <summary>UI color for transitions from right-left.</summary>
    private Color rightToLeftColor = new Color(200f / 255f, 0f, 0f);

    /// <summary>UI color for active transitions</summary>
    private Color highlihtColor = Color.green;

    /// <summary>UI prefab view for showing descriptions.</summary>
    public GameObject prefabDescriptionHUD;

    /// <summary>UI reference position for transitions in the state-machine.</summary>
    private Vector2 middleLinePosition;

    /// <summary>Model for transition descriptions.</summary>
    private string transitionDescrition;

    /// <summary>UI offset for positions on y-coordinate.</summary>
    private float yOffset;

    private void OnDestroy()
    {
        foreach (IRTransition transition in transitionModelList)
        {
            transition.transitionStatusChanged -= UpdateTransitionColor;
        }
    }

    /// <summary>
    /// Initialize view transitions by transition model.
    /// </summary>
    /// <param name="transitionModel">Transition model.</param>
    public void InitModelTransition(IRTransition transitionModel)
    {
        this.transitionModelList.Add(transitionModel);
        transitionModel.transitionStatusChanged += UpdateTransitionColor;
        int numOfTransitios = transitionModelList.Count;
        if (numOfTransitios == 1)
        {
            descritionText = "1 Transition";
            transitionDescrition = transitionModel.eventTopic;
        }
        else
        {
            transitionDescrition = transitionDescrition + "\n" + transitionModel.eventTopic;
            descritionText = numOfTransitios + " Transitions";
        }
        descritionTextObject.text = descritionText;

        descritionTextObject.gameObject.GetComponent<PresenterSMTransition>().SetValueDescription(transitionDescrition);
    }

    /// <summary>
    /// Drawing lines on the UI between a start and target state.
    /// </summary>
    /// <param name="start">Start state.</param>
    /// <param name="target">Target state.</param>
    /// <param name="numberOfExistingTransitions">Factor for offset transitions.</param>
    public void GenerateLineBetweenGO(GameObject start, GameObject target, int numberOfExistingTransitions)
    {
        this.start = start;
        this.target = target;

        yOffset = this.GetComponent<RectTransform>().rect.height * numberOfExistingTransitions + this.GetComponent<RectTransform>().rect.height / 2;

        float ratioWidth = (float)(1920f / Screen.width);
        float heigt = start.GetComponent<RectTransform>().rect.height;
        Vector2 targetPosition = (target.transform.position - start.transform.position);
        targetPosition.x *= ratioWidth;

        Vector2 startPosition = new Vector2(0, yOffset);

        float curveLength = (targetPosition.x - startPosition.x);
        float curveHeight = curveLength / 4;
        float descritionYOffset = -15;

        Vector2 middle = new Vector2(curveLength / 2, curveHeight);

        if (start.transform.position.x < target.transform.position.x)
        {
            //arrows above
            startPosition.y += heigt;
            targetPosition.y = yOffset + heigt;
            middle.y += heigt + yOffset;
            lineRenderer.color = leftToRightColor;

            descritionTextObject.gameObject.GetComponent<PresenterSMTransition>().SetTextObjectOrientation(1);
        }
        else
        {
            descritionYOffset *= -1;
            middle.y = yOffset + curveHeight;
            targetPosition.y = yOffset;
            lineRenderer.color = rightToLeftColor;
            arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 180f);

            descritionTextObject.gameObject.GetComponent<PresenterSMTransition>().SetTextObjectOrientation(-1);
        }

        mainColor = lineRenderer.color;

        float t;
        int numberOfPoints = 50;
        Vector2[] result = new Vector2[numberOfPoints];
        for (int i = 0; i < numberOfPoints; i++)
        {
            t = i / (numberOfPoints - 1.0f);
            result[i] = (1.0f - t) * (1.0f - t) * startPosition
            + 2.0f * (1.0f - t) * t * middle + t * t * targetPosition;
        }
        lineRenderer.Points = result;
        middleLinePosition = result[numberOfPoints / 2];
        arrow.transform.localPosition = middleLinePosition;
        descritionTextObject.transform.localPosition = middle;
        descritionTextObject.transform.localPosition = new Vector2(middleLinePosition.x, middleLinePosition.y + descritionYOffset);
    }

    /// <summary>
    /// Update UI view transition color by changes in the model.
    /// </summary>
    /// <param name="transition">Name of the transitions to be change.</param>
    public void UpdateTransitionColor(IRTransition transition)
    {
        StopAllCoroutines();

        if(descritionTextObject.gameObject.activeInHierarchy)
        {
            if (transition.isActive)
            {
                descritionTextObject.fontStyle = FontStyle.Bold;
                descritionTextObject.text = transition.eventTopic;
                StartCoroutine(LerpColor(mainColor, highlihtColor));
            }
            else
            {
                descritionTextObject.fontStyle = FontStyle.Normal;
                descritionTextObject.text = descritionText;
                StartCoroutine(LerpColor(highlihtColor, mainColor));
            }
        }
    }

    /// <summary>
    /// Lerp between to colors for representing active states of a transition.
    /// </summary>
    /// <param name="start">Starting color.</param>
    /// <param name="end">Targeting color.</param>
    /// <returns>Coroutine for lerping over time.</returns>
    private IEnumerator LerpColor(Color start, Color end)
    {
        colorProgressLerp = 0;
        float increment = 0.075f / 1f;
        while (colorProgressLerp < 1)
        {
            lineRenderer.color = Color.Lerp(start, end, colorProgressLerp);
            colorProgressLerp += increment;
            yield return new WaitForSeconds(0.01f);
        }

        lineRenderer.color = end;
    }
}