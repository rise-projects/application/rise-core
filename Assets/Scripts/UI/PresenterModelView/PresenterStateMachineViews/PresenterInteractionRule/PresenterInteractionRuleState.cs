using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (InteractionRule state) and corresponding view.
/// </summary>
public class PresenterInteractionRuleState : MonoBehaviour
{
    /// <summary>UI view of an InteractionRule state.</summary>
    public GameObject viewInteractionRuleState { get; set; }

    /// <summary>Model representation of an IR state.</summary>
    public IRState modelInteractionRuleState { get; set; }

    /// <summary>State if an view element is an start or end state in the state-machine structure.</summary>
    public bool isDummyState = false;

    /// <summary>UI view for display description informations about a state.</summary>
    public GameObject descriptionPrefab;

    /// <summary>Color offset for lerping between two status of states.</summary>
    private float colorProgressLerp;

    void OnDestroy()
    {
        if (!isDummyState)
        {
            modelInteractionRuleState.irStatusChanged -= UpdateStatus;
        }
    }

    /// <summary>
    /// Initialize UI view elements by IR state model.
    /// </summary>
    /// <param name="go">UI view of an IR state.</param>
    /// <param name="action">Model of an IR state itself.</param>
    public void Initialize(GameObject go, IRState action)
    {
        viewInteractionRuleState = go;
        modelInteractionRuleState = action;
        modelInteractionRuleState.irStatusChanged += UpdateStatus;

        viewInteractionRuleState.transform.Find("Header/HeaderTitle/IDBackground/ID").GetComponent<Text>().text = modelInteractionRuleState.id.ToString();

        GameObject onEntry = Instantiate(descriptionPrefab, new Vector3(0, 0, 0), Quaternion.identity, viewInteractionRuleState.transform.Find("Body/ContentParameters")) as GameObject;
        GameObject transitions = Instantiate(descriptionPrefab, new Vector3(0, 0, 0), Quaternion.identity, viewInteractionRuleState.transform.Find("Body/ContentParameters")) as GameObject;

        onEntry.transform.Find("Key").GetComponent<Text>().text = "OnEntry";
        onEntry.transform.GetComponent<PresenterDescriptionInformation>().SetValueDescription(modelInteractionRuleState.GetOnEntryInformations());

        transitions.transform.Find("Key").GetComponent<Text>().text = "Transitions";
        transitions.transform.GetComponent<PresenterDescriptionInformation>().SetValueDescription(modelInteractionRuleState.GetTransitionInformations());

        if (action.stateIsRunning)
        {
            viewInteractionRuleState.transform.Find("Header/Status/Image").GetComponent<Image>().color = Color.green;
        }
        else
        {
            viewInteractionRuleState.transform.Find("Header/Status/Image").GetComponent<Image>().color = Color.red;
        }
    }

    /// <summary>
    /// Updates the UI view status of a state, corresponding the state changes in the IR.
    /// </summary>
    public void UpdateStatus()
    {
        if(this.viewInteractionRuleState.activeInHierarchy)
        {
            if (this.modelInteractionRuleState != null)
            {
                StopAllCoroutines();

                switch (modelInteractionRuleState.stateIsRunning)
                {
                    case true:
                        StartCoroutine(LerpColor(Color.red, Color.green));
                        break;

                    case false:
                        StartCoroutine(LerpColor(Color.green, Color.red));
                        break;
                }
            }
        }
    }

    /// <summary>
    /// Lerping between two colors and visualize the enabling/disabling of a state in the IR.
    /// </summary>
    /// <param name="start">Starting color.</param>
    /// <param name="end">Targeting color.</param>
    /// <returns>Coroutine for the lerping process.</returns>
    private IEnumerator LerpColor(Color start, Color end)
    {
        colorProgressLerp = 0;
        float increment = 0.075f / 1f;
        while (colorProgressLerp < 1)
        {
            viewInteractionRuleState.transform.Find("Header/Status/Image").GetComponent<Image>().color = Color.Lerp(start, end, colorProgressLerp);
            colorProgressLerp += increment;
            yield return new WaitForSeconds(0.01f);
        }

        viewInteractionRuleState.transform.Find("Header/Status/Image").GetComponent<Image>().color = end;
    }
}