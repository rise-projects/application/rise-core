using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (InteractionRule state-machine) and corresponding UI view.
/// </summary>
public class PresenterStateMachineIR : MonoBehaviour
{
    /// <summary>UI view for the content of the state-machine.</summary>
    public GameObject contentIRStateMachine;

    /// <summary>UI view for container elements containing state-machines on execution level.</summary>
    public GameObject containerElementPrefab;

    /// <summary>UI view as prefab for a state of an IR.</summary>
    public GameObject viewInteractionRuleStatePrefab;

    /// <summary>Ui view as prefab for a transition of states.</summary>
    public GameObject transitionPrefab;

    /// <summary>UI view for an empty state in the state-machine.</summary>
    public GameObject stateMachineStartPrefab;

    /// <summary>UI view for an empty state in the state-machine.</summary>
    public GameObject stateMachineEndPrefab;

    /// <summary>UI view as prefab for initializing transitions in the UI.</summary>
    public GameObject stateMachineIRTransitionPrefab;

    /// <summary>List with all containers/levels in the state-machine.</summary>
    public List<GameObject> containerList;

    /// <summary>List of alle states in the state-machine.</summary>
    public List<GameObject> stateList;

    /// <summary>List of all executed nodes of the state-machine.</summary>
    public List<GameObject> executedNodes;

    /// <summary>UI view for scroll area of the state-machine.</summary>
    public GameObject scrollRect;

    /// <summary>UI view for the whole state-machine.</summary>
    public RectTransform contentPanel;

    /// <summary>Reference to the current selected IR.</summary>
    private InteractionRule selectedIR;

    // Start is called before the first frame update
    void Start()
    {
        executedNodes = new List<GameObject>();
    }

    private void OnEnable()
    {
        PresenterInteractionRuleListView.selectedInteractionRule += InitializeStateMachineInteractionRule;
    }

    private void OnDisable()
    {
        PresenterInteractionRuleListView.selectedInteractionRule -= InitializeStateMachineInteractionRule;
    }

    /// <summary>
    /// Initialize IR in the view of state-machines.
    /// </summary>
    /// <param name="interactionRule">Model of the IR.</param>
    public void InitializeStateMachineInteractionRule(InteractionRule interactionRule)
    {
        selectedIR = interactionRule;

        this.ResetStateMachine();
        this.ResetPivotContainer();

        containerList = new List<GameObject>();
        stateList = new List<GameObject>();

        if (interactionRule != null)
        {
            //Adding start state
            containerList.Add(Instantiate(containerElementPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentIRStateMachine.transform) as GameObject);

            GameObject gos = Instantiate(stateMachineStartPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentIRStateMachine.transform) as GameObject;
            gos.transform.SetParent(containerList.Last().transform);
            gos.transform.GetComponent<PresenterBehaviorAction>().SetupDummy(-1);
            gos.transform.localScale = Vector3.one;
            stateList.Add(gos);

            //Adding interaction rule states
            foreach (IRState state in interactionRule.stateList)
            {
                containerList.Add(Instantiate(containerElementPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentIRStateMachine.transform) as GameObject);

                GameObject go = Instantiate(viewInteractionRuleStatePrefab) as GameObject;
                go.transform.GetComponent<PresenterInteractionRuleState>().Initialize(go, state);

                go.transform.SetParent(containerList.Last().transform);

                go.transform.localScale = Vector3.one;
                stateList.Add(go);
            }

            //Adding end state
            containerList.Add(Instantiate(containerElementPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentIRStateMachine.transform) as GameObject);

            gos = Instantiate(stateMachineEndPrefab) as GameObject;
            gos.transform.SetParent(containerList.Last().transform);
            gos.transform.GetComponent<PresenterBehaviorAction>().SetupDummy(-1);
            gos.transform.localScale = Vector3.one;
            stateList.Add(gos);
            ResetPivotContainer();

            StartCoroutine(GenerateTransitions());
        }
    }
    
    /// <summary>
    /// Drawing transitions in the UI.
    /// This function is executed one frame after the initialization of the states.
    /// </summary>
    /// <returns>Coroutine for running the function over time.</returns>
    public IEnumerator GenerateTransitions()
    {
        yield return 0; //wait 1 frame for final gameobject positions

        //Adding transitions
        List<PresenterIRTransition> presenterIRTransitionList = new List<PresenterIRTransition>();
        foreach (GameObject goViewIR in stateList)
        {
            PresenterInteractionRuleState presenterIRstate = goViewIR.transform.GetComponent<PresenterInteractionRuleState>();
            if (presenterIRstate != null)
            {
                foreach (IRTransition transition in presenterIRstate.modelInteractionRuleState.transitionList)
                {
                    GameObject start = presenterIRstate.viewInteractionRuleState.gameObject;


                    GameObject targetGO = stateList.Find(e =>
                        e.transform.GetComponent<PresenterInteractionRuleState>() != null &&
                        e.transform.GetComponent<PresenterInteractionRuleState>().modelInteractionRuleState.id == transition.target);
                    if (targetGO != null)
                    {
                        PresenterIRTransition existingTransition = presenterIRTransitionList.Find(irt => irt.start.Equals(start) && irt.target.Equals(targetGO));
                        if (existingTransition != null)
                        {
                            existingTransition.InitModelTransition(transition);
                        }
                        else
                        {
                            int existingTransitionOnThisStart = presenterIRTransitionList.FindAll(irt => irt.start.Equals(start)).Count();
                            GameObject lineRenderer = Instantiate(stateMachineIRTransitionPrefab, presenterIRstate.transform.position, Quaternion.identity, contentIRStateMachine.transform) as GameObject;
                            lineRenderer.transform.SetParent(goViewIR.transform);

                            PresenterIRTransition transitionLine = lineRenderer.GetComponent<PresenterIRTransition>();

                            transitionLine.InitModelTransition(transition);
                            transitionLine.GenerateLineBetweenGO(start, targetGO, existingTransitionOnThisStart);
                            presenterIRTransitionList.Add(transitionLine);
                        }


                    }
                }
            }
        }
    }

    /// <summary>
    /// Resets the whole state-machine. Deletion of all states.
    /// </summary>
    public void ResetStateMachine()
    {
        //CanvasScaler canvasScaler = GameObject.Find("Canvas").gameObject.GetComponent<CanvasScaler>();
        contentIRStateMachine.transform.localScale = new Vector3(1, 1, 1);

        foreach (GameObject state in this.stateList)
        {
            Destroy(state);
        }

        foreach (GameObject cont in this.containerList)
        {
            Destroy(cont);
        }
    }

    /// <summary>
    /// Resets the view anchor of the state-machine.
    /// </summary>
    public void ResetPivotContainer()
    {
        Vector2 anchorMin = new Vector2(0.5f, 0.5f);
        Vector2 anchorMax = new Vector2(0.5f, 0.5f);
        Vector2 pivot = new Vector2(0.5f, 0.5f);

        if (this.containerList.Count > 6)
        {
            anchorMin = new Vector2(0, 0.5f);
            anchorMax = new Vector2(0, 0.5f);
            pivot = new Vector2(0, 0.5f);
        }

        if (this.contentIRStateMachine.activeSelf)
        {
            this.contentIRStateMachine.GetComponent<RectTransform>().anchorMin = anchorMin;
            this.contentIRStateMachine.GetComponent<RectTransform>().anchorMax = anchorMax;
            this.contentIRStateMachine.GetComponent<RectTransform>().pivot = pivot;
        }
    }

    /// <summary>
    /// Snapp the view point of the state-machin in the scroll area to the current running state.
    /// </summary>
    /// <param name="target">Running state.</param>
    public void SnapScrollRectTo(RectTransform target)
    {
        if (contentPanel.rect.width > scrollRect.GetComponent<RectTransform>().rect.width)
        {
            Vector2 diffVec = (Vector2)scrollRect.GetComponent<ScrollRect>().transform.InverseTransformPoint(contentPanel.position)
                            - (Vector2)scrollRect.GetComponent<ScrollRect>().transform.InverseTransformPoint(target.position);

            float focusArea = scrollRect.GetComponent<RectTransform>().rect.width / 2;

            if (Mathf.Abs(diffVec.x) >= focusArea && (Mathf.Abs(diffVec.x) + (focusArea - 100)) <= contentPanel.rect.width || contentPanel.anchoredPosition.x >= Mathf.Abs(diffVec.x) + focusArea)
            {
                contentPanel.anchoredPosition = new Vector2(diffVec.x + focusArea, 0);
            }
        }
    }

    /// <summary>
    /// Returns the view element of a IR state ID.
    /// </summary>
    /// <param name="irStateID">Searching the view element by ID.</param>
    /// <returns>UI view object.</returns>
    /// <exception cref="System.Exception">Exception if state is not found.</exception>
    private GameObject GetIRState(string irStateID)
    {
        foreach (GameObject state in stateList)
        {
            if (state.GetComponent<PresenterInteractionRuleState>() != null)
            {
                if (state.GetComponent<PresenterInteractionRuleState>().modelInteractionRuleState.id == irStateID)
                {
                    return state;
                }
            }
        }

        throw new System.Exception("No available state");
    }
}