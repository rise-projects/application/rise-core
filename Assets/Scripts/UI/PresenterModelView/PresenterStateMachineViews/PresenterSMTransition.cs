using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (State-Machine Transitions) and corresponding UI view.
/// </summary>
public class PresenterSMTransition : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    /// <summary>UI view transform for showing informations about a state.</summary>
    private RectTransform canvas;

    /// <summary>UI view prefab for visualizing a HUD by hovering over a state.</summary>
    public GameObject prefabDescriptionHUD;

    /// <summary>UI view of the HUD.</summary>
    private GameObject shownHUD;

    /// <summary>Model of the text informations to display.</summary>
    public string valueDescription { get; set; }

    /// <summary>Max. line length for making line breaks automatically.</summary>
    public int lineLength;

    /// <summary>UI information about direction of the view. On bottom/top of the cursor.</summary>
    public int orientation;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (shownHUD == null)
        {
            shownHUD = Instantiate(prefabDescriptionHUD, eventData.position, Quaternion.identity) as GameObject;
            shownHUD.transform.SetParent(canvas.transform, false);
            shownHUD.GetComponentInChildren<Text>().text = valueDescription;
            shownHUD.transform.position = new Vector2(GetComponent<Transform>().position.x, GetComponent<Transform>().position.y + (((shownHUD.GetComponentInChildren<Text>().preferredHeight / 2) + (gameObject.GetComponent<RectTransform>().rect.height * 2)) * orientation));
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (shownHUD != null)
        {
            Destroy(shownHUD);
            shownHUD = null;
        }
    }

    /// <summary>
    /// Set text informations for the UI view.
    /// </summary>
    /// <param name="valueDescription">Text as model information.</param>
    public void SetValueDescription(string valueDescription)
    {
        this.valueDescription = Regex.Replace(valueDescription, "(.{" + lineLength + "})", "$1" + Environment.NewLine);
    }

    /// <summary>
    /// Set text orientation of the UI view object.
    /// </summary>
    /// <param name="direction">Direction as top/bottom.</param>
    public void SetTextObjectOrientation(int direction)
    {
        this.orientation = direction;
    }
}