using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (BehaviorAction) and corresponding UI view.
/// </summary>
public class PresenterBehaviorAction : MonoBehaviour
{
    /// <summary>UI view of a BehaviorAction.</summary>
    public GameObject viewBehaviorAction { get; set; }

    /// <summary>Model representation of a BehaviorAction.</summary>
    public BehaviorAction modelBehaviorAction { get; set; }

    /// <summary>Execution level of an action.</summary>
    public int waitingLevel { get; set; }

    /// <summary>Status if an view state is a start or end element in the state-machine.</summary>
    public bool isDummyState = false;

    /// <summary>UI view prefab for setting description texts of an action.</summary>
    public GameObject descriptionPrefab;

    void OnDestroy()
    {
        if(!isDummyState)
        {
            modelBehaviorAction.actionStatusChanged -= UpdateStatus;
            modelBehaviorAction.attributeValueChanged -= UpdateAttributeLabels;
        }
    }

    /// <summary>
    /// Initialize view by model of a BehaviorAction.
    /// </summary>
    /// <param name="view">UI view of an action.</param>
    /// <param name="action">The action as model.</param>
    /// <param name="waitingLevel">The execution level for starting the action.</param>
    public void Initialize(GameObject view, BehaviorAction action, int waitingLevel)
    {
        this.viewBehaviorAction = view;
        this.modelBehaviorAction = action;
        this.waitingLevel = waitingLevel;

        viewBehaviorAction.transform.Find("Header/Status/Icon").GetComponent<Image>().sprite = ActionEnum.GetSpriteForAction(modelBehaviorAction.actionState);
        viewBehaviorAction.transform.Find("Header/HeaderTitle/IDBackground/ID").GetComponent<Text>().text = modelBehaviorAction.actionID.ToString();
        viewBehaviorAction.transform.Find("Header/HeaderTitle/TitleBackground/Title").GetComponent<Text>().text = modelBehaviorAction.actionState.ToString();

        this.modelBehaviorAction.actionStatusChanged += UpdateStatus;
        this.modelBehaviorAction.attributeValueChanged += UpdateAttributeLabels;


        foreach(KeyValuePair<string, string> descEntry in modelBehaviorAction.GetAttributeDescriptions())
        {
            GameObject go = Instantiate(descriptionPrefab, new Vector3(0, 0, 0), Quaternion.identity, viewBehaviorAction.transform.Find("Body/ContentParameters")) as GameObject;
            go.transform.Find("Key").GetComponent<Text>().text = descEntry.Key;
            go.transform.GetComponent<PresenterDescriptionInformation>().SetValueDescription(descEntry.Value.ToString());
        }

        UpdateStatus();
    }

    /// <summary>
    /// Setup a start or end state in the state-machine view by an empty action state UI element.
    /// </summary>
    /// <param name="waitingLevel"></param>
    public void SetupDummy(int waitingLevel)
    {
        this.isDummyState = true;
        this.waitingLevel = waitingLevel;
    }

    /// <summary>
    /// Get the status of the action of the model in the current presenter.
    /// </summary>
    /// <returns>ActionID of the models action state.</returns>
    public int GetActionState()
    {
        if(this.modelBehaviorAction != null)
        {
            return this.modelBehaviorAction.actionID;
        }

        return 0;
    }

    /// <summary>
    /// Update UI view status by the change of the status in the model.
    /// </summary>
    public void UpdateStatus()
    {
        if(this.modelBehaviorAction != null)
        {
            switch (modelBehaviorAction.actionIsRunning)
            {
                case true:
                    viewBehaviorAction.transform.Find("Header/Status/Image").GetComponent<Image>().color = Color.green;
                    break;

                case false:
                    viewBehaviorAction.transform.Find("Header/Status/Image").GetComponent<Image>().color = Color.red;
                    break;
            }
        }
    }

    /// <summary>
    /// Update UI view attributes by changes in the models attributes list.
    /// </summary>
    public void UpdateAttributeLabels()
    {
        int idx = 0;

        foreach (KeyValuePair<string, string> descEntry in modelBehaviorAction.GetAttributeDescriptions())
        {
            viewBehaviorAction.transform.Find("Body/ContentParameters").GetChild(idx).GetComponent<PresenterDescriptionInformation>().SetValueDescription(descEntry.Value.ToString());
            idx++;
        }
    }
}