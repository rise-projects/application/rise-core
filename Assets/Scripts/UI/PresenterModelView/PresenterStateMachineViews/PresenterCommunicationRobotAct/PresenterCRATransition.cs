using System.Collections;
using UnityEngine;
using UnityEngine.UI.Extensions;

/// <summary>
/// Presenter between model (Transition of CRAs) and corresponding UI view.
/// </summary>
public class PresenterCRATransition : MonoBehaviour
{
    /// <summary>UI view for rendering transitions as lines on the UI.</summary>
    public UILineRenderer lineRenderer;

    /// <summary>UI color of the line renderer.</summary>
    private Color mainColor;

    /// <summary>UI color of a transition.</summary>
    private Color transitionColor = new Color(0f, 0f, 200f / 255f);

    /// <summary>UI color for an active transition.</summary>
    private Color highlightColor = Color.green;

    /// <summary>UI color offset for lerping colors by transitioning.</summary>
    private float colorProgressLerp;

    /// <summary>Model of a transitions start state.</summary>
    private BehaviorAction startState;

    /// <summary>Model fo a stransitions target start.</summary>
    private BehaviorAction targetState;

    /// <summary>Trigger for highlighting a transition.</summary>
    private bool startStateIsRunning = false;

    private void OnDestroy()
    {
        startState.actionStatusChanged -= StartIsActive;
        targetState.actionStatusChanged -= UpdateTransitionColor;
    }

    /// <summary>
    /// Generate and draw lines on the UI between to states.
    /// </summary>
    /// <param name="start">Start state.</param>
    /// <param name="target">End or target state.</param>
    public void GenerateLineBetweenGO(GameObject start, GameObject target)
    {
        float ratioWidth = (float)(Screen.width / 1920f);
        float ratioHeight = (float)(Screen.height / 1080f);
        float height = start.GetComponent<RectTransform>().rect.height * ratioHeight;
        float width = start.GetComponent<RectTransform>().rect.width * ratioWidth;

        Vector2 startPosition = new Vector2(width / 2, height / 2);
        Vector2 targetPosition = (target.transform.position - start.transform.position);

        targetPosition.x -= width / 2;
        targetPosition.y += height / 2;

        lineRenderer.color = transitionColor;
        mainColor = lineRenderer.color;
        lineRenderer.Points = new Vector2[2] { startPosition, targetPosition };

        startState = start.transform.GetComponent<PresenterBehaviorAction>().modelBehaviorAction;
        startState.actionStatusChanged += StartIsActive;

        targetState = target.transform.GetComponent<PresenterBehaviorAction>().modelBehaviorAction;
        targetState.actionStatusChanged += UpdateTransitionColor;
    }

    /// <summary>
    /// Update the status of a transitions start by changes in the status of the start state.
    /// </summary>
    public void StartIsActive()
    {
        if (startState.actionIsRunning)
        {
            startStateIsRunning = true;
        }
    }

    /// <summary>
    /// Update the UI view of a transition. Change color.
    /// </summary>
    public void UpdateTransitionColor()
    {
        StopAllCoroutines();

        if (targetState.actionIsRunning && startStateIsRunning)
        {
            StartCoroutine(LerpColor(mainColor, highlightColor));
        }
        else if (startStateIsRunning)
        {
            StartCoroutine(LerpColor(highlightColor, mainColor));
            startStateIsRunning = false;
        }
    }

    /// <summary>
    /// Lerp between two colors to realize short blinking for an active transition.
    /// </summary>
    /// <param name="start">Starting color.</param>
    /// <param name="end">Finished color.</param>
    /// <returns>Coroutine for lerping over time.</returns>
    private IEnumerator LerpColor(Color start, Color end)
    {
        colorProgressLerp = 0;
        float increment = 0.075f / 1f;
        while (colorProgressLerp < 1)
        {
            lineRenderer.color = Color.Lerp(start, end, colorProgressLerp);
            colorProgressLerp += increment;
            yield return new WaitForSeconds(0.01f);
        }

        lineRenderer.color = end;
    }
}