using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (State Machine of a CRA) and corresponding UI view.
/// </summary>
public class PresenterStateMachineCRA : MonoBehaviour
{
    /// <summary>UI view container with all states of a CRA.</summary>
    public GameObject contentCRAStateMachine;

    /// <summary>UI view for a container, containing all CRAs on the same waiting level.</summary>
    public GameObject containerElementPrefab;

    /// <summary>UI view for a BehaviorAction as prefab.</summary>
    public GameObject viewBehaviorActionStatePrefab;

    /// <summary>UI view for Transitions between states.</summary>
    public GameObject transitionPrefab;

    /// <summary>UI view for start-state in state-machine.</summary>
    public GameObject stateMachineStartPrefab;

    /// <summary>UI view for end-state in state-machine.</summary>
    public GameObject stateMachineEndPrefab;

    /// <summary>UI view for making transitions in state-machine of CRA.</summary>
    public GameObject stateMachineCRATransitionPrefab;

    /// <summary>List of all contrainer in the state-machine.</summary>
    public List<GameObject> containerList;

    /// <summary>List of all states in a state-machine.</summary>
    public List<GameObject> stateList;

    /// <summary>UI view for scroll area in the state-machine.</summary>
    public GameObject scrollRect;

    /// <summary>UU view for the content of a state-machine and the scroll area.</summary>
    public RectTransform contentPanel;

    /// <summary>UI view as prefab for a progress bar with informations about the progress in the state-machine.</summary>
    public GameObject progressBar;

    /// <summary>Reference to the selected CRA from the UI listview of CRAs.</summary>
    private CommunicationRobotAct selectedCRA;

    /// <summary>Handling starting/ending of CRAs.</summary>
    public CommunicationRobotActController communicationRobotActController;

    private void OnEnable()
    {
        PresenterCommunicationRobotActListView.selectedCommunicationRobotAct += InitializeStateMachineRobotAct;

        CommunicationRobotActController.progressHasChanged += UpdateProgressBar;

        UILoadingController.resetStateMachineCRA += ResetProgressBar;
    }

    private void OnDisable()
    {
        PresenterCommunicationRobotActListView.selectedCommunicationRobotAct -= InitializeStateMachineRobotAct;

        CommunicationRobotActController.progressHasChanged -= UpdateProgressBar;

        UILoadingController.resetStateMachineCRA -= ResetProgressBar;
    }

    /// <summary>
    /// Resets the progress bar in the state-machine.
    /// </summary>
    private void ResetProgressBar()
    {
        progressBar.GetComponent<ProgressBarScript>().UpdateProgressBar(0.0f);
    }

    /// <summary>
    /// Initialize state-machine by selected CRA.
    /// </summary>
    /// <param name="communicationRobotAct">Selected CRA in listview.</param>
    public void InitializeStateMachineRobotAct(CommunicationRobotAct communicationRobotAct)
    {
        if (selectedCRA != null)
        {
            selectedCRA.communicationRobotActStatusChanged -= UpdateCRARunningStatus;
        }

        selectedCRA = communicationRobotAct;

        if (communicationRobotAct != null)
        {
            selectedCRA.communicationRobotActStatusChanged += UpdateCRARunningStatus;
        }

        this.ResetStateMachine();
        this.ResetPivotContainer();
        ResetProgressBar();

        containerList = new List<GameObject>();
        stateList = new List<GameObject>();

        int maxWaitID;

        if (communicationRobotAct != null)
        {
            InitProgressBar();

            // Init start state
            containerList.Add(Instantiate(containerElementPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentCRAStateMachine.transform) as GameObject);

            GameObject gos = Instantiate(stateMachineStartPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentCRAStateMachine.transform) as GameObject;
            gos.transform.GetComponent<PresenterBehaviorAction>().SetupDummy(-1);
            gos.transform.SetParent(containerList[0].transform);
            gos.transform.localScale = Vector3.one;
            stateList.Add(gos);

            // Initialize states
            foreach (BehaviorAction action in communicationRobotAct.actionList)
            {
                maxWaitID = Mathf.Max(action.waitForActions.ToArray());

                GameObject go = Instantiate(viewBehaviorActionStatePrefab, new Vector3(0, 0, 0), Quaternion.identity, contentCRAStateMachine.transform) as GameObject;
                go.transform.GetComponent<PresenterBehaviorAction>().Initialize(go, action, maxWaitID);

                go.transform.localScale = Vector3.one;
                stateList.Add(go);
            }

            for (int i = 0; i <= stateList[stateList.Count - 1].transform.GetComponent<PresenterBehaviorAction>().waitingLevel; i++)
            {
                containerList.Add(Instantiate(containerElementPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentCRAStateMachine.transform) as GameObject);

                foreach (GameObject state in stateList)
                {
                    if (state.transform.GetComponent<PresenterBehaviorAction>().waitingLevel == i)
                    {
                        state.transform.SetParent(containerList[i + 1].transform);
                    }
                }

                if (containerList.Last().transform.childCount == 0)
                {
                    Destroy(containerList.Last());
                }
            }

            // Init end state
            containerList.Add(Instantiate(containerElementPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentCRAStateMachine.transform) as GameObject);

            gos = Instantiate(stateMachineEndPrefab, new Vector3(0, 0, 0), Quaternion.identity, contentCRAStateMachine.transform) as GameObject;
            gos.transform.GetComponent<PresenterBehaviorAction>().SetupDummy(int.MaxValue);
            gos.transform.SetParent(containerList.Last().transform);
            gos.transform.localScale = Vector3.one;
            stateList.Add(gos);

            ResetPivotContainer();
            StartCoroutine(GenerateTransitions());
        }
    }

    /// <summary>
    /// Generate all transitions after initializing all states of the state-machine.
    /// Function is executed one frame after the initialization of the state itself.
    /// </summary>
    /// <returns>Coroutine of the drawing process.</returns>
    public IEnumerator GenerateTransitions()
    {
        yield return 0; //wait 1 frame for final gameobject positions
        foreach (GameObject goViewCRA in stateList)
        {
            PresenterBehaviorAction presenterBehaviorAction = goViewCRA.transform.GetComponent<PresenterBehaviorAction>();
            if (presenterBehaviorAction.modelBehaviorAction != null)
            {
                int waitingLevel = presenterBehaviorAction.waitingLevel;
                GameObject targetElementLeftSide = stateList.Find(e =>
                    e.transform.GetComponent<PresenterBehaviorAction>().modelBehaviorAction != null &&
                    e.transform.GetComponent<PresenterBehaviorAction>().modelBehaviorAction.actionID == presenterBehaviorAction.waitingLevel);

                if (targetElementLeftSide != null)
                {
                    GameObject lineRenderer = Instantiate(stateMachineCRATransitionPrefab, targetElementLeftSide.transform.position, Quaternion.identity) as GameObject;
                    lineRenderer.transform.SetParent(targetElementLeftSide.transform);
                    PresenterCRATransition transitionLine = lineRenderer.GetComponent<PresenterCRATransition>();

                    transitionLine.GenerateLineBetweenGO(targetElementLeftSide, goViewCRA);

                }
            }
        }
    }

    /// <summary>
    /// Initialize the progress bar.
    /// </summary>
    private void InitProgressBar()
    {
        if (communicationRobotActController.runningCRA != null && selectedCRA != null)
        {
            UpdateProgressBar(communicationRobotActController.CalculateTrainingsProgress());
        }

        UpdateCRARunningStatus();
    }

    /// <summary>
    /// Update UI view of the status of an CRA.
    /// Updates the running-status by a loading bar/cycle.
    /// </summary>
    private void UpdateCRARunningStatus()
    {
        if (contentCRAStateMachine.activeInHierarchy)
        {
            if (selectedCRA.communicationRobotActIsRunning)
            {
                progressBar.transform.Find("RunningCircle").gameObject.SetActive(true);
            }
            else
            {
                progressBar.transform.Find("RunningCircle").gameObject.SetActive(false);
            }
        }
    }

    /// <summary>
    /// Update the progress in the progress bar, by changing the models progress.
    /// </summary>
    /// <param name="increase">Increase of the updated progress in the state-machine.</param>
    private void UpdateProgressBar(float increase)
    {
        if (contentCRAStateMachine.activeInHierarchy && selectedCRA != null && selectedCRA.communicationRobotActIsRunning)
        {
            progressBar.GetComponent<ProgressBarScript>().UpdateProgressBar(increase);
        }
    }

    /// <summary>
    /// Resets the whole state-machine. Deletion of all states.
    /// </summary>
    public void ResetStateMachine()
    {
        //CanvasScaler canvasScaler = GameObject.Find("Canvas").gameObject.GetComponent<CanvasScaler>();
        contentCRAStateMachine.transform.localScale = new Vector3(1, 1, 1);

        foreach (GameObject state in this.stateList)
        {
            Destroy(state);
        }

        foreach (GameObject cont in this.containerList)
        {
            Destroy(cont);
        }
    }

    /// <summary>
    /// Resets the view-anchor of the transform which contains all view elements of the state-machine.
    /// </summary>
    public void ResetPivotContainer()
    {
        Vector2 anchorMin = new Vector2(0.5f, 0.5f);
        Vector2 anchorMax = new Vector2(0.5f, 0.5f);
        Vector2 pivot = new Vector2(0.5f, 0.5f);

        if (this.containerList.Count > 6)
        {
            anchorMin = new Vector2(0, 0.5f);
            anchorMax = new Vector2(0, 0.5f);
            pivot = new Vector2(0, 0.5f);
        }

        if (this.contentCRAStateMachine.activeSelf)
        {
            this.contentCRAStateMachine.GetComponent<RectTransform>().anchorMin = anchorMin;
            this.contentCRAStateMachine.GetComponent<RectTransform>().anchorMax = anchorMax;
            this.contentCRAStateMachine.GetComponent<RectTransform>().pivot = pivot;
        }
    }

    /// <summary>
    /// Scnapping the scroll rect automatically to the middle of the execution in the state-machine.
    /// </summary>
    /// <param name="target">Current executed view representation of the currently running state.</param>
    public void SnapScrollRectTo(RectTransform target)
    {
        if (contentPanel.rect.width > scrollRect.GetComponent<RectTransform>().rect.width)
        {
            Vector2 diffVec = (Vector2)scrollRect.GetComponent<ScrollRect>().transform.InverseTransformPoint(contentPanel.position)
                            - (Vector2)scrollRect.GetComponent<ScrollRect>().transform.InverseTransformPoint(target.position);

            float focusArea = scrollRect.GetComponent<RectTransform>().rect.width / 2;

            if (Mathf.Abs(diffVec.x) >= focusArea && (Mathf.Abs(diffVec.x) + (focusArea - 100)) <= contentPanel.rect.width || contentPanel.anchoredPosition.x >= Mathf.Abs(diffVec.x) + focusArea)
            {
                contentPanel.anchoredPosition = new Vector2(diffVec.x + focusArea, 0);
            }
        }
    }

    /// <summary>
    /// Returns the view-element in the state-machine.
    /// </summary>
    /// <param name="actionID">ID for searching the state.</param>
    /// <returns>View element of state.</returns>
    /// <exception cref="Exception">Exception if state is not available.</exception>
    private GameObject GetState(int actionID)
    {
        foreach (GameObject state in stateList)
        {
            if (state.GetComponent<PresenterBehaviorAction>() != null)
            {
                if (state.GetComponent<PresenterBehaviorAction>().GetActionState() == actionID)
                {
                    return state;
                }
            }
        }

        throw new Exception("No available state");
    }
}