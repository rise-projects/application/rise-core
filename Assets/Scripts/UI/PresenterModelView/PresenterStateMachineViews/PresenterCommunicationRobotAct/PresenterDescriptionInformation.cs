using System;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (State information descriptions) and corresponding UI view.
/// </summary>
public class PresenterDescriptionInformation : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    /// <summary>UI view for displaying the informations of a state.</summary>
    private RectTransform canvas;

    /// <summary>Prefab UI element of representing state informations in the UI.</summary>
    public GameObject prefabDescriptionHUD;

    /// <summary>Reference to the UI object of as contrainer for all description informations.</summary>
    private GameObject shownHUD;

    /// <summary>The model of the description of a state.</summary>
    public string valueDescription { get; set; }

    /// <summary>Max. range of line length for making automatic line breaks.</summary>
    public int lineLength;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (shownHUD == null)
        {
            shownHUD = Instantiate(prefabDescriptionHUD, eventData.position, Quaternion.identity) as GameObject;
            shownHUD.transform.SetParent(canvas.transform, false);
            shownHUD.GetComponentInChildren<Text>().text = valueDescription;

            shownHUD.transform.position = new Vector2(GetComponent<Transform>().position.x, GetComponent<Transform>().position.y + (shownHUD.GetComponentInChildren<Text>().preferredHeight / 2) + (gameObject.GetComponent<RectTransform>().rect.height * 2));
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (shownHUD != null)
        {
            Destroy(shownHUD);
            shownHUD = null;
        }
    }

    /// <summary>
    /// Set text informations into the UI view.
    /// </summary>
    /// <param name="valueDescription">Informations to display as text.</param>
    public void SetValueDescription(string valueDescription)
    {
        this.valueDescription = Regex.Replace(valueDescription, "(.{" + lineLength + "})", "$1" + Environment.NewLine);
    }
}