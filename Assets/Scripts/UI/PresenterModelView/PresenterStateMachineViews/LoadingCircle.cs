using UnityEngine;

/// <summary>
/// Let the progress bar circle running.
/// </summary>
public class LoadingCircle : MonoBehaviour
{
    /// <summary>UI for the loading circle.</summary>
    private RectTransform rectComponent;

    /// <summary>Rotation speed of the object.</summary>
    private float rotateSpeed = 200f;

    private void Start()
    {
        rectComponent = GetComponent<RectTransform>();
    }

    /// <summary>
    /// Running circle each frame if the trainingsact is running and the object is enabled.
    /// </summary>
    private void Update()
    {
        if (this.enabled)
        {
            rectComponent.Rotate(0f, 0f, rotateSpeed * Time.deltaTime);
        }
    }
}