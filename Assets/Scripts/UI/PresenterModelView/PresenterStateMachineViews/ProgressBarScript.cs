using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Update script for the progressbar in the state machine visualization.
/// </summary>
public class ProgressBarScript : MonoBehaviour
{
    /// <summary>UI view for the progress bar slider.</summary>
    public Slider slider;

    /// <summary>UI view for displaying text informations.</summary>
    public Text displayText;

    /// <summary>Time for lerping between current progress and increased progress.</summary>
    public float lerpSpeed;

    /// <summary>Factor for lerping progress in the UI slider.</summary>
    private float timeElapsed;

    /// <summary>Value of the progress bar slider.</summary>
    private float sliderValue;

    /// <summary>Max. value for making progress.</summary>
    public float CurrentMaxValue { get; set; } = 0f;

    /// <summary>Status if the state-machine is in progress.</summary>
    private bool isInProgress;

    void Start()
    {
        CurrentMaxValue = 0f;
        timeElapsed = 0f;
        sliderValue = 0f;
        isInProgress = false;
    }

    /// <summary>
    /// Callable function for the state machine to set new max value of the progress bar.
    /// </summary>
    /// <param name="value">Max value of progress.</param>
    public void UpdateProgressBar(float value)
    {
        timeElapsed = 0f;
        CurrentMaxValue = value;
        isInProgress = true;
    }

    public void Update()
    {
        // Update progress bar by interpolation between actual slider value and max value.
        if (isInProgress)
        {
            if (CurrentMaxValue > 0)
            {
                sliderValue = Mathf.Lerp(sliderValue, CurrentMaxValue, timeElapsed);
                slider.value = sliderValue;

                timeElapsed += lerpSpeed * Time.deltaTime;
            }
            else
            {
                sliderValue = 0.0f;
                slider.value = sliderValue;
                isInProgress = false;
            }

            displayText.text = (slider.value * 100).ToString("0") + "%";
        }
    }
}