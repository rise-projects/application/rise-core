using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (InteractionRule) and corresponding UI view.
/// </summary>
public class PresenterInteractionRuleListElement : MonoBehaviour
{
    /// <summary>UI view for an IR.</summary>
    public GameObject viewInteractionRule { get; set; }

    /// <summary>Model representation of an IR.</summary>
    public InteractionRule modelInteractionRule { get; set; }

    private void OnDisable()
    {
        this.modelInteractionRule.updateInteractionRule -= UpdateStatus;
    }

    private void OnDestroy()
    {
        this.modelInteractionRule.updateInteractionRule -= UpdateStatus;
    }

    /// <summary>
    /// Initialize view by model of the IR.
    /// </summary>
    /// <param name="view">UI view component.</param>
    /// <param name="model">Model of the IR.</param>
    public void Initialize(GameObject view, InteractionRule model)
    {
        //Debug.Log("Init");

        this.viewInteractionRule = view;
        this.modelInteractionRule = model;
        this.modelInteractionRule.updateInteractionRule += UpdateStatus;

        viewInteractionRule.transform.Find("IRName").GetComponent<Text>().text = modelInteractionRule.name;

        UpdateStatus();
    }


    /// <summary>
    /// Update view status in UI by IR model.
    /// </summary>
    private void UpdateStatus()
    {
        ButtonToggle buttonToggle = viewInteractionRule.transform.Find("Status/Button").GetComponent<ButtonToggle>();

        if (this.modelInteractionRule.interactionRuleIsRunning != buttonToggle.running)
        {
            buttonToggle.SetButtonStatus(this.modelInteractionRule.interactionRuleIsRunning);
        }

        viewInteractionRule.transform.Find("IRDescription").GetComponent<Text>().text = modelInteractionRule.GetInteractionRuleDescription();
    }
}