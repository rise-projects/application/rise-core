using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (InteractionRule List) and corresponding UI view.
/// </summary>
public class PresenterInteractionRuleListView : MonoBehaviour
{
    /// <summary>UI view of the list of all IRs.</summary>
    public GameObject viewInteractionRuleListView;

    /// <summary>UI view prefab for one IR element in listview.</summary>
    public GameObject viewElementInteractionRulePrefab;

    /// <summary>Model of a collection of all InteractionRules.</summary>
    public CommunicationActContainer modelCommunicationContainer;

    /// <summary>Global pointer to selected IR in the listview.</summary>
    public static GameObject selectedInteractionRuleElement;

    /// <summary>Default color of an IR.</summary>
    private Color defaultIRColor;

    /// <summary>Color offset for fading an IR.</summary>
    public float colorOffset;

    /// <summary>Trigger for new selected IR in list.</summary>
    public static event Action<InteractionRule> selectedInteractionRule;

    /// <summary>Trigger for new started IR in list.</summary>
    public static event Action<InteractionRule> newStartedInteractionRule;

    // Start is called before the first frame update
    void Start()
    {
        this.defaultIRColor = viewElementInteractionRulePrefab.GetComponent<Image>().color;
    }

    private void OnEnable()
    {
        CommunicationActContainer.updateInteractionRuleList += UpdateInteractionRuleListView;
        PresenterCommunicationRobotActListView.selectedCommunicationRobotAct += DeselectIR;
    }

    private void OnDisable()
    {
        CommunicationActContainer.updateInteractionRuleList -= UpdateInteractionRuleListView;
        PresenterCommunicationRobotActListView.selectedCommunicationRobotAct -= DeselectIR;
    }
    /// <summary>
    /// Update listview by changes in the collection of IRs.
    /// </summary>
    public void UpdateInteractionRuleListView()
    {
        string selectedIRName = "";
        if (selectedInteractionRuleElement != null)
        {
            selectedIRName = selectedInteractionRuleElement.transform.Find("IRName").GetComponent<Text>().text;
        }

        foreach (Transform child in this.viewInteractionRuleListView.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (InteractionRule interactionRule in this.modelCommunicationContainer.GetInteractionRuleList())
        {
            GameObject irPrefab = Instantiate(viewElementInteractionRulePrefab, new Vector3(0, 0, 0), Quaternion.identity, viewInteractionRuleListView.transform) as GameObject;
            irPrefab.GetComponent<Button>().onClick.AddListener(delegate { SelectListViewItem(irPrefab); });
            irPrefab.GetComponent<PresenterInteractionRuleListElement>().Initialize(irPrefab, interactionRule);

            irPrefab.transform.Find("Status/Button").GetComponent<Button>().onClick.AddListener(delegate { RaiseStartIREvent(irPrefab); });


            if (interactionRule.name.Equals(selectedIRName))
            {
                selectedInteractionRuleElement = irPrefab;
            }
        }
    }

    /// <summary>
    /// Raise an event by starting an IR.
    /// </summary>
    /// <param name="interactionRule">Object of the IR to start.</param>
    private void RaiseStartIREvent(GameObject interactionRule)
    {
        newStartedInteractionRule?.Invoke(interactionRule.transform.GetComponent<PresenterInteractionRuleListElement>().modelInteractionRule);
    }

    /// <summary>
    /// Toggle-logic for selecting an IR in the listview and deselect the last selected one.
    /// </summary>
    /// <param name="listItem">Object in the listview that is selected.</param>
    private void SelectListViewItem(GameObject listItem)
    {
        if (selectedInteractionRuleElement != null)
        {
            selectedInteractionRuleElement.GetComponent<Image>().color = defaultIRColor;
        }

        if (listItem != null && selectedInteractionRuleElement != listItem)
        {
            listItem.GetComponent<Image>().color = Color.Lerp(listItem.GetComponent<Image>().color, Color.white, colorOffset);
            selectedInteractionRuleElement = listItem;

            selectedInteractionRule?.Invoke(modelCommunicationContainer.GetInteractionRuleByName(selectedInteractionRuleElement.transform.GetChild(1).GetComponent<Text>().text));
        }
        else
        {
            selectedInteractionRuleElement = null;
            selectedInteractionRule?.Invoke(null);
        }
    }

    /// <summary>
    /// Deselect the selected IR when a CRA is selected.
    /// </summary>
    /// <param name="obj">Selected CRA.</param>
    private void DeselectIR(CommunicationRobotAct obj)
    {
        if (selectedInteractionRuleElement != null)
        {
            selectedInteractionRuleElement.GetComponent<Image>().color = defaultIRColor;
        }

        selectedInteractionRuleElement = null;
    }
}