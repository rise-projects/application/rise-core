using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (CommunicationRobotAct List) and corresponding UI view.
/// </summary>
public class PresenterCommunicationRobotActListView : MonoBehaviour
{
    /// <summary>UI view for a list of CRAs.</summary>
    public GameObject viewCommunicationRobotActListView;

    /// <summary>UI view for one element in the list-view of CRAs.</summary>
    public GameObject viewElementCommunicationRobotActPrefab;

    /// <summary>Model representation of the list of all CRAs.</summary>
    public CommunicationActContainer modelCommunicationContainer;

    /// <summary>Model of selected CRA in listview.</summary>
    public static GameObject selectedCRAElement;

    /// <summary>Default color of CRA.</summary>
    private Color defaultCRAColor;

    /// <summary>Color offset for fading.</summary>
    public float colorOffset;

    /// <summary>Trigger selection of a CRA.</summary>
    public static event Action<CommunicationRobotAct> selectedCommunicationRobotAct;

    /// <summary>Trigger for a new started CRA.</summary>
    public static event Action<CommunicationRobotAct> newStartedCommunicationRobotAct;

    // Start is called before the first frame update
    void Start()
    {
        this.defaultCRAColor = viewElementCommunicationRobotActPrefab.GetComponent<Image>().color;
    }

    private void OnEnable()
    {
        CommunicationActContainer.updateCRAList += UpdateCommunicationRobotActListView;
        PresenterInteractionRuleListView.selectedInteractionRule += DeselectCRA;
    }

    private void OnDisable()
    {
        CommunicationActContainer.updateCRAList -= UpdateCommunicationRobotActListView;
        PresenterInteractionRuleListView.selectedInteractionRule -= DeselectCRA;
    }

    /// <summary>
    /// Update UI view by changes in the listview model.
    /// </summary>
    public void UpdateCommunicationRobotActListView()
    {
        foreach (Transform child in this.viewCommunicationRobotActListView.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (CommunicationRobotAct act in this.modelCommunicationContainer.communicationRobotActList)
        {
            GameObject trainingsPrefab = Instantiate(viewElementCommunicationRobotActPrefab, new Vector3(0, 0, 0), Quaternion.identity, viewCommunicationRobotActListView.transform) as GameObject;
            trainingsPrefab.GetComponent<Button>().onClick.AddListener(delegate { SelectListViewItem(trainingsPrefab); });
            trainingsPrefab.transform.GetComponent<PresenterCommunicationRobotActListElement>().Initialize(trainingsPrefab, act);

            trainingsPrefab.transform.Find("Status/Button").GetComponent<Button>().onClick.AddListener(delegate { RaiseStartCRAEvent(trainingsPrefab); });
        }
    }

    /// <summary>
    /// Raise event by starting a CRA.
    /// </summary>
    /// <param name="cra">Name of the CRA to start.</param>
    private void RaiseStartCRAEvent(GameObject cra)
    {
        newStartedCommunicationRobotAct?.Invoke(cra.transform.GetComponent<PresenterCommunicationRobotActListElement>().modelCommunicationRobotAct);
    }

    /// <summary>
    /// Toogle-logic for selecting an item in the listview and deselect the last selected.
    /// </summary>
    /// <param name="listItem">Object of the selected item in listview.</param>
    private void SelectListViewItem(GameObject listItem)
    {
        if (selectedCRAElement != null)
        {
            selectedCRAElement.GetComponent<Image>().color = defaultCRAColor;
        }

        if (listItem != null && selectedCRAElement != listItem)
        {
            listItem.GetComponent<Image>().color = Color.Lerp(listItem.GetComponent<Image>().color, Color.white, colorOffset);
            selectedCRAElement = listItem;

            selectedCommunicationRobotAct?.Invoke(modelCommunicationContainer.GetCommunicationRobotActByName(selectedCRAElement.transform.GetChild(1).GetComponent<Text>().text));
        }
        else
        {
            selectedCRAElement = null;
            selectedCommunicationRobotAct?.Invoke(null);
        }
    }

    /// <summary>
    /// Deselect CRA by selecting an IR.
    /// </summary>
    /// <param name="obj">Object of the selected IR.</param>
    private void DeselectCRA(InteractionRule obj)
    {
        if (selectedCRAElement != null)
        {
            selectedCRAElement.GetComponent<Image>().color = defaultCRAColor;
        }
        selectedCRAElement = null;
    }
}