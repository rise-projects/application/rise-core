using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (CommunicationRobotAct) and corresponding UI view.
/// </summary>
public class PresenterCommunicationRobotActListElement : MonoBehaviour
{
    /// <summary>UI view for a CRA.</summary>
    public GameObject viewCommunicationRobotAct { get; set; }

    /// <summary>Model representation of a CRA.</summary>
    public CommunicationRobotAct modelCommunicationRobotAct { get; set; }

    void OnDisable()
    {
        this.modelCommunicationRobotAct.communicationRobotActStatusChanged -= UpdateStatus;
    }

    void OnDestroy()
    {
        this.modelCommunicationRobotAct.communicationRobotActStatusChanged -= UpdateStatus;
    }

    /// <summary>
    /// Initialize view by model of the CRA.
    /// </summary>
    /// <param name="view">UI element for representing the model.</param>
    /// <param name="model">CRA model.</param>
    public void Initialize(GameObject view, CommunicationRobotAct model)
    {
        this.viewCommunicationRobotAct = view;
        this.modelCommunicationRobotAct = model;
        this.modelCommunicationRobotAct.communicationRobotActStatusChanged += UpdateStatus;

        viewCommunicationRobotAct.transform.Find("ActName").GetComponent<Text>().text = model.communicationRobotActName;
        viewCommunicationRobotAct.transform.Find("ActDescription").GetComponent<Text>().text = model.GetCRADescription();
        
        
        UpdateStatus();
    }

    /// <summary>
    /// Update the status-element in the view by status of the CRA.
    /// </summary>
    private void UpdateStatus()
    {
        viewCommunicationRobotAct.transform.Find("Status/Button").GetComponent<ButtonToggle>().SetButtonStatus(this.modelCommunicationRobotAct.communicationRobotActIsRunning);
    }
}