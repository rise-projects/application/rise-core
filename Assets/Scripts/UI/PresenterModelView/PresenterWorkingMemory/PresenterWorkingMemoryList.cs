using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Presenter between model (ParameterServer) and corresponding UI view.
/// </summary>
public class PresenterWorkingMemoryList : MonoBehaviour
{
    /// <summary>Handling of all entries in the WM.</summary>
    private WorkingMemoryController workingMemoryController { get; set; }

    /// <summary>UI view of all WM entries in one container, collection view.</summary>
    public GameObject workingMemoryContainerView;

    /// <summary>UI view for one WM entry.</summary>
    public GameObject workingMemoryEntryViewPrefab;

    /// <summary>UI field for searching in the list of WM entries.</summary>
    public InputField searchingInputField;

    /// <summary>UI field for adding new WM entries.</summary>
    public Button addMemoryEntry;

    // Start is called before the first frame update
    void Start()
    {
        this.workingMemoryController = WorkingMemoryController.Instance;
        searchingInputField.onValueChanged.AddListener(delegate { SetVisibilityOfViewsBySearch(searchingInputField.text); });
        addMemoryEntry.onClick.AddListener(delegate { AddEmptyMemoryEntry(); });
    }

    void OnEnable()
    {
        UITabButton.workingMemoryTabSelected += InitializeView;
        WorkingMemory.workingMemoryChangedByKey += UpdateViewEntry;
        WorkingMemoryController.workingMemoryCleared += ClearWorkingMemoryList;
    }

    void OnDisable()
    {
        UITabButton.workingMemoryTabSelected -= InitializeView;
        WorkingMemory.workingMemoryChangedByKey -= UpdateViewEntry;
        WorkingMemoryController.workingMemoryCleared -= ClearWorkingMemoryList;
    }

    /// <summary>
    /// Initialize view by WM list of all entries.
    /// </summary>
    public void InitializeView()
    {
        foreach(string memoryKey in workingMemoryController.GetWorkingMemoryKeys())
        {
            UpdateViewEntry(memoryKey);
        }
    }

    /// <summary>
    /// Update a single entry in the list view.
    /// </summary>
    /// <param name="key">Key of entry to be updated.</param>
    private void UpdateViewEntry(string key)
    {
        Transform view = GetViewEntryFromContainer(key);
        dynamic model = workingMemoryController.GetMemoryData(key);

        if (view != null)
        {
            if(model != null)
            {
                view.Find("Type").Find("TypeText").GetComponent<Text>().text = Convert.ToString(model.GetType());
                view.Find("InputValue").GetComponent<InputField>().text = Convert.ToString(model);
            }
            else
            {
                Destroy(view.gameObject);
            }
        }
        else
        {
            GameObject wmEntryGo = Instantiate(workingMemoryEntryViewPrefab, new Vector3(0, 0, 0), Quaternion.identity, workingMemoryContainerView.transform) as GameObject;

            wmEntryGo.transform.Find("Key").Find("KeyText").GetComponent<Text>().text = key;
            wmEntryGo.transform.Find("Type").Find("TypeText").GetComponent<Text>().text = Convert.ToString(model.GetType());
            wmEntryGo.transform.Find("InputValue").GetComponent<InputField>().text = Convert.ToString(model);

            wmEntryGo.transform.Find("Settings").Find("DeleteButton").GetComponent<Button>().onClick.AddListener(delegate { DeleteEntryByKey(key); });
            wmEntryGo.transform.Find("Settings").Find("UnlockButton").GetComponent<Button>().onClick.AddListener(delegate { ToggleLock(key); });
        }
    }

    public void ClearWorkingMemoryList()
    {
        foreach(Transform wmEntry in workingMemoryContainerView.transform)
        {
            Destroy(wmEntry.gameObject);
        }
    }

    /// <summary>
    /// Update the view field of the editing status of an WM entry.
    /// </summary>
    /// <param name="key">Represents the WM entry.</param>
    private void ToggleLock(string key)
    {
        Transform view = GetViewEntryFromContainer(key);

        if(view != null)
        {
            view.Find("InputValue").GetComponent<InputField>().interactable = !view.Find("InputValue").GetComponent<InputField>().interactable;
            view.Find("Settings").Find("UnlockButton").Find("ButtonImage").GetComponent<LockButtonToggle>().SetButtonStatus(view.Find("InputValue").GetComponent<InputField>().interactable);

            // Save value
            if(!view.Find("InputValue").GetComponent<InputField>().interactable)
            {
                workingMemoryController.AddMemoryEntryFromDomainTask("{" + key + ":" + view.Find("InputValue").GetComponent<InputField>().text + "}");
            }
        }
    }

    /// <summary>
    /// Delete an WM entry in the view.
    /// </summary>
    /// <param name="key">Represents the WM entry.</param>
    private void DeleteEntryByKey(string key)
    {
        workingMemoryController.DeleteWorkingMemoryEntryByKey(key);
    }

    /// <summary>
    /// Find WM element in listview by key.
    /// </summary>
    /// <param name="key">Key of the search entry.</param>
    /// <returns>Object in list view with key.</returns>
    private Transform GetViewEntryFromContainer(string key)
    {
        foreach (Transform child in this.workingMemoryContainerView.transform)
        {
            if (child.Find("Key").Find("KeyText").GetComponent<Text>().text == key)
            {
                return child;
            }
        }

        return null;
    }

    /// <summary>
    /// Disable visibility status by searching a specific element in the listview.
    /// </summary>
    /// <param name="key">Disable all entries without a match by the key.</param>
    private void SetVisibilityOfViewsBySearch(string key)
    {
        foreach (Transform child in this.workingMemoryContainerView.transform)
        {
            if (!child.Find("Key").Find("KeyText").GetComponent<Text>().text.Contains(key, StringComparison.OrdinalIgnoreCase))
            {
                child.gameObject.SetActive(false);
            }
            else
            {
                child.gameObject.SetActive(true);
            }
        }
    }

    /// <summary>
    /// Add a empty WM element into the listview.
    /// </summary>
    public void AddEmptyMemoryEntry()
    {
        if (addMemoryEntry.gameObject.transform.Find("AddButtonController").GetComponent<AddWMButtonToggle>().submit)
        {
            Tuple<string, string> kvp = addMemoryEntry.gameObject.transform.Find("AddButtonController").GetComponent<AddWMButtonToggle>().GetKeyValueEntry();

            if(kvp != null && kvp.Item1 != "" && kvp.Item2 != "")
            {
                workingMemoryController.AddMemoryEntryFromDomainTask("{" + kvp.Item1 + ":" + kvp.Item2 + "}");
            }
        }

        addMemoryEntry.gameObject.transform.Find("AddButtonController").GetComponent<AddWMButtonToggle>().SetButtonStatus();
    }
}