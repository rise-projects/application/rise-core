using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI toggle helper for switching the functions of adding WorkingMemory entries.
/// </summary>
public class AddWMButtonToggle : MonoBehaviour
{
    /// <summary>Button state of the add-entry button.</summary>
    public bool submit = false;

    /// <summary>UI view image for adding entries.</summary>
    private Sprite addImage;

    /// <summary>UI view image for submit new entries.</summary>
    private Sprite submitImage;

    /// <summary>UI view field for enter entrie's key.</summary>
    public InputField inputFieldKey;

    /// <summary>UI view field for enter entrie's value.</summary>
    public InputField inputFieldValue;

    void Awake()
    {
        addImage = Resources.Load<Sprite>("Images/AddEmptySprite");
        submitImage = Resources.Load<Sprite>("Images/AddSubmitSprite");
    }

    /// <summary>
    /// Set the state of the button by changing the sprite.
    /// </summary>
    public void SetButtonStatus()
    {
        this.submit = !submit;

        inputFieldKey.gameObject.SetActive(this.submit);
        inputFieldValue.gameObject.SetActive(this.submit);

        if (this.submit)
        {
            this.gameObject.GetComponent<Image>().sprite = submitImage;
        }
        else
        {
            this.gameObject.GetComponent<Image>().sprite = addImage;
        }
    }

    /// <summary>
    /// Create an WorkingMemory entry by the UI view fields.
    /// </summary>
    /// <returns>WorkingMemory entry as tuple.</returns>
    public Tuple<string, string> GetKeyValueEntry()
    {
        Tuple<string, string> entry = new Tuple<string, string>(this.inputFieldKey.text, this.inputFieldValue.text);

        this.inputFieldKey.text = "";
        this.inputFieldValue.text = "";

        return entry;
    }
}