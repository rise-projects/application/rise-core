using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Helper script for allowing switching a toggle between two images.
/// </summary>
public class ToggleBetweenImages : MonoBehaviour
{
    public Toggle toggle;
    public Image toggleImage;
    public Sprite toggleOffSprite;
    public Sprite toggleOnSprite;

    // Start is called before the first frame update
    void Start()
    {
        toggle.onValueChanged.AddListener(delegate
        {
            ToggleImage();
        });
    }

    private void ToggleImage()
    {
        if(!toggle.isOn)
        {
            toggleImage.sprite = toggleOffSprite;
        }
        else
        {
            toggleImage.sprite = toggleOnSprite;
        }
    }
}