using UnityEngine;

/// <summary>
/// Model helper for realizing informations about CA.
/// </summary>
public class CommunicationActAttributes : MonoBehaviour
{
    /// <summary>Name of the Communication Act.</summary>
    public string caName = "";

    /// <summary>Status if the Communication Act is an IR.</summary>
    public bool isInteractionRule = false;
}