using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI toggle button script for switching between functionalities and sprites.
/// </summary>
public class ButtonToggle : MonoBehaviour
{
    /// <summary>Status of the button.</summary>
    public bool running = false;

    /// <summary>UI view sprite for status 1.</summary>
    private Sprite playImage;

    /// <summary>UI view sprite for status 2.</summary>
    private Sprite cancelImage;

    void Awake()
    {
        playImage = Resources.Load<Sprite>("Images/PlaySprite");
        cancelImage = Resources.Load<Sprite>("Images/CancelSprite");
    }

    /// <summary>
    /// Set and update UI view status by given status.
    /// </summary>
    /// <param name="status">Status to be change.</param>
    public void SetButtonStatus(bool status)
    {
        this.running = status;

        if (this.running)
        {
            this.gameObject.GetComponent<Image>().sprite = cancelImage;
            this.gameObject.transform.parent.GetComponent<Image>().color = Color.red;
        }
        else
        {
            this.gameObject.GetComponent<Image>().sprite = playImage;
            this.gameObject.transform.parent.GetComponent<Image>().color = Color.white;
        }
    }
}