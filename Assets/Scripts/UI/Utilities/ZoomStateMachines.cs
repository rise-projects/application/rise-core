using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI helper for zooming in and out of state-machines in the tab-area.
/// </summary>
public class ZoomStateMachines : MonoBehaviour
{
    /// <summary>UI view button for zooming in the state-machines.</summary>
    public Button zoomInButton;

    /// <summary>UI view button for zooming out of the state-machines.</summary>
    public Button zoomOutButton;

    /// <summary>UI view transform that is rescaled by zooming.</summary>
    public Transform viewTransform;

    /// <summary>Increase zoom by button press.</summary>
    private float zoomIncrease = 0.25f;

    /// <summary>Limit min. of zooming.</summary>
    private float zoomLimitMin = 0.5f;

    /// <summary>Limit max. of zooming.</summary>
    private float zoomLimitMax = 3.0f;

    void Start()
    {
        zoomInButton.onClick.AddListener(delegate { ZoomIn(); });
        zoomOutButton.onClick.AddListener(delegate { ZoomOut(); });
    }

    /// <summary>
    /// Rescale the transform by zooming in/out by button-press.
    /// </summary>
    private void ZoomOut()
    {
        if (viewTransform.childCount > 0)
        {
            if (viewTransform.localScale.x - zoomIncrease >= zoomLimitMin)
            {
                viewTransform.localScale = new Vector3(viewTransform.localScale.x - zoomIncrease, viewTransform.localScale.y - zoomIncrease, viewTransform.localScale.z - zoomIncrease);
            }
        }
    }

    /// <summary>
    /// Rescale the transform by zooming in/out by button-press.
    /// </summary>
    private void ZoomIn()
    {
        if (viewTransform.childCount > 0)
        {
            if (viewTransform.localScale.x + zoomIncrease <= zoomLimitMax)
            {
                viewTransform.localScale = new Vector3(viewTransform.localScale.x + zoomIncrease, viewTransform.localScale.y + zoomIncrease, viewTransform.localScale.z + zoomIncrease);
            }
        }
    }
}