using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI status helper for receiving setting events, warnings, etc.
/// </summary>
public class SettingStatusHelper : MonoBehaviour
{
    /// <summary>UI view image for status.</summary>
    public Image settingStatusImage;

    /// <summary>UI view icon for status.</summary>
    public Image settingStatusSprite;

    /// <summary>UI view sprite for "working" settings.</summary>
    private Sprite statusOkSprite;

    /// <summary>UI view sprite for "not working" settings.</summary>
    private Sprite statusErrorSprite;

    void Start()
    {
        statusOkSprite = Resources.Load<Sprite>("Images/VerifiedSprite");
        statusErrorSprite = Resources.Load<Sprite>("Images/WarningSprite");

        UpdateStatus(false, "");
    }

    private void OnEnable()
    {
        UILoadingController.updateSettingStatus += UpdateStatus;
        IPChangeScript.ipWarningSetting += UpdateStatus;
    }

    private void OnDisable()
    {
        UILoadingController.updateSettingStatus -= UpdateStatus;
        IPChangeScript.ipWarningSetting -= UpdateStatus;
    }

    /// <summary>
    /// Update UI view status.
    /// </summary>
    /// <param name="status">Updated status.</param>
    /// <param name="statusText">Updated status information text.</param>
    private void UpdateStatus(bool status, string statusText)
    {
        if (status)
        {
            settingStatusImage.color = Color.green;
            settingStatusSprite.sprite = statusOkSprite;
        }
        else
        {
            settingStatusImage.color = Color.red;
            settingStatusSprite.sprite = statusErrorSprite;
        }
    }
}