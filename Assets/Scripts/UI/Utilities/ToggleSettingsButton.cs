using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI helper for enable/disable the settings view of the system.
/// </summary>
public class ToggleSettingsButton : MonoBehaviour
{
    /// <summary>UI view button for showing advanced settings.</summary>
    private Button button;

    /// <summary>UI view object to be enabled/disabled.</summary>
    public GameObject toggleObject;

    void Start()
    {
        this.button = GetComponent<Button>();
        this.button.onClick.AddListener(delegate { ToggleGameObject(); });
    }

    /// <summary>
    /// Toggle between active and inactive visibility state by button-press.
    /// </summary>
    private void ToggleGameObject()
    {
        toggleObject.SetActive(!toggleObject.activeSelf);
    }
}