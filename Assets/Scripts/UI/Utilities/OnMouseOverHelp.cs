using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Handling mouse events for helping functions in the UI.
/// </summary>
public class OnMouseOverHelp : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    /// <summary>UI view element for reference the position.</summary>
    private RectTransform canvas;

    /// <summary>UI view prefab for display informations in a popup.</summary>
    public GameObject prefabHelpPopup;

    /// <summary>UI view prefab of the container for showing informations.</summary>
    private GameObject popupGo;

    public void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
    }

    /// <summary>
    /// Mouse pointer enter the object where this script is attached to.
    /// Show popup with informations about the ui.
    /// </summary>
    /// <param name="eventData">Mouse pointer event data.</param>
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (popupGo == null)
        {
            popupGo = Instantiate(prefabHelpPopup, new Vector2(0, 0), Quaternion.identity) as GameObject;
            popupGo.transform.SetParent(canvas.transform, false);

            popupGo.transform.position = new Vector2(canvas.rect.width / 2, canvas.rect.height / 2);
        }
    }

    /// <summary>
    /// Mouse pointer exit the object where this script is attached to.
    /// </summary>
    /// <param name="eventData">Mouse pointer event data.</param>
    public void OnPointerExit(PointerEventData eventData)
    {
        if (popupGo != null)
        {
            Destroy(popupGo);
            popupGo = null;
        }
    }
}