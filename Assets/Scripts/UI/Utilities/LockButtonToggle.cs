using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// UI toggle script for changing functionalities for lock and unlock entries in the UI.
/// </summary>
public class LockButtonToggle : MonoBehaviour
{
    /// <summary>Status of the image UI button.</summary>
    public bool locked = true;

    /// <summary>UI view image for state 1 of the button.</summary>
    private Sprite lockImage;

    /// <summary>UI view image for state 2 of the button.</summary>
    private Sprite unlockImage;

    void Awake()
    {
        lockImage = Resources.Load<Sprite>("Images/LockSprite");
        unlockImage = Resources.Load<Sprite>("Images/UnlockSprite");
    }

    /// <summary>
    /// Set and update UI view status by model status.
    /// </summary>
    /// <param name="status">Status to be updated.</param>
    public void SetButtonStatus(bool status)
    {
        this.locked = status;

        if (!this.locked)
        {
            this.gameObject.GetComponent<Image>().sprite = lockImage;
        }
        else
        {
            this.gameObject.GetComponent<Image>().sprite = unlockImage;
        }
    }
}