using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Linq;

public class KeyboardControls : MonoBehaviour
{
    /// </summary>Parent gameobject from WM UI.</summary>
    public List<GameObject> UiPageList, UiTabList;
    /// </summary>Parent gameobject from InputField.</summary>
    public GameObject inputFieldKeyObject, inputFieldValueObject, inputFieldSearchObject;
    public GameObject graphy;
    bool submit;

    // Update is called once per frame
    public void Update()
    {
        keyboardControls();
    }

    /// <summary>
    /// manages keyboard control in rise ui 
    /// </summary>
    public void keyboardControls()
    {
        if(UiPageList[0].activeInHierarchy)//if parameterServer UI is active
        {
            submit = FindAnyObjectByType<AddWMButtonToggle>().submit;

            if((!submit)&&Input.GetKeyDown(KeyCode.Return))
            {
                inputFieldKeyObject.SetActive(true);
                inputFieldValueObject.SetActive(true);
                FindAnyObjectByType<AddWMButtonToggle>().SetButtonStatus();
                EventSystem.current.SetSelectedGameObject(null);
                inputFieldKeyObject.GetComponent<InputField>().Select();
            }
            else if(submit&&Input.GetKeyDown(KeyCode.Escape))
            {
                inputFieldKeyObject.SetActive(false);
                inputFieldValueObject.SetActive(false);
                FindAnyObjectByType<AddWMButtonToggle>().SetButtonStatus();
                EventSystem.current.SetSelectedGameObject(null);
            }

            if(submit&&Input.GetKeyDown(KeyCode.Return))
            {
                FindAnyObjectByType<PresenterWorkingMemoryList>().AddEmptyMemoryEntry();
            }

            if(submit&&Input.GetKeyDown(KeyCode.Tab))
            {
                if(inputFieldKeyObject.GetComponent<InputField>().isFocused)
                    inputFieldValueObject.GetComponent<InputField>().Select();
                else if(inputFieldValueObject.GetComponent<InputField>().isFocused)
                    inputFieldKeyObject.GetComponent<InputField>().Select();
                else
                    inputFieldKeyObject.GetComponent<InputField>().Select();
            }
            else if(!submit&&Input.GetKeyDown(KeyCode.Tab))
            {
                rotateBetweenUiPages();
            }

            if((Input.GetKey(KeyCode.LeftCommand)||Input.GetKey(KeyCode.LeftControl))&&Input.GetKeyDown(KeyCode.F))
            {
                EventSystem.current.SetSelectedGameObject(null);
                inputFieldSearchObject.GetComponent<InputField>().Select();
            }
        }
        else
        {
            if(Input.GetKeyDown(KeyCode.Tab))
            {
                rotateBetweenUiPages();
            }
        }

        if(Input.GetKeyDown(KeyCode.F3))
        {
            if(!graphy.activeInHierarchy)
            {
                graphy.SetActive(true);
            }
            else
            {
                graphy.SetActive(false);
            }
        }
    }

    public void rotateBetweenUiPages()
    {
        int lengthOfList = UiPageList.Count();
        int activePageIndex=0;
        int newActivePageIndex;

        for(int i=0; i<lengthOfList; i++)//get active page index
        {
            if(UiPageList[i].activeInHierarchy)
            {
                activePageIndex = i;
            }
        }

        if(activePageIndex>=UiPageList.Count()-1)//get new active page index
            newActivePageIndex=0;
        else
            newActivePageIndex=activePageIndex+1;

        UiPageList[activePageIndex].SetActive(false);//set new active page and change color from Tabs
        UiTabList[activePageIndex].GetComponent<Image>().color = new Color(255, 255, 255);

        UiPageList[newActivePageIndex].SetActive(true);
        UiTabList[newActivePageIndex].GetComponent<Image>().color = new Color(255, 255, 0);
        //active tab muss deselected werden
        //new active tab muss selected werden, sonst ändert sich die farbe auf weiß, wenn mit maus über anderen tab gehovert wird
        //die logik dafür ist in UITabButton, UITabGroup
    }
}
