using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// FileManager to handle open file browser script.
/// To run this as an executable you should enable the filepath Input text in ui.
/// </summary>
public class FileManager : MonoBehaviour
{
    /// <summary>Trigger for loading configs headless and on startup.</summary>
    public static event Action loadOnHeadlessMode; 

    /// <summary>UI field for the CRA path.</summary>
    public InputField configurationRootDirPathInput;
    
    /// <summary>Path to root directory of configurations.</summary>
    public string configurationRootDirPath;

    /// <summary>Initial path to subdirectory of the root directory.</summary>
    public string initConfigurationCategoryDirPath;

    /// <summary>Path to the project directory of configurations.</summary>
    public string initConfigurationProjectDirPath;

    /// <summary>Path to directory for XSD files.</summary>
    public string xsdValidationDirPath;

    /// <summary>Dropdown UI element for directory selection.</summary>
    public Dropdown configCategoryDropdown;

    /// <summary>Dropdown UI element for sub directory selection.</summary>
    public Dropdown configProjectDropdown;

    /// <summary>Root-element tag for configuration: CRA.</summary>
    public string craRootTag = "Agent-Dialogs";

    /// <summary>Root-element tag for configuration: IR.</summary>
    public string irRootTag = "InteractionRules";


    void Start()
    {
        this.configCategoryDropdown.onValueChanged.AddListener(delegate
        {
            SetSubDirectoryConfigurationDropdowns();
        });
    }

    /// <summary>
    /// Reload files related to a new root path.
    /// </summary>
    public void ChangeRootPath()
    {
        this.configurationRootDirPath = configurationRootDirPathInput.text;

        SetupConfigurationFilePathsFromCommandLine(this.configurationRootDirPath, this.initConfigurationCategoryDirPath, this.initConfigurationProjectDirPath, this.xsdValidationDirPath);
    }

    /// <summary>
    /// Load configuration files from command line.
    /// </summary>
    /// <param name="configurationPathRoot">Path to the root of the configuration repository.</param>
    /// <param name="configPathCategory">Path to the subdirectory in the configuration repository.</param>
    /// <param name="configPathProject">Path to the project directory in the subdirectory.</param>
    /// <param name="configPathXSDs">Path to the directory which contains the XSD files.</param>
    public void SetupConfigurationFilePathsFromCommandLine(string configurationPathRoot, string configPathCategory = "", string configPathProject = "", string configPathXSDs = "")
    {
        configurationPathRoot = configurationPathRoot.Replace("\\", "/");
        configPathCategory = configPathCategory.Replace("\\", "/");
        configPathProject = configPathProject.Replace("\\", "/");
        configPathXSDs = configPathXSDs.Replace("\\", "/");

        if (configurationPathRoot.EndsWith('/'))
        {
            configurationPathRoot = configurationPathRoot.Remove(configurationPathRoot.Length - 1, 1);
        }

        if (!configPathCategory.StartsWith('/'))
        {
            configPathCategory = '/' + configPathCategory;
        }
        
        if (!configPathProject.StartsWith('/'))
        {
            configPathProject = '/' + configPathProject;
        }
        
        configurationRootDirPathInput.text = configurationPathRoot;
        this.configurationRootDirPath = configurationRootDirPathInput.text;
        this.initConfigurationCategoryDirPath = configPathCategory;
        this.initConfigurationProjectDirPath = configPathProject;

        if(configPathXSDs == "")
        {
            configPathXSDs = configurationPathRoot + "/Validation";
        }

        this.xsdValidationDirPath = configPathXSDs;

        SetDirectoryConfigurationDropdowns(this.configurationRootDirPath, this.initConfigurationCategoryDirPath, this.initConfigurationProjectDirPath);

        loadOnHeadlessMode?.Invoke();
    }

    /// <summary>
    /// Get all files from a specific xml type.
    /// </summary>
    /// <param name="pathDir">Root directory for the search function.</param>
    /// <param name="xmlRootTag">Type to check for filter files.</param>
    /// <returns></returns>
    public List<string> GetConfigurationsFromPath(string pathDir, string xmlRootTag)
    {
        List<string> configs = new List<string>();

        foreach(string file in Directory.EnumerateFiles(pathDir, "*.xml", SearchOption.AllDirectories))
        {
            if(CheckTypeOfXMLFile(file, xmlRootTag))
            {
                configs.Add(file);
            }
        }

        return configs;
    }

    public List<string> GetJSONConfigurationsFromPath(string pathDir)
    {
        List<string> configs = new List<string>();

        foreach (string file in Directory.EnumerateFiles(pathDir, "*.json", SearchOption.AllDirectories))
        {
            configs.Add(file);
        }

        return configs;
    }

    /// <summary>
    /// Get all directories from a root path.
    /// </summary>
    /// <param name="pathRoot">Path to configuration repository.</param>
    /// <returns>List with all top sub directories.</returns>
    public List<string> GetConfigurationDirectoriesFromPath(string pathRoot)
    {
        List<string> dirs = new List<string>();

        foreach (string directory in Directory.EnumerateDirectories(pathRoot, "*", SearchOption.TopDirectoryOnly))
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(directory);

            if (!directoryInfo.Attributes.HasFlag(FileAttributes.Hidden))
            {
                dirs.Add(directory.Replace(pathRoot, "").Replace("\\", "/"));
            }            
        }

        return dirs;
    }

    /// <summary>
    /// Check if xml type is matching with a xml root tag. Filter files.
    /// </summary>
    /// <param name="pathToXML">Path to xml-file.</param>
    /// <param name="rootTag">Filter xml file by root-element tag.</param>
    /// <returns>Match if file and root tag.</returns>
    public bool CheckTypeOfXMLFile(string pathToXML, string rootTag)
    {
        var xdoc = XDocument.Load(pathToXML);

        if(xdoc != null && xdoc.Root != null)
        {
            if (xdoc.Root.Name.LocalName == rootTag)
            {
                return true;
            }
        }   

        return false;
    }

    /// <summary>
    /// Set dropdown UI element dropdowns by new root directory.
    /// </summary>
    /// <param name="pathRootDir">Path to configuration directory.</param>
    public void SetDirectoryConfigurationDropdowns(string pathRootDir, string pathDirectoryDir = "", string pathProjectDir = "")
    {
        List<string> directory_DropOptions = GetConfigurationDirectoriesFromPath(pathRootDir);
        List<string> valid_directory_options = new List<string>();
        configCategoryDropdown.ClearOptions();

        DirectoryInfo directoryInfo;
        foreach (string directory in directory_DropOptions)
        {
            directoryInfo = new DirectoryInfo(pathRootDir + directory);
            if (directoryInfo.GetDirectories().Length > 0)
            {
                valid_directory_options.Add(directory);
            }
        }

        configCategoryDropdown.AddOptions(valid_directory_options);

        string subdirectory = pathRootDir + configCategoryDropdown.options[configCategoryDropdown.value].text;

        List<string> subdirectory_DropOptions = GetConfigurationDirectoriesFromPath(subdirectory);

        configProjectDropdown.ClearOptions();
        configProjectDropdown.AddOptions(subdirectory_DropOptions);

        if(pathDirectoryDir != "")
        {
            this.configCategoryDropdown.value = this.configCategoryDropdown.options.FindIndex(option => option.text == pathDirectoryDir);
        }

        if(pathProjectDir != "")
        {
            this.configProjectDropdown.value = this.configProjectDropdown.options.FindIndex(option => option.text == pathProjectDir);
        }
    }

    /// <summary>
    /// Set subdirectory UI dropdown by selection from top directory dropdown.
    /// </summary>
    public void SetSubDirectoryConfigurationDropdowns()
    {
        string subdirectory = configurationRootDirPath + configCategoryDropdown.options[configCategoryDropdown.value].text;

        List<string> subdirectory_DropOptions = GetConfigurationDirectoriesFromPath(subdirectory);

        configProjectDropdown.ClearOptions();
        configProjectDropdown.AddOptions(subdirectory_DropOptions);

        loadOnHeadlessMode?.Invoke();
    }

    /// <summary>
    /// Get XML files by root tag as fitler from Dropdown selection.
    /// </summary>
    /// <returns>List with file paths to all XML files filtered by root tag.</returns>
    public List<string> GetXMLFilesFromSelectionByRootTag(string rootTag)
    {
        string category = configCategoryDropdown.options[configCategoryDropdown.value].text;
        string pathToProject = this.configurationRootDirPath + category + configProjectDropdown.options[configProjectDropdown.value].text;
        return GetConfigurationsFromPath(pathToProject, rootTag);
    }

    public List<string> GetJSONWMFilesFromSelection()
    {
        string category = configCategoryDropdown.options[configCategoryDropdown.value].text;
        string pathToProject = this.configurationRootDirPath + category + configProjectDropdown.options[configProjectDropdown.value].text;
        return GetJSONConfigurationsFromPath(pathToProject);
    }
}