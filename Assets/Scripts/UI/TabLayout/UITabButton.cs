using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Tab-Button logic with UI views and model representation of buttons.
/// </summary>
public class UITabButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    /// <summary>TabGroup model for allowing switching between different tabs.</summary>
    public UITabGroup tabGroup;

    /// <summary>UI view for the background of the tab button.</summary>
    public Image background;

    /// <summary>Button tab number, for reference in the tab group.</summary>
    public int tabNumber;

    /// <summary>Triggers the selection of a new tab-button in the tab-group.</summary>
    public static event Action<int> tabSelectedNumber;

    /// <summary>Triggers the deselection of a tab-button in the group.</summary>
    public static event Action<int> tabDeselectedNumber;

    /// <summary>WorkingMemory tab-button is selected.</summary>
    public static event Action workingMemoryTabSelected;

    /// <summary>TaskScheduler tab-button is selected.</summary>
    public static event Action taskSchedulerTabSelected;

    /// <summary>ActionServer tab-button is selected.</summary>
    public static event Action actionServerTabSelected;

    public void OnPointerClick(PointerEventData eventData)
    {
        tabGroup.OnTabSelected(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tabGroup.OnTabEnter(this);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tabGroup.OnTabExit(this);
    }

    void Start()
    {
        background = GetComponent<Image>();
    }

    /// <summary>
    /// Raise corresponding events by selecting different tab-buttons in the group.
    /// </summary>
    public void Select()
    {
        tabSelectedNumber?.Invoke(tabNumber);

        if(tabNumber == 1)
        {
            workingMemoryTabSelected?.Invoke();
        }
        if(tabNumber == 2)
        {
            taskSchedulerTabSelected?.Invoke();
        }
        if(tabNumber == 5)
        {
            actionServerTabSelected?.Invoke();
        }
    }

    /// <summary>
    /// Deselecting tabs in the group by selection.
    /// </summary>
    public void Deselect()
    {
        tabDeselectedNumber?.Invoke(tabNumber);
    }
}