using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Tab-Group logic for handling different views and switching UI views by events.
/// </summary>
public class UITabGroup : MonoBehaviour
{
    /// <summary>List of all possible tab-buttons.</summary>
    public List<UITabButton> tabButtons;

    /// <summary>UI color for idle state of a button.</summary>
    public Color tabIdle;

    /// <summary>UI color for hover state of a button.</summary>
    public Color tabHover;

    /// <summary>UI color for active state of a button.</summary>
    public Color tabActive;

    /// <summary>Reference to the selected tab-button.</summary>
    public UITabButton selectedTab;

    /// <summary>List of all views (tabs) in the group.</summary>
    public List<GameObject> objectsToSwap;

    private void Start()
    {
        Invoke("SelectTabOnStartup", 0.001f);
    }

    private void SelectTabOnStartup()
    {
        OnTabSelected(tabButtons[0]);
    }

    private void OnEnable()
    {
        PresenterCommunicationRobotActListView.selectedCommunicationRobotAct += ForceButtonCRA;
        PresenterInteractionRuleListView.selectedInteractionRule += ForceButtonIR;
    }

    private void OnDisable()
    {
        PresenterCommunicationRobotActListView.selectedCommunicationRobotAct -= ForceButtonCRA;
        PresenterInteractionRuleListView.selectedInteractionRule -= ForceButtonIR;
    }

    /// <summary>
    /// Add listener to button.
    /// </summary>
    /// <param name="button">Button in tab-group list.</param>
    public void Subscribe(UITabButton button)
    {
        if(tabButtons == null)
        {
            tabButtons = new List<UITabButton>();
        }

        tabButtons.Add(button);
    }

    public void OnTabEnter(UITabButton button)
    {
        ResetTabs();

        if(selectedTab == null || button != selectedTab)
        {
            button.background.color = tabHover;
        }
    }

    public void OnTabExit(UITabButton button)
    {
        ResetTabs();
    }

    public void OnTabSelected(UITabButton button)
    {
        if(selectedTab != null)
        {
            selectedTab.Deselect();
        }

        selectedTab = button;

        selectedTab.Select();

        ResetTabs();
        button.background.color = tabActive;

        int index = button.transform.GetSiblingIndex();

        for(int i = 0; i < objectsToSwap.Count; i++)
        {
            if(i == index)
            {
                objectsToSwap[i].SetActive(true);
            }
            else
            {
                objectsToSwap[i].SetActive(false);
            }
        }
    }

    /// <summary>
    /// Resets all tab-button colors to default.
    /// </summary>
    public void ResetTabs()
    {
        foreach(UITabButton button in tabButtons)
        {
            if (selectedTab != null && button == selectedTab) { continue; }

            button.background.color = tabIdle;
        }
    }

    /// <summary>
    /// Force button press during the start of a CRA.
    /// </summary>
    /// <param name="obj">CRA to start.</param>
    private void ForceButtonCRA(CommunicationRobotAct obj)
    {
        this.OnTabSelected(tabButtons[3]);
    }

    /// <summary>
    /// Force button press during the start of an IR.
    /// </summary>
    /// <param name="obj">IR to start.</param>
    private void ForceButtonIR(InteractionRule obj)
    {
        this.OnTabSelected(tabButtons[2]);
    }
}