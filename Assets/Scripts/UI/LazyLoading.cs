using System.Collections;
using UnityEngine;

public class LazyLoading : MonoBehaviour
{
    public GameObject workingMemoryDataList;
    public GameObject rosActionServerList;
    public GameObject taskSchedulerList;
    public int alwaysActiveEntrys;
    public float entryLoadingInterval;
    bool wmDataListActive = true;
    int numberOfActiveTaskSchedulerEntries = 30;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!workingMemoryDataList.activeInHierarchy && wmDataListActive)
        {
            //Debug.Log("deactivate entries in workingMemoryDataList");
            wmDataListActive = false;
            deactivateEntries(workingMemoryDataList);
        }
        else if(workingMemoryDataList.activeInHierarchy && !wmDataListActive)
        {
            //Debug.Log("activate entries in workingMemoryDataList");
            wmDataListActive = true;
            StartCoroutine(activateEntries(workingMemoryDataList));
        }

        if(taskSchedulerList.activeInHierarchy)
        {
            manageEntrys(taskSchedulerList, numberOfActiveTaskSchedulerEntries);
        }
    }

    public void deactivateEntries(GameObject uiList)
    {
        for (int i = alwaysActiveEntrys; i < uiList.transform.childCount; i++)
        {
            Transform child = uiList.transform.GetChild(i);
            child.gameObject.SetActive(false);
        }
    }

    public IEnumerator activateEntries(GameObject uiList)
    {
        for (int i = alwaysActiveEntrys; i < uiList.transform.childCount; i++)
        {
            if(!uiList.activeInHierarchy)
            {
                yield break;
            }
        
            Transform child = uiList.transform.GetChild(i);
            child.gameObject.SetActive(true);

            yield return new WaitForSeconds(entryLoadingInterval);
        }
    }
    /// <summary>
    /// makes the first n entries from a list active, disables the rest
    /// </summary>
    /// <param name="uiList"></param> name of ui list gameobject
    /// <param name="n"></param> number of max entries that are beeing shown at the same time
    public void manageEntrys(GameObject uiList, int n)
    {
        int childCount = uiList.transform.childCount;

        for (int i = 0; i < childCount; i++)
        {
            if(i <= n)
            {
                Transform child = uiList.transform.GetChild(i);
                child.gameObject.SetActive(true);
            }
            else
            {
                Transform child = uiList.transform.GetChild(i);
                child.gameObject.SetActive(false);
            }
           
        }
    }
}
