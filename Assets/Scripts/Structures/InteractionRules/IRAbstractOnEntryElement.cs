/// <summary>
/// Abstract onEntry element.
/// </summary>
abstract public class IRAbstractOnEntryElement
{
    public int ordering { get; set; }
}