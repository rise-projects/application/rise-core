using System;
using System.Collections.Generic;
/// <summary>
/// Representation of an IR State.
/// </summary>
public class IRState
{
    /// <summary>Unique ID as name of the state.</summary>
    public string id { get; set; }
    /// <summary>On Entry as collection of all OnEntryElements like conditions and functions.</summary>
    public IROnEntry onEntry { get; set; }
    /// <summary>List of all usable transitions from this state</summary>
    public List<IRTransition> transitionList { get; set; }
    /// <summary>bool, if this state is currently the active</summary>
    public bool stateIsRunning { get; set; }

    public event Action irStatusChanged;

    public IRState(string id, IROnEntry onEntry, List<IRTransition> transitionList)
    {
        this.id = id;
        this.onEntry = onEntry;
        this.transitionList = transitionList;
        this.stateIsRunning = false;
    }

    /// <summary>
    /// Displays informations of this state. Useful for debugging.
    /// </summary>
    /// <returns>Readable String with informations of the state</returns>
    public string GetInformations()
    {
        return "State ID: " + id + "\n\n" +
               "OnEntry:\n" + this.GetOnEntryInformations() + "\n\n" + 
               "Transitions:\n" + this.GetTransitionInformations();
    }

    /// <summary>
    /// Informations of the OnEntry element for the hovering inside the UI.
    /// </summary>
    /// <returns>Readable String with line breaks</returns>
    public string GetOnEntryInformations()
    {
        string fct = "";
        if (onEntry != null)
        {
            foreach (IRAbstractOnEntryElement element in onEntry.onEntryElements)
            {
                if (element is IRFunction)
                {
                    fct += "Fct: " + ((IRFunction)element).ordering + " " + ((IRFunction)element).functionType.ToString() + " -> " + ((IRFunction)element).referenceBehavior + "\n";
                }
                else if (element is IRIFCond)
                {
                    fct += "If: " + ((IRIFCond)element).ordering + " " + ((IRIFCond)element).conditionStatement.ToString() + "\n";

                    foreach (IRFunction function in ((IRIFCond)element).functionList)
                    {
                        fct += "Cond-Fct: " + function.ordering + " " + function.functionType.ToString() + " -> " + function.referenceBehavior + "\n";
                    }
                }

            }
            
            if(onEntry.onEntryElements.Count > 0)
            {
                fct = fct[0..^1];
            }
        }

        return fct;
    }
    /// <summary>
    /// Informations of the Transitions for the hovering inside the UI.
    /// </summary>
    /// <returns>Readable String with line breaks</returns>
    public string GetTransitionInformations()
    {
        string trs = "";
        if (transitionList.Count > 0)
        {
            foreach (IRTransition transition in transitionList)
            {
                trs += transition.eventTopic + " -> " + transition.target + "\n";
            }
            trs = trs.Substring(0, trs.Length - 1);
        }

        return trs;
    }

    /// <summary>
    /// Changes the status to the given one and informs per invoke event about the changes.
    /// </summary>
    /// <param name="status">New status of the state.</param>
    public void ChangeIRStatus(bool status)
    {
        this.stateIsRunning = status;
        irStatusChanged?.Invoke();
    }
    /// <summary>
    /// Resets all transitions to false.
    /// </summary>
    public void DeactivateAllTransitions()
    {
        foreach(IRTransition transition in transitionList)
        {
            transition.SetActive(false);
        }
    }
}