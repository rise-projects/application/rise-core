using System.Collections.Generic;
/// <summary>
/// IR if conditions as implementation of the OnEntryElement class.
/// </summary>
public class IRIFCond : IRAbstractOnEntryElement
{
    /// <summary>Conditional statement as String.</summary>
    public string conditionStatement { get; set; }
    /// <summary>List of all functions inside the conditional statement.</summary>
    public List<IRFunction> functionList { get; set; }

    public IRIFCond(string conditionStatement, List<IRFunction> functionList, int order)
    {
        this.conditionStatement = conditionStatement;
        this.functionList = functionList;
        this.ordering = order;
    }
}