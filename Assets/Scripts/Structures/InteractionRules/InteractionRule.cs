using System;
using System.Collections.Generic;
using UnityEngine;

public class InteractionRule
{
    /// <summary>name of the interaction rule. Also used as unique ID</summary>
    public string name { get; set; }
    /// <summary>Initial starting state.</summary>
    public string initialState { get; set; }
    /// <summary>decides, if the IR should be started with the start of the application. Especially interesting if the application runs headless.</summary>
    public bool autostart { get; set; }
    /// <summary>current priority of the IR. Decides who is allowed to use a matching eventTopic for the transition if multiple IRs are waiting for the same.</summary>
    public int priority { get; set; }
    /// <summary>In the XML defined initial priority.</summary>
    public int initialPriority { get; set; }
    /// <summary>List of all states with onEntry functions and transitions.</summary>
    public List<IRState> stateList { get; set; }
    /// <summary>Currently active state.</summary>
    public IRState currentState { get; private set; }
    /// <summary>Previous state in interaction rule.</summary>
    public IRState previousState { get; private set; }
    /// <summary>Bool, if the IR is active.</summary>
    public bool interactionRuleIsRunning { get; set; }

    public event Action updateInteractionRule;

    public InteractionRule(string name, string initialState, List<IRState> stateList, bool autostart = false, int priority = 0)
    {
        this.name = name;
        this.initialState = initialState;
        this.autostart = autostart;
        this.priority = priority;
        this.stateList = stateList;
        this.initialPriority = priority;
        this.currentState = null;
        this.previousState = null;
        this.interactionRuleIsRunning = false;

        if (!autostart)
        {
            this.priority = -1;
        }
    }

    /// <summary>
    /// Getter for a specific state by name.
    /// </summary>
    /// <param name="irStateName">name of the state</param>
    /// <returns>The state, if exists, else null.</returns>
    /// <exception cref="InvalidOperationException">Throws an exception, if the state do not exists.</exception>
    public IRState GetIRStateByName(string irStateName)
    {
        foreach (IRState state in stateList)
        {
            if (state.id.Equals(irStateName))
            {
                return state;
            }
        }

        return null;
        throw new InvalidOperationException("No state with id found!");
    }

    /// <summary>
    /// Change of the currently active state with an event invoking, that the IR has changed.
    /// </summary>
    /// <param name="state">name of the new active state.</param>
    public void ChangeCurrentState(IRState state)
    {
        this.previousState = this.currentState;
        this.currentState = state;
        updateInteractionRule?.Invoke();
        
    }
    /// <summary>
    /// Description of the IR for the UI
    /// </summary>
    /// <returns>Returns a string with readable informations about the current state and priority of the IR.</returns>
    public string GetInteractionRuleDescription()
    {
        string currentState = "NA";
        if (this.currentState != null)
        {
            currentState = this.currentState.id;
        }
        
        string resOutput = "Current State: " + currentState + " and Priority: " + priority;

        return resOutput;
    }

    /// <summary>
    /// Resets the priority to the initial priority.
    /// </summary>
    public void ResetPriority()
    {
        this.priority = initialPriority;
    }

    /// <summary>
    /// Resets and deactivates the IR and invokes an event to inform about the changes.
    /// </summary>
    public void DeactivateInteractionRule()
    {
        this.priority = -1;
        this.interactionRuleIsRunning = false;
        this.currentState = GetIRStateByName(initialState);

        foreach(IRState state in stateList)
        {
            state.DeactivateAllTransitions();
        }

        updateInteractionRule?.Invoke();
    }

    /// <summary>
    /// Activates the IR by setting the priority to the initial priority, setting the IR to running and invoking an event to inform about the changes.
    /// </summary>
    public void ActivateInteractionRule()
    {
        this.ResetPriority();
        this.interactionRuleIsRunning = true;

        updateInteractionRule?.Invoke();
    }
}