using System.Collections.Generic;
/// <summary>
/// onEntry elements as data struct for all onEntryElements inside a state.
/// </summary>
public class IROnEntry
{
    /// <summary>List of all onEntryElements</summary>
    public List<IRAbstractOnEntryElement> onEntryElements { get; set; }

    public IROnEntry(List<IRAbstractOnEntryElement> onEntryElements)
    {
        this.onEntryElements = onEntryElements;
        this.onEntryElements.Sort((ele1, ele2) => ele1.ordering.CompareTo(ele2.ordering));
    }
}