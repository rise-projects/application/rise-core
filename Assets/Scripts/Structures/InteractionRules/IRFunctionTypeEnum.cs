using UnityEngine;
/// <summary>
/// Enums for the IR Functions.
/// </summary>
public class IRFunctionTypeEnum
{
    /// <summary>
    /// defines the implemented IR functions
    /// </summary>
    public enum IRFunctionType
    {
        none,
        startInteractionRule,
        startCommunicationRobotAct,
        stopInteractionRule,
        stopCommunicationRobotAct,
        raiseEventTopic,
        assignValue
    }

    /// <summary>
    /// Getter for sprites depended on the functionType
    /// </summary>
    /// <param name="functionType">FunctionType the sprite is needed for.</param>
    /// <returns>A sprite for the requested functionType</returns>
    public static Sprite GetSpriteForIRFunction(IRFunctionType functionType)
    {
        switch (functionType)
        {
            case IRFunctionType.startInteractionRule:
                return Resources.Load<Sprite>("Images/HumanSprite");
            case IRFunctionType.startCommunicationRobotAct:
                return Resources.Load<Sprite>("Images/RobotSprite");
            case IRFunctionType.raiseEventTopic:
                return Resources.Load<Sprite>("Images/RaiseEventSprite");
            case IRFunctionType.assignValue:
                return Resources.Load<Sprite>("Images/AssignValueSprite");
            default:
                break;
        }

        return null;
    }

    public static bool IsTimeCritical(IRFunctionType type)
    {
        if(type.Equals(IRFunctionType.startInteractionRule) || type.Equals(IRFunctionType.raiseEventTopic))
        {
            return true;
        }

        return false;
    }
}