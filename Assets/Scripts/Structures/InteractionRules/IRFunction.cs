using static IRFunctionTypeEnum;
using static TaskManagementEnum;

/// <summary>
/// IR Function as implementation of OnEntryElement
/// </summary>
public class IRFunction : IRAbstractOnEntryElement
{
    /// <summary>Defines the function type.</summary>
    public IRFunctionType functionType { get; set; }
    /// <summary>Defines the behavior like the Name of the CRA that should be started.</summary>
    public string referenceBehavior { get; set; }
    /// <summary>blocking/none as executing type.</summary>
    public ExecutionType executionType { get; set; }

    public IRFunction(IRFunctionType functionType, string referenceBehavior, ExecutionType executionType, int order)
    {
        this.functionType = functionType;
        this.referenceBehavior = referenceBehavior;
        this.executionType = executionType;
        this.ordering = order;
    }
}