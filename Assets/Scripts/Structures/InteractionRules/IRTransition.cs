using System;
using UnityEngine;

/// <summary>
/// Transition of an Interaction Rule
/// </summary>
public class IRTransition
{
    /// <summary>eventTopic to take this transition</summary>
    public string eventTopic { get; set; }
    /// <summary>Target of this transition</summary>
    public string target { get; set; }

    /// <summary>Bool, if this transition is currently taken, to highlight it inside the UI.</summary>
    public bool isActive { get; set; }

    public event Action<IRTransition> transitionStatusChanged;

    public IRTransition(string eventTopic, string target)
    {
        this.eventTopic = eventTopic;
        this.target = target;
    }
    /// <summary>
    /// Setter of the status of the transition. On changes, invokes an event.
    /// </summary>
    /// <param name="status">New status.</param>
    public void SetActive(bool status)
    {
        this.isActive = status;
        transitionStatusChanged?.Invoke(this);
    }
}