using Newtonsoft.Json;
using System;
using System.Collections.Generic;

public class WorkingMemory
{
    public Dictionary<string, dynamic> dictionaryData { get; }

    public static event Action<string> workingMemoryChangedByKey;

    public WorkingMemory()
    {
        dictionaryData = new Dictionary<string, dynamic>();
    }

    public void WriteIntoMemory(string jsonEntry)
    {
        Dictionary<string, dynamic> parsedEntries = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(jsonEntry);

        foreach (KeyValuePair<string, dynamic> kvp in parsedEntries)
        {
            if (this.dictionaryData.ContainsKey(kvp.Key))
            {
                dictionaryData[kvp.Key] = (dynamic) kvp.Value;
            }
            else
            {
                dictionaryData.Add(kvp.Key, (dynamic) kvp.Value);
            }

            workingMemoryChangedByKey?.Invoke(kvp.Key);
        }
    }

    public void DeleteWorkingMemoryEntry(string key)
    {
        dictionaryData.Remove(key);
        workingMemoryChangedByKey?.Invoke(key);
    }

    public string GetEntryAsJSON(string key)
    {
        Dictionary<string, dynamic> res = new Dictionary<string, dynamic>();

        if (this.dictionaryData.ContainsKey(key))
        {
            res.Add(key, dictionaryData[key]);
        }
        else
        {
            res.Add(key, null);
        }

        return JsonConvert.SerializeObject(res);
    }

    public dynamic GetMemoryValue(string key)
    {
        if (this.dictionaryData.ContainsKey(key))
        {
            return dictionaryData[key];
        }

        return null;
    }

    public string GetMemoryValueAsString(string key)
    {
        if (GetMemoryValue(key) != null)
        {
            string res = GetMemoryValue(key);
            return res;
        }

        return null;
    }

    public string DebugWorkingMemory()
    {
        foreach (KeyValuePair<string, dynamic> mem in dictionaryData)
        {
            return (mem.Key + " --- " + mem.Key + " --- " + mem.Value.GetType());
        }

        return "";
    }
}