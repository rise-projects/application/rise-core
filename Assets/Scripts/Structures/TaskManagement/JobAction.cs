using UnityEngine;

/// <summary>
/// JobAction structure to store informations of a running coroutine
/// </summary>
public class JobAction 
{
    public int actionID;
    public ActionEnum.Action actionType;
    public Coroutine runningCoroutine;

    /// <summary>
    /// JobAction calling initial constructor
    /// </summary>
    /// <param name="actionID">Identify running action by id</param>
    /// <param name="actionType">Define the running action type</param>
    /// <param name="runningCoroutine">Running coroutine object</param>
    public JobAction(int actionID, ActionEnum.Action actionType, Coroutine runningCoroutine)
    {
        this.actionID = actionID;
        this.actionType = actionType;
        this.runningCoroutine = runningCoroutine;
    }
}