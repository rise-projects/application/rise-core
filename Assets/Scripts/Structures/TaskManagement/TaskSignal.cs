﻿using UnityEngine;
using static TaskManagementEnum;

/// <summary>
/// Signal of the Task.
/// </summary>
public class TaskSignal
{
    /// <summary>Uniqe ID of the Task</summary>
    public int taskID { get; set; }
    /// <summary>
    /// Getter for the signal of the Task.
    /// </summary>
    public ProcessSignal signal { get; set; }

    public TaskSignal(int taskID, ProcessSignal signal)
    {
        this.taskID = taskID;
        this.signal = signal;
    }

    public TaskSignal()
    {
        this.taskID = -1;
    }
}