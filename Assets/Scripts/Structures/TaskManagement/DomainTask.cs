using UnityEngine;
using static TaskManagementEnum;
/// <summary>
/// DomainTask for incoming task through the action server
/// </summary>
public class DomainTask 
{
    /// <summary>Input unit with informations about the function type and behavior which should be executed.</summary>
    public DomainInputUnit domainInputUnit { get; set; }
    /// <summary>Coroutine of the task, to cancel the execution if necessary.</summary>
    public Coroutine runningCoroutine { get; set; }
    /// <summary>Current status of the Task</summary>
    public TaskStatus taskStatus { get; set; }

    public DomainTask(DomainInputUnit domainInputUnit, Coroutine runningCoroutine, TaskStatus taskStatus)
    {
        this.domainInputUnit = domainInputUnit;
        this.runningCoroutine = runningCoroutine;
        this.taskStatus = taskStatus;
    }
}