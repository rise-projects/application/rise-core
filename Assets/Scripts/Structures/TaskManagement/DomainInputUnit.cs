using static IRFunctionTypeEnum;
using static TaskManagementEnum;
/// <summary>
/// DomainInputUnit, as class for incoming input through the action server.
/// </summary>
public class DomainInputUnit
{
    /// <summary>Unique ID</summary>
    public string domainInputID { get; set; }
    /// <summary>Behavior name like the name of the IR which should be started.</summary>
    public string behaviorName { get; set; }
    /// <summary>Function type of the input, like startInteractionRule.</summary>
    public IRFunctionType functionType { get; set; }
    /// <summary>blocking/non as execution type</summary>
    public ExecutionType executionType { get; set; }

    public DomainInputUnit()
    {

    }

    public DomainInputUnit(string domainInputID, string behaviorName, IRFunctionType functionType, ExecutionType executionType)
    {
        this.domainInputID = domainInputID;
        this.behaviorName = behaviorName;
        this.functionType = functionType;
        this.executionType = executionType;
    }
    /// <summary>
    /// Debugging function.
    /// </summary>
    /// <returns>Readable string with all informations.</returns>
    public string DebugInteractionTask()
    {
        return this.domainInputID + ", " + this.behaviorName + ", " + this.functionType.ToString() + ", " + this.executionType.ToString();
    }
}