using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Structure as collection of all CommunicationActs in the system.
/// Contains all CommunicationRobotActs and InteractionRules from the parser.
/// </summary>
public class CommunicationActContainer : MonoBehaviour
{
    /// <summary>List of all CRAs.</summary>
    public List<CommunicationRobotAct> communicationRobotActList;

    /// <summary>List of all IRs.</summary>
    private List<InteractionRule> interactionRuleList;

    /// <summary>UI view button for sorting the lists.</summary>
    public Toggle sortToggle;

    /// <summary>Trigger an update of the IR listview.</summary>
    public static event Action updateInteractionRuleList;

    /// <summary>Trigger an update of the CRA listview.</summary>
    public static event Action updateCRAList;

    void Start()
    {
        this.communicationRobotActList = new List<CommunicationRobotAct>();
        this.interactionRuleList = new List<InteractionRule>();

        //Value Change Event for the Sort Toggle
        sortToggle.onValueChanged.AddListener(delegate {
            SortToggleValueChanged(sortToggle);
        });
    }

    /// <summary>
    /// Function for sorting the lists by button-press.
    /// </summary>
    /// <param name="sortToggle"></param>
    void SortToggleValueChanged(Toggle sortToggle)
    {
        //Trigger sort, after switch to true
        if (sortToggle.isOn)
            SortInteractionRuleList();
    }

    /// <summary>
    /// Adding interaction rule to list.
    /// </summary>
    /// <param name="interactionRule">IR to add.</param>
    public void AddInteractionRuleToList(InteractionRule interactionRule)
    {
        this.interactionRuleList.Add(interactionRule);
        this.SortInteractionRuleList();
    }

    /// <summary>
    /// Adding a range of IRs to list.
    /// </summary>
    /// <param name="interactionRules">List of IRs to add.</param>
    public void AddRangeInteractionRuleToList(List<InteractionRule> interactionRules)
    {
        this.interactionRuleList.AddRange(interactionRules);
        this.SortInteractionRuleList();
    }

    /// <summary>
    /// Get the list of all IRs.
    /// </summary>
    /// <returns>List of all IRs.</returns>
    public List<InteractionRule> GetInteractionRuleList()
    {
        return new List<InteractionRule>(this.interactionRuleList);
    }

    /// <summary>
    /// Change the priority of one IR in the list of IRs.
    /// </summary>
    /// <param name="interactionRule">IR to change.</param>
    /// <param name="priority">Priority to set.</param>
    public void ChangePriorityInteractionRule(InteractionRule interactionRule, int priority)
    {
        GetInteractionRuleByName(interactionRule.name).priority = priority;
        SortInteractionRuleList();
    }

    /// <summary>
    /// Get all active IRs.
    /// </summary>
    /// <returns>List with all active IRs.</returns>
    public List<InteractionRule> GetActiveInteractionRules()
    {
        return interactionRuleList.Where(x => x.interactionRuleIsRunning).ToList();
    }

    /// <summary>
    /// Set an IR to the top of the list, set the highest priority to the IR.
    /// </summary>
    /// <param name="interactionRule">IR to update.</param>
    public void SetInteractionRuleOnTop(InteractionRule interactionRule)
    {
        int maxPriority = interactionRuleList.Max(t => t.priority);
        interactionRule.priority = maxPriority + 1;

        SortInteractionRuleList();
    }

    /// <summary>
    /// Deactive an IR.
    /// </summary>
    /// <param name="interactionRule">IR to deactivate.</param>
    public void DeactivateInteractionRule(InteractionRule interactionRule)
    {
        interactionRule.currentState.ChangeIRStatus(false);
        interactionRule.DeactivateInteractionRule();

        SortInteractionRuleList();
    }

    /// <summary>
    /// Deactive an IR.
    /// </summary>
    /// <param name="interactionRuleName">IR name to deactivate.</param>
    public void DeactivateInteractionRule(string interactionRuleName)
    {
        InteractionRule ir = GetInteractionRuleByName(interactionRuleName);

        if (ir.interactionRuleIsRunning)
        {
            ir.currentState.ChangeIRStatus(false);
            ir.DeactivateInteractionRule();
        }

        SortInteractionRuleList();
    }

    /// <summary>
    /// Active an IR.
    /// </summary>
    /// <param name="interactionRule">IR to activate.</param>
    public void ActivateInteractionRule(InteractionRule interactionRule)
    {
        interactionRule.ActivateInteractionRule();

        SortInteractionRuleList();
    }

    /// <summary>
    /// Check if the name of a Communication Act is a CRA.
    /// </summary>
    /// <param name="communicationRobotActName">Name of the CA.</param>
    /// <returns>True if name is found in list of CRAs.</returns>
    public bool IsCommunicationRobotAct(string communicationRobotActName)
    {
        foreach (CommunicationRobotAct communicationRobotAct in this.communicationRobotActList)
        {
            if (communicationRobotAct.communicationRobotActName == communicationRobotActName) return true;
        }

        return false;
    }

    /// <summary>
    /// Check if the name of a Communication Act is a IR.
    /// </summary>
    /// <param name="interactionRuleName">Name of the CA.</param>
    /// <returns>True if name is found in the list of IRs.</returns>
    public bool IsInteractionRule(string interactionRuleName)
    {
        foreach (InteractionRule interactionRule in this.interactionRuleList)
        {
            if (interactionRule.name == interactionRuleName) return true;
        }

        return false;
    }

    /// <summary>
    /// Get trainings act by trainings name.
    /// </summary>
    /// <param name="communicationRobotActName">Given trainings name as identifier.</param>
    /// <returns>Return TrainingsAct by trainings name.</returns>
    public CommunicationRobotAct GetCommunicationRobotActByName(string communicationRobotActName)
    {
        foreach (CommunicationRobotAct communicationRobotAct in this.communicationRobotActList)
        {
            if (communicationRobotAct.communicationRobotActName == communicationRobotActName) return communicationRobotAct;
        }

        throw new InvalidOperationException("CommunicationRobotAct not found");
    }

    /// <summary>
    /// Get the IR by the name.
    /// </summary>
    /// <param name="interactionRuleName">Name of the IR.</param>
    /// <returns>IR found in the list of IRs.</returns>
    /// <exception cref="InvalidOperationException">Exception if IR is not found.</exception>
    public InteractionRule GetInteractionRuleByName(string interactionRuleName)
    {
        foreach (InteractionRule interactionRule in this.interactionRuleList)
        {
            if (interactionRule.name == interactionRuleName) return interactionRule;
        }

        throw new InvalidOperationException("InteractionRule not found");
    }

    /// <summary>
    /// Sort list of IR by priority.
    /// </summary>
    public void SortInteractionRuleList()
    {
        if (this.sortToggle.isOn)
        {
            interactionRuleList.Sort((ir1, ir2) => ir2.priority.CompareTo(ir1.priority));

        }
        updateInteractionRuleList?.Invoke();
    }

    /// <summary>
    /// Clear the whole list of IRs.
    /// </summary>
    public void ClearInteractionRuleList()
    {
        this.interactionRuleList.Clear();
        updateInteractionRuleList?.Invoke();
    }

    /// <summary>
    /// Clear the whole list of CRAs.
    /// </summary>
    public void ClearCommunicationRobotActList()
    {
        this.communicationRobotActList.Clear();
        updateCRAList?.Invoke();
    }

    /// <summary>
    /// Add a range of CRAs to the list of the CRAs.
    /// </summary>
    /// <param name="craList">List of CRAs.</param>
    public void AddRangeCommunicationRobotActsToList(List<CommunicationRobotAct> craList)
    {
        this.communicationRobotActList.AddRange(craList);
        updateCRAList?.Invoke();
    }

    /// <summary>
    /// Rename duplicates after parsing from InteractionRule list!
    /// </summary>
    public void UniqueInteractionRuleList()
    {
        var distinctItems = this.interactionRuleList.GroupBy(x => x.name)
              .SelectMany(g => g.Skip(1))
              .ToList();

        foreach (InteractionRule item in distinctItems)
        {
            int number = 1;
            string suffix = "_" + number;

            InteractionRule irDup = this.interactionRuleList.Find((x) => x.name.Equals(item.name));

            while (this.interactionRuleList.Find((x) => x.name.Equals(item.name + suffix)) != null)
            {
                number++;
                suffix = "_" + number;
            }

            irDup.name = irDup.name + suffix;
        }

        updateInteractionRuleList?.Invoke();
    }

    /// <summary>
    /// Rename duplicates after parsing from CommunicationRobotAct list!
    /// </summary>
    public void UniqueCommunicationRobotActList()
    {
        var distinctItems = this.communicationRobotActList.GroupBy(x => x.communicationRobotActName)
              .SelectMany(g => g.Skip(1))
              .ToList();

        foreach (CommunicationRobotAct item in distinctItems)
        {
            int number = 1;
            string suffix = "_" + number;

            CommunicationRobotAct craDup = this.communicationRobotActList.Find((x) => x.communicationRobotActName.Equals(item.communicationRobotActName));

            while (this.communicationRobotActList.Find((x) => x.communicationRobotActName.Equals(item.communicationRobotActName + suffix)) != null)
            {
                number++;
                suffix = "_" + number;
            }

            craDup.communicationRobotActName = craDup.communicationRobotActName + suffix;
        }

        updateCRAList?.Invoke();
    }
}