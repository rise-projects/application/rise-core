using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Structure to store informations about a Communication Robot Act.
/// </summary>
public class CommunicationRobotAct
{
    /// <summary>Name of the CRA.</summary>
    public string communicationRobotActName;

    /// <summary>List of all actions in this CRA.</summary>
    public List<BehaviorAction> actionList;

    /// <summary>Status if the CRA is running.</summary>
    public bool communicationRobotActIsRunning;

    /// <summary>Trigger for update the CRA informations in the view.</summary>
    public event Action communicationRobotActStatusChanged;

    /// <summary>
    /// CRA initial constructor.
    /// </summary>
    /// <param name="craName">Identify trainign by name.</param>
    /// <param name="actionDict">List with all actions.</param>
    public CommunicationRobotAct(string craName, List<BehaviorAction> actionDict)
    {
        this.communicationRobotActName = craName;
        this.actionList = new List<BehaviorAction>();
        this.communicationRobotActIsRunning = false;

        foreach (BehaviorAction action in actionDict)
        {
            this.actionList.Add(action);
        }

        SortBehaviorDictByWaitRef();
    }

    /// <summary>
    /// Print all informations of the CRA.
    /// </summary>
    public void DebugTrainingsAct()
    {
        string resOutput = "";
        foreach (BehaviorAction action in GetDictSortedByActionID())
        {
            resOutput = resOutput + action.DebugBehaviorAction() + '\n' + '\n';
        }

        Debug.Log("DialogAct: " + communicationRobotActName + '\n' + '\n' + resOutput);
    }

    /// <summary>
    /// Get CRA descriptions as informations in text.
    /// </summary>
    /// <returns>Return description as string for visualization in the UI.</returns>
    public string GetCRADescription()
    {
        string resOutput = "Contains: " + actionList.Count + "x actions";

        return resOutput;
    }

    /// <summary>
    /// Sorting dict and all actions by waiting reference.
    /// </summary>
    public void SortBehaviorDictByWaitRef()
    {
        actionList.Sort(delegate (BehaviorAction act1, BehaviorAction act2)
        {
            if (Mathf.Max(act1.waitForActions.ToArray()) < Mathf.Max(act2.waitForActions.ToArray()))
            {
                return -1;
            }
            else
            {
                if (act1.actionID < act2.actionID)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        });
    }

    /// <summary>
    /// Sorting all actions by action id.
    /// </summary>
    /// <returns>Returns list with all actions in sorted order.</returns>
    public List<BehaviorAction> GetDictSortedByActionID()
    {
        return this.actionList.OrderBy(a => a.actionID).ToList();
    }

    /// <summary>
    /// Get the behavior action object by given action id.
    /// </summary>
    /// <param name="actionID">Translate the given action id to an action object.</param>
    /// <returns>Action by action id.</returns>
    public BehaviorAction GetBehaviorActionByID(int actionID)
    {
        foreach (BehaviorAction action in this.actionList)
        {
            if (actionID == action.actionID)
            {
                return action;
            }
        }

        return null;
    }

    /// <summary>
    /// Check CRA running status.
    /// </summary>
    /// <returns>Check if CRA is running and return true if its running.</returns>
    public bool IsCRAIsRunning()
    {
        foreach (BehaviorAction action in this.actionList)
        {
            if (action.actionIsRunning)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Get all running actions of the CRA.
    /// </summary>
    /// <returns>List with all running actions.</returns>
    public List<BehaviorAction> GetRunningActions()
    {
        List<BehaviorAction> runningActions = new List<BehaviorAction>();

        foreach (BehaviorAction action in this.actionList)
        {
            if (action.actionIsRunning)
            {
                runningActions.Add(action);
            }
        }

        if (runningActions.Count() == 0)
        {
            runningActions.Add(this.actionList.Last());
        }

        return runningActions;
    }

    /// <summary>
    /// Set and update the status of a CRA.
    /// </summary>
    /// <param name="status">Updated status.</param>
    public void SetCRAStatus(bool status)
    {
        this.communicationRobotActIsRunning = status;
        communicationRobotActStatusChanged?.Invoke();
    }
}