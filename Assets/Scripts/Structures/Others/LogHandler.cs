using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script for logging system activities and debug informations.
/// </summary>
public class LogHandler : MonoBehaviour
{
    /// <summary>Singleton instance for log handler.</summary>
    public static LogHandler Instance { get; set; }

    /// <summary>File path to log file.</summary>
    private string logFilePath;

    /// <summary>Max. queue size of logging outputs into file.</summary>
    private int queueSize = 25;

    /// <summary>List of loglist elements as buffer.</summary>
    public List<string> logList;

    /// <summary>UI view element for showing logging informations.</summary>
    public Text debugUITextElement;

    /// <summary>UI view button for showing full log stack.</summary>
    public Button showFullLogStack;

    /// <summary>UI view container for showing console as logging output in the UI.</summary>
    public GameObject consoleLayout;

    /// <summary>UI view text for full debug informations.</summary>
    public Text fullConsoleTextbox;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    void Start()
    {
        logList = new List<string>();

        logFilePath = Application.dataPath + "/log.data";
        if (File.Exists(logFilePath))
        {
            File.Delete(logFilePath);
        }

        showFullLogStack.onClick.AddListener(VisualizeFullLog);
    }

    private void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    private void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    /// <summary>
    /// Handle log informations, receiving informations from standard unity debug functions.
    /// </summary>
    /// <param name="logString">Informations for logging.</param>
    /// <param name="stackTrace">Full stack trace.</param>
    /// <param name="type">Logging type.</param>
    private void HandleLog(string logString, string stackTrace, LogType type)
    {
        if (type == LogType.Error)
        {
            logList.Add("<color=red>[" + type + "] : " + logString + "</color>");
            consoleLayout.SetActive(true);
        }
        else
        {
            logList.Add("[" + type + "] : " + logString);
        }
        WriteLogFile(logList[logList.Count - 1]);

        if (logList.Count > queueSize)
        {
            logList.RemoveAt(0);
        }

        UpdateConsoleLogs();
    }

    /// <summary>
    /// Write log into file and console.
    /// </summary>
    /// <param name="log">Logging information text.</param>
    public void WriteLog(string log)
    {
        HandleLog(log, "", LogType.Log);
    }

    /// <summary>
    /// Update UI view console for logging.
    /// </summary>
    private void UpdateConsoleLogs()
    {
        debugUITextElement.text = logList[logList.Count - 1];

        if (consoleLayout.activeSelf)
        {
            fullConsoleTextbox.text = "";
            foreach (string log in logList)
            {
                fullConsoleTextbox.text = log + '\n' + fullConsoleTextbox.text;
            }

            fullConsoleTextbox.text = fullConsoleTextbox.text.Remove(fullConsoleTextbox.text.Length - 1);
        }
    }

    /// <summary>
    /// Show full UI view elements.
    /// </summary>
    private void VisualizeFullLog()
    {
        consoleLayout.SetActive(!consoleLayout.activeSelf);
        UpdateConsoleLogs();
    }

    /// <summary>
    /// Write logging informations into file only.
    /// </summary>
    /// <param name="logString">String to write into file.</param>
    public void WriteLogFile(string logString)
    {
        using (StreamWriter writer = new StreamWriter(logFilePath, true))
        {
            writer.WriteLine(logString);
        }
    }
}