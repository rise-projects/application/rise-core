using UnityEngine;

/// <summary>
/// Inspector config file for creating inspector GUI.
/// </summary>
public class StartUpConfigs : MonoBehaviour
{
    /// <summary>Systems execution type.</summary>
    public string executionMode;

    /// <summary>Path to CRAs.</summary>
    public string pathToConfigurationRoot;

    /// <summary>Path to IRs.</summary>
    public string pathToConfigurationCategory;

    /// <summary>Path to WM.</summary>
    public string pathToConfigurationProject;

    /// <summary>Path to XSD files for automated validation.</summary>
    public string pathToConfigurationXSD;

    /// <summary>IP address to ROS-Master.</summary>
    public string ipAddress;

    /// <summary>Default robots name for topics.</summary>
    public string robotName;
}