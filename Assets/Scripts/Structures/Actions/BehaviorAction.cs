﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Abstract class BehaviorAction to define all necessary and joint attributes/methods for all actions.
/// </summary>
abstract public class BehaviorAction
{
    /// <summary>Identifier of actions.</summary>
    public int actionID { get; set; }

    /// <summary>Type of action.</summary>
    public ActionEnum.Action actionState { get; set; }

    /// <summary>Handle the controlling and execution of an action.</summary>
    public BehaviorActionController behaviorActionController;

    /// <summary>List of conditions before executing this action.</summary>
    public List<int> waitForActions { get; set; }

    /// <summary>Running state of the action.</summary>
    public bool actionIsRunning { get; set; }

    /// <summary>List of attributes that can be replaced by variables from the WorkingMemory.</summary>
    public Dictionary<string, string> replaceableAttributes;

    /// <summary>Triggers view and controller that status has changed.</summary>
    public event Action actionStatusChanged;

    /// <summary>Trigger attribute value changed for update.</summary>
    public event Action attributeValueChanged;

    /// <summary>
    /// Initialize all object attributes by given attribute list.
    /// </summary>
    /// <param name="attributeList">Parsed List with values into object attributes.</param>
    public virtual void Initialize(List<string> attributeList)
    {
        this.waitForActions = new List<int>();
        this.replaceableAttributes = new Dictionary<string, string>();

        this.actionIsRunning = false;
        if(GameObject.Find("ActionController") != null)
        {
            this.behaviorActionController = GameObject.Find("ActionController").GetComponent<BehaviorActionController>();
        }
        actionID = int.Parse(attributeList[0]);
        foreach (string id in attributeList[1].Split(','))
        {
            waitForActions.Add(int.Parse(id));
        }
    }

    /// <summary>
    /// Set status of an action.
    /// </summary>
    /// <param name="status">Status update.</param>
    public void SetActionStatus(bool status)
    {
        this.actionIsRunning = status;
        actionStatusChanged?.Invoke();
    }

    /// <summary>
    /// Returns a list with all needed xml tags for this object.
    /// </summary>
    /// <returns>List with all XML tags for this object.</returns>
    public virtual List<string> GetAttributeTags()
    {
        List<string> tagList = new List<string>();
        tagList.Add("actionID");
        tagList.Add("waitForActions");
        return tagList;
    }

    /// <summary>
    /// Showing informations of the object. Using for debugging and show state machine informations.
    /// </summary>
    /// <returns>String with informations about the object.</returns>
    public virtual string DebugBehaviorAction()
    {
        string waitIds = "";
        foreach (int id in this.waitForActions)
        {
            waitIds += id.ToString() + ", ";
        }
        waitIds = waitIds.Substring(0, waitIds.Length - 2);

        return "ActionID: " + this.actionID + '\n' +
                "Action: " + this.actionState + '\n' +
                "WaitForActions: " + waitIds + '\n';
    }

    /// <summary>
    /// Get all textual informations and descriptions of an attribute of this action.
    /// </summary>
    /// <returns>Informations by key and value of descriptions.</returns>
    public abstract Dictionary<string, string> GetAttributeDescriptions();

    /// <summary>
    /// Start and run this action in coroutine management environment.
    /// Implement waiting condition here!
    /// </summary>
    /// <returns>Running coroutine object.</returns>
    public abstract IEnumerator StartBehaviorAction();

    public void CancelBehaviorAction()
    {
        this.behaviorActionController.PublishCancelAction(this);
    }

    public bool IsAttributeValueVariable(string value)
    {
        if(value.StartsWith("%") && value.EndsWith("%"))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Notify the view that model attributes has changed.
    /// </summary>
    public void NotifyModelChanges()
    {
        this.attributeValueChanged?.Invoke();
    }
}