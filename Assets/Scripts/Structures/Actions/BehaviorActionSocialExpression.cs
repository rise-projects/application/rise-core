using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BehaviorAction for SocialExpressions with all special attributes for the behavior.
/// Implements the BehaviorAction abstract class.
/// </summary>
public class BehaviorActionSocialExpression : BehaviorAction
{
    /// <summary>SocialExpressions are coded with predefined numbers in the robot-wrappers.</summary>
    public int value { get; set; }

    /// <summary>Duration of the execution of a social expression.</summary>
    public int duration { get; set; }

    /// <summary>Intensitiy of how highlighted (low-high) the social expression is!</summary>
    public int intensity { get; set; }

    /// <summary>
    /// Initialize the parsed attributes from XML into the action model.
    /// </summary>
    /// <param name="attributeList">Parsed XML attributes.</param>
    public override void Initialize(List<string> attributeList)
    {
        base.Initialize(attributeList);
        actionState = ActionEnum.Action.SocialExpression;

        if (IsAttributeValueVariable(attributeList[2]))
        {
            replaceableAttributes.Add("value", attributeList[2]);
        }
        else
        {
            value = int.Parse(attributeList[2]);
        }

        if (IsAttributeValueVariable(attributeList[3]))
        {
            replaceableAttributes.Add("duration", attributeList[3]);
        }
        else
        {
            duration = int.Parse(attributeList[3]);
        }

        if (IsAttributeValueVariable(attributeList[4]))
        {
            replaceableAttributes.Add("intensity", attributeList[4]);
        }
        else
        {
            intensity = int.Parse(attributeList[4]);
        }
    }

    /// <summary>
    /// Get all attribute tags for the XML parser.
    /// </summary>
    /// <returns>XML tags for parsing.</returns>
    public override List<string> GetAttributeTags()
    {
        List<string> tagList = base.GetAttributeTags();
        tagList.Add("value");
        tagList.Add("duration");
        tagList.Add("intensity");

        return tagList;
    }

    /// <summary>
    /// Debug and prints all parsed values from the action.
    /// </summary>
    /// <returns>Informations as text.</returns>
    public override string DebugBehaviorAction()
    {
        return base.DebugBehaviorAction() + '\n' +
            "Social Expression Value: " + this.value + '\n' +
            "Duration: " + this.duration + '\n' +
            "Intensity: " + this.intensity;
    }

    /// <summary>
    /// Starts an action in a Coroutine.
    /// </summary>
    /// <returns>Coroutine for paralell execution.</returns>
    public override IEnumerator StartBehaviorAction()
    {
        this.SetActionStatus(true);

        behaviorActionController.PublishTranslatedAction(this);
        yield return new WaitForSeconds(duration / 1000);

        this.SetActionStatus(false);
    }

    /// <summary>
    /// Gets textual informations about attributes for displaying the informations in the UI.
    /// </summary>
    /// <returns>Key-Value informations about the attributes.</returns>
    public override Dictionary<string, string> GetAttributeDescriptions()
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("Social Expression Value", this.value.ToString());
        attributes.Add("Duration", this.duration.ToString());
        attributes.Add("Intensity", this.intensity.ToString());

        return attributes;
    }
}