using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BehaviorAction for Wait with all special attributes for the task to let the system wait.
/// </summary>
public class BehaviorActionWait : BehaviorAction
{
    /// <summary>Time in ms for waiting.</summary>
    public int duration { get; set; }

    /// <summary>
    /// Initialize the parsed attributes from XML into the action model.
    /// </summary>
    /// <param name="attributeList">Parsed XML attributes.</param>
    public override void Initialize(List<string> attributeList)
    {
        base.Initialize(attributeList);

        if (IsAttributeValueVariable(attributeList[2]))
        {
            replaceableAttributes.Add("duration", attributeList[2]);
        }
        else
        {
            duration = int.Parse(attributeList[2]);
        }

        this.actionState = ActionEnum.Action.Wait;
    }

    /// <summary>
    /// Get all attribute tags for the XML parser.
    /// </summary>
    /// <returns>XML tags for parsing.</returns>
    public override List<string> GetAttributeTags()
    {
        List<string> tagList = base.GetAttributeTags();
        tagList.Add("duration");

        return tagList;
    }

    /// <summary>
    /// Debug and prints all parsed values from the action.
    /// </summary>
    /// <returns>Informations as text.</returns>
    public override string DebugBehaviorAction()
    {
        return base.DebugBehaviorAction() + '\n' +
            "Duration: " + this.duration.ToString();
    }

    /// <summary>
    /// Gets textual informations about attributes for displaying the informations in the UI.
    /// </summary>
    /// <returns>Key-Value informations about the attributes.</returns>
    public override Dictionary<string, string> GetAttributeDescriptions()
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("Duration", this.duration.ToString());

        return attributes;
    }

    /// <summary>
    /// Starts an action in a Coroutine.
    /// </summary>
    /// <returns>Coroutine for paralell execution.</returns>
    public override IEnumerator StartBehaviorAction()
    {
        this.SetActionStatus(true);

        behaviorActionController.PublishTranslatedAction(this);
        yield return new WaitForSeconds(this.duration / 1000);

        this.SetActionStatus(false);
    }
}