﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BehaviorAction for Animations with all special attributes for the animations.
/// Implements the BehaviorAction abstract class.
/// </summary>
public class BehaviorActionAnimation : BehaviorAction
{
    /// <summary>Animations are described by predefined animations in the robot-wrapper. Selection of an animation.</summary>
    public int target { get; set; }

    /// <summary>The number of repetitions of an animation.</summary>
    public int repetitions { get; set; }

    /// <summary>The time of each repetition.</summary>
    public int duration_each { get; set; }

    /// <summary>The velocity of the animations execution.</summary>
    public float scale { get; set; }

    /// <summary>
    /// Initialize the parsed attributes from XML into the action model.
    /// </summary>
    /// <param name="attributeList">Parsed XML attributes.</param>
    public override void Initialize(List<string> attributeList)
    {
        base.Initialize(attributeList);
        actionState = ActionEnum.Action.Animation;

        if (IsAttributeValueVariable(attributeList[2]))
        {
            replaceableAttributes.Add("target", attributeList[2]);
        }
        else
        {
            target = int.Parse(attributeList[2]);
        }

        if (IsAttributeValueVariable(attributeList[3]))
        {
            replaceableAttributes.Add("repetitions", attributeList[3]);
        }
        else
        {
            repetitions = int.Parse(attributeList[3]);
        }

        if (IsAttributeValueVariable(attributeList[4]))
        {
            replaceableAttributes.Add("duration_each", attributeList[4]);
        }
        else
        {
            duration_each = int.Parse(attributeList[4]);
        }

        if (IsAttributeValueVariable(attributeList[5]))
        {
            replaceableAttributes.Add("scale", attributeList[5]);
        }
        else
        {
            scale = int.Parse(attributeList[5], System.Globalization.CultureInfo.InvariantCulture);
        }
    }

    /// <summary>
    /// Get all attribute tags for the XML parser.
    /// </summary>
    /// <returns>XML tags for parsing.</returns>
    public override List<string> GetAttributeTags()
    {
        List<string> tagList = base.GetAttributeTags();
        tagList.Add("target");
        tagList.Add("repetitions");
        tagList.Add("duration_each");
        tagList.Add("scale");

        return tagList;
    }

    /// <summary>
    /// Debug and prints all parsed values from the action.
    /// </summary>
    /// <returns>Informations as text.</returns>
    public override string DebugBehaviorAction()
    {
        return base.DebugBehaviorAction() + '\n' +
            "Target: " + this.target + '\n' +
            "Repetitions: " + this.repetitions + '\n' +
            "Duration_each: " + this.duration_each + '\n' +
            "Scale: " + this.scale;
    }

    /// <summary>
    /// Starts an action in a Coroutine.
    /// </summary>
    /// <returns>Coroutine for paralell execution.</returns>
    public override IEnumerator StartBehaviorAction()
    {
        this.SetActionStatus(true);

        behaviorActionController.PublishTranslatedAction(this);
        yield return new WaitForSeconds((repetitions * duration_each) / 1000);

        this.SetActionStatus(false);
    }

    /// <summary>
    /// Gets textual informations about attributes for displaying the informations in the UI.
    /// </summary>
    /// <returns>Key-Value informations about the attributes.</returns>
    public override Dictionary<string, string> GetAttributeDescriptions()
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("Animation Value", this.target.ToString());
        attributes.Add("Repetitions", this.repetitions.ToString());
        attributes.Add("Duration", this.duration_each.ToString());
        attributes.Add("Scale", this.scale.ToString());

        return attributes;
    }
}