﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// BehaviorAction for Emotions with all special attributes for the emotions.
/// Implements the BehaviorAction abstract class.
/// </summary>
public class BehaviorActionEmotion : BehaviorAction
{
    /// <summary>Emotions are coded with predefined numbers in the robot-wrappers.</summary>
    public int value { get; set; }

    /// <summary>Duration of execution of an emotion.</summary>
    public int duration { get; set; }

    /// <summary>
    /// Initialize the parsed attributes from XML into the action model.
    /// </summary>
    /// <param name="attributeList">Parsed XML attributes.</param>
    public override void Initialize(List<string> attributeList)
    {
        base.Initialize(attributeList);
        actionState = ActionEnum.Action.Emotion;

        if (IsAttributeValueVariable(attributeList[2]))
        {
            replaceableAttributes.Add("value", attributeList[2]);
        }
        else
        {
            value = int.Parse(attributeList[2]);
        }

        if (IsAttributeValueVariable(attributeList[3]))
        {
            replaceableAttributes.Add("duration", attributeList[3]);
        }
        else
        {
            duration = int.Parse(attributeList[3]);
        }
    }

    /// <summary>
    /// Get all attribute tags for the XML parser.
    /// </summary>
    /// <returns>XML tags for parsing.</returns>
    public override List<string> GetAttributeTags()
    {
        List<string> tagList = base.GetAttributeTags();
        tagList.Add("value");
        tagList.Add("duration");

        return tagList;
    }

    /// <summary>
    /// Debug and prints all parsed values from the action.
    /// </summary>
    /// <returns>Informations as text.</returns>
    public override string DebugBehaviorAction()
    {
        return base.DebugBehaviorAction() + '\n' +
            "EmotionValue: " + this.value + '\n' +
            "Duration: " + this.duration;
    }

    /// <summary>
    /// Starts an action in a Coroutine.
    /// </summary>
    /// <returns>Coroutine for paralell execution.</returns>
    public override IEnumerator StartBehaviorAction()
    {
        this.SetActionStatus(true);

        behaviorActionController.PublishTranslatedAction(this);
        yield return new WaitForSeconds(duration / 1000);

        this.SetActionStatus(false);
    }

    /// <summary>
    /// Gets textual informations about attributes for displaying the informations in the UI.
    /// </summary>
    /// <returns>Key-Value informations about the attributes.</returns>
    public override Dictionary<string, string> GetAttributeDescriptions()
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("Emotion Value", this.value.ToString());
        attributes.Add("Duration", this.duration.ToString());

        return attributes;
    }
}