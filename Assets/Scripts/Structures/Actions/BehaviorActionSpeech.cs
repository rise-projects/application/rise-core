﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

/// <summary>
/// BehaviorAction for Speech with all special attributes for the task to speech.
/// Implements the BehaviorAction abstract class.
/// </summary>
public class BehaviorActionSpeech : BehaviorAction
{
    /// <summary>The text-to-speech message for the robots speech synthesis.</summary>
    public string message { get; set; }

    /// <summary>The text-to-speech message after replacing optional variables from the WM.</summary>
    public string producedMessage { get; set; }

    /// <summary>
    /// Initialize the parsed attributes from XML into the action model.
    /// </summary>
    /// <param name="attributeList">Parsed XML attributes.</param>
    public override void Initialize(List<string> attributeList)
    {
        base.Initialize(attributeList);
        actionState = ActionEnum.Action.Speech;

        MatchCollection matches = Regex.Matches(attributeList[2], @"\%.*?\%", RegexOptions.IgnoreCase);
        foreach (Match pat in matches)
        {
            if(!replaceableAttributes.ContainsKey("message"))
            {
                replaceableAttributes.Add("message", pat.Value);
            }
            else 
            {
                replaceableAttributes["message"] = replaceableAttributes["message"]+"|"+pat.Value;
            }
        }

        message = attributeList[2];
        producedMessage = message;
    }

    /// <summary>
    /// Get all attribute tags for the XML parser.
    /// </summary>
    /// <returns>XML tags for parsing.</returns>
    public override List<string> GetAttributeTags()
    {
        List<string> tagList = base.GetAttributeTags();
        tagList.Add("message");

        return tagList;
    }

    /// <summary>
    /// Debug and prints all parsed values from the action.
    /// </summary>
    /// <returns>Informations as text.</returns>
    public override string DebugBehaviorAction()
    {
        return base.DebugBehaviorAction() + '\n' +
            "Message: " + this.producedMessage;
    }

    /// <summary>
    /// Starts an action in a Coroutine.
    /// </summary>
    /// <returns>Coroutine for paralell execution.</returns>
    public override IEnumerator StartBehaviorAction()
    {
        this.SetActionStatus(true);

        behaviorActionController.PublishTranslatedAction(this);

        if (behaviorActionController.uiComponentController.IsDebugMode())
        {
            yield return new WaitForSeconds(ActionEnum.APPROXIMATION_TIME);
        }
        else
        {
            yield return new WaitUntil(() => behaviorActionController.speechActionClient.IsSpeechActionFinished());
        }

        this.SetActionStatus(false);
    }

    /// <summary>
    /// Gets textual informations about attributes for displaying the informations in the UI.
    /// </summary>
    /// <returns>Key-Value informations about the attributes.</returns>
    public override Dictionary<string, string> GetAttributeDescriptions()
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("Message", this.message.ToString());

        return attributes;
    }
}