﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.EventSystems.EventTrigger;

/// <summary>
/// BehaviorAction for LookAt with all special attributes for the task to look at objects.
/// </summary>
public class BehaviorActionLookAt : BehaviorAction
{
    /// <summary>The x coordinate in worl-space for looking on an point.</summary>
    public double pointx { get; set; }

    /// <summary>The y coordinate in worl-space for looking on an point.</summary>
    public double pointy { get; set; }

    /// <summary>The z coordinate in worl-space for looking on an point.</summary>
    public double pointz { get; set; }

    /// <summary>The angle of the head for looking on an point.</summary>
    public float roll { get; set; }

    /// <summary>
    /// Initialize the parsed attributes from XML into the action model.
    /// </summary>
    /// <param name="attributeList">Parsed XML attributes.</param>
    public override void Initialize(List<string> attributeList)
    {
        base.Initialize(attributeList);

        if (IsAttributeValueVariable(attributeList[2]))
        {
            replaceableAttributes.Add("pointx", attributeList[2]);
        }
        else
        {
            pointx = double.Parse(attributeList[2], System.Globalization.CultureInfo.InvariantCulture);
        }

        if (IsAttributeValueVariable(attributeList[3]))
        {
            replaceableAttributes.Add("pointy", attributeList[3]);
        }
        else
        {
            pointy = double.Parse(attributeList[3], System.Globalization.CultureInfo.InvariantCulture);
        }

        if (IsAttributeValueVariable(attributeList[4]))
        {
            replaceableAttributes.Add("pointz", attributeList[4]);
        }
        else
        {
            pointz = double.Parse(attributeList[4], System.Globalization.CultureInfo.InvariantCulture);
        }

        if (IsAttributeValueVariable(attributeList[5]))
        {
            replaceableAttributes.Add("roll", attributeList[5]);
        }
        else
        {
            roll = float.Parse(attributeList[5], System.Globalization.CultureInfo.InvariantCulture);
        }
   
        this.actionState = ActionEnum.Action.LookAt;
    }

    /// <summary>
    /// Get all attribute tags for the XML parser.
    /// </summary>
    /// <returns>XML tags for parsing.</returns>
    public override List<string> GetAttributeTags()
    {
        List<string> tagList = base.GetAttributeTags();
        tagList.Add("pointx");
        tagList.Add("pointy");
        tagList.Add("pointz");
        tagList.Add("roll");

        return tagList;
    }

    /// <summary>
    /// Debug and prints all parsed values from the action.
    /// </summary>
    /// <returns>Informations as text.</returns>
    public override string DebugBehaviorAction()
    {
        return base.DebugBehaviorAction() + '\n' +
            "TargetPoint: " + this.pointx.ToString() + ", " + this.pointy.ToString() + ", " + this.pointz.ToString() + '\n' +
            "Roll: " + this.roll;
    }

    /// <summary>
    /// Starts an action in a Coroutine.
    /// </summary>
    /// <returns>Coroutine for paralell execution.</returns>
    public override IEnumerator StartBehaviorAction()
    {
        this.SetActionStatus(true);

        behaviorActionController.PublishTranslatedAction(this);
        yield return new WaitForSeconds(ActionEnum.APPROXIMATION_TIME);

        this.SetActionStatus(false);
    }

    /// <summary>
    /// Gets textual informations about attributes for displaying the informations in the UI.
    /// </summary>
    /// <returns>Key-Value informations about the attributes.</returns>
    public override Dictionary<string, string> GetAttributeDescriptions()
    {
        Dictionary<string, string> attributes = new Dictionary<string, string>();

        attributes.Add("Target X", this.pointx.ToString());
        attributes.Add("Target Y", this.pointy.ToString());
        attributes.Add("Target Z", this.pointz.ToString());
        attributes.Add("Roll", this.roll.ToString());

        return attributes;
    }
}