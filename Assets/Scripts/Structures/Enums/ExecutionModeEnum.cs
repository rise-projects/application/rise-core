/// <summary>
/// ActionEnum to define all possible actions of the XML file.
/// </summary>
public static class ExecutionModeEnum
{
    /// <summary>
    /// All defined enums of type action.
    /// </summary>
    public enum ExecutionMode
    {
        none,
        debug,
        real,
        headless
    }
}