using UnityEngine;

/// <summary>
/// ActionEnum to define all possible actions of the XML file.
/// </summary>
public static class ActionEnum
{
    /// <summary>
    /// Approximated running time for actions with no result callback via ros (HLRC).
    /// </summary>
    public static int APPROXIMATION_TIME = 1;

    /// <summary>
    /// All defined enums of type action.
    /// </summary>
    public enum Action
    {
        None,
        Emotion,
        LookAt,
        Speech,
        Animation,
        SocialExpression,
        Wait
    }

    /// <summary>
    /// Translate an action type into an empty behavior action with type.
    /// </summary>
    /// <param name="actionState">Actiontype to translate into object.</param>
    /// <returns>Return empty behavior action object by type.</returns>
    public static BehaviorAction GetBehaviorObjectByEnum(Action actionState)
    {
        switch (actionState)
        {
            case Action.Emotion:
                return new BehaviorActionEmotion();
            case Action.LookAt:
                return new BehaviorActionLookAt();
            case Action.Speech:
                return new BehaviorActionSpeech();
            case Action.Animation:
                return new BehaviorActionAnimation();
            case Action.SocialExpression:
                return new BehaviorActionSocialExpression();
            case Action.Wait:
                return new BehaviorActionWait();
            default:
                break;
        }

        return null;
    }

    /// <summary>
    /// Translate the actiontype into an image.
    /// </summary>
    /// <param name="actionState">Actiontype to be translated into an image.</param>
    /// <returns>Returns an image for given an action type.</returns>
    public static Sprite GetSpriteForAction(Action actionState)
    {
        switch (actionState)
        {
            case Action.Emotion:
                return Resources.Load<Sprite>("Images/EmotionSprite");
            case Action.LookAt:
                return Resources.Load<Sprite>("Images/LookAtSprite");
            case Action.Speech:
                return Resources.Load<Sprite>("Images/SpeechSprite");
            case Action.Animation:
                return Resources.Load<Sprite>("Images/AnimationSprite");
            case Action.SocialExpression:
                return Resources.Load<Sprite>("Images/SocialExpressionSprite");
            case Action.Wait:
                return Resources.Load<Sprite>("Images/WaitSprite");
            default:
                break;
        }

        return null;
    }
}