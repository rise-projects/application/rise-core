/// <summary>
/// ROS-ActionGoal/Server states of the lifecycle.
/// </summary>
public static class ROSCommunicationEnum
{
    /// <summary>All actionstates corresponding to the action goal lifecycle in ROS.</summary>
    public enum ActionStatus
    {
        NO_GOAL = -1,
        LOST = 9,
        RECALLED = 8,
        PREEMPTING = 6,
        REJECTED = 5,
        ABORTED = 4,
        RECALLING = 7,
        PREEMPTED = 2,
        ACTIVE = 1,
        PENDING = 0,
        SUCCEEDED = 3
    }
}