using System;
using System.Collections.Generic;
using System.Diagnostics;

/// <summary>
/// Enum of all supported robots and their modalities.
/// </summary>
public static class RobotEnum
{
    /// <summary>
    /// All defined enums of type robot.
    /// </summary>
    public enum Robot
    {
        Floka,
        Pepper,
        NAO
    }

    public static Robot GetRobotEnumByString(string robotName)
    {
        foreach(Robot robot in Enum.GetValues(typeof(Robot)))
        {
            if(robot.ToString().ToLower().Equals(robotName.ToLower()))
            {
                return robot;
            }
        }

        // Default robot
        return Robot.NAO;
    }

    public static string GetTopicForRobot(Robot robot)
    {
        switch (robot)
        {
            case Robot.Pepper:
                return "naoqi";

            case Robot.NAO:
                return "naoqi";

            case Robot.Floka:
                return "floka";

            default:
                break;
        }

        return null;
    }

    /// <summary>List with all modalities of the robot Floka.</summary>
    private static List<ActionEnum.Action> modalities_floka = new List<ActionEnum.Action>()
    {
        ActionEnum.Action.Speech,
        ActionEnum.Action.Emotion,
        ActionEnum.Action.Animation,
        ActionEnum.Action.LookAt,
        ActionEnum.Action.Wait,
        ActionEnum.Action.SocialExpression
    };

    /// <summary>List with all modalities of the robot Pepper.</summary>
    private static List<ActionEnum.Action> modalities_pepper = new List<ActionEnum.Action>()
    {
        ActionEnum.Action.Speech,
        ActionEnum.Action.Emotion,
        ActionEnum.Action.Animation,
        ActionEnum.Action.LookAt,
        ActionEnum.Action.Wait,
        ActionEnum.Action.SocialExpression
    };

    /// <summary>List with all modalities of the robot Nao.</summary>
    private static List<ActionEnum.Action> modalities_nao = new List<ActionEnum.Action>()
    {
        ActionEnum.Action.Speech,
        ActionEnum.Action.Emotion,
        ActionEnum.Action.Animation,
        ActionEnum.Action.LookAt,
        ActionEnum.Action.Wait,
        ActionEnum.Action.SocialExpression
    };

    /// <summary>
    /// Translate a robot type into executable action types in list.
    /// </summary>
    /// <param name="robot">Given robot by enum type.</param>
    /// <returns>Return list with all executable actions for this robot.</returns>
    public static List<ActionEnum.Action> GetPossibleActionListForRobot(Robot robot)
    {
        switch (robot)
        {
            case Robot.Pepper:
                return modalities_pepper;

            case Robot.NAO:
                return modalities_nao;

            case Robot.Floka:
                return modalities_floka;

            default:
                break;
        }

        return null;
    }

    /// <summary>
    /// Translate a action and a target robot into an executable action for this robot.
    /// </summary>
    /// <param name="robot">Given robot by enum type.</param>
    /// <param name="action">Given robot by enum type.</param>
    /// <returns>Returns possible action for this robot by given action.</returns>
    public static ActionEnum.Action GetActionTranslationForRobot(Robot robot, ActionEnum.Action action)
    {
        foreach (ActionEnum.Action possibleAction in GetPossibleActionListForRobot(robot))
        {
            if (possibleAction.Equals(action))
            {
                return action;
            }
        }

        return ActionEnum.Action.None;
    }
}