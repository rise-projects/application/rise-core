﻿/// <summary>
/// States and wording for the Task Management in the system.
/// </summary>
public static class TaskManagementEnum
{
    /// <summary>
    /// All defined enums of type task-state.
    /// </summary>
    public enum TaskStatus
    {
        None,
        Waiting,
        Running,
        Paused,
        Interrupted,
        Finished
    }

    /// <summary>
    /// Get the status if a task is open and has to be executed next.
    /// </summary>
    /// <param name="status">Status to be checked.</param>
    /// <returns>True if the status is not closed.</returns>
    public static bool IsOpenStatus(TaskStatus status)
    {
        if(status.Equals(TaskStatus.Waiting))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Type of a tasks execution.
    /// </summary>
    public enum ExecutionType
    {
        None,
        Blocking
    }

    /// <summary>
    /// Signal of tasks to interrupt them.
    /// </summary>
    public enum ProcessSignal
    {
        None,
        Interrupt
    }

    /// <summary>
    /// Type of a task.
    /// </summary>
    public enum InteractionType
    {
        None,
        Dialog,
        Interaction,
        Task
    }
}