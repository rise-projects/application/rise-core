using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

/// <summary>
/// BuildPipeline for CI.
/// </summary>
public class BuildApplication : MonoBehaviour
{
    /// <summary>
    /// Build for Windows systems.
    /// </summary>
    public static void BuildWindows()
    {
        Build(BuildTarget.StandaloneWindows);
    }
    /// <summary>
    /// Build for Linux systems.
    /// </summary>
    public static void BuildLinux()
    {
        Build(BuildTarget.StandaloneLinux64);
    }

    public static void BuildMac()
    {
        Build(BuildTarget.StandaloneOSX);
    }


    /// <summary>
    /// Executable function from commandline tools for CI.
    /// </summary>
    /// <param name="target">Targeting operating system.</param>
    public static void Build(BuildTarget target)
    {
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scenes/RISE-ManagementScene.unity" };
        buildPlayerOptions.locationPathName = Environment.GetCommandLineArgs().Last();
        buildPlayerOptions.target = target;
        buildPlayerOptions.options = BuildOptions.None;

        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }
}