using Newtonsoft.Json;
using System.IO;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Build-in inspector GUI for exporting settings from the IDEA.
/// </summary>
[CustomEditor(typeof(StartUpConfigs))]
public class StartUpConfigsEditor : Editor
{
    /// <summary>
    /// Generating json config file by values from the inspector fields of the startup-settings.
    /// </summary>
    public override void OnInspectorGUI()
    {
        StartUpConfigs configs = (StartUpConfigs)target;

        configs.executionMode = EditorGUILayout.TextField("ExecutionMode", configs.executionMode);
        configs.pathToConfigurationRoot = EditorGUILayout.TextField("DirPathToConfigurationRoot", configs.pathToConfigurationRoot);
        configs.pathToConfigurationCategory = EditorGUILayout.TextField("DirPathToCategory", configs.pathToConfigurationCategory);
        configs.pathToConfigurationProject = EditorGUILayout.TextField("DirPathToProject", configs.pathToConfigurationProject);
        configs.pathToConfigurationXSD = EditorGUILayout.TextField("DirPathToXSDs", configs.pathToConfigurationXSD);
        configs.robotName = EditorGUILayout.TextField("RobotName", configs.robotName);
        configs.ipAddress = EditorGUILayout.TextField("IPAddress", configs.ipAddress);

        if (GUILayout.Button("Create JSON by Settings"))
        {
            string saveFile = Application.dataPath + "/args.data";
            Debug.Log("Filepath for configs: " + saveFile);

            string[] args = new string[15];
            args[0] = "0";
            args[1] = "-mode";
            args[2] = configs.executionMode;
            args[3] = "-path_config_root";
            args[4] = configs.pathToConfigurationRoot;
            args[5] = "-path_config_category";
            args[6] = configs.pathToConfigurationCategory;
            args[7] = "-path_config_project";
            args[8] = configs.pathToConfigurationProject;
            args[9] = "-path_config_xsds";
            args[10] = configs.pathToConfigurationXSD;
            args[11] = "-ip";
            args[12] = configs.ipAddress;
            args[13] = "-robot";
            args[14] = configs.robotName;

            File.WriteAllText(saveFile, JsonConvert.SerializeObject(args));
        }
    }
}