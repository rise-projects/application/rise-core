using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.RosTcpEndpoint
{
    public class SocialExpressionActionResult : ActionResult<SocialExpressionResult>
    {
        public const string k_RosMessageName = "hlrc_server/SocialExpressionActionResult";
        public override string RosMessageName => k_RosMessageName;


        public SocialExpressionActionResult() : base()
        {
            this.result = new SocialExpressionResult();
        }

        public SocialExpressionActionResult(HeaderMsg header, GoalStatusMsg status, SocialExpressionResult result) : base(header, status)
        {
            this.result = result;
        }
        public static SocialExpressionActionResult Deserialize(MessageDeserializer deserializer) => new SocialExpressionActionResult(deserializer);

        SocialExpressionActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = SocialExpressionResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
