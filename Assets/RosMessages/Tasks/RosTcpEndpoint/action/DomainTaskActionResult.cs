using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.RosTcpEndpoint
{
    public class DomainTaskActionResult : ActionResult<DomainTaskResult>
    {
        public const string k_RosMessageName = "rise_core/DomainTaskActionResult";
        public override string RosMessageName => k_RosMessageName;


        public DomainTaskActionResult() : base()
        {
            this.result = new DomainTaskResult();
        }

        public DomainTaskActionResult(HeaderMsg header, GoalStatusMsg status, DomainTaskResult result) : base(header, status)
        {
            this.result = result;
        }
        public static DomainTaskActionResult Deserialize(MessageDeserializer deserializer) => new DomainTaskActionResult(deserializer);

        DomainTaskActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = DomainTaskResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
