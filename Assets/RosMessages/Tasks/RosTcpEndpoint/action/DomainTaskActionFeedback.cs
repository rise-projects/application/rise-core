using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.RosTcpEndpoint
{
    public class DomainTaskActionFeedback : ActionFeedback<DomainTaskFeedback>
    {
        public const string k_RosMessageName = "rise_core/DomainTaskActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public DomainTaskActionFeedback() : base()
        {
            this.feedback = new DomainTaskFeedback();
        }

        public DomainTaskActionFeedback(HeaderMsg header, GoalStatusMsg status, DomainTaskFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static DomainTaskActionFeedback Deserialize(MessageDeserializer deserializer) => new DomainTaskActionFeedback(deserializer);

        DomainTaskActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = DomainTaskFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
