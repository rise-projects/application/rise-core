using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.RosTcpEndpoint
{
    public class DomainTaskActionGoal : ActionGoal<DomainTaskGoal>
    {
        public const string k_RosMessageName = "rise_core/DomainTaskActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public DomainTaskActionGoal() : base()
        {
            this.goal = new DomainTaskGoal();
        }

        public DomainTaskActionGoal(HeaderMsg header, GoalIDMsg goal_id, DomainTaskGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static DomainTaskActionGoal Deserialize(MessageDeserializer deserializer) => new DomainTaskActionGoal(deserializer);

        DomainTaskActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = DomainTaskGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
