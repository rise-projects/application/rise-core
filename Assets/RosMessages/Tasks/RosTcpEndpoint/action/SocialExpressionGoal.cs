//Do not edit! This file was generated by Unity-ROS MessageGeneration.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessageTypes.RosTcpEndpoint
{
    [Serializable]
    public class SocialExpressionGoal : Message
    {
        public const string k_RosMessageName = "hlrc_server/SocialExpressionGoal";
        public override string RosMessageName => k_RosMessageName;

        // social-expression
        public uint expression_id;
        public uint expression_duration;
        public uint expression_intensity;

        public SocialExpressionGoal()
        {
            this.expression_id = 0;
            this.expression_duration = 0;
            this.expression_intensity = 0;
        }

        public SocialExpressionGoal(uint expression_id, uint expression_duration, uint expression_intensity)
        {
            this.expression_id = expression_id;
            this.expression_duration = expression_duration;
            this.expression_intensity = expression_intensity;
        }

        public static SocialExpressionGoal Deserialize(MessageDeserializer deserializer) => new SocialExpressionGoal(deserializer);

        private SocialExpressionGoal(MessageDeserializer deserializer)
        {
            deserializer.Read(out this.expression_id);
            deserializer.Read(out this.expression_duration);
            deserializer.Read(out this.expression_intensity);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.expression_id);
            serializer.Write(this.expression_duration);
            serializer.Write(this.expression_intensity);
        }

        public override string ToString()
        {
            return "SocialExpressionGoal: " +
            "\nexpression_id: " + expression_id.ToString() +
            "\nexpression_duration: " + expression_duration.ToString() +
            "\nexpression_intensity: " + expression_intensity.ToString();
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize, MessageSubtopic.Goal);
        }
    }
}
