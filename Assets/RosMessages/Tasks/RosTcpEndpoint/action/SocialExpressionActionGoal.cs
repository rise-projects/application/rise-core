using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.RosTcpEndpoint
{
    public class SocialExpressionActionGoal : ActionGoal<SocialExpressionGoal>
    {
        public const string k_RosMessageName = "hlrc_server/SocialExpressionActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public SocialExpressionActionGoal() : base()
        {
            this.goal = new SocialExpressionGoal();
        }

        public SocialExpressionActionGoal(HeaderMsg header, GoalIDMsg goal_id, SocialExpressionGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static SocialExpressionActionGoal Deserialize(MessageDeserializer deserializer) => new SocialExpressionActionGoal(deserializer);

        SocialExpressionActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = SocialExpressionGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
