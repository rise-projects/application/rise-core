using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.RosTcpEndpoint
{
    public class SocialExpressionActionFeedback : ActionFeedback<SocialExpressionFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/SocialExpressionActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public SocialExpressionActionFeedback() : base()
        {
            this.feedback = new SocialExpressionFeedback();
        }

        public SocialExpressionActionFeedback(HeaderMsg header, GoalStatusMsg status, SocialExpressionFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static SocialExpressionActionFeedback Deserialize(MessageDeserializer deserializer) => new SocialExpressionActionFeedback(deserializer);

        SocialExpressionActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = SocialExpressionFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
