using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.RosTcpEndpoint
{
    public class DomainTaskAction : Action<DomainTaskActionGoal, DomainTaskActionResult, DomainTaskActionFeedback, DomainTaskGoal, DomainTaskResult, DomainTaskFeedback>
    {
        public const string k_RosMessageName = "rise_core/DomainTaskAction";
        public override string RosMessageName => k_RosMessageName;


        public DomainTaskAction() : base()
        {
            this.action_goal = new DomainTaskActionGoal();
            this.action_result = new DomainTaskActionResult();
            this.action_feedback = new DomainTaskActionFeedback();
        }

        public static DomainTaskAction Deserialize(MessageDeserializer deserializer) => new DomainTaskAction(deserializer);

        DomainTaskAction(MessageDeserializer deserializer)
        {
            this.action_goal = DomainTaskActionGoal.Deserialize(deserializer);
            this.action_result = DomainTaskActionResult.Deserialize(deserializer);
            this.action_feedback = DomainTaskActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
