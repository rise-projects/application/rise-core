using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.RosTcpEndpoint
{
    public class SocialExpressionAction : Action<SocialExpressionActionGoal, SocialExpressionActionResult, SocialExpressionActionFeedback, SocialExpressionGoal, SocialExpressionResult, SocialExpressionFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/SocialExpressionAction";
        public override string RosMessageName => k_RosMessageName;


        public SocialExpressionAction() : base()
        {
            this.action_goal = new SocialExpressionActionGoal();
            this.action_result = new SocialExpressionActionResult();
            this.action_feedback = new SocialExpressionActionFeedback();
        }

        public static SocialExpressionAction Deserialize(MessageDeserializer deserializer) => new SocialExpressionAction(deserializer);

        SocialExpressionAction(MessageDeserializer deserializer)
        {
            this.action_goal = SocialExpressionActionGoal.Deserialize(deserializer);
            this.action_result = SocialExpressionActionResult.Deserialize(deserializer);
            this.action_feedback = SocialExpressionActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
