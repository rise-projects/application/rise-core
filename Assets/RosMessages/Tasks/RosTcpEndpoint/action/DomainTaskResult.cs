//Do not edit! This file was generated by Unity-ROS MessageGeneration.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessageTypes.RosTcpEndpoint
{
    [Serializable]
    public class DomainTaskResult : Message
    {
        public const string k_RosMessageName = "rise_core/DomainTask";
        public override string RosMessageName => k_RosMessageName;

        // response
        public uint task_id;
        public uint result;

        public DomainTaskResult()
        {
            this.task_id = 0;
            this.result = 0;
        }

        public DomainTaskResult(uint task_id, uint result)
        {
            this.task_id = task_id;
            this.result = result;
        }

        public static DomainTaskResult Deserialize(MessageDeserializer deserializer) => new DomainTaskResult(deserializer);

        private DomainTaskResult(MessageDeserializer deserializer)
        {
            deserializer.Read(out this.task_id);
            deserializer.Read(out this.result);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.task_id);
            serializer.Write(this.result);
        }

        public override string ToString()
        {
            return "DomainTaskResult: " +
            "\ntask_id: " + task_id.ToString() +
            "\nresult: " + result.ToString();
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize, MessageSubtopic.Result);
        }
    }
}
