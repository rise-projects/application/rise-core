using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class AnimationAction : Action<AnimationActionGoal, AnimationActionResult, AnimationActionFeedback, AnimationGoal, AnimationResult, AnimationFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/animationAction";
        public override string RosMessageName => k_RosMessageName;


        public AnimationAction() : base()
        {
            this.action_goal = new AnimationActionGoal();
            this.action_result = new AnimationActionResult();
            this.action_feedback = new AnimationActionFeedback();
        }

        public static AnimationAction Deserialize(MessageDeserializer deserializer) => new AnimationAction(deserializer);

        AnimationAction(MessageDeserializer deserializer)
        {
            this.action_goal = AnimationActionGoal.Deserialize(deserializer);
            this.action_result = AnimationActionResult.Deserialize(deserializer);
            this.action_feedback = AnimationActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
