using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class MouthtargetActionResult : ActionResult<MouthtargetResult>
    {
        public const string k_RosMessageName = "hlrc_server/mouthtargetActionResult";
        public override string RosMessageName => k_RosMessageName;


        public MouthtargetActionResult() : base()
        {
            this.result = new MouthtargetResult();
        }

        public MouthtargetActionResult(HeaderMsg header, GoalStatusMsg status, MouthtargetResult result) : base(header, status)
        {
            this.result = result;
        }
        public static MouthtargetActionResult Deserialize(MessageDeserializer deserializer) => new MouthtargetActionResult(deserializer);

        MouthtargetActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = MouthtargetResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}