using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class LookattargetActionGoal : ActionGoal<LookattargetGoal>
    {
        public const string k_RosMessageName = "hlrc_server/lookattargetActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public LookattargetActionGoal() : base()
        {
            this.goal = new LookattargetGoal();
        }

        public LookattargetActionGoal(HeaderMsg header, GoalIDMsg goal_id, LookattargetGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static LookattargetActionGoal Deserialize(MessageDeserializer deserializer) => new LookattargetActionGoal(deserializer);

        LookattargetActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = LookattargetGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}