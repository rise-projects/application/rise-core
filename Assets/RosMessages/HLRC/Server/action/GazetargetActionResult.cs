using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class GazetargetActionResult : ActionResult<GazetargetResult>
    {
        public const string k_RosMessageName = "hlrc_server/gazetargetActionResult";
        public override string RosMessageName => k_RosMessageName;


        public GazetargetActionResult() : base()
        {
            this.result = new GazetargetResult();
        }

        public GazetargetActionResult(HeaderMsg header, GoalStatusMsg status, GazetargetResult result) : base(header, status)
        {
            this.result = result;
        }
        public static GazetargetActionResult Deserialize(MessageDeserializer deserializer) => new GazetargetActionResult(deserializer);

        GazetargetActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = GazetargetResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
