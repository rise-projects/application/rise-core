using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class TTSAction : Action<TTSActionGoal, TTSActionResult, TTSActionFeedback, TTSGoal, TTSResult, TTSFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/ttsAction";
        public override string RosMessageName => k_RosMessageName;


        public TTSAction() : base()
        {
            this.action_goal = new TTSActionGoal();
            this.action_result = new TTSActionResult();
            this.action_feedback = new TTSActionFeedback();
        }

        public static TTSAction Deserialize(MessageDeserializer deserializer) => new TTSAction(deserializer);

        TTSAction(MessageDeserializer deserializer)
        {
            this.action_goal = TTSActionGoal.Deserialize(deserializer);
            this.action_result = TTSActionResult.Deserialize(deserializer);
            this.action_feedback = TTSActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
