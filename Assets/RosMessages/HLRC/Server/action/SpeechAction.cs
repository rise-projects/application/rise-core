using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class SpeechAction : Action<SpeechActionGoal, SpeechActionResult, SpeechActionFeedback, SpeechGoal, SpeechResult, SpeechFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/speechAction";
        public override string RosMessageName => k_RosMessageName;


        public SpeechAction() : base()
        {
            this.action_goal = new SpeechActionGoal();
            this.action_result = new SpeechActionResult();
            this.action_feedback = new SpeechActionFeedback();
        }

        public static SpeechAction Deserialize(MessageDeserializer deserializer) => new SpeechAction(deserializer);

        SpeechAction(MessageDeserializer deserializer)
        {
            this.action_goal = SpeechActionGoal.Deserialize(deserializer);
            this.action_result = SpeechActionResult.Deserialize(deserializer);
            this.action_feedback = SpeechActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
