using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class MouthtargetActionFeedback : ActionFeedback<MouthtargetFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/mouthtargetActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public MouthtargetActionFeedback() : base()
        {
            this.feedback = new MouthtargetFeedback();
        }

        public MouthtargetActionFeedback(HeaderMsg header, GoalStatusMsg status, MouthtargetFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static MouthtargetActionFeedback Deserialize(MessageDeserializer deserializer) => new MouthtargetActionFeedback(deserializer);

        MouthtargetActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = MouthtargetFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
