using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class LookattargetActionFeedback : ActionFeedback<LookattargetFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/lookattargetActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public LookattargetActionFeedback() : base()
        {
            this.feedback = new LookattargetFeedback();
        }

        public LookattargetActionFeedback(HeaderMsg header, GoalStatusMsg status, LookattargetFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static LookattargetActionFeedback Deserialize(MessageDeserializer deserializer) => new LookattargetActionFeedback(deserializer);

        LookattargetActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = LookattargetFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
