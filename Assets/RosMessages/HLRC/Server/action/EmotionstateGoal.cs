//Do not edit! This file was generated by Unity-ROS MessageGeneration.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessageTypes.Server
{
    [Serializable]
    public class EmotionstateGoal : Message
    {
        public const string k_RosMessageName = "hlrc_server/emotionstate";
        public override string RosMessageName => k_RosMessageName;

        // emotionstate
        public const byte NEUTRAL = 0;
        public const byte HAPPY = 1;
        public const byte SAD = 2;
        public const byte ANGRY = 3;
        public const byte SURPRISED = 4;
        public const byte FEAR = 5;
        public byte value;
        // in milliseconds
        public uint duration;

        public EmotionstateGoal()
        {
            this.value = 0;
            this.duration = 0;
        }

        public EmotionstateGoal(byte value, uint duration)
        {
            this.value = value;
            this.duration = duration;
        }

        public static EmotionstateGoal Deserialize(MessageDeserializer deserializer) => new EmotionstateGoal(deserializer);

        private EmotionstateGoal(MessageDeserializer deserializer)
        {
            deserializer.Read(out this.value);
            deserializer.Read(out this.duration);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.value);
            serializer.Write(this.duration);
        }

        public override string ToString()
        {
            return "EmotionstateGoal: " +
            "\nvalue: " + value.ToString() +
            "\nduration: " + duration.ToString();
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize, MessageSubtopic.Goal);
        }
    }
}
