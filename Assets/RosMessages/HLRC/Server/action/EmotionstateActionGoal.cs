using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class EmotionstateActionGoal : ActionGoal<EmotionstateGoal>
    {
        public const string k_RosMessageName = "hlrc_server/emotionstateActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public EmotionstateActionGoal() : base()
        {
            this.goal = new EmotionstateGoal();
        }

        public EmotionstateActionGoal(HeaderMsg header, GoalIDMsg goal_id, EmotionstateGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static EmotionstateActionGoal Deserialize(MessageDeserializer deserializer) => new EmotionstateActionGoal(deserializer);

        EmotionstateActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = EmotionstateGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }


#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
