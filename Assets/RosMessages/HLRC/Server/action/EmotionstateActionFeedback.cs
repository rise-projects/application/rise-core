using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class EmotionstateActionFeedback : ActionFeedback<EmotionstateFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/emotionstateActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public EmotionstateActionFeedback() : base()
        {
            this.feedback = new EmotionstateFeedback();
        }

        public EmotionstateActionFeedback(HeaderMsg header, GoalStatusMsg status, EmotionstateFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static EmotionstateActionFeedback Deserialize(MessageDeserializer deserializer) => new EmotionstateActionFeedback(deserializer);

        EmotionstateActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = EmotionstateFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
