using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class GazetargetAction : Action<GazetargetActionGoal, GazetargetActionResult, GazetargetActionFeedback, GazetargetGoal, GazetargetResult, GazetargetFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/gazetargetAction";
        public override string RosMessageName => k_RosMessageName;


        public GazetargetAction() : base()
        {
            this.action_goal = new GazetargetActionGoal();
            this.action_result = new GazetargetActionResult();
            this.action_feedback = new GazetargetActionFeedback();
        }

        public static GazetargetAction Deserialize(MessageDeserializer deserializer) => new GazetargetAction(deserializer);

        GazetargetAction(MessageDeserializer deserializer)
        {
            this.action_goal = GazetargetActionGoal.Deserialize(deserializer);
            this.action_result = GazetargetActionResult.Deserialize(deserializer);
            this.action_feedback = GazetargetActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
