using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class EmotionstateActionResult : ActionResult<EmotionstateResult>
    {
        public const string k_RosMessageName = "hlrc_server/emotionstateActionResult";
        public override string RosMessageName => k_RosMessageName;


        public EmotionstateActionResult() : base()
        {
            this.result = new EmotionstateResult();
        }

        public EmotionstateActionResult(HeaderMsg header, GoalStatusMsg status, EmotionstateResult result) : base(header, status)
        {
            this.result = result;
        }
        public static EmotionstateActionResult Deserialize(MessageDeserializer deserializer) => new EmotionstateActionResult(deserializer);

        EmotionstateActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = EmotionstateResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }


#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}