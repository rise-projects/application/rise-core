using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class UtteranceAction : Action<UtteranceActionGoal, UtteranceActionResult, UtteranceActionFeedback, UtteranceGoal, UtteranceResult, UtteranceFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/utteranceAction";
        public override string RosMessageName => k_RosMessageName;


        public UtteranceAction() : base()
        {
            this.action_goal = new UtteranceActionGoal();
            this.action_result = new UtteranceActionResult();
            this.action_feedback = new UtteranceActionFeedback();
        }

        public static UtteranceAction Deserialize(MessageDeserializer deserializer) => new UtteranceAction(deserializer);

        UtteranceAction(MessageDeserializer deserializer)
        {
            this.action_goal = UtteranceActionGoal.Deserialize(deserializer);
            this.action_result = UtteranceActionResult.Deserialize(deserializer);
            this.action_feedback = UtteranceActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
