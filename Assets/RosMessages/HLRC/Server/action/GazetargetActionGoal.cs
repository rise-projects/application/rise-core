using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class GazetargetActionGoal : ActionGoal<GazetargetGoal>
    {
        public const string k_RosMessageName = "hlrc_server/gazetargetActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public GazetargetActionGoal() : base()
        {
            this.goal = new GazetargetGoal();
        }

        public GazetargetActionGoal(HeaderMsg header, GoalIDMsg goal_id, GazetargetGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static GazetargetActionGoal Deserialize(MessageDeserializer deserializer) => new GazetargetActionGoal(deserializer);

        GazetargetActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = GazetargetGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
