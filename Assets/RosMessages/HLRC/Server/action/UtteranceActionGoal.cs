using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class UtteranceActionGoal : ActionGoal<UtteranceGoal>
    {
        public const string k_RosMessageName = "hlrc_server/utteranceActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public UtteranceActionGoal() : base()
        {
            this.goal = new UtteranceGoal();
        }

        public UtteranceActionGoal(HeaderMsg header, GoalIDMsg goal_id, UtteranceGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static UtteranceActionGoal Deserialize(MessageDeserializer deserializer) => new UtteranceActionGoal(deserializer);

        UtteranceActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = UtteranceGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
