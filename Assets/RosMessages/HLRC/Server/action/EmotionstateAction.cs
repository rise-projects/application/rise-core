using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class EmotionstateAction : Action<EmotionstateActionGoal, EmotionstateActionResult, EmotionstateActionFeedback, EmotionstateGoal, EmotionstateResult, EmotionstateFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/emotionstateAction";
        public override string RosMessageName => k_RosMessageName;


        public EmotionstateAction() : base()
        {
            this.action_goal = new EmotionstateActionGoal();
            this.action_result = new EmotionstateActionResult();
            this.action_feedback = new EmotionstateActionFeedback();
        }

        public static EmotionstateAction Deserialize(MessageDeserializer deserializer) => new EmotionstateAction(deserializer);

        EmotionstateAction(MessageDeserializer deserializer)
        {
            this.action_goal = EmotionstateActionGoal.Deserialize(deserializer);
            this.action_result = EmotionstateActionResult.Deserialize(deserializer);
            this.action_feedback = EmotionstateActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
