using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class SpeechActionResult : ActionResult<SpeechResult>
    {
        public const string k_RosMessageName = "hlrc_server/speechActionResult";
        public override string RosMessageName => k_RosMessageName;


        public SpeechActionResult() : base()
        {
            this.result = new SpeechResult();
        }

        public SpeechActionResult(HeaderMsg header, GoalStatusMsg status, SpeechResult result) : base(header, status)
        {
            this.result = result;
        }
        public static SpeechActionResult Deserialize(MessageDeserializer deserializer) => new SpeechActionResult(deserializer);

        SpeechActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = SpeechResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}