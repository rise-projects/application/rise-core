using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class SpeechActionGoal : ActionGoal<SpeechGoal>
    {
        public const string k_RosMessageName = "hlrc_server/speechActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public SpeechActionGoal() : base()
        {
            this.goal = new SpeechGoal();
        }

        public SpeechActionGoal(HeaderMsg header, GoalIDMsg goal_id, SpeechGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static SpeechActionGoal Deserialize(MessageDeserializer deserializer) => new SpeechActionGoal(deserializer);

        SpeechActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = SpeechGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
