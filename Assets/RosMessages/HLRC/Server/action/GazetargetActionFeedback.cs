using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class GazetargetActionFeedback : ActionFeedback<GazetargetFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/gazetargetActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public GazetargetActionFeedback() : base()
        {
            this.feedback = new GazetargetFeedback();
        }

        public GazetargetActionFeedback(HeaderMsg header, GoalStatusMsg status, GazetargetFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static GazetargetActionFeedback Deserialize(MessageDeserializer deserializer) => new GazetargetActionFeedback(deserializer);

        GazetargetActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = GazetargetFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
