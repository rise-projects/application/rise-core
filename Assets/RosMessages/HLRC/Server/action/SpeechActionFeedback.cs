using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class SpeechActionFeedback : ActionFeedback<SpeechFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/speechActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public SpeechActionFeedback() : base()
        {
            this.feedback = new SpeechFeedback();
        }

        public SpeechActionFeedback(HeaderMsg header, GoalStatusMsg status, SpeechFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static SpeechActionFeedback Deserialize(MessageDeserializer deserializer) => new SpeechActionFeedback(deserializer);

        SpeechActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = SpeechFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
