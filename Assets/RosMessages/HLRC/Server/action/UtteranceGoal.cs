//Do not edit! This file was generated by Unity-ROS MessageGeneration.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.HlrcServer;

namespace RosMessageTypes.Server
{
    [Serializable]
    public class UtteranceGoal : Message
    {
        public const string k_RosMessageName = "hlrc_server/utteranceGoal";
        public override string RosMessageName => k_RosMessageName;

        // utterance
        public UtteranceMsg utterance;

        public UtteranceGoal()
        {
            this.utterance = new UtteranceMsg();
        }

        public UtteranceGoal(UtteranceMsg utterance)
        {
            this.utterance = utterance;
        }

        public static UtteranceGoal Deserialize(MessageDeserializer deserializer) => new UtteranceGoal(deserializer);

        private UtteranceGoal(MessageDeserializer deserializer)
        {
            this.utterance = UtteranceMsg.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.utterance);
        }

        public override string ToString()
        {
            return "UtteranceGoal: " +
            "\nutterance: " + utterance.ToString();
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize, MessageSubtopic.Goal);
        }
    }
}
