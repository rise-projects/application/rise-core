using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class UtteranceActionResult : ActionResult<UtteranceResult>
    {
        public const string k_RosMessageName = "hlrc_server/utteranceActionResult";
        public override string RosMessageName => k_RosMessageName;


        public UtteranceActionResult() : base()
        {
            this.result = new UtteranceResult();
        }

        public UtteranceActionResult(HeaderMsg header, GoalStatusMsg status, UtteranceResult result) : base(header, status)
        {
            this.result = result;
        }
        public static UtteranceActionResult Deserialize(MessageDeserializer deserializer) => new UtteranceActionResult(deserializer);

        UtteranceActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = UtteranceResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
