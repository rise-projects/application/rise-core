using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class LookattargetActionResult : ActionResult<LookattargetResult>
    {
        public const string k_RosMessageName = "hlrc_server/lookattargetActionResult";
        public override string RosMessageName => k_RosMessageName;


        public LookattargetActionResult() : base()
        {
            this.result = new LookattargetResult();
        }

        public LookattargetActionResult(HeaderMsg header, GoalStatusMsg status, LookattargetResult result) : base(header, status)
        {
            this.result = result;
        }
        public static LookattargetActionResult Deserialize(MessageDeserializer deserializer) => new LookattargetActionResult(deserializer);

        LookattargetActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = LookattargetResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}