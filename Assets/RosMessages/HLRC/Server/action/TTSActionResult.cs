using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class TTSActionResult : ActionResult<TTSResult>
    {
        public const string k_RosMessageName = "hlrc_server/ttsActionResult";
        public override string RosMessageName => k_RosMessageName;


        public TTSActionResult() : base()
        {
            this.result = new TTSResult();
        }

        public TTSActionResult(HeaderMsg header, GoalStatusMsg status, TTSResult result) : base(header, status)
        {
            this.result = result;
        }
        public static TTSActionResult Deserialize(MessageDeserializer deserializer) => new TTSActionResult(deserializer);

        TTSActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = TTSResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
