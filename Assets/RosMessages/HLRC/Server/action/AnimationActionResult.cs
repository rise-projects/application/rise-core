using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class AnimationActionResult : ActionResult<AnimationResult>
    {
        public const string k_RosMessageName = "hlrc_server/animationActionResult";
        public override string RosMessageName => k_RosMessageName;


        public AnimationActionResult() : base()
        {
            this.result = new AnimationResult();
        }

        public AnimationActionResult(HeaderMsg header, GoalStatusMsg status, AnimationResult result) : base(header, status)
        {
            this.result = result;
        }
        public static AnimationActionResult Deserialize(MessageDeserializer deserializer) => new AnimationActionResult(deserializer);

        AnimationActionResult(MessageDeserializer deserializer) : base(deserializer)
        {
            this.result = AnimationResult.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.result);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}