using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class AnimationActionFeedback : ActionFeedback<AnimationFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/animationActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public AnimationActionFeedback() : base()
        {
            this.feedback = new AnimationFeedback();
        }

        public AnimationActionFeedback(HeaderMsg header, GoalStatusMsg status, AnimationFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static AnimationActionFeedback Deserialize(MessageDeserializer deserializer) => new AnimationActionFeedback(deserializer);

        AnimationActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = AnimationFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
