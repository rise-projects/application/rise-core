using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class TTSActionFeedback : ActionFeedback<TTSFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/ttsActionFeedback";
        public override string RosMessageName => k_RosMessageName;


        public TTSActionFeedback() : base()
        {
            this.feedback = new TTSFeedback();
        }

        public TTSActionFeedback(HeaderMsg header, GoalStatusMsg status, TTSFeedback feedback) : base(header, status)
        {
            this.feedback = feedback;
        }
        public static TTSActionFeedback Deserialize(MessageDeserializer deserializer) => new TTSActionFeedback(deserializer);

        TTSActionFeedback(MessageDeserializer deserializer) : base(deserializer)
        {
            this.feedback = TTSFeedback.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.status);
            serializer.Write(this.feedback);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
