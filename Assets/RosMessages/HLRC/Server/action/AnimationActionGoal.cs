using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class AnimationActionGoal : ActionGoal<AnimationGoal>
    {
        public const string k_RosMessageName = "hlrc_server/animationActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public AnimationActionGoal() : base()
        {
            this.goal = new AnimationGoal();
        }

        public AnimationActionGoal(HeaderMsg header, GoalIDMsg goal_id, AnimationGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static AnimationActionGoal Deserialize(MessageDeserializer deserializer) => new AnimationActionGoal(deserializer);

        AnimationActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = AnimationGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}