using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class MouthtargetActionGoal : ActionGoal<MouthtargetGoal>
    {
        public const string k_RosMessageName = "hlrc_server/mouthtargetActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public MouthtargetActionGoal() : base()
        {
            this.goal = new MouthtargetGoal();
        }

        public MouthtargetActionGoal(HeaderMsg header, GoalIDMsg goal_id, MouthtargetGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static MouthtargetActionGoal Deserialize(MessageDeserializer deserializer) => new MouthtargetActionGoal(deserializer);

        MouthtargetActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = MouthtargetGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
