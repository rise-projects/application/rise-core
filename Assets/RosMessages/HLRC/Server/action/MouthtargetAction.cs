using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class MouthtargetAction : Action<MouthtargetActionGoal, MouthtargetActionResult, MouthtargetActionFeedback, MouthtargetGoal, MouthtargetResult, MouthtargetFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/mouthtargetAction";
        public override string RosMessageName => k_RosMessageName;


        public MouthtargetAction() : base()
        {
            this.action_goal = new MouthtargetActionGoal();
            this.action_result = new MouthtargetActionResult();
            this.action_feedback = new MouthtargetActionFeedback();
        }

        public static MouthtargetAction Deserialize(MessageDeserializer deserializer) => new MouthtargetAction(deserializer);

        MouthtargetAction(MessageDeserializer deserializer)
        {
            this.action_goal = MouthtargetActionGoal.Deserialize(deserializer);
            this.action_result = MouthtargetActionResult.Deserialize(deserializer);
            this.action_feedback = MouthtargetActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}
