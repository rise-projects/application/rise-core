using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;


namespace RosMessageTypes.Server
{
    public class LookattargetAction : Action<LookattargetActionGoal, LookattargetActionResult, LookattargetActionFeedback, LookattargetGoal, LookattargetResult, LookattargetFeedback>
    {
        public const string k_RosMessageName = "hlrc_server/lookattargetAction";
        public override string RosMessageName => k_RosMessageName;


        public LookattargetAction() : base()
        {
            this.action_goal = new LookattargetActionGoal();
            this.action_result = new LookattargetActionResult();
            this.action_feedback = new LookattargetActionFeedback();
        }

        public static LookattargetAction Deserialize(MessageDeserializer deserializer) => new LookattargetAction(deserializer);

        LookattargetAction(MessageDeserializer deserializer)
        {
            this.action_goal = LookattargetActionGoal.Deserialize(deserializer);
            this.action_result = LookattargetActionResult.Deserialize(deserializer);
            this.action_feedback = LookattargetActionFeedback.Deserialize(deserializer);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.action_goal);
            serializer.Write(this.action_result);
            serializer.Write(this.action_feedback);
        }
    }
}