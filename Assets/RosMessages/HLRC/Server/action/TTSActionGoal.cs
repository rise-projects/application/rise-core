using System.Collections.Generic;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;
using RosMessageTypes.Std;
using RosMessageTypes.Actionlib;

namespace RosMessageTypes.Server
{
    public class TTSActionGoal : ActionGoal<TTSGoal>
    {
        public const string k_RosMessageName = "hlrc_server/ttsActionGoal";
        public override string RosMessageName => k_RosMessageName;


        public TTSActionGoal() : base()
        {
            this.goal = new TTSGoal();
        }

        public TTSActionGoal(HeaderMsg header, GoalIDMsg goal_id, TTSGoal goal) : base(header, goal_id)
        {
            this.goal = goal;
        }
        public static TTSActionGoal Deserialize(MessageDeserializer deserializer) => new TTSActionGoal(deserializer);

        TTSActionGoal(MessageDeserializer deserializer) : base(deserializer)
        {
            this.goal = TTSGoal.Deserialize(deserializer);
        }
        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.header);
            serializer.Write(this.goal_id);
            serializer.Write(this.goal);
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
