//Do not edit! This file was generated by Unity-ROS MessageGeneration.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessageTypes.Server
{
    [Serializable]
    public class AnimationGoal : Message
    {
        public const string k_RosMessageName = "hlrc_server/animation";
        public override string RosMessageName => k_RosMessageName;

        // robot head animation
        public const byte IDLE = 0;
        public const byte HEAD_NOD = 1;
        public const byte HEAD_SHAKE = 2;
        public const byte EYEBLINK_L = 3;
        public const byte EYEBLINK_R = 4;
        public const byte EYEBLINK_BOTH = 5;
        public const byte EYEBROWS_RAISE = 6;
        public const byte EYEBROWS_LOWER = 7;
        public const byte ENGAGEMENT_LEFT = 8;
        public const byte ENGAGEMENT_RIGHT = 9;
        public byte target;
        public uint repetitions;
        // in milliseconds
        public uint duration_each;
        // influence how pronounced the animation should be executed
        // ]0, 1[  => less pronounced
        // [1, 1]  => normal version
        // ]1, n]  => over pronounced
        // [-n, 0] => undefined / ignored
        public float scale;

        public AnimationGoal()
        {
            this.target = 0;
            this.repetitions = 0;
            this.duration_each = 0;
            this.scale = 0.0f;
        }

        public AnimationGoal(byte target, uint repetitions, uint duration_each, float scale)
        {
            this.target = target;
            this.repetitions = repetitions;
            this.duration_each = duration_each;
            this.scale = scale;
        }

        public static AnimationGoal Deserialize(MessageDeserializer deserializer) => new AnimationGoal(deserializer);

        private AnimationGoal(MessageDeserializer deserializer)
        {
            deserializer.Read(out this.target);
            deserializer.Read(out this.repetitions);
            deserializer.Read(out this.duration_each);
            deserializer.Read(out this.scale);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.Write(this.target);
            serializer.Write(this.repetitions);
            serializer.Write(this.duration_each);
            serializer.Write(this.scale);
        }

        public override string ToString()
        {
            return "AnimationGoal: " +
            "\ntarget: " + target.ToString() +
            "\nrepetitions: " + repetitions.ToString() +
            "\nduration_each: " + duration_each.ToString() +
            "\nscale: " + scale.ToString();
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize, MessageSubtopic.Goal);
        }
    }
}
