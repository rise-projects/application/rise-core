//Do not edit! This file was generated by Unity-ROS MessageGeneration.
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Unity.Robotics.ROSTCPConnector.MessageGeneration;

namespace RosMessageTypes.HlrcServer
{
    [Serializable]
    public class SoundchunkMsg : Message
    {
        public const string k_RosMessageName = "hlrc_server/Soundchunk";
        public override string RosMessageName => k_RosMessageName;

        // sound chunk
        // sample type constants:
        public const byte SAMPLE_S8 = 0;
        public const byte SAMPLE_U8 = 1;
        public const byte SAMPLE_S16 = 2;
        public const byte SAMPLE_U16 = 4;
        public const byte SAMPLE_S24 = 8;
        public const byte SAMPLE_U24 = 16;
        // endianess constants:
        public const byte ENDIAN_LITTLE = 0;
        public const byte ENDIAN_BIG = 1;
        // raw audio data
        public byte[] data;
        // sample count
        public uint samplecount;
        // channel count
        public uint channels;
        // sampling rate in hz
        public uint rate;
        // sample type (SAMPLE_S8, ...)
        public byte sample_type;
        // endianess (ENDIAN_LITTLE or ENDIAN_BIG)
        public byte endianess;

        public SoundchunkMsg()
        {
            this.data = new byte[0];
            this.samplecount = 0;
            this.channels = 0;
            this.rate = 0;
            this.sample_type = 0;
            this.endianess = 0;
        }

        public SoundchunkMsg(byte[] data, uint samplecount, uint channels, uint rate, byte sample_type, byte endianess)
        {
            this.data = data;
            this.samplecount = samplecount;
            this.channels = channels;
            this.rate = rate;
            this.sample_type = sample_type;
            this.endianess = endianess;
        }

        public static SoundchunkMsg Deserialize(MessageDeserializer deserializer) => new SoundchunkMsg(deserializer);

        private SoundchunkMsg(MessageDeserializer deserializer)
        {
            deserializer.Read(out this.data, sizeof(byte), deserializer.ReadLength());
            deserializer.Read(out this.samplecount);
            deserializer.Read(out this.channels);
            deserializer.Read(out this.rate);
            deserializer.Read(out this.sample_type);
            deserializer.Read(out this.endianess);
        }

        public override void SerializeTo(MessageSerializer serializer)
        {
            serializer.WriteLength(this.data);
            serializer.Write(this.data);
            serializer.Write(this.samplecount);
            serializer.Write(this.channels);
            serializer.Write(this.rate);
            serializer.Write(this.sample_type);
            serializer.Write(this.endianess);
        }

        public override string ToString()
        {
            return "SoundchunkMsg: " +
            "\ndata: " + System.String.Join(", ", data.ToList()) +
            "\nsamplecount: " + samplecount.ToString() +
            "\nchannels: " + channels.ToString() +
            "\nrate: " + rate.ToString() +
            "\nsample_type: " + sample_type.ToString() +
            "\nendianess: " + endianess.ToString();
        }

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
#else
        [UnityEngine.RuntimeInitializeOnLoadMethod]
#endif
        public static void Register()
        {
            MessageRegistry.Register(k_RosMessageName, Deserialize);
        }
    }
}
