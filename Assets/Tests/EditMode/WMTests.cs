using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;
using B83.LogicExpressionParser;

public class WMTests
{
    [Test]
    public void ReadAndWrite()
    {
        WorkingMemory workingMemory = new WorkingMemory();

        //write
        workingMemory.WriteIntoMemory("{\"stringVariable1\": \"StringValue1\"}");
        workingMemory.WriteIntoMemory("{\"longVariable1\": " + 1 + "}");
        workingMemory.WriteIntoMemory("{\"BooleanVariable1\": true}");
        workingMemory.WriteIntoMemory("{\"BooleanVariable2\": false}");

        //read
        string OutputString1 = workingMemory.GetMemoryValueAsString("stringVariable1");
        long OutputLong1 = workingMemory.GetMemoryValue("longVariable1");
        bool OutputBool1 = workingMemory.GetMemoryValue("BooleanVariable1");
        bool OutputBool2 = workingMemory.GetMemoryValue("BooleanVariable2");

        //compare
        Assert.AreEqual("StringValue1",OutputString1);
        Assert.AreEqual(1, OutputLong1);
        Assert.AreEqual(true, OutputBool1);                
        Assert.AreEqual(false, OutputBool2);                
    }

    [Test]
    public void StringReadAndWriteWithScuffedUserInput()
    {
        WorkingMemory workingMemory = new WorkingMemory();

        //write
        workingMemory.WriteIntoMemory("{\"stringVariable1\": \"!§$%&/()=? ^°><+±'-.∞,`\"}");
        workingMemory.WriteIntoMemory("{\"stringVariable2\": \"alsdkjf lkajfka sjdflkajsdfl kajsdflkjasdfow328u428u1nfgalkjg 01438u jdfk slfdajf\"}");

        //read
        string OutputString1 = workingMemory.GetMemoryValueAsString("stringVariable1");
        string OutputString2 = workingMemory.GetMemoryValueAsString("stringVariable2");

        //compare
        Assert.AreEqual("!§$%&/()=? ^°><+±'-.∞,`",OutputString1);
        Assert.AreEqual("alsdkjf lkajfka sjdflkajsdfl kajsdflkjasdfow328u428u1nfgalkjg 01438u jdfk slfdajf", OutputString2);
    }
}