using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using B83.LogicExpressionParser;

public class LEPAndWMTests : MonoBehaviour
{
   [Test]
    public void WorkingMemoryLEPTests()
    {
        WorkingMemory workingMemory = new WorkingMemory();

        Parser parser = new Parser();

        LogicExpression solutionN = parser.Parse("");

        Assert.AreEqual(true, solutionN.GetResult());

        //write
        workingMemory.WriteIntoMemory("{\"variable1\": 'Test String 1'}");
        workingMemory.WriteIntoMemory("{\"variable2\": 'Test String 2'}");

        parser.ExpressionContext["variable1"].Set(workingMemory.GetMemoryValue("variable1"));
        parser.ExpressionContext["variable2"].Set(workingMemory.GetMemoryValue("variable2"));

        // LEP condition e.g. "variable1 == variable2"

        LogicExpression test1 = parser.Parse("variable1 == variable2");

        Assert.AreEqual(false, test1.GetResult());

        LogicExpression test2 = parser.Parse("variable1 != variable2");
        Assert.AreEqual(true, test2.GetResult());

        workingMemory.WriteIntoMemory("{\"variable2\": 'Test String 1'}");

        parser.ExpressionContext["variable2"].Set(workingMemory.GetMemoryValue("variable2"));

        LogicExpression test3 = parser.Parse("variable1 == variable2");
        Assert.AreEqual(true, test3.GetResult());

        LogicExpression test4 = parser.Parse("variable1 != variable2");

        Assert.AreEqual(false, test4.GetResult());

        
        workingMemory.WriteIntoMemory("{\"variable1\": 1}");
        workingMemory.WriteIntoMemory("{\"variable2\": 0}");
        parser.ExpressionContext["variable1"].Set(workingMemory.GetMemoryValue("variable1"));
        parser.ExpressionContext["variable2"].Set(workingMemory.GetMemoryValue("variable2"));

        LogicExpression test5 = parser.Parse("variable1 == 1 and variable2 == 0");
        Assert.AreEqual(true, test5.GetResult());
    }

    [Test]
    public void MyLEPTests()
    {
        WorkingMemory workingMemory = new WorkingMemory();

        //write
        workingMemory.WriteIntoMemory("{\"variable1\": 'hallo'}");
        workingMemory.WriteIntoMemory("{\"variable2\": 'not hallo'}");

        Parser parser = new Parser();

        parser.ExpressionContext["freshVariable1"].Set(workingMemory.GetMemoryValue("variable1"));
        parser.ExpressionContext["freshVariable2"].Set(workingMemory.GetMemoryValue("variable2"));

        LogicExpression solutionN = parser.Parse("");

        Assert.AreEqual(true, solutionN.GetResult());


        // LEP condition e.g. "variable1 == variable2"
        LogicExpression test1 = parser.Parse("freshVariable1 == freshVariable2");

        Assert.AreEqual(false, test1.GetResult());

        // NOT WORKING CURRENTLY!
        LogicExpression testMissing = parser.Parse("missingVariable");
        Assert.AreEqual(false, testMissing.GetResult());
    }
}
