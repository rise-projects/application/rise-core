using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using B83.LogicExpressionParser;

public class LEPTests : MonoBehaviour
{
    public class BasicArithmeticOperations
    {
        [Test]
        public void Sum()
        {
            Parser parser = new Parser();

            // + plus +
            NumberExpression exp1 = parser.ParseNumber("1+1");
            NumberExpression exp2 = parser.ParseNumber("1+2");
            NumberExpression exp3 = parser.ParseNumber("2+1");

            // + plus -
            NumberExpression exp4 = parser.ParseNumber("1+(-1)");
            NumberExpression exp5 = parser.ParseNumber("1+(-2)");
            NumberExpression exp6 = parser.ParseNumber("2+(-1)");

            // - plus +
            NumberExpression exp7 = parser.ParseNumber("(-1)+1");
            NumberExpression exp8 = parser.ParseNumber("(-1)+2");
            NumberExpression exp9 = parser.ParseNumber("(-2)+1");

            // - plus -
            NumberExpression exp10 = parser.ParseNumber("(-1)+(-1)");
            NumberExpression exp11 = parser.ParseNumber("(-1)+(-2)");
            NumberExpression exp12 = parser.ParseNumber("(-2)+(-1)");


            Assert.AreEqual(1+1,exp1.GetNumber());
            Assert.AreEqual(1+2,exp2.GetNumber());
            Assert.AreEqual(2+1,exp3.GetNumber());

            Assert.AreEqual(1+(-1),exp4.GetNumber());
            Assert.AreEqual(1+(-2),exp5.GetNumber());
            Assert.AreEqual(2+(-1),exp6.GetNumber());

            Assert.AreEqual((-1)+1,exp7.GetNumber());
            Assert.AreEqual((-1)+2,exp8.GetNumber());
            Assert.AreEqual((-2)+1,exp9.GetNumber());

            Assert.AreEqual((-1)+(-1),exp10.GetNumber());
            Assert.AreEqual((-1)+(-2),exp11.GetNumber());
            Assert.AreEqual((-2)+(-1),exp12.GetNumber());
        }

        [Test]
        public void Difference()
        {
            Parser parser = new Parser();

            // + minus +
            NumberExpression exp1 = parser.ParseNumber("1-1");
            NumberExpression exp2 = parser.ParseNumber("1-2");
            NumberExpression exp3 = parser.ParseNumber("2-1");

            // + minus -
            NumberExpression exp4 = parser.ParseNumber("1-(-1)");
            NumberExpression exp5 = parser.ParseNumber("1-(-2)");
            NumberExpression exp6 = parser.ParseNumber("2-(-1)");

            // - minus +
            NumberExpression exp7 = parser.ParseNumber("(-1)-1");
            NumberExpression exp8 = parser.ParseNumber("(-1)-2");
            NumberExpression exp9 = parser.ParseNumber("(-2)-1");

            // - minus -
            NumberExpression exp10 = parser.ParseNumber("(-1)-(-1)");
            NumberExpression exp11 = parser.ParseNumber("(-1)-(-2)");
            NumberExpression exp12 = parser.ParseNumber("(-2)-(-1)");


            Assert.AreEqual(1-1,exp1.GetNumber());
            Assert.AreEqual(1-2,exp2.GetNumber());
            Assert.AreEqual(2-1,exp3.GetNumber());

            Assert.AreEqual(1-(-1),exp4.GetNumber());
            Assert.AreEqual(1-(-2),exp5.GetNumber());
            Assert.AreEqual(2-(-1),exp6.GetNumber());

            Assert.AreEqual((-1)-1,exp7.GetNumber());
            Assert.AreEqual((-1)-2,exp8.GetNumber());
            Assert.AreEqual((-2)-1,exp9.GetNumber());

            Assert.AreEqual((-1)-(-1),exp10.GetNumber());
            Assert.AreEqual((-1)-(-2),exp11.GetNumber());
            Assert.AreEqual((-2)-(-1),exp12.GetNumber());
        }

        [Test]
        public void Product()
        {
            Parser parser = new Parser();

            // + multiplied by +
            NumberExpression exp1 = parser.ParseNumber("2*2");
            NumberExpression exp2 = parser.ParseNumber("2*3");
            NumberExpression exp3 = parser.ParseNumber("3*2");

            // + multiplied by -
            NumberExpression exp4 = parser.ParseNumber("2*(-2)");
            NumberExpression exp5 = parser.ParseNumber("2*(-3)");
            NumberExpression exp6 = parser.ParseNumber("3*(-2)");

            // - multiplied by +
            NumberExpression exp7 = parser.ParseNumber("(-2)*2");
            NumberExpression exp8 = parser.ParseNumber("(-2)*3");
            NumberExpression exp9 = parser.ParseNumber("(-3)*2");

            // - multiplied by -
            NumberExpression exp10 = parser.ParseNumber("(-2)*(-2)");
            NumberExpression exp11 = parser.ParseNumber("(-2)*(-3)");
            NumberExpression exp12 = parser.ParseNumber("(-3)*(-2)");


            Assert.AreEqual(2*2,exp1.GetNumber());
            Assert.AreEqual(2*3,exp2.GetNumber());
            Assert.AreEqual(3*2,exp3.GetNumber());

            Assert.AreEqual(2*(-2),exp4.GetNumber());
            Assert.AreEqual(2*(-3),exp5.GetNumber());
            Assert.AreEqual(3*(-2),exp6.GetNumber());

            Assert.AreEqual((-2)*2,exp7.GetNumber());
            Assert.AreEqual((-2)*3,exp8.GetNumber());
            Assert.AreEqual((-3)*2,exp9.GetNumber());

            Assert.AreEqual((-2)*(-2),exp10.GetNumber());
            Assert.AreEqual((-2)*(-3),exp11.GetNumber());
            Assert.AreEqual((-3)*(-2),exp12.GetNumber());
        }

        [Test]
        public void Quotient()
        {
            Parser parser = new Parser();

            // + divided by +
            NumberExpression exp1 = parser.ParseNumber("2/2");
            NumberExpression exp2 = parser.ParseNumber("2/3");
            NumberExpression exp3 = parser.ParseNumber("3/2");

            // + divided by -
            NumberExpression exp4 = parser.ParseNumber("2/(-2)");
            NumberExpression exp5 = parser.ParseNumber("2/(-3)");
            NumberExpression exp6 = parser.ParseNumber("3/(-2)");

            // - divided by +
            NumberExpression exp7 = parser.ParseNumber("(-2)/2");
            NumberExpression exp8 = parser.ParseNumber("(-2)/3");
            NumberExpression exp9 = parser.ParseNumber("(-3)/2");

            // - divided by -
            NumberExpression exp10 = parser.ParseNumber("(-2)/(-2)");
            NumberExpression exp11 = parser.ParseNumber("(-2)/(-3)");
            NumberExpression exp12 = parser.ParseNumber("(-3)/(-2)");


            Assert.AreEqual(1,exp1.GetNumber());
            Assert.AreEqual(0.66666666666666663,exp2.GetNumber());
            Assert.AreEqual(1.5,exp3.GetNumber());

            Assert.AreEqual(-1,exp4.GetNumber());
            Assert.AreEqual(-0.66666666666666663,exp5.GetNumber());
            Assert.AreEqual(-1.5,exp6.GetNumber());

            Assert.AreEqual(-1,exp7.GetNumber());
            Assert.AreEqual(-0.66666666666666663,exp8.GetNumber());
            Assert.AreEqual(-1.5,exp9.GetNumber());

            Assert.AreEqual(1,exp10.GetNumber());
            Assert.AreEqual(0.66666666666666663,exp11.GetNumber());
            Assert.AreEqual(1.5,exp12.GetNumber());
        }

        [Test]
        public void ToThePowerOf()
        {
            Parser parser = new Parser();

            // + to the power of +
            NumberExpression exp1 = parser.ParseNumber("2^2");
            NumberExpression exp2 = parser.ParseNumber("2^3");
            NumberExpression exp3 = parser.ParseNumber("3^2");

            // + to the power of -
            NumberExpression exp4 = parser.ParseNumber("2^(-2)");
            NumberExpression exp5 = parser.ParseNumber("2^(-3)");
            NumberExpression exp6 = parser.ParseNumber("3^(-2)");

            // - to the power of +
            NumberExpression exp7 = parser.ParseNumber("(-2)^2");
            NumberExpression exp8 = parser.ParseNumber("(-2)^3");
            NumberExpression exp9 = parser.ParseNumber("(-3)^2");

            // - to the power of -
            NumberExpression exp10 = parser.ParseNumber("(-2)^(-2)");
            NumberExpression exp11 = parser.ParseNumber("(-2)^(-3)");
            NumberExpression exp12 = parser.ParseNumber("(-3)^(-2)");


            Assert.AreEqual(4,exp1.GetNumber());
            Assert.AreEqual(8,exp2.GetNumber());
            Assert.AreEqual(9,exp3.GetNumber());

            Assert.AreEqual(0.25,exp4.GetNumber());
            Assert.AreEqual(0.125,exp5.GetNumber());
            Assert.AreEqual( 0.1111111111111111,exp6.GetNumber());

            Assert.AreEqual(4,exp7.GetNumber());
            Assert.AreEqual(-8,exp8.GetNumber());
            Assert.AreEqual(9,exp9.GetNumber());

            Assert.AreEqual(0.25,exp10.GetNumber());
            Assert.AreEqual(-0.125,exp11.GetNumber());
            Assert.AreEqual(0.1111111111111111,exp12.GetNumber());
        }
    }

    public class BasicLogicalOperations
    {
        [Test]
        public void Equality()
        {
            Parser parser = new Parser();

            // + == +
            LogicExpression exp1 = parser.Parse("1==1");
            LogicExpression exp2 = parser.Parse("1==2");
            LogicExpression exp3 = parser.Parse("2==1");

            // + == -
            LogicExpression exp4 = parser.Parse("1==-1");
            LogicExpression exp5 = parser.Parse("1==-2");
            LogicExpression exp6 = parser.Parse("2==-1");

            // - == +
            LogicExpression exp7 = parser.Parse("-1==1");
            LogicExpression exp8 = parser.Parse("-1==2");
            LogicExpression exp9 = parser.Parse("-2==1");

            // - == -
            LogicExpression exp10 = parser.Parse("-1==-1");
            LogicExpression exp11 = parser.Parse("-1==-2");
            LogicExpression exp12 = parser.Parse("-2==-1");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());

            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());

            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());
        }

        [Test]
        public void Inequality()
        {
            Parser parser = new Parser();

            // + != +
            LogicExpression exp1 = parser.Parse("1!=1");
            LogicExpression exp2 = parser.Parse("1!=2");
            LogicExpression exp3 = parser.Parse("2!=1");

            // + != -
            LogicExpression exp4 = parser.Parse("1!=-1");
            LogicExpression exp5 = parser.Parse("1!=-2");
            LogicExpression exp6 = parser.Parse("2!=-1");

            // - != +
            LogicExpression exp7 = parser.Parse("-1!=1");
            LogicExpression exp8 = parser.Parse("-1!=2");
            LogicExpression exp9 = parser.Parse("-2!=1");

            // - != -
            LogicExpression exp10 = parser.Parse("-1!=-1");
            LogicExpression exp11 = parser.Parse("-1!=-2");
            LogicExpression exp12 = parser.Parse("-2!=-1");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }


        [Test]
        public void GreaterThan()
        {
            Parser parser = new Parser();

            // + > +
            LogicExpression exp1 = parser.Parse("1>1");
            LogicExpression exp2 = parser.Parse("1>2");
            LogicExpression exp3 = parser.Parse("2>1");

            // + > -
            LogicExpression exp4 = parser.Parse("1>-1");
            LogicExpression exp5 = parser.Parse("1>-2");
            LogicExpression exp6 = parser.Parse("2>-1");

            // - > +
            LogicExpression exp7 = parser.Parse("-1>1");
            LogicExpression exp8 = parser.Parse("-1>2");
            LogicExpression exp9 = parser.Parse("-2>1");

            // - > -
            LogicExpression exp10 = parser.Parse("-1>-1");
            LogicExpression exp11 = parser.Parse("-1>-2");
            LogicExpression exp12 = parser.Parse("-2>-1");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());
        }

        [Test]
        public void GreaterOrEqualThan()
        {
            Parser parser = new Parser();

            // + >= +
            LogicExpression exp1 = parser.Parse("1>=1");
            LogicExpression exp2 = parser.Parse("1>=2");
            LogicExpression exp3 = parser.Parse("2>=1");

            // + >= -
            LogicExpression exp4 = parser.Parse("1>=-1");
            LogicExpression exp5 = parser.Parse("1>=-2");
            LogicExpression exp6 = parser.Parse("2>=-1");

            // - >= +
            LogicExpression exp7 = parser.Parse("-1>=1");
            LogicExpression exp8 = parser.Parse("-1>=2");
            LogicExpression exp9 = parser.Parse("-2>=1");

            // - >= -
            LogicExpression exp10 = parser.Parse("-1>=-1");
            LogicExpression exp11 = parser.Parse("-1>=-2");
            LogicExpression exp12 = parser.Parse("-2>=-1");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());

            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());
        }


        [Test]
        public void LessThan()
        {
            Parser parser = new Parser();

            // + < +
            LogicExpression exp1 = parser.Parse("1<1");
            LogicExpression exp2 = parser.Parse("1<2");
            LogicExpression exp3 = parser.Parse("2<1");

            // + < -
            LogicExpression exp4 = parser.Parse("1<-1");
            LogicExpression exp5 = parser.Parse("1<-2");
            LogicExpression exp6 = parser.Parse("2<-1");

            // - < +
            LogicExpression exp7 = parser.Parse("-1<1");
            LogicExpression exp8 = parser.Parse("-1<2");
            LogicExpression exp9 = parser.Parse("-2<1");

            // - < -
            LogicExpression exp10 = parser.Parse("-1<-1");
            LogicExpression exp11 = parser.Parse("-1<-2");
            LogicExpression exp12 = parser.Parse("-2<-1");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());

            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }


        [Test]
        public void LessOrEqualThan()
        {
            Parser parser = new Parser();

            // + <= +
            LogicExpression exp1 = parser.Parse("1<=1");
            LogicExpression exp2 = parser.Parse("1<=2");
            LogicExpression exp3 = parser.Parse("2<=1");

            // + <= -
            LogicExpression exp4 = parser.Parse("1<=-1");
            LogicExpression exp5 = parser.Parse("1<=-2");
            LogicExpression exp6 = parser.Parse("2<=-1");

            // - <= +
            LogicExpression exp7 = parser.Parse("-1<=1");
            LogicExpression exp8 = parser.Parse("-1<=2");
            LogicExpression exp9 = parser.Parse("-2<=1");

            // - <= -
            LogicExpression exp10 = parser.Parse("-1<=-1");
            LogicExpression exp11 = parser.Parse("-1<=-2");
            LogicExpression exp12 = parser.Parse("-2<=-1");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());

            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }


        [Test]
        public void And1()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("true && true");
            LogicExpression exp2 = parser.Parse("true && false");
            LogicExpression exp3 = parser.Parse("false && true");
            LogicExpression exp4 = parser.Parse("false && false");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }

        [Test]
        public void And2()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("true and true");
            LogicExpression exp2 = parser.Parse("true and false");
            LogicExpression exp3 = parser.Parse("false and true");
            LogicExpression exp4 = parser.Parse("false and false");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }





        [Test]
        public void XOR1()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("true xor true");
            LogicExpression exp2 = parser.Parse("true xor false");
            LogicExpression exp3 = parser.Parse("false xor true");
            LogicExpression exp4 = parser.Parse("false xor false");

            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }

        [Test]
        public void XOR2()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("true ^ true");
            LogicExpression exp2 = parser.Parse("true ^ false");
            LogicExpression exp3 = parser.Parse("false ^ true");
            LogicExpression exp4 = parser.Parse("false ^ false");

            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }

        [Test]
        public void Or1()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("true or true");
            LogicExpression exp2 = parser.Parse("true or false");
            LogicExpression exp3 = parser.Parse("false or true");
            LogicExpression exp4 = parser.Parse("false or false");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }

        [Test]
        public void Or2()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("true || true");
            LogicExpression exp2 = parser.Parse("true || false");
            LogicExpression exp3 = parser.Parse("false || true");
            LogicExpression exp4 = parser.Parse("false || false");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }

        [Test]
        public void Not1() 
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("not true");
            LogicExpression exp2 = parser.Parse("not false");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
        }

        [Test]
        public void Not2() 
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse(" !true");
            LogicExpression exp2 = parser.Parse(" !false");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
        }

        [Test]
        public void BoolVarEquality() 
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("trueBoolean");
            LogicExpression exp2 = parser.Parse("falseBoolean");
            parser.ExpressionContext["trueBoolean"].Set(true);
            parser.ExpressionContext["falseBoolean"].Set(false);

            LogicExpression solution1 = parser.Parse("trueBoolean == true");
            LogicExpression solution2 = parser.Parse("falseBoolean == false");

            LogicExpression solution3 = parser.Parse("trueBoolean != true");
            LogicExpression solution4 = parser.Parse("falseBoolean != false");

            LogicExpression solution5 = parser.Parse("trueBoolean == false");
            LogicExpression solution6 = parser.Parse("falseBoolean == true");

            LogicExpression solution7 = parser.Parse("trueBoolean != false");
            LogicExpression solution8 = parser.Parse("falseBoolean != true");

            Assert.AreEqual(true, solution1.GetResult());
            Assert.AreEqual(true, solution2.GetResult());
            Assert.AreEqual(false, solution3.GetResult());
            Assert.AreEqual(false, solution4.GetResult());
            Assert.AreEqual(false, solution5.GetResult());
            Assert.AreEqual(false, solution6.GetResult());
            Assert.AreEqual(true, solution7.GetResult());
            Assert.AreEqual(true, solution8.GetResult());
        }

        [Test]
        public void EmptyExpressionLEP()
        {
            Parser parser = new Parser();

            LogicExpression solutionN = parser.Parse("");
            Assert.AreEqual(true, solutionN.GetResult());
        }

        [Test]
        public void DifferentLogicalOperatorsComparison()
        {
            Parser parser = new Parser();

            parser.ExpressionContext["number"].Set(1);
            LogicExpression test1 = parser.Parse("number == 1");

            Assert.AreEqual(true, test1.GetResult());

            parser.ExpressionContext["number"].Set(0);
            LogicExpression test2 = parser.Parse("number != 1");


            Assert.AreEqual(true, test2.GetResult());

            parser.ExpressionContext["number"].Set(1);
            LogicExpression test3 = parser.Parse("number != 1");

            Assert.AreEqual(false, test3.GetResult());
        }

        [Test]
        public void EqualityWithDifferentTypes()
        {
            Parser parser = new Parser();

            // + == +
            LogicExpression exp1 = parser.Parse("1.0==1");
            LogicExpression exp2 = parser.Parse("1==1.0");

            // + == -
            LogicExpression exp3 = parser.Parse("1.0==-1");
            LogicExpression exp4 = parser.Parse("1==-1.0");

            // - == +
            LogicExpression exp5 = parser.Parse("-1.0==1");
            LogicExpression exp6 = parser.Parse("-1==1.0");

            // - == -
            LogicExpression exp7 = parser.Parse("-1.0==-1");
            LogicExpression exp8 = parser.Parse("-1==-1.0");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());
            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
        }

        [Test]
        public void InequalityWithDifferentTypes()
        {
            Parser parser = new Parser();

            // + != +
            LogicExpression exp1 = parser.Parse("1.0!=1");
            LogicExpression exp2 = parser.Parse("1.0!=2");
            LogicExpression exp3 = parser.Parse("2!=1.0");

            // + != -
            LogicExpression exp4 = parser.Parse("1.0!=-1");
            LogicExpression exp5 = parser.Parse("1.0!=-2");
            LogicExpression exp6 = parser.Parse("2!=-1.0");

            // - != +
            LogicExpression exp7 = parser.Parse("-1.0!=1");
            LogicExpression exp8 = parser.Parse("-1.0!=2");
            LogicExpression exp9 = parser.Parse("-2!=1.0");

            // - != -
            LogicExpression exp10 = parser.Parse("-1.0!=-1");
            LogicExpression exp11 = parser.Parse("-1.0!=-2");
            LogicExpression exp12 = parser.Parse("-2!=-1.0");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }

        [Test]
        public void GreaterThanWithDifferentTypes()
        {
            Parser parser = new Parser();

            // + > +
            LogicExpression exp1 = parser.Parse("1.0>1");
            LogicExpression exp2 = parser.Parse("1.0>2");
            LogicExpression exp3 = parser.Parse("2>1.0");

            // + > -
            LogicExpression exp4 = parser.Parse("1.0>-1");
            LogicExpression exp5 = parser.Parse("1.0>-2");
            LogicExpression exp6 = parser.Parse("2>-1.0");

            // - > +
            LogicExpression exp7 = parser.Parse("-1.0>1");
            LogicExpression exp8 = parser.Parse("-1.0>2");
            LogicExpression exp9 = parser.Parse("-2>1.0");

            // - > -
            LogicExpression exp10 = parser.Parse("-1.0>-1");
            LogicExpression exp11 = parser.Parse("-1.0>-2");
            LogicExpression exp12 = parser.Parse("-2>-1.0");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());
        }

        [Test]
        public void GreaterOrEqualThanWithDifferentTypes()
        {
            Parser parser = new Parser();

            // + >= +
            LogicExpression exp1 = parser.Parse("1.0>=1");
            LogicExpression exp2 = parser.Parse("1.0>=2");
            LogicExpression exp3 = parser.Parse("2>=1.0");

            // + >= -
            LogicExpression exp4 = parser.Parse("1.0>=-1");
            LogicExpression exp5 = parser.Parse("1.0>=-2");
            LogicExpression exp6 = parser.Parse("2>=-1.0");

            // - >= +
            LogicExpression exp7 = parser.Parse("-1.0>=1");
            LogicExpression exp8 = parser.Parse("-1.0>=2");
            LogicExpression exp9 = parser.Parse("-2>=1.0");

            // - >= -
            LogicExpression exp10 = parser.Parse("-1.0>=-1");
            LogicExpression exp11 = parser.Parse("-1.0>=-2");
            LogicExpression exp12 = parser.Parse("-2>=-1.0");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());

            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());
        }


        [Test]
        public void LessThanWithDifferentTypes()
        {
            Parser parser = new Parser();

            // + < +
            LogicExpression exp1 = parser.Parse("1.0<1");
            LogicExpression exp2 = parser.Parse("1.0<2");
            LogicExpression exp3 = parser.Parse("2<1.0");

            // + < -
            LogicExpression exp4 = parser.Parse("1.0<-1");
            LogicExpression exp5 = parser.Parse("1.0<-2");
            LogicExpression exp6 = parser.Parse("2<-1.0");

            // - < +
            LogicExpression exp7 = parser.Parse("-1.0<1");
            LogicExpression exp8 = parser.Parse("-1.0<2");
            LogicExpression exp9 = parser.Parse("-2<1.0");

            // - < -
            LogicExpression exp10 = parser.Parse("-1.0<-1");
            LogicExpression exp11 = parser.Parse("-1.0<-2");
            LogicExpression exp12 = parser.Parse("-2<-1.0");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());

            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }


        [Test]
        public void LessOrEqualThanWithDifferentTypes()
        {
            Parser parser = new Parser();

            // + <= +
            LogicExpression exp1 = parser.Parse("1.0<=1");
            LogicExpression exp2 = parser.Parse("1.0<=2");
            LogicExpression exp3 = parser.Parse("2<=1.0");

            // + <= -
            LogicExpression exp4 = parser.Parse("1.0<=-1");
            LogicExpression exp5 = parser.Parse("1.0<=-2");
            LogicExpression exp6 = parser.Parse("2<=-1.0");

            // - <= +
            LogicExpression exp7 = parser.Parse("-1.0<=1");
            LogicExpression exp8 = parser.Parse("-1.0<=2");
            LogicExpression exp9 = parser.Parse("-2<=1.0");

            // - <= -
            LogicExpression exp10 = parser.Parse("-1.0<=-1");
            LogicExpression exp11 = parser.Parse("-1.0<=-2");
            LogicExpression exp12 = parser.Parse("-2<=-1.0");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());

            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }
    }

    public class BasicStringOperations
    {
        [Test]
        public void Equality()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[a]==[a]");
            LogicExpression exp2 = parser.Parse("[a]==[b]");
            LogicExpression exp3 = parser.Parse("[b]==[a]");
            LogicExpression exp4 = parser.Parse("[a a]==[a a]");
            LogicExpression exp5 = parser.Parse("[a a]==[a b]");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(true, exp4.GetResult());
            Assert.AreEqual(false, exp5.GetResult());
        }

        [Test]
        public void Inequality()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[a]!=[a]");
            LogicExpression exp2 = parser.Parse("[a]!=[b]");
            LogicExpression exp3 = parser.Parse("[b]!=[a]");

            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
        }

        [Test]
        public void GreaterThan()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[a]>[a]");
            LogicExpression exp2 = parser.Parse("[a]>[b]");
            LogicExpression exp3 = parser.Parse("[b]>[a]");

            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
        }

        [Test]
        public void GreaterOrEqualThan()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[a]>=[a]");
            LogicExpression exp2 = parser.Parse("[a]>=[b]");
            LogicExpression exp3 = parser.Parse("[b]>=[a]");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
        }

        [Test]
        public void LessThan()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[a]<[a]");
            LogicExpression exp2 = parser.Parse("[a]<[b]");
            LogicExpression exp3 = parser.Parse("[b]<[a]");

            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
        }

        [Test]
        public void LessOrEqualThan()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[a]<=[a]");
            LogicExpression exp2 = parser.Parse("[a]<=[b]");
            LogicExpression exp3 = parser.Parse("[b]<=[a]");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
        }

        [Test]
        public void Contains()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[abcd] contains [a]");
            LogicExpression exp2 = parser.Parse("[abcd] contains [z]");
            LogicExpression exp3 = parser.Parse("[a] contains [abcd]");
            LogicExpression exp4 = parser.Parse("[z] contains [abcd]");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }

        [Test]
        public void StartsWith()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[abcd] starts with [a]");
            LogicExpression exp2 = parser.Parse("[abcd] starts with [d]");
            LogicExpression exp3 = parser.Parse("[a] starts with [abcd]");
            LogicExpression exp4 = parser.Parse("[d] starts with [abcd]");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }

        [Test]
        public void EndsWith()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("[abcd] ends with [d]");
            LogicExpression exp2 = parser.Parse("[abcd] ends with [a]");
            LogicExpression exp3 = parser.Parse("[a] ends with [abcd]");
            LogicExpression exp4 = parser.Parse("[d] ends with [abcd]");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
        }
    }

    public class SetVariables
    {
        [Test]
        public void SetDouble()
        {
            Parser parser = new Parser();

            NumberExpression exp1 = parser.ParseNumber("variable1");
            exp1["variable1"].Set(1);
            NumberExpression exp2 = parser.ParseNumber("variable2");
            exp2["variable2"].Set(-1);
            NumberExpression exp3 = parser.ParseNumber("variable3");
            exp3["variable3"].Set(0);
            NumberExpression exp4 = parser.ParseNumber("variable4");
            exp4["variable4"].Set(3.14159);
            NumberExpression exp5 = parser.ParseNumber("variable5");
            exp4["variable5"].Set(-3.14159);

            Assert.AreEqual(1,exp1.GetNumber());
            Assert.AreEqual(-1,exp2.GetNumber());
            Assert.AreEqual(0,exp3.GetNumber());
            Assert.AreEqual(3.14159,exp4.GetNumber());
            Assert.AreEqual(-3.14159,exp5.GetNumber());
        }

        [Test]
        public void SetBool()
        {
            Parser parser = new Parser();

            LogicExpression exp1 = parser.Parse("variable1");
            exp1["variable1"].Set(true);
            LogicExpression exp2 = parser.Parse("variable2");
            exp2["variable2"].Set(false);

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
        }

        [Test]
        public void MissingVariables()
        {
            Parser parser = new Parser();
            parser.ExpressionContext["variable2"].Set(10);

            LogicExpression exp1 = parser.Parse("");

            LogicExpression exp2 = parser.Parse("variable1");

            LogicExpression exp3 = parser.Parse("variable1 == 5 || variable2 == 10");

            Assert.AreEqual(true,exp1.GetResult());
            //Assert.AreEqual(false,exp2.GetResult()); // does not work
            Assert.AreEqual(true,exp3.GetResult());
        }
    }

    public class CompareVariables
    {
        [Test]
        public void equalityWithIntegerVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1);
            parser.ExpressionContext["oneVar2"].Set(1);
            parser.ExpressionContext["twoVar1"].Set(2);

            // + == +
            LogicExpression exp1 = parser.Parse("oneVar1==oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2==oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1==twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2==twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1==oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1==oneVar2");

            // + == -
            LogicExpression exp7 = parser.Parse("oneVar1==-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2==-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1==-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2==-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1==-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1==-oneVar2");

            // - == +
            LogicExpression exp13 = parser.Parse("-oneVar1==oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2==oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1==twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2==twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1==oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1==oneVar2");

            // - == -
            LogicExpression exp19 = parser.Parse("-oneVar1==-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2==-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1==-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2==-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1==-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1==-oneVar2");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());
            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());

            Assert.AreEqual(false,exp13.GetResult());
            Assert.AreEqual(false,exp14.GetResult());
            Assert.AreEqual(false,exp15.GetResult());
            Assert.AreEqual(false,exp16.GetResult());
            Assert.AreEqual(false,exp17.GetResult());
            Assert.AreEqual(false,exp18.GetResult());

            Assert.AreEqual(true,exp19.GetResult());
            Assert.AreEqual(true,exp20.GetResult());
            Assert.AreEqual(false,exp21.GetResult());
            Assert.AreEqual(false,exp22.GetResult());
            Assert.AreEqual(false,exp23.GetResult());
            Assert.AreEqual(false,exp24.GetResult());
        }

        [Test]
        public void inequalityWithIntegerVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1);
            parser.ExpressionContext["oneVar2"].Set(1);
            parser.ExpressionContext["twoVar1"].Set(2);

            // + != +
            LogicExpression exp1 = parser.Parse("oneVar1!=oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2!=oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1!=twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2!=twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1!=oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1!=oneVar2");

            // + != -
            LogicExpression exp7 = parser.Parse("oneVar1!=-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2!=-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1!=-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2!=-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1!=-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1!=-oneVar2");

            // - != +
            LogicExpression exp13 = parser.Parse("-oneVar1!=oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2!=oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1!=twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2!=twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1!=oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1!=oneVar2");

            // - != -
            LogicExpression exp19 = parser.Parse("-oneVar1!=-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2!=-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1!=-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2!=-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1!=-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1!=-oneVar2");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());
            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());

            Assert.AreEqual(true,exp13.GetResult());
            Assert.AreEqual(true,exp14.GetResult());
            Assert.AreEqual(true,exp15.GetResult());
            Assert.AreEqual(true,exp16.GetResult());
            Assert.AreEqual(true,exp17.GetResult());
            Assert.AreEqual(true,exp18.GetResult());

            Assert.AreEqual(false,exp19.GetResult());
            Assert.AreEqual(false,exp20.GetResult());
            Assert.AreEqual(true,exp21.GetResult());
            Assert.AreEqual(true,exp22.GetResult());
            Assert.AreEqual(true,exp23.GetResult());
            Assert.AreEqual(true,exp24.GetResult());
        }

        public void greaterThanWithIntegerVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1);
            parser.ExpressionContext["oneVar2"].Set(1);
            parser.ExpressionContext["twoVar1"].Set(2);

            // + > +
            LogicExpression exp1 = parser.Parse("oneVar1>oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2>oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1>twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2>twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1>oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1>oneVar2");

            // + > -
            LogicExpression exp7 = parser.Parse("oneVar1>-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2>-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1>-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2>-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1>-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1>-oneVar2");

            // - > +
            LogicExpression exp13 = parser.Parse("-oneVar1>oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2>oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1>twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2>twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1>oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1>oneVar2");

            // - > -
            LogicExpression exp19 = parser.Parse("-oneVar1>-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2>-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1>-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2>-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1>-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1>-oneVar2");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());
            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());

            Assert.AreEqual(false,exp13.GetResult());
            Assert.AreEqual(false,exp14.GetResult());
            Assert.AreEqual(false,exp15.GetResult());
            Assert.AreEqual(false,exp16.GetResult());
            Assert.AreEqual(false,exp17.GetResult());
            Assert.AreEqual(false,exp18.GetResult());

            Assert.AreEqual(false,exp19.GetResult());
            Assert.AreEqual(false,exp20.GetResult());
            Assert.AreEqual(true,exp21.GetResult());
            Assert.AreEqual(true,exp22.GetResult());
            Assert.AreEqual(false,exp23.GetResult());
            Assert.AreEqual(false,exp24.GetResult());
        }

        [Test]
        public void greaterOrEqualThanWithIntegerVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1);
            parser.ExpressionContext["oneVar2"].Set(1);
            parser.ExpressionContext["twoVar1"].Set(2);

            // + >= +
            LogicExpression exp1 = parser.Parse("oneVar1>=oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2>=oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1>=twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2>=twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1>=oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1>=oneVar2");

            // + >= -
            LogicExpression exp7 = parser.Parse("oneVar1>=-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2>=-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1>=-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2>=-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1>=-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1>=-oneVar2");

            // - >= +
            LogicExpression exp13 = parser.Parse("-oneVar1>=oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2>=oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1>=twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2>=twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1>=oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1>=oneVar2");

            // - >= -
            LogicExpression exp19 = parser.Parse("-oneVar1>=-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2>=-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1>=-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2>=-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1>=-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1>=-oneVar2");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());
            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());

            Assert.AreEqual(false,exp13.GetResult());
            Assert.AreEqual(false,exp14.GetResult());
            Assert.AreEqual(false,exp15.GetResult());
            Assert.AreEqual(false,exp16.GetResult());
            Assert.AreEqual(false,exp17.GetResult());
            Assert.AreEqual(false,exp18.GetResult());

            Assert.AreEqual(true,exp19.GetResult());
            Assert.AreEqual(true,exp20.GetResult());
            Assert.AreEqual(true,exp21.GetResult());
            Assert.AreEqual(true,exp22.GetResult());
            Assert.AreEqual(false,exp23.GetResult());
            Assert.AreEqual(false,exp24.GetResult());
        }

        [Test]
        public void lessThanWithIntegerVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1);
            parser.ExpressionContext["oneVar2"].Set(1);
            parser.ExpressionContext["twoVar1"].Set(2);

            // + < +
            LogicExpression exp1 = parser.Parse("oneVar1<oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2<oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1<twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2<twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1<oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1<oneVar2");

            // + < -
            LogicExpression exp7 = parser.Parse("oneVar1<-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2<-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1<-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2<-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1<-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1<-oneVar2");

            // - < +
            LogicExpression exp13 = parser.Parse("-oneVar1<oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2<oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1<twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2<twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1<oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1<oneVar2");

            // - < -
            LogicExpression exp19 = parser.Parse("-oneVar1<-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2<-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1<-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2<-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1<-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1<-oneVar2");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());
            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());

            Assert.AreEqual(true,exp13.GetResult());
            Assert.AreEqual(true,exp14.GetResult());
            Assert.AreEqual(true,exp15.GetResult());
            Assert.AreEqual(true,exp16.GetResult());
            Assert.AreEqual(true,exp17.GetResult());
            Assert.AreEqual(true,exp18.GetResult());

            Assert.AreEqual(false,exp19.GetResult());
            Assert.AreEqual(false,exp20.GetResult());
            Assert.AreEqual(false,exp21.GetResult());
            Assert.AreEqual(false,exp22.GetResult());
            Assert.AreEqual(true,exp23.GetResult());
            Assert.AreEqual(true,exp24.GetResult());
        }

        [Test]
        public void lessOrEqualThanWithIntegerVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1);
            parser.ExpressionContext["oneVar2"].Set(1);
            parser.ExpressionContext["twoVar1"].Set(2);

            // + <= +
            LogicExpression exp1 = parser.Parse("oneVar1<=oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2<=oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1<=twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2<=twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1<=oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1<=oneVar2");

            // + <= -
            LogicExpression exp7 = parser.Parse("oneVar1<=-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2<=-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1<=-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2<=-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1<=-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1<=-oneVar2");

            // - <= +
            LogicExpression exp13 = parser.Parse("-oneVar1<=oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2<=oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1<=twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2<=twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1<=oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1<=oneVar2");

            // - <= -
            LogicExpression exp19 = parser.Parse("-oneVar1<=-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2<=-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1<=-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2<=-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1<=-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1<=-oneVar2");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());
            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());

            Assert.AreEqual(true,exp13.GetResult());
            Assert.AreEqual(true,exp14.GetResult());
            Assert.AreEqual(true,exp15.GetResult());
            Assert.AreEqual(true,exp16.GetResult());
            Assert.AreEqual(true,exp17.GetResult());
            Assert.AreEqual(true,exp18.GetResult());

            Assert.AreEqual(true,exp19.GetResult());
            Assert.AreEqual(true,exp20.GetResult());
            Assert.AreEqual(false,exp21.GetResult());
            Assert.AreEqual(false,exp22.GetResult());
            Assert.AreEqual(true,exp23.GetResult());
            Assert.AreEqual(true,exp24.GetResult());
        }

        [Test]
        public void equalityWithDoubleVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1.0);
            parser.ExpressionContext["oneVar2"].Set(1.0);
            parser.ExpressionContext["twoVar1"].Set(2.0);

            // + == +
            LogicExpression exp1 = parser.Parse("oneVar1==oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2==oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1==twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2==twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1==oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1==oneVar2");

            // + == -
            LogicExpression exp7 = parser.Parse("oneVar1==-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2==-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1==-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2==-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1==-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1==-oneVar2");

            // - == +
            LogicExpression exp13 = parser.Parse("-oneVar1==oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2==oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1==twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2==twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1==oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1==oneVar2");

            // - == -
            LogicExpression exp19 = parser.Parse("-oneVar1==-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2==-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1==-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2==-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1==-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1==-oneVar2");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());
            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());

            Assert.AreEqual(false,exp13.GetResult());
            Assert.AreEqual(false,exp14.GetResult());
            Assert.AreEqual(false,exp15.GetResult());
            Assert.AreEqual(false,exp16.GetResult());
            Assert.AreEqual(false,exp17.GetResult());
            Assert.AreEqual(false,exp18.GetResult());

            Assert.AreEqual(true,exp19.GetResult());
            Assert.AreEqual(true,exp20.GetResult());
            Assert.AreEqual(false,exp21.GetResult());
            Assert.AreEqual(false,exp22.GetResult());
            Assert.AreEqual(false,exp23.GetResult());
            Assert.AreEqual(false,exp24.GetResult());
        }

        [Test]
        public void inequalityWithDoubleVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1.0);
            parser.ExpressionContext["oneVar2"].Set(1.0);
            parser.ExpressionContext["twoVar1"].Set(2.0);

            // + != +
            LogicExpression exp1 = parser.Parse("oneVar1!=oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2!=oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1!=twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2!=twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1!=oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1!=oneVar2");

            // + != -
            LogicExpression exp7 = parser.Parse("oneVar1!=-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2!=-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1!=-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2!=-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1!=-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1!=-oneVar2");

            // - != +
            LogicExpression exp13 = parser.Parse("-oneVar1!=oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2!=oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1!=twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2!=twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1!=oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1!=oneVar2");

            // - != -
            LogicExpression exp19 = parser.Parse("-oneVar1!=-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2!=-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1!=-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2!=-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1!=-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1!=-oneVar2");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());
            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());

            Assert.AreEqual(true,exp13.GetResult());
            Assert.AreEqual(true,exp14.GetResult());
            Assert.AreEqual(true,exp15.GetResult());
            Assert.AreEqual(true,exp16.GetResult());
            Assert.AreEqual(true,exp17.GetResult());
            Assert.AreEqual(true,exp18.GetResult());

            Assert.AreEqual(false,exp19.GetResult());
            Assert.AreEqual(false,exp20.GetResult());
            Assert.AreEqual(true,exp21.GetResult());
            Assert.AreEqual(true,exp22.GetResult());
            Assert.AreEqual(true,exp23.GetResult());
            Assert.AreEqual(true,exp24.GetResult());
        }

        public void greaterThanWithDoubleVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1.0);
            parser.ExpressionContext["oneVar2"].Set(1.0);
            parser.ExpressionContext["twoVar1"].Set(2.0);

            // + > +
            LogicExpression exp1 = parser.Parse("oneVar1>oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2>oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1>twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2>twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1>oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1>oneVar2");

            // + > -
            LogicExpression exp7 = parser.Parse("oneVar1>-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2>-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1>-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2>-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1>-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1>-oneVar2");

            // - > +
            LogicExpression exp13 = parser.Parse("-oneVar1>oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2>oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1>twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2>twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1>oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1>oneVar2");

            // - > -
            LogicExpression exp19 = parser.Parse("-oneVar1>-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2>-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1>-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2>-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1>-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1>-oneVar2");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());
            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());

            Assert.AreEqual(false,exp13.GetResult());
            Assert.AreEqual(false,exp14.GetResult());
            Assert.AreEqual(false,exp15.GetResult());
            Assert.AreEqual(false,exp16.GetResult());
            Assert.AreEqual(false,exp17.GetResult());
            Assert.AreEqual(false,exp18.GetResult());

            Assert.AreEqual(false,exp19.GetResult());
            Assert.AreEqual(false,exp20.GetResult());
            Assert.AreEqual(true,exp21.GetResult());
            Assert.AreEqual(true,exp22.GetResult());
            Assert.AreEqual(false,exp23.GetResult());
            Assert.AreEqual(false,exp24.GetResult());
        }

        [Test]
        public void greaterOrEqualThanWithDoubleVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1.0);
            parser.ExpressionContext["oneVar2"].Set(1.0);
            parser.ExpressionContext["twoVar1"].Set(2.0);

            // + >= +
            LogicExpression exp1 = parser.Parse("oneVar1>=oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2>=oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1>=twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2>=twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1>=oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1>=oneVar2");

            // + >= -
            LogicExpression exp7 = parser.Parse("oneVar1>=-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2>=-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1>=-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2>=-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1>=-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1>=-oneVar2");

            // - >= +
            LogicExpression exp13 = parser.Parse("-oneVar1>=oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2>=oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1>=twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2>=twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1>=oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1>=oneVar2");

            // - >= -
            LogicExpression exp19 = parser.Parse("-oneVar1>=-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2>=-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1>=-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2>=-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1>=-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1>=-oneVar2");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());
            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());

            Assert.AreEqual(false,exp13.GetResult());
            Assert.AreEqual(false,exp14.GetResult());
            Assert.AreEqual(false,exp15.GetResult());
            Assert.AreEqual(false,exp16.GetResult());
            Assert.AreEqual(false,exp17.GetResult());
            Assert.AreEqual(false,exp18.GetResult());

            Assert.AreEqual(true,exp19.GetResult());
            Assert.AreEqual(true,exp20.GetResult());
            Assert.AreEqual(true,exp21.GetResult());
            Assert.AreEqual(true,exp22.GetResult());
            Assert.AreEqual(false,exp23.GetResult());
            Assert.AreEqual(false,exp24.GetResult());
        }

        [Test]
        public void lessThanWithDoubleVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1.0);
            parser.ExpressionContext["oneVar2"].Set(1.0);
            parser.ExpressionContext["twoVar1"].Set(2.0);

            // + < +
            LogicExpression exp1 = parser.Parse("oneVar1<oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2<oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1<twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2<twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1<oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1<oneVar2");

            // + < -
            LogicExpression exp7 = parser.Parse("oneVar1<-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2<-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1<-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2<-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1<-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1<-oneVar2");

            // - < +
            LogicExpression exp13 = parser.Parse("-oneVar1<oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2<oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1<twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2<twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1<oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1<oneVar2");

            // - < -
            LogicExpression exp19 = parser.Parse("-oneVar1<-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2<-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1<-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2<-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1<-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1<-oneVar2");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());
            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());

            Assert.AreEqual(true,exp13.GetResult());
            Assert.AreEqual(true,exp14.GetResult());
            Assert.AreEqual(true,exp15.GetResult());
            Assert.AreEqual(true,exp16.GetResult());
            Assert.AreEqual(true,exp17.GetResult());
            Assert.AreEqual(true,exp18.GetResult());

            Assert.AreEqual(false,exp19.GetResult());
            Assert.AreEqual(false,exp20.GetResult());
            Assert.AreEqual(false,exp21.GetResult());
            Assert.AreEqual(false,exp22.GetResult());
            Assert.AreEqual(true,exp23.GetResult());
            Assert.AreEqual(true,exp24.GetResult());
        }

        [Test]
        public void lessOrEqualThanWithDoubleVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneVar1");
            NumberExpression expN2 = parser.ParseNumber("oneVar2");
            NumberExpression expN3 = parser.ParseNumber("twoVar1");
            parser.ExpressionContext["oneVar1"].Set(1.0);
            parser.ExpressionContext["oneVar2"].Set(1.0);
            parser.ExpressionContext["twoVar1"].Set(2.0);

            // + <= +
            LogicExpression exp1 = parser.Parse("oneVar1<=oneVar2");
            LogicExpression exp2 = parser.Parse("oneVar2<=oneVar1");
            LogicExpression exp3 = parser.Parse("oneVar1<=twoVar1");
            LogicExpression exp4 = parser.Parse("oneVar2<=twoVar1");
            LogicExpression exp5 = parser.Parse("twoVar1<=oneVar1");
            LogicExpression exp6 = parser.Parse("twoVar1<=oneVar2");

            // + <= -
            LogicExpression exp7 = parser.Parse("oneVar1<=-oneVar2");
            LogicExpression exp8 = parser.Parse("oneVar2<=-oneVar1");
            LogicExpression exp9 = parser.Parse("oneVar1<=-twoVar1");
            LogicExpression exp10 = parser.Parse("oneVar2<=-twoVar1");
            LogicExpression exp11 = parser.Parse("twoVar1<=-oneVar1");
            LogicExpression exp12 = parser.Parse("twoVar1<=-oneVar2");

            // - <= +
            LogicExpression exp13 = parser.Parse("-oneVar1<=oneVar2");
            LogicExpression exp14 = parser.Parse("-oneVar2<=oneVar1");
            LogicExpression exp15 = parser.Parse("-oneVar1<=twoVar1");
            LogicExpression exp16 = parser.Parse("-oneVar2<=twoVar1");
            LogicExpression exp17 = parser.Parse("-twoVar1<=oneVar1");
            LogicExpression exp18 = parser.Parse("-twoVar1<=oneVar2");

            // - <= -
            LogicExpression exp19 = parser.Parse("-oneVar1<=-oneVar2");
            LogicExpression exp20 = parser.Parse("-oneVar2<=-oneVar1");
            LogicExpression exp21 = parser.Parse("-oneVar1<=-twoVar1");
            LogicExpression exp22 = parser.Parse("-oneVar2<=-twoVar1");
            LogicExpression exp23 = parser.Parse("-twoVar1<=-oneVar1");
            LogicExpression exp24 = parser.Parse("-twoVar1<=-oneVar2");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());
            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());
            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());

            Assert.AreEqual(true,exp13.GetResult());
            Assert.AreEqual(true,exp14.GetResult());
            Assert.AreEqual(true,exp15.GetResult());
            Assert.AreEqual(true,exp16.GetResult());
            Assert.AreEqual(true,exp17.GetResult());
            Assert.AreEqual(true,exp18.GetResult());

            Assert.AreEqual(true,exp19.GetResult());
            Assert.AreEqual(true,exp20.GetResult());
            Assert.AreEqual(false,exp21.GetResult());
            Assert.AreEqual(false,exp22.GetResult());
            Assert.AreEqual(true,exp23.GetResult());
            Assert.AreEqual(true,exp24.GetResult());
        }                                
        
        [Test]
        public void equalityWithDifferentTypesOfVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneInt");
            NumberExpression expN2 = parser.ParseNumber("oneDouble");
            parser.ExpressionContext["oneInt"].Set(1);
            parser.ExpressionContext["oneDouble"].Set(1.0);

            // + == +
            LogicExpression exp1 = parser.Parse("oneDouble==oneInt");
            LogicExpression exp2 = parser.Parse("oneInt==oneDouble");

            // + == -
            LogicExpression exp3 = parser.Parse("oneDouble==-oneInt");
            LogicExpression exp4 = parser.Parse("oneInt==-oneDouble");

            // - == +
            LogicExpression exp5 = parser.Parse("-oneDouble==oneInt");
            LogicExpression exp6 = parser.Parse("-oneInt==oneDouble");

            // - == -
            LogicExpression exp7 = parser.Parse("-oneDouble==-oneInt");
            LogicExpression exp8 = parser.Parse("-oneInt==-oneDouble");

            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());

            Assert.AreEqual(false,exp3.GetResult());
            Assert.AreEqual(false,exp4.GetResult());

            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
        }

        [Test]
        public void inequalityWithDifferentTypesOfVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneInt");
            NumberExpression expN2 = parser.ParseNumber("oneDouble");
            NumberExpression expN3 = parser.ParseNumber("twoDouble");
            parser.ExpressionContext["oneInt"].Set(1);
            parser.ExpressionContext["oneDouble"].Set(1.0);
            parser.ExpressionContext["twoDouble"].Set(2.0);

            // + != +
            LogicExpression exp1 = parser.Parse("oneDouble!=oneInt");
            LogicExpression exp2 = parser.Parse("oneDouble!=twoDouble");
            LogicExpression exp3 = parser.Parse("twoDouble!=oneDouble");

            // + != -
            LogicExpression exp4 = parser.Parse("oneDouble!=-oneInt");
            LogicExpression exp5 = parser.Parse("oneDouble!=-twoDouble");
            LogicExpression exp6 = parser.Parse("twoDouble!=-oneDouble");

            // - != +
            LogicExpression exp7 = parser.Parse("-oneDouble!=oneInt");
            LogicExpression exp8 = parser.Parse("-oneDouble!=twoDouble");
            LogicExpression exp9 = parser.Parse("-twoDouble!=oneDouble");

            // - != -
            LogicExpression exp10 = parser.Parse("-oneDouble!=-oneInt");
            LogicExpression exp11 = parser.Parse("-oneDouble!=-twoDouble");
            LogicExpression exp12 = parser.Parse("-twoDouble!=-oneDouble");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }

        [Test]
        public void greaterThanWithDifferentTypesOfVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneInt");
            NumberExpression expN2 = parser.ParseNumber("oneDouble");
            NumberExpression expN3 = parser.ParseNumber("twoDouble");
            parser.ExpressionContext["oneInt"].Set(1);
            parser.ExpressionContext["oneDouble"].Set(1.0);
            parser.ExpressionContext["twoDouble"].Set(2.0);

            // + > +
            LogicExpression exp1 = parser.Parse("oneDouble>oneInt");
            LogicExpression exp2 = parser.Parse("oneDouble>twoDouble");
            LogicExpression exp3 = parser.Parse("twoDouble>oneDouble");

            // + > -
            LogicExpression exp4 = parser.Parse("oneDouble>-oneInt");
            LogicExpression exp5 = parser.Parse("oneDouble>-twoDouble");
            LogicExpression exp6 = parser.Parse("twoDouble>-oneDouble");

            // - > +
            LogicExpression exp7 = parser.Parse("-oneDouble>oneInt");
            LogicExpression exp8 = parser.Parse("-oneDouble>twoDouble");
            LogicExpression exp9 = parser.Parse("-twoDouble>oneDouble");

            // - > -
            LogicExpression exp10 = parser.Parse("-oneDouble>-oneInt");
            LogicExpression exp11 = parser.Parse("-oneDouble>-twoDouble");
            LogicExpression exp12 = parser.Parse("-twoDouble>-oneDouble");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());
        }

        [Test]
        public void greaterOrEqualThanWithDifferentTypesOfVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneInt");
            NumberExpression expN2 = parser.ParseNumber("oneDouble");
            NumberExpression expN3 = parser.ParseNumber("twoDouble");
            parser.ExpressionContext["oneInt"].Set(1);
            parser.ExpressionContext["oneDouble"].Set(1.0);
            parser.ExpressionContext["twoDouble"].Set(2.0);

            // + >= +
            LogicExpression exp1 = parser.Parse("oneDouble>=oneInt");
            LogicExpression exp2 = parser.Parse("oneDouble>=twoDouble");
            LogicExpression exp3 = parser.Parse("twoDouble>=oneDouble");

            // + >= -
            LogicExpression exp4 = parser.Parse("oneDouble>=-oneInt");
            LogicExpression exp5 = parser.Parse("oneDouble>=-twoDouble");
            LogicExpression exp6 = parser.Parse("twoDouble>=-oneDouble");

            // - >= +
            LogicExpression exp7 = parser.Parse("-oneDouble>=oneInt");
            LogicExpression exp8 = parser.Parse("-oneDouble>=twoDouble");
            LogicExpression exp9 = parser.Parse("-twoDouble>=oneDouble");

            // - >= -
            LogicExpression exp10 = parser.Parse("-oneDouble>=-oneInt");
            LogicExpression exp11 = parser.Parse("-oneDouble>=-twoDouble");
            LogicExpression exp12 = parser.Parse("-twoDouble>=-oneDouble");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(false,exp2.GetResult());
            Assert.AreEqual(true,exp3.GetResult());

            Assert.AreEqual(true,exp4.GetResult());
            Assert.AreEqual(true,exp5.GetResult());
            Assert.AreEqual(true,exp6.GetResult());

            Assert.AreEqual(false,exp7.GetResult());
            Assert.AreEqual(false,exp8.GetResult());
            Assert.AreEqual(false,exp9.GetResult());

            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(true,exp11.GetResult());
            Assert.AreEqual(false,exp12.GetResult());
        }


        [Test]
        public void lessThanWithDifferentTypesOfVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneInt");
            NumberExpression expN2 = parser.ParseNumber("oneDouble");
            NumberExpression expN3 = parser.ParseNumber("twoDouble");
            parser.ExpressionContext["oneInt"].Set(1);
            parser.ExpressionContext["oneDouble"].Set(1.0);
            parser.ExpressionContext["twoDouble"].Set(2.0);

            // + < +
            LogicExpression exp1 = parser.Parse("oneDouble<oneInt");
            LogicExpression exp2 = parser.Parse("oneDouble<twoDouble");
            LogicExpression exp3 = parser.Parse("twoDouble<oneDouble");

            // + < -
            LogicExpression exp4 = parser.Parse("oneDouble<-oneInt");
            LogicExpression exp5 = parser.Parse("oneDouble<-twoDouble");
            LogicExpression exp6 = parser.Parse("twoDouble<-oneDouble");

            // - < +
            LogicExpression exp7 = parser.Parse("-oneDouble<oneInt");
            LogicExpression exp8 = parser.Parse("-oneDouble<twoDouble");
            LogicExpression exp9 = parser.Parse("-twoDouble<oneDouble");

            // - < -
            LogicExpression exp10 = parser.Parse("-oneDouble<-oneInt");
            LogicExpression exp11 = parser.Parse("-oneDouble<-twoDouble");
            LogicExpression exp12 = parser.Parse("-twoDouble<-oneDouble");


            Assert.AreEqual(false,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());

            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(false,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }


        [Test]
        public void lessOrEqualThanWithDifferentTypesOfVariables()
        {
            Parser parser = new Parser();

            //set variables
            NumberExpression expN1 = parser.ParseNumber("oneInt");
            NumberExpression expN2 = parser.ParseNumber("oneDouble");
            NumberExpression expN3 = parser.ParseNumber("twoDouble");
            parser.ExpressionContext["oneInt"].Set(1);
            parser.ExpressionContext["oneDouble"].Set(1.0);
            parser.ExpressionContext["twoDouble"].Set(2.0);

            // + <= +
            LogicExpression exp1 = parser.Parse("oneDouble<=oneInt");
            LogicExpression exp2 = parser.Parse("oneDouble<=twoDouble");
            LogicExpression exp3 = parser.Parse("twoDouble<=oneDouble");

            // + <= -
            LogicExpression exp4 = parser.Parse("oneDouble<=-oneInt");
            LogicExpression exp5 = parser.Parse("oneDouble<=-twoDouble");
            LogicExpression exp6 = parser.Parse("twoDouble<=-oneDouble");

            // - <= +
            LogicExpression exp7 = parser.Parse("-oneDouble<=oneInt");
            LogicExpression exp8 = parser.Parse("-oneDouble<=twoDouble");
            LogicExpression exp9 = parser.Parse("-twoDouble<=oneDouble");

            // - <= -
            LogicExpression exp10 = parser.Parse("-oneDouble<=-oneInt");
            LogicExpression exp11 = parser.Parse("-oneDouble<=-twoDouble");
            LogicExpression exp12 = parser.Parse("-twoDouble<=-oneDouble");


            Assert.AreEqual(true,exp1.GetResult());
            Assert.AreEqual(true,exp2.GetResult());
            Assert.AreEqual(false,exp3.GetResult());

            Assert.AreEqual(false,exp4.GetResult());
            Assert.AreEqual(false,exp5.GetResult());
            Assert.AreEqual(false,exp6.GetResult());

            Assert.AreEqual(true,exp7.GetResult());
            Assert.AreEqual(true,exp8.GetResult());
            Assert.AreEqual(true,exp9.GetResult());

            Assert.AreEqual(true,exp10.GetResult());
            Assert.AreEqual(false,exp11.GetResult());
            Assert.AreEqual(true,exp12.GetResult());
        }
    }
}       
