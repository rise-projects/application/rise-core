using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class WMParserTests
{
    [Test]
    /// <summary>
    /// Tests if every key in TestWM.json exists
    /// </summary>
    public void keyExists() {
        //create new empty gameobject and add JSONWorkingMemoryParser, WorkingMemoryController
        GameObject gameObject = new GameObject();
        JSONWorkingMemoryParser xmlWorkingMemoryParser = gameObject.AddComponent<JSONWorkingMemoryParser>();
        WorkingMemoryController workingMemoryController = gameObject.AddComponent<WorkingMemoryController>();

        //parse json
        workingMemoryController.ImportDataModel(xmlWorkingMemoryParser.ParseDataModels("Assets/Tests/PlayMode/Resources/TestWM.json", ""));

        //check if every key exist
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("String"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("StringEmpty"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("StringWhitespace"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("StringEscapeChars"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("StringUnicode"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("Number"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("NumberNegative"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("NumberZero"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("NumberLarge"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("BoolTrue"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("BoolFalse"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("Float"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("FloatNegative"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("FloatZero"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("FloatFraction"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("FloatLarge"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("FloatLargeNegative"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("UPPERCASEVAR"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("snake_case_var"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("kebab-case-var"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("scuffed!@#$%^&*()_+{}[];':,.<>?/|`~Var"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("varWithDigits0123456789"), true);
        Assert.AreEqual(workingMemoryController.KeyExistsInMemory("dot.case.var"), true);
    }

    [Test]
    /// <summary>
    /// Tests if key value in TestWM.json is equal to parsed key value
    /// </summary>
    public void keyParsedCorrectly() {
        //create new empty gameobject and add JSONWorkingMemoryParser, WorkingMemoryController
        GameObject gameObject = new GameObject();
        JSONWorkingMemoryParser xmlWorkingMemoryParser = gameObject.AddComponent<JSONWorkingMemoryParser>();
        WorkingMemoryController workingMemoryController = gameObject.AddComponent<WorkingMemoryController>();

        //parse json
        //wenn genau hier eine nullReferenceException ist, starte Test einzelnd (nicht run all drücken)
        workingMemoryController.ImportDataModel(xmlWorkingMemoryParser.ParseDataModels("Assets/Tests/PlayMode/Resources/TestWM.json", ""));

        //check if parsed key has the expected value
        Assert.AreEqual(workingMemoryController.GetMemoryData("String"), "Hallo ich bin 'RISE'");
        Assert.AreEqual(workingMemoryController.GetMemoryData("StringEmpty"), "");
        Assert.AreEqual(workingMemoryController.GetMemoryData("StringWhitespace"), "   ");
        Assert.AreEqual(workingMemoryController.GetMemoryData("StringEscapeChars"), "This contains escaped characters like \t, \n, and \"quotes\".");
        Assert.AreEqual(workingMemoryController.GetMemoryData("StringUnicode"), "This contains Unicode characters like \u00E4 and \u03B2.");
        Assert.AreEqual(workingMemoryController.GetMemoryData("Number"), 123);
        Assert.AreEqual(workingMemoryController.GetMemoryData("NumberNegative"), -456);
        Assert.AreEqual(workingMemoryController.GetMemoryData("NumberZero"), 0);
        Assert.AreEqual(workingMemoryController.GetMemoryData("NumberLarge"), 987654321);
        Assert.AreEqual(workingMemoryController.GetMemoryData("BoolTrue"), true);
        Assert.AreEqual(workingMemoryController.GetMemoryData("BoolFalse"), false);
        Assert.AreEqual(workingMemoryController.GetMemoryData("Float"), 0.23);
        Assert.AreEqual(workingMemoryController.GetMemoryData("FloatNegative"), -1.5);
        Assert.AreEqual(workingMemoryController.GetMemoryData("FloatZero"), 0.0);
        Assert.AreEqual(workingMemoryController.GetMemoryData("FloatFraction"), 3.14159);
        Assert.AreEqual(workingMemoryController.GetMemoryData("FloatLarge"), 123456789.987654);
        Assert.AreEqual(workingMemoryController.GetMemoryData("FloatLargeNegative"), -987654321.123456);
        Assert.AreEqual(workingMemoryController.GetMemoryData("UPPERCASEVAR"), "This is an UPPERCASEVAR");
        Assert.AreEqual(workingMemoryController.GetMemoryData("snake_case_var"), "This is a snake_case_var");
        Assert.AreEqual(workingMemoryController.GetMemoryData("kebab-case-var"), "This is a kebab-case-var");
        Assert.AreEqual(workingMemoryController.GetMemoryData("scuffed!@#$%^&*()_+{}[];':,.<>?/|`~Var"), "This is a scuffed var");
        Assert.AreEqual(workingMemoryController.GetMemoryData("varWithDigits0123456789"), "This is a var with digits");
        Assert.AreEqual(workingMemoryController.GetMemoryData("dot.case.var"), "This is a dot.case.var");
    }
}
