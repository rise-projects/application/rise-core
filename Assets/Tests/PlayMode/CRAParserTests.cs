using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class CRAParserTests
{
    [Test]
    public void numberOfParsedActions()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // check size of parsed CRAs from file
        Assert.AreEqual(13, craList.Count);
    }

    [Test]
    public void behaviorActionsExist()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // check if behavoirAction name is known
        List<string> behaviorActionNames = new List<string>{ "Speech1", "Speech2", "Speech3", "LookAt1", "LookAt2", "Emotion1", "Emotion2", "Animation1", "Animation2", "Wait1", "Wait2", "SocialExpression1", "SocialExpression2" };
        for(int i = 0; i < 13; i++)
        {
            Assert.AreEqual(behaviorActionNames[i], craList[i].communicationRobotActName);
        }
    }

    [Test]
    public void actionId()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // checks if action ids are parsed correctly. Assumption: actionIDs are always incremented by one.
        for(int i = 1; i <= 13; i++)
        {
            Assert.AreEqual(i, craList[i - 1].actionList[0].actionID);
        }
    }

    [Test]
    public void behaviorActionSpeech()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // get spech messages
        Dictionary<string, string> speech1Message = craList[0].actionList[0].GetAttributeDescriptions();
        Dictionary<string, string> speech2Message = craList[1].actionList[0].GetAttributeDescriptions();
        Dictionary<string, string> speech3Message = craList[2].actionList[0].GetAttributeDescriptions();

        // check if speech messages are parsed correctly
        foreach (var kvp in speech1Message)
            Assert.AreEqual("Du bist sehr emotional!", kvp.Value);
        foreach (var kvp in speech2Message)
            Assert.AreEqual("Hallo %Person1%, ich bin Floka!", kvp.Value);
        foreach (var kvp in speech3Message)
            Assert.AreEqual("%Person1% %Person2%", kvp.Value);
    }

    [Test]
    public void behaviorActionLookAt()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // get lookAt attributes
        Dictionary<string, string> lookAt1Attributes = craList[3].actionList[0].GetAttributeDescriptions();
        Dictionary<string, string> lookAt2Attributes = craList[4].actionList[0].GetAttributeDescriptions();

        // get lookAt attributes values

        List<int> lookAt1Values = new List<int>();
        foreach (var kvp in lookAt1Attributes)
            lookAt1Values.Add(int.Parse(kvp.Value));
        
        List<int> lookAt2Values = new List<int>();
        foreach (var kvp in lookAt2Attributes)
            lookAt2Values.Add(int.Parse(kvp.Value));

        // check if values are parsed correctly

        Assert.AreEqual(1, lookAt1Values[0]);
        Assert.AreEqual(1, lookAt1Values[1]);
        Assert.AreEqual(1, lookAt1Values[2]);
        Assert.AreEqual(1, lookAt1Values[3]);

        Assert.AreEqual(-1, lookAt2Values[0]);
        Assert.AreEqual(-1, lookAt2Values[1]);
        Assert.AreEqual(-1, lookAt2Values[2]);
        Assert.AreEqual(-1, lookAt2Values[3]);
    }

    [Test]
    public void behaviorActionEmotion()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // get emotion attributes
        Dictionary<string, string> emotion1Attributes = craList[5].actionList[0].GetAttributeDescriptions();
        Dictionary<string, string> emotion2Attributes = craList[6].actionList[0].GetAttributeDescriptions();

        // get emotion attributes values

        List<int> emotion1Values= new List<int>();
        foreach (var kvp in emotion1Attributes)
            emotion1Values.Add(int.Parse(kvp.Value));

        List<int> emotion2Values= new List<int>();
        foreach (var kvp in emotion2Attributes)
            emotion2Values.Add(int.Parse(kvp.Value));

        // check if values are parsed correctly
        Assert.AreEqual(1, emotion1Values[0]);
        Assert.AreEqual(5, emotion2Values[0]);
        
        // check if durations are parsed correctly
        Assert.AreEqual(2000, emotion1Values[1]);
        Assert.AreEqual(23, emotion2Values[1]);
    }

    [Test]
    public void behaviorActionAnimation()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // get animation attributes
        Dictionary<string, string> animation1Attributes = craList[7].actionList[0].GetAttributeDescriptions();
        Dictionary<string, string> animation2Attributes = craList[8].actionList[0].GetAttributeDescriptions();

        // get animation attributes values

        List<int> animation1Values= new List<int>();
        foreach (var kvp in animation1Attributes)
            animation1Values.Add(int.Parse(kvp.Value));

        List<int> animation2Values= new List<int>();
        foreach (var kvp in animation2Attributes)
            animation2Values.Add(int.Parse(kvp.Value));

        // check if targets are parsed correctly
        Assert.AreEqual(1, animation1Values[0]);
        Assert.AreEqual(2, animation2Values[0]);
        
        // check if repetitions are parsed correctly
        Assert.AreEqual(3, animation1Values[1]);
        Assert.AreEqual(100, animation2Values[1]);

        // check if duration_each is parsed correctly
        Assert.AreEqual(1000, animation1Values[2]);
        Assert.AreEqual(40, animation2Values[2]);

        // check if scale is parsed correctly
        Assert.AreEqual(1, animation1Values[3]);
        Assert.AreEqual(3, animation2Values[3]);
    }

    [Test]
    public void behaviorActionWait()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // get wait attributes
        Dictionary<string, string> wait1Attributes = craList[9].actionList[0].GetAttributeDescriptions();
        Dictionary<string, string> wait2Attributes = craList[10].actionList[0].GetAttributeDescriptions();

        // get wait attributes values

        List<int> wait1Values= new List<int>();
        foreach (var kvp in wait1Attributes)
            wait1Values.Add(int.Parse(kvp.Value));

        List<int> wait2Values= new List<int>();
        foreach (var kvp in wait2Attributes)
            wait2Values.Add(int.Parse(kvp.Value));

        // check if duration is parsed correctly
        Assert.AreEqual(9, wait1Values[0]);
        Assert.AreEqual(10000, wait2Values[0]);
    }

    [Test]
    public void behaviorActionSocialExpression()
    {
        // create new parser
        GameObject go = new GameObject();
        XMLCommunicationRobotActParser xmlCommunicationRobotActParser = go.AddComponent<XMLCommunicationRobotActParser>();

        // parse TestCRA.xml
        List<CommunicationRobotAct> craList = xmlCommunicationRobotActParser.ParseTrainingsActsFromFile("Assets/Tests/PlayMode/Resources/TestCRA.xml", "Assets/Tests/PlayMode/Resources/");

        // get socialExpression attributes
        Dictionary<string, string> socialExpression1Attributes = craList[11].actionList[0].GetAttributeDescriptions();
        Dictionary<string, string> socialExpression2Attributes = craList[12].actionList[0].GetAttributeDescriptions();

        // get socialExpression attributes values

        List<int> socialExpression1Values= new List<int>();
        foreach (var kvp in socialExpression1Attributes)
            socialExpression1Values.Add(int.Parse(kvp.Value));

        List<int> socialExpression2Values= new List<int>();
        foreach (var kvp in socialExpression2Attributes)
            socialExpression2Values.Add(int.Parse(kvp.Value));

        // check if values are parsed correctly
        Assert.AreEqual(1, socialExpression1Values[0]);
        Assert.AreEqual(3, socialExpression2Values[0]);
        
        // check if durations are parsed correctly
        Assert.AreEqual(200000, socialExpression1Values[1]);
        Assert.AreEqual(100, socialExpression2Values[1]);

        // check if intensitys are parsed correctly
        Assert.AreEqual(100, socialExpression1Values[2]);
        Assert.AreEqual(5, socialExpression2Values[2]);
    }
}