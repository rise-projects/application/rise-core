using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.Text.RegularExpressions;
using System;
using System.Linq.Expressions;

public class IRParserTests
{
    [Test]
    public void numberOfIRs()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");
        
        // Check size of parsed IRs from File
        Assert.AreEqual(1, irList.Count);
    }

    [Test]
    public void namesOfIRs()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");
        
        // Check if names of parsed IRs are parsed correctly
        List<string> irNameList = new List<string>{ "interactionRule1" };
        for(int i = 0; i < irList.Count; i++)
        {
            Assert.AreEqual(irNameList[i], irList[i].name);
        }
    }

    [Test]
    public void initialStatesOfIRs()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");
        
        // Check if initial states of IRs are parsed correctly
        List<string> irInitialStateList = new List<string>{ "initialState" };
        for(int i = 0; i < irList.Count; i++)
        {
            Assert.AreEqual(irInitialStateList[i], irList[i].initialState);
        }
    }

    [Test]
    public void priorityOfIRs()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");
        
        // Check if priority of IRs are parsed correctly
        List<int> irPriorityList = new List<int>{ 1 };
        for(int i = 0; i < irList.Count; i++)
        {
            Assert.AreEqual(irPriorityList[i], irList[i].priority); // ist aus irgendwelchen gründen -1 statt, wie in xml 1
        }
    }

    [Test]
    public void namesOfIRStates()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Check if names of states in interactionRule1 are parsed correctly
        List<string> expectedNamesOfIR1statesList = new List<string>{ "functionTestState", "transitionTestState", "ifStatementTestState" };
        List<IRState> ir1StateList = irList[0].stateList;
        for(int i = 0; i < ir1StateList.Count; i++)
        {
            Assert.AreEqual(expectedNamesOfIR1statesList[i], ir1StateList[i].id);
        }
    }

    [Test]
    public void numberOfFunctions()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Get onEntry information from "functionTestState"
        IROnEntry functionTestStateOnEntry = irList[0].stateList[0].onEntry;

        // Checks if number of functions is expected value
        int expectedNumberOfFunctions = 8;
        int actualNumberOfIfFunctions = 0;
        foreach(IRAbstractOnEntryElement elem in functionTestStateOnEntry.onEntryElements)
        {
            actualNumberOfIfFunctions++;
        }
        Assert.AreEqual(expectedNumberOfFunctions, actualNumberOfIfFunctions);
    }

    [Test]
    public void functionTypes()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Get onEntry information from "functionTestState"
        IROnEntry functionTestStateOnEntry = irList[0].stateList[0].onEntry;

        // Checks if parsed functions have expected function type
        List<string> expectedFunctionTypes = new List<string>{"startCommunicationRobotAct", "startCommunicationRobotAct", "stopCommunicationRobotAct", "startInteractionRule",
         "stopInteractionRule", "raiseEventTopic", "assignValue", "assignValue"};
        for(int i = 0; i < functionTestStateOnEntry.onEntryElements.Count; i++)
        {
            Assert.AreEqual(expectedFunctionTypes[i], ((IRFunction)functionTestStateOnEntry.onEntryElements[i]).functionType.ToString());
        }
    }

    [Test]
    public void functionArguments()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Get onEntry information from "functionTestState"
        IROnEntry functionTestStateOnEntry = irList[0].stateList[0].onEntry;

        // Checks if parsed functions have expected reference behavior
        List<string> expectedFunctionArguments = new List<string>{"cra1", "[cra2, cra3]", "cra1", "exampleInteractionRule1",
         "exampleInteractionRule1", "/eventTopic1", "boolVar1 = true", "stringVar1 = 'Hello'"};
        for(int i = 0; i < functionTestStateOnEntry.onEntryElements.Count; i++)
        {
            Assert.AreEqual(expectedFunctionArguments[i], ((IRFunction)functionTestStateOnEntry.onEntryElements[i]).referenceBehavior.ToString());
        }
    }

    [Test]
    public void numberOfTransitions()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Get transition list from "transitionTestState"
        List<IRTransition> transitionTestStateTransitionList = irList[0].stateList[1].transitionList;

        // Checks if number of transitions is expected value
        int expectedNumberOfTransitions = 2;
        Assert.AreEqual(expectedNumberOfTransitions, transitionTestStateTransitionList.Count);
    }

    [Test]
    public void transitionArguments()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Get transition list from "transitionTestState"
        List<IRTransition> transitionTestStateTransitionList = irList[0].stateList[1].transitionList;

        // Checks if eventTopics are parsed correctly
        List<string> expectedEventTopics = new List<string>{"/placeholderEventTopic", "/placeholderEventTopic"};
        int numberOfTransitions = 2;
        for(int i = 0; i < numberOfTransitions; i++)
        {
            Assert.AreEqual(expectedEventTopics[i], transitionTestStateTransitionList[i].eventTopic);
        }

        // Checks if targets are parsed correctly
        List<string> expectedTargets = new List<string>{"placeholderState", "placeholderStateWithDigits0123456789"};
        for(int i = 0; i < numberOfTransitions; i++)
        {
            Assert.AreEqual(expectedTargets[i], transitionTestStateTransitionList[i].target);
        }
    }

    [Test]
    public void numberOfIfStatements()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Get onEntry information from "ifStatementTestState"
        IROnEntry ifStatementTestStateOnEntry = irList[0].stateList[2].onEntry;

        // Checks if number of if statements is expected value
        int expectedNumberOfIfStatements = 3;
        int actualNumberOfIfStatements = 0;
        foreach(IRAbstractOnEntryElement elem in ifStatementTestStateOnEntry.onEntryElements)
        {
            actualNumberOfIfStatements++;
        }
        Assert.AreEqual(expectedNumberOfIfStatements, actualNumberOfIfStatements);
    }

    [Test]
    public void ifStatementCondition()
    {
        // Create new parser
        GameObject go = new GameObject();
        XMLInteractionRuleParser xmlInteractionRuleParser = go.AddComponent<XMLInteractionRuleParser>();

        // Parse TestIR.xml
        List<InteractionRule> irList = xmlInteractionRuleParser.ParseInteractionRulesFromFile("Assets/Tests/PlayMode/Resources/TestIR.xml", "Assets/Tests/PlayMode/Resources/");

        // Get onEntry information from "ifStatementTestState"
        IROnEntry ifStatementTestStateOnEntry = irList[0].stateList[2].onEntry;

        // Checks parsed if statement conditions have expected value
        List<string> expectedConditions = new List<string>{"exampleCondition", "exampleElseIfCondition"};

        Assert.AreEqual(expectedConditions[0], ((IRIFCond)ifStatementTestStateOnEntry.onEntryElements[0]).conditionStatement);
        Assert.AreEqual(expectedConditions[1], ((IRIFCond)ifStatementTestStateOnEntry.onEntryElements[1]).conditionStatement);
    }
}
