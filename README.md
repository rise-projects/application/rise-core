# Robotics Integration and Scenario-Management Extensible-Architecture (RISE)

[![Latest Release](https://gitlab.ub.uni-bielefeld.de/rise-projects/application/rise-core/-/badges/release.svg?value_width=130)](https://gitlab.ub.uni-bielefeld.de/rise-projects/application/rise-core/-/releases)
[![pipeline status](https://gitlab.ub.uni-bielefeld.de/rise-projects/application/rise-core/badges/main/pipeline.svg)](https://gitlab.ub.uni-bielefeld.de/rise-projects/application/rise-core/-/commits/main)
[![Unity Version](https://img.shields.io/badge/Unity-2022.3%2B-blue)]()
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
 <br>

## RISE-Core

The RISE system enables easy robot control through its user-friendly UI. With features like [Communication Robot Acts](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/structures.html), [Interaction Rules](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/structures.html), and [Working Memory](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/structures.html), users can effortlessly navigate and manage the system.<br>
For more information, please refer to the official [RISE documentation](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/contents.html).

<img src="Assets/Resources/Images/GUI_Overview.jpg" height="400" />


## License

This work is licensed under a [MIT License](https://opensource.org/licenses/MIT), which means you are free to use, modify, and distribute the source-code, but you must include the original license in your projects.